@/afs/ipp-garching.mpg.de/u/cxrs/idl/cxsfit64/machines/aug/read_aug_nbi_power.pro
; A.Kappatou - 12112013

pro get_nbi_on,shot,source,timein,$ ; <- input
    timeout,PNBIout,nbion          ; -> output

; provide info
if n_params() eq 0 then begin
    print,'USAGE:'
    print,'pro get_nbi_on,shot,source,timein,$ ; <- input'
    print,'    timeout,PNBIout,nbion           ; -> output'
    print,'timein eq -1: NBI time array'
    goto,endline
endif
;if isdefined(timein) eq 0 then begin
;    if timein ne -1 then begin 
;    print,'time array input not defined.'
;    stop
;    endif
;endif

; read nbi data
shot=long(shot)
;t1=0. & t2=10.
read_aug_nbi_power,shot,timein,PNBI,PNBItime,no_shf_flag
; select source (input is 1-8)
if source ge 1 AND source le 4 then begin
    ibox=0 
    isrc=source-1
endif else begin
    if source ge 5 AND source le 8 then begin
        ibox=1 
        isrc=(source-4)-1
    endif
endelse

if n_elements(timein) eq 1 then begin
if timein eq -1 then begin
    timein=PNBItime 
    timeout=PNBItime
endif
endif else begin
    timeout=timein
endelse
if no_shf_flag eq 0 then begin
    ; interpolate NBI power on given time array
    PNBIout=interpol(PNBI[*,isrc,ibox],PNBItime,timein)
endif else begin
    PNBIout=fltarr(n_elements(timein)) ; all powers zero, no shotfile, no beams
endelse
; threshold to consider NBI on
pthresh=0.5e6 ; 0.5MW
ntm=n_elements(timein)
nbion=fltarr(ntm)

for i=0,ntm-1 do begin
    if PNBIout[i] gt pthresh then nbion[i]=1
endfor

endline:
end
