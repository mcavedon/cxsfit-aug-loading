pro cxf_last_shotnr,shot
;; routine that returns the last shot-number
  shot=-1L
  dumstring=string('') 
  file = '/afs/ipp/www/aug/local/aug_only/shotstat.txt'
  if file_test(file) then begin
     openr, lun, file, /get_lun
     readf, lun, shot
     readf, lun, dumstring
     readf, lun, dumstring
     close, lun
     shotstatus=dumstring
     if strcmp(shotstatus, 'COMPLETED', /fold) eq 0 then shot-=1
  endif
end
