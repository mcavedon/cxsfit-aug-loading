pro trace_field_line_fast,surfaces,time,r_start,z_start,phi_start,npnts,delta,r_line,z_line,phi_line, $
                    no_plot=no_plot,b_abs=b_abs


;   -> shot         : shotnumber
;   -> time         : time in discharge
;   -> r_start      : r-coordinate in m at middle point of magnetic field line
;   -> z_start      : z-coordinate in m at middle point of magnetic field line 
;   -> phi_start    : phi-coordinate in degrees at middle point of magnetic field line 
;   -> npnts        : number of points along magnetic field line
;   -> delta        : distance of points in m
;   <- r_line       : array of size npnts containing r-coordinates (in m) of points along magnetic field line
;   <- z_line       : array of size npnts containing z-coordinates (in m) of points along magnetic field line
;   <- phi_line     : array of size npnts containing phi-coordinates (in deg) of points along magnetic field line
;   keywords for FPP/EQU diagnostic
;

n=2l
r0=[r_start,r_start]
z0=[z_start,z_start]
phi0=[phi_start,phi_start]
fpf=r0
fJp=r0
B_r=r0*0.
B_z=r0*0.
B_t=r0*0.
B_norm=r0*0.

r_line=fltarr(npnts)
z_line=fltarr(npnts)
phi_line=fltarr(npnts)
b_abs=fltarr(npnts)



result = min(abs(time-surfaces.time),i_time_eq)

for i_pnt = npnts/2., npnts-1 do begin 

    r_line(i_pnt)=r0(0)
    z_line(i_pnt)=z0(0)
    phi_line(i_pnt)=phi0(0)
    b_abs(i_pnt) = b_norm

    B_z = interp2d(surfaces.Bz[*,*,i_time_eq],surfaces.R,surfaces.Z,r0,z0,/nearest_neighbor)
    B_r = interp2d(surfaces.Br[*,*,i_time_eq],surfaces.R,surfaces.Z,r0,z0,/nearest_neighbor)
    B_t = interp2d(surfaces.Bt[*,*,i_time_eq],surfaces.R,surfaces.Z,r0,z0,/nearest_neighbor)

    B_norm=sqrt(B_r^2+B_z^2+B_t^2)
    r0(0)=r0(0)-B_r/B_norm*delta
    z0(0)=z0(0)-B_z/B_norm*delta
    phi0(0)=(phi0(0)/180.*!Pi*r0(0)-B_t/B_norm*delta)/r0(0)/!Pi*180.
endfor

r0=[r_start,r_start]
z0=[z_start,z_start]
phi0=[phi_start,phi_start]
fpf=r0
fJp=r0
B_r=r0*0.
B_z=r0*0.
B_t=r0*0.

for i_pnt = npnts/2., 0,-1 do begin 

    r_line(i_pnt)=r0(0)
    z_line(i_pnt)=z0(0)
    phi_line(i_pnt)=phi0(0)
    b_abs(i_pnt) = b_norm
    
    B_z = interp2d(surfaces.Bz[*,*,i_time_eq],surfaces.R,surfaces.Z,r0,z0,/nearest_neighbor)
    B_r = interp2d(surfaces.Br[*,*,i_time_eq],surfaces.R,surfaces.Z,r0,z0,/nearest_neighbor)
    B_t = interp2d(surfaces.Bt[*,*,i_time_eq],surfaces.R,surfaces.Z,r0,z0,/nearest_neighbor)

    B_norm=sqrt(B_r^2+B_z^2+B_t^2)

    r0(0)=r0(0)+B_r/B_norm*delta
    z0(0)=z0(0)+B_z/B_norm*delta
    phi0(0)=(phi0(0)/180.*!Pi*r0(0)+B_t/B_norm*delta)/r0(0)/!Pi*180.
    
endfor

if not keyword_set(no_plot) then begin
    !P.multi=[0.,2.,1.]
    set_color_right

    plot_asdup,shot=32000,/new_coord
    oplot,r_line,z_line,psym=3,color=!mycol.blue
    oplot,r_line[0:n_elements(r_line)/2],z_line[0:n_elements(r_line)/2],psym=4,color=!mycol.blue
    oplot,[r_start],[z_start],psym=1,thick=3
    fluss,shotl,time
    plot_asdup,shot=32000,/top_view,/new_coord
    oplot,r_line*cos(phi_line/180.*!Pi),r_line*sin(phi_line/180.*!Pi),psym=3,color=!mycol.blue
    oplot,r_line[0:n_elements(r_line)/2]*cos(phi_line[0:n_elements(r_line)/2]/180.*!Pi),r_line[0:n_elements(r_line)/2]*sin(phi_line[0:n_elements(r_line)/2]/180.*!Pi),psym=4,color=!mycol.blue
    oplot,[r_start*cos(phi_start/180.*!Pi)],[r_start*sin(phi_start/180.*!Pi)],psym=1,thick=3

endif

end
