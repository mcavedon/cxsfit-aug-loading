pro correct_ti,shot,time_array, los_struct,wvl,t_i_array,t_i_err,t_i_corr_array,t_i_err_corr_array

; -> shot           :   shotnumber, long
; -> time_array     :   times of ti-array, float-array
; -> los_struct     :   structure containing the LOS-geometry (as available in cxf_save_aug_fit.pro)
; -> line_name      :   name of spectral line, string
; -> t_i_array      :   t_i for each time points and LOS, float-array
; <- t_i_corr_array :   corrected t_i for each time points and LOS, float-array
;
;please not that:
;a) the correction is including the fine-structure correction
;b) the correction is including a fully-mixed l and m distribution within each main quantum number
;


; details of LOS structure:
; struct.R_pos      : R positions of measurement
; struct.z_pos      : z positions of measurement
; struct.phi_pos    : phi positions of measurement
; 
; struct.R_orig      : R positions of optical head
; struct.z_orig      : z positions of optical head
; struct.phi_orig    : phi positions of optical head
; 

; first get angles between LOS and magnetic field line at point of measurement
; for each time point: 1) get field line geometry 2) determine angle between LOS 3) store in array


t_i_corr_array=t_i_array*0.
t_i_err_corr_array=t_i_array*0.

intersect_angles = fltarr(n_elements(time_array), n_elements(los_struct.r_pos))
b_absolute = fltarr(n_elements(time_array), n_elements(los_struct.r_pos))
los_vector1 = fltarr(n_elements(los_struct.r_pos),3)
los_vector2 = fltarr(n_elements(los_struct.r_pos),3)
los_vector= fltarr(n_elements(los_struct.r_pos),3)

for i_los = 0, n_elements(los_struct.r_pos)-1 do begin

    los_vector1(i_los,0)=los_struct.r_pos(i_los)*cos(los_struct.phi_pos(i_los)/180.*!Pi)
    los_vector1(i_los,1)=los_struct.r_pos(i_los)*sin(los_struct.phi_pos(i_los)/180.*!Pi) 
    los_vector1(i_los,2)= los_struct.z_pos(i_los)

    los_vector2(i_los,0)=los_struct.r_orig(i_los)*cos(los_struct.phi_orig(i_los)/180.*!Pi)
    los_vector2(i_los,1)=los_struct.r_orig(i_los)*sin(los_struct.phi_orig(i_los)/180.*!Pi) 
    los_vector2(i_los,2)= los_struct.z_orig(i_los)

    los_vector(i_los,0)=los_vector2(i_los,0)-los_vector1(i_los,0)
    los_vector(i_los,1)=los_vector2(i_los,1)-los_vector1(i_los,1)
    los_vector(i_los,2)= los_vector2(i_los,2)-los_vector1(i_los,2)
    los_vector(i_los,*)=los_vector(i_los,*)/sqrt(los_vector(i_los,0)^2+los_vector(i_los,1)^2+los_vector(i_los,2)^2)

endfor


field_line_vector1=fltarr(3)
field_line_vector2=fltarr(3)
field_line_vector=fltarr(3)
alter_shot = 0l

spawn,'date'
for i_time = 0., n_elements(time_array)-1 do begin
    print,'Time point: ',i_time+1,'/',n_elements(time_array)
    for i_los = 0, n_elements(los_struct.r_pos)-1 do begin
        trace_field_line,shot,time_array(i_time),los_struct.r_pos(i_los),los_struct.z_pos(i_los),los_struct.phi_pos(i_los), $
                        5.,0.002,r,z,phi,edition=edition,b_abs=b_abs,/no_plot,alter_shot=alter_shot
        
        field_line_vector1(0)=r(0)*cos(phi(0)/180.*!Pi)
        field_line_vector1(1)=r(0)*sin(phi(0)/180.*!Pi) 
        field_line_vector1(2)= z(0)

        field_line_vector2(0)=r(3)*cos(phi(3)/180.*!Pi)
        field_line_vector2(1)=r(3)*sin(phi(3)/180.*!Pi) 
        field_line_vector2(2)= z(3)

        field_line_vector(0)=field_line_vector2(0)-field_line_vector1(0)
        field_line_vector(1)=field_line_vector2(1)-field_line_vector1(1)
        field_line_vector(2)= field_line_vector2(2)-field_line_vector1(2)
        field_line_vector(*)=field_line_vector(*)/sqrt(field_line_vector(0)^2+field_line_vector(1)^2+field_line_vector(2)^2)

        intersect_angles(i_time,i_los) = acos(abs(transpose(field_line_vector)#reform(los_vector(i_los,*))))/!Pi*180.
        b_absolute(i_time,i_los) = b_abs(2)
        if intersect_angles(i_time,i_los) lt 0.0 or intersect_angles(i_time,i_los) gt 90.0 then begin
            print,'There is a problem with the intersection angle! It should be smaller than 90deg and larger than 0deg. It is, however,'
            print,intersect_angles(i_time,i_los),'deg'
            stop
        endif
    endfor

endfor
spawn,'date'

proper_filename = ' '
if wvl gt 655.0 and wvl lt 657.0 then proper_filename= 'ti_corr_d23_dense.idlsave'
if wvl gt 466.0 and wvl lt 471.0 then proper_filename= 'ti_corr_he34_dense.idlsave'
if wvl gt 447.0 and wvl lt 453.0 then proper_filename= 'ti_corr_li45_dense.idlsave'
if wvl gt 513.0 and wvl lt 519.0 then proper_filename= 'ti_corr_li57_dense.idlsave'
if wvl gt 466.0 and wvl lt 471.0 then proper_filename= 'ti_corr_he34_dense.idlsave'
if wvl gt 493.0 and wvl lt 498.0 then proper_filename= 'ti_corr_b67_dense.idlsave'
if wvl gt 527.0 and wvl lt 531.0 then proper_filename= 'ti_corr_c78_dense.idlsave'
if wvl gt 563.0 and wvl lt 569.0 then proper_filename= 'ti_corr_n89_dense.idlsave'
if wvl gt 603.0 and wvl lt 609.0 then proper_filename= 'ti_corr_o910_dense.idlsave'
if wvl gt 522.0 and wvl lt 526.0 then proper_filename= 'ti_corr_ne1011_dense.idlsave'
if proper_filename eq ' ' then begin
    t_i_corr_array=t_i_array
    t_i_err_corr_array=t_i_err
    goto,ende
endif
print,proper_filename,' is used for Ti correction!'

restore,proper_filename

for i_time = 0., n_elements(time_array)-1 do begin

    for i_los = 0, n_elements(los_struct.r_pos)-1 do begin


        if finite(t_i_array(i_time,i_los)) then begin

; 100% accurate but too slow - use with sparse set of idlsave-file 'proper_filename'           
;            t_i_app_arr_here = make_ti_app_arr_here(b_absolute(i_time,i_los),intersect_angles(i_time,i_los),b_angle,b_t,T_I_APP_ALL)
;            t_i_corr_array(i_time,i_los) =  interpol(t_i,t_i_app_arr_here,abs(t_i_array(i_time,i_los)))

;faster implementation use only with dense set of idlsave-file 'proper_filename'  
            index_bt = where(abs(b_t-b_absolute(i_time,i_los)) eq min(abs(b_t-b_absolute(i_time,i_los))))
            index_bangle = where(abs(b_angle-intersect_angles(i_time,i_los)) eq min(abs(b_angle-intersect_angles(i_time,i_los))))
            index_ti_app = where(abs(T_I_APP_ALL(*,index_bt[0],index_bangle[0])-t_i_array(i_time,i_los)) eq min(abs(T_I_APP_ALL(*,index_bt[0],index_bangle[0])-t_i_array(i_time,i_los))))
            t_i_corr_array(i_time,i_los) = t_i_array(i_time,i_los) *t_i(index_ti_app[0])/T_I_APP_ALL(index_ti_app[0],index_bt[0],index_bangle[0])
            
        endif else begin
            t_i_corr_array(i_time,i_los) = 0.
        endelse

        if finite(t_i_array(i_time,i_los)) then begin
;100% accurate but too slow - use with sparse set of idlsave-file 'proper_filename'        
;           t_i_err_corr_array(i_time,i_los) = abs(t_i_corr_array(i_time,i_los)-interpol(t_i,t_i_app_arr_here,abs(t_i_array(i_time,i_los))+abs(t_i_err(i_time,i_los))))

;faster implementation use only with dense set of idlsave-file 'proper_filename'  
            index_ti_app_err = where(abs(T_I_APP_ALL(*,index_bt[0],index_bangle[0])-t_i_array(i_time,i_los)-t_i_err(i_time,i_los)) eq min(abs(T_I_APP_ALL(*,index_bt[0],index_bangle[0])-t_i_array(i_time,i_los)-t_i_err(i_time,i_los))))
            t_i_corr_array_err_tmp= (abs(t_i_array(i_time,i_los))+abs(t_i_err(i_time,i_los))) *t_i(index_ti_app_err[0])/T_I_APP_ALL(index_ti_app_err[0],index_bt[0],index_bangle[0])
            t_i_err_corr_array(i_time,i_los) = abs(t_i_corr_array(i_time,i_los)-t_i_corr_array_err_tmp)

        endif else begin
            t_i_err_corr_array(i_time,i_los) = 0.
        endelse


    endfor
    

endfor

;stop




ende:


end
