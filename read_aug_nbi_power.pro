@/afs/ipp-garching.mpg.de/u/cxrs/idl/cxsfit64/machines/aug/read_signal_mrm.pro
pro read_aug_nbi_power,shot,time,$
                PNBI,PNBItime,no_shf_flag
; ----------------------------------------------------------
; A.Kappatou
; input: t1, t2 (from which time to which time)
; output: - Beam power
;           PNBItime time array, PNBI [time, sources, box]
;           e.g. for source 3 of NBI 1: [*,2,0]
; ----------------------------------------------------------
    
    ; intialise
    ier        = 0L
    dia_ref    = 0L
    datum      = string(' ',format='(a18)')
    dim        = 0L
    phys_unit  = 0L
    err_string = string(' ',format='(a80)')

    ; Open NI shot file
    NI_exp  =  'AUGD'
    NI_diag =  'NIS'
    NI_shot =  shot
    NI_edn  =  0L
    no_shf_flag = 0
    
    s = call_external(!libddww, 'ddgetaug', 'ddopen', $
                    ier, NI_exp, NI_diag, NI_shot, NI_edn, $
                    dia_ref, datum)
    if (ier gt 0) then begin
        if ier eq 553713686 then no_shf_flag=1
        s = call_external(!libddww,'ddgetaug','xxerrprt',     $
                      -1L,err_string,ier,3L,'ddopen:')
        GOTO, FINISH
    endif

    ; READ NBI POWER
    ; ------------------
    name =  'PNIQ    '
    NI_time = 1L ; to force returning time base as well
    read_signal_mrm,ier,NI_shot,NI_diag,name,PNBItime,PNBI,phys_unit
    IF ier NE 0 THEN BEGIN
        err_string =  'read_signal error: '+STRTRIM(STRING(ier),2)
        GOTO, FINISH
    ENDIF
    
    FINISH:
    ;; Close shotfile
    s = call_external(!libddww,'ddgetaug','ddclose', $
                    ier,dia_ref)
    if (ier gt 0 and err_string EQ '') then begin
        s = call_external(!libddww,'ddgetaug','xxerrprt', $
                      -1L,err_string,ier,3L,'ddclose:')
    endif
    if STRLEN(STRTRIM(err_string, 2)) GT 0 THEN $
        err = err_string $
    else err =  ''
  
    
end


