pro remove_He74_cmr,cmr,cmr_err
        print,'REMOVING HE 7-4 BACKGROUND'
	time=cmr.data.time
        data=cmr.data.intens_calib
        w=cmr.header.cor_wavel
        bckf1=where(time ge 2.4 and time le 2.5,nbf1)   
                
        if nbf1 eq 1 then begin
            bf1=data[*,*,bckf1]        
        endif else begin
            bf1=total(data[*,*,bckf1],3)/nbf1
        endelse
        
        dif1=bf1*0.0
        data2=data*0.0
        
        nr=n_elements(data[0,*])
        for ir=0,nr-1 do begin          
            ord11=sort(bf1[0:200,ir])
            ord12=sort(bf1[301:510,ir])          
                        
           dmind1=[ord11[0:9],ord12[0:9]+301]            
           Bckfit1=poly_fit(w[dmind1,ir],bf1[dmind1,ir],1)                       
           dif1[*,ir]=bf1[*,ir]-poly(w[*,ir],bckfit1)
               
           for it=0,n_elements(time)-1 do data2[*,ir,it]=data[*,ir,it]-dif1[*,ir]
        endfor
            
       cmr.data.intens_calib=data2   
       ;window,0
       ;wset,0
       ;dm=min(abs(time-2.35),ind)
       ;test=cmr.data.intens  
       ;;This looks fine. 
       ;plot,data[*,9,ind]
       ;oplot,data2[*,9,ind]
       ;oplot,test[*,9,ind] ; also fine. 
       
       ;But it doesn't seem to arrive in cxsfit
       
       ;stop     

return
end
