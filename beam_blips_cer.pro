;RMM - August 2012 - pixel by pixel interpolation of the background before and after a beam blip

;This code will take the spectrometer_data array created in
;cxf_load_aug_data.pro (datain) as well as the shotfile information structure from load_c(e/h)r_pi (diagdata) and create a new structure to be given to
         
;INPUT INFORMATION
;datain = structure containing spectrometer_Data created for cxsfit
;diagdata -structure contianing shotfile information from a load_cer_pi routine or similar
;whtrack - track information for the case where a row is not being used see cxf_load_aug_data.pro line 60-70ish
;waveL - calculated wavelength interval (assumed continuous pixels) from cxf_load_aug_data.pro (line 112 for example)

;**********************************************************************************
     
PRO beam_blips_cer, datain, diagdata, whtrack,waveL,dataout
    shot=diagdata.header.shot
    if shot lt 25000 then begin
        print,'Background subtraction method only for data after 25000'
        return
    endif

;raw CXRS data
    data=datain.intensity.data ; fltarr(pixel,track,frame)
    ntrack=n_elements(data[0,*,0])
    dataerror=datain.intensity.error
    cxrstime=datain.time
    npix=n_elements(data[*,0,0])
    ntm=n_elements(cxrstime)
    expt=mean(cxrstime[1:ntm-1]-cxrstime[0:ntm-2])
    
;pull out NBI time trace again. don't like the way this is done by cxrs_beam status
; intialise
  ier        = 0L
  dia_ref    = 0L
  datum      = string(' ',format='(a18)')
  dim        = 0L
  phys_unit  = 0L
  err_string = string(' ',format='(a80)')

; Open NI shot file
  NI_exp  =  'AUGD'
  NI_diag =  'NIS'
  NI_shot =  shot
  NI_edn  =  0L
  s = call_external(!libddww, 'ddgetaug', 'ddopen', $
                    ier, NI_exp, NI_diag, NI_shot, NI_edn, $
                    dia_ref, datum)
  if (ier gt 0) then begin
    stop
  endif

; Read power waveforms

  t1 = 0.0
  t2 = 100.0
  name =  'PNIQ    '
  NI_time = 1L ; to force returning time base as well
  read_signal,ier,dia_ref,name,t1,t2,NI_powers,$
              time=NI_time
;  IF ier NE 0 THEN BEGIN
;    stop
;  ENDIF

; Extract the required source
  NI_power = NI_powers[*,2,0];beam 3
 ;NI_power = NI_powers[*,1,0]



    ;plot,ni_time,ni_power
    beam=cxrstime*0.0 ; frames to be used
    beam2=cxrstime*0.0 ; frames to 
    ;iterate through cxrs time points
    for i=0,n_elements(cxrstime)-1 do begin
        in=where(ni_time ge cxrstime[i] -expt/2. and ni_time le cxrstime[i]+expt/2.)
        if in[0] ne -1 then begin
                
            if mean(ni_power[in])/1e6 ge 1.5 then beam[i]=1.
            
            ;low power beam blips******************************************************                    
            ;if mean(ni_power[in])/max(ni_power) ge .2 then beam[i]=1.
            ;low power beam blips *****************************************************
            
            notzero=where(ni_power[in] ne 0.0)
            if notzero[0] ne -1 then beam2[i]=1.
        endif
    endfor    
;stop
;this is the beam info from cxrs_beam_status - that is not stringent enough.
;Flags for neutral beam power.  0 - beam is off, 1 beam is on
;These flags correspond to an average over the CXRS exposure time.
;    beam=datain.beam
   
;Get an array of times when the beam is off
    ;offindex=where(beam2 eq 0,noff) & offtime=cxrstime[offindex]
    offindex=where(beam eq 0,noff) & offtime=cxrstime[offindex]
     
; Get array of times when the beam in on
    onindex=where(beam eq 1,non) & ontime=cxrstime[onindex]

    datanew=fltarr(npix,ntrack,non)
    dataerr=fltarr(npix,ntrack,non)


;iterate through the on points
    for i=0, non -1 do begin
        
        ;find the nearest off point before and after
        before=where(offindex lt onindex[i],nbefore)
        after=where(offindex gt onindex[i],nafter)
        
        ;the indicies of the before and after frames
        bframe=offindex(before[nbefore-1])
        aframe=offindex(after[0])
        
        ;Testing code for Ulrike's beam blips with gas puff
        ;also good for runaway shots where the plasma disrupts after the blip
        ;aframe=bframe
        
        btime=offtime[before[nbefore -1]]
        atime=offtime[after[0]]
        
        ;now get the data
        ;iterate through number of tracks
        for t=0,ntrack-1 do begin
            Bdata=reform(data[*,t,bframe])
            Adata=reform(data[*,t,aframe])
            Berr=reform(dataerror[*,t,bframe])
            Aerr=reform(dataerror[*,t,aframe])
            
            ;y=mx+b for every pixel - need to know time
            m=(adata-bdata)/(atime-btime)
            b=bdata-m*btime

            back=smooth((bdata+adata)/2.,3);smooth(m*ontime[i]+b,3)
            ;introduce scaling factor if appropriate
            if datain.wlength ge 4900 and datain.wlength le 5000 then begin
                ;I'm working with B
                BIIline=where(datain.wavelength.data[*,t] ge 493.94 and datain.wavelength.data[*,t] le 494.08)
                border=sort(back)
                dorder=sort(data[*,t,onindex[i]])
                offsetb=mean(back[border[0:100]])
                offsetd=mean(data[dorder[0:100],t,onindex[i]])
                
                maxback=mean(back[bIIline]-offsetb)
                maxdataframe=mean(data[BIIline,t,onindex[i]]-offsetd)
                factor=maxdataframe/maxback
                factor=1.
                back=(back-offsetb)*factor+offsetb
                ;print,'scaled passive data in background subtration'
            endif
            
            berr=(berr+aerr)/2. ; this isn't right - but probably not too bad
            datanew[*,t,i]=data[*,t,onindex[i]]-back
            dataerr[*,t,i]=sqrt(dataerror[*,t,onindex[i]]^2.+berr^2.)/2.
            
;            plot,data[*,t,onindex[i]],thi=2,line=2,xran=[200,300],yran=[-2e16,1.e17],/ysty
;            oplot,bdata
;            oplot,adata                       
;            oplot,back,col=200,thi=2
;            oplot,datanew[*,t,i],thi=2
            
;            stop
        endfor
    endfor

    ;Most irritatingly I need to completely redefine the entire structure at the moment.
    ;In the future the original structure will be defined with pointers so that the values 
    ;can be changed - but first I need to find the rest of the code so I can propagate the 
    ;the consequences of that change forward consistently.  Of course - making the changes
    ;and seeing where the code breaks is another way to go =) - But, some other day.
    
    cxf_define_spectrometer_data,dataout,err
        dataout.machine.name =  datain.machine.name
        dataout.shot_nr = datain.shot_nr
        dataout.spectrometer.name = datain.spectrometer.name
        dataout.npixel = datain.npixel
        dataout.ntrack = datain.ntrack
        dataout.nframe = non
        dataout.ninst  = datain.ninst

        cxf_define_spectrometer_data,dataout,err
        IF err NE '' THEN RETURN
        dataout.LOS_name = datain.LOS_name
        dataout.R_pos    = datain.R_pos
        dataout.z_pos    = datain.z_pos
        dataout.phi_pos  = datain.phi_pos
        dataout.R_orig   = datain.R_orig
        dataout.z_orig   = datain.z_orig
        dataout.phi_orig = datain.phi_orig
        dataout.wlength  = datain.wlength
        dataout.slit     = datain.slit

         dataout = create_struct(dataout,'R_pos_time',datain.R_pos_time[*,onindex],'z_pos_time',datain.z_pos_time[*,onindex],'phi_pos_time',datain.phi_pos_time[*,onindex],'dR_pos_time',datain.dR_pos_time[*,onindex],'dz_pos_time',datain.dz_pos_time[*,onindex],'dphi_pos_time',datain.dphi_pos_time[*,onindex],'externalcostheta',datain.externalcostheta[*,onindex])


        dataout.time = ontime
        dataout.exposure = datain.exposure[onindex]

        dataout.wavelength.data      = datain.wavelength.data
        dataout.wavelength.error     = datain.wavelength.error
        dataout.wavelength.reference = datain.wavelength.reference

        dataout.dispersion.data      = datain.dispersion.data
        dataout.dispersion.error     = datain.dispersion.error
        dataout.dispersion.reference = datain.dispersion.reference
        
        dataout.intensity.validity = datain.intensity.validity[*,*,onindex]
        dataout.intensity.reference = datain.intensity.reference

        dataout.instfu.y0 = datain.instfu.y0
        dataout.instfu.xw = datain.instfu.xw
        dataout.instfu.xs = datain.instfu.xs
        dataout.instfu.reference = datain.instfu.reference

        dataout.beam = datain.beam[onindex]
        dataout.intensity.data=datanew
        dataout.intensity.error=dataerr
           
return
end
