pro cxf_fit_shotfile_e,event
 
	widget_control,event.top,get_uvalue=info

	if event.id eq info.cancel_button then begin
		widget_control,event.top,/destroy
		return
	end

	if event.id eq info.ok_button then begin
		widget_control,info.shotfile_widget,get_value=shotfile
		widget_control,/hourglass
		cxf_load_aug_fit,	shotfile.experiment, $
					shotfile.shot, $
					shotfile.diagnostic, $
					shotfile.edition, $
					indata, err
		widget_control,hourglass=0

		if err ne '' then begin
			a=dialog_message(err,/error)
			return
		end
		
		shotfile.shot_found    = indata.shot_f
		shotfile.edition_found = indata.edition_f

                history=indata.history
		
		widget_control,info.topinfo_base,get_uvalue=topinfo
		
		if ptr_valid(topinfo.historydata) then ptr_free,topinfo.historydata
		topinfo.historydata=ptr_new(history)
		
		current=*history[(size(history))[1]-1]
		
		widget_control,topinfo.panel_window,set_value=current
		widget_control,topinfo.graph_panel,get_uvalue=info2
		widget_control,info2.track_slider,set_value=current.track
		widget_control,info2.frame_slider,set_value=current.frame
                
		widget_control,info.topinfo_base,set_uvalue=topinfo
		cxf_plotspec,topinfo

		widget_control,event.top,/destroy
		return
	endif
	
end	
