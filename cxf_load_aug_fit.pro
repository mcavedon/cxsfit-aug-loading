
; Load the indata structure with data from an AUG shot file
@/afs/ipp-garching.mpg.de/home/c/cxrs/idl_copy/cxsfit64/amd64_sles15/idl/cxf_read_switches.pro

PRO cxf_load_aug_fit, experiment, shot, diagnostic, edition, $
                      indata, err
;stop
;
; ldh 20.12.05 - first version
;
;  experiment : I/O : experiment name of shot file [e.g.: 'AUGD']
;  shot       : I/O : shot number of shot file [e.g.: 17148L]
;  diagnostic : I/O : shot file diagnostic name [e.g.: 'CEZ']
;  edition    : I/O : edition number of shot file [e.g.: 0L for most recent]
;  indata     : I/O : structure to contain all the information
;                     stored in the shot file.  The caller is
;                     required to extract the parts he/she needs.
;  err        : O   : character string for error messages, null
;                     string on successful completion

  debug = 0

  err  = ''
  ID  = 'CXF_LOAD_AUG_FIT: '

; Check that link to shot file software library is defined
  defsysv, '!libddww', exist=exist

;  if not exist then $
;    defsysv, '!libddww', '/usr/ads/lib/libddww.so.6.1',1
  if not exist then $
    defsysv, '!LIBDDWW', '/afs/ipp/aug/ads/lib64/@sys/libddww8.so', 1 ;RMM March 9 2020!!
    

; Paths to diagnostic software
  if (strmatch(!PATH, '*/cxrs/idl/CXRS_ausw*') eq 0L) then $
     !PATH = !PATH + ':' + expand_path('+/afs/ipp/home/c/cxrs/idl/CXRS_ausw')

; Input part of indata structure:
  indata = { experiment : experiment, $
             shot       : shot,       $
             diagnostic : diagnostic, $
             edition    : edition     }
; Open the requested shot file
  error      = 0L
  diaref     = 0L
  tim        = '                  '
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddopen', $
                     error, experiment, diagnostic, $
                     shot, edition, diaref, tim)
  IF error NE 0 THEN GOTO, DDERROR

  indata = CREATE_STRUCT(indata, $
                         'shot_f', shot, $
                         'edition_f', edition)

; Read the shot file

; Shot file parameters: General information
  name     = 'Info    '
  parm     = 'CodeName'
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 6L
  lbuf     = 16L ; in bytes
  CodeName = ''
  cxf_string_pad, CodeName, 16L
  physunit = 0L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, CodeName, physunit)
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'Version '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 2L
  lbuf     = 1L
  Version  = 0.0
  physunit = 0L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, Version, physunit)
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'UserID  '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 6L
  lbuf     = 16L ; in bytes
  UserID   = ''
  cxf_string_pad, UserID, 16L
  physunit = 0L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, UserID, physunit)
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'wave_ref'
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 6L
  lbuf     = 72L ; in bytes
  wave_ref = ''
  cxf_string_pad, wave_ref, 72L
  physunit = 0L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, wave_ref, physunit)
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'disp_ref'
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 6L
  lbuf     = 72L ; in bytes
  disp_ref = ''
  cxf_string_pad, disp_ref, 72L
  physunit = 0L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, disp_ref, physunit)
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'inte_ref'
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 6L
  lbuf     = 72L ; in bytes
  inte_ref = ''
  cxf_string_pad, inte_ref, 72L
  physunit = 0L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, inte_ref, physunit)
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'inst_ref'
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 6L
  lbuf     = 72L ; in bytes
  inst_ref = ''
  cxf_string_pad, inst_ref, 72L
  physunit = 0L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, inst_ref, physunit)
  IF error NE 0 THEN GOTO, DDERROR

  Info = { CodeName : CodeName, $
           Version  : Version,  $
           UserID   : UserID,   $
           WaveRef  : wave_ref, $
           DispRef  : disp_ref, $
           InteRef  : inte_ref, $
           InstRef  : inst_ref  }
  indata = CREATE_STRUCT(indata, 'Info', Info)

; Comment text
  name     = 'Comment '
  IF debug THEN print,name

  cxf_read_signal,error,diaref,name,time1,time2,comment,/raw

  IF error NE 0 THEN BEGIN
    print,ID+'No comments found, setting = ""'
    comment = ['']
  ENDIF
  indata = CREATE_STRUCT(indata, 'comment', comment)

; Get time base information
  name       = 'time    '
  IF debug THEN print, name
  time1      = 0.0
  time2      = 0.0
  ntval      = 0L
  npretrig   = 0L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddtrange', $
                     error, diaref, name, $
                     time1, time2, ntval, npretrig)
  IF error NE 0 THEN GOTO, DDERROR
  IF debug THEN print,'ntval ', ntval
  k1 = 0L
  k2 = 0L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddtindex', $
                     error, diaref, name, $
                     time1, time2, k1, k2)
  IF error NE 0 THEN GOTO, DDERROR
  IF debug THEN print,'k1, k2 ', k1, k2
  type   = 2L
  lbuf   = ntval
  buffer = FLTARR(lbuf)
  leng   = 0L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddtbase', $
                     error, diaref, name, $
                     k1, k2, type, lbuf, buffer, leng)
  IF error NE 0 THEN GOTO, DDERROR

  ntime  = lbuf
  indata = CREATE_STRUCT(indata, 'ntime', ntime, 'time', buffer)

; exposure time
  name     = 'texp    '
  IF debug THEN print, STRTRIM(name,2)
  type     = 2L
  lbuf     = ntime
  buffer   = FLTARR(lbuf)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddsignal', $
                     error, diaref, name, $
                     k1, k2, type, lbuf, buffer, leng)
  IF error NE 0 THEN GOTO, DDERROR

  indata = CREATE_STRUCT(indata, 'texp', buffer)

; Shot file parameters: LOS information
  name     = 'LOSInfo '
  parm     = 'Number  '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 1L
  lbuf     = 1L
  ntrack   = 0L
  physunit = 0L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, ntrack, physunit)
  IF error NE 0 THEN GOTO, DDERROR


; This is a patch to avoid a bug in the DDPARM
;I added the if error= 553844740 statement 10/20/2010 - RMM
;*********************************************************
  maxtrack = 30L  ; patch This was 16 before
;  if diagnostic eq 'CEZ' and shot gt 25000l then maxtrack = 30
;  if diagnostic eq 'CHZ' and shot gt 25000l then maxtrack = 30
;  if diagnostic eq 'CHZ' and shot eq 25161l then maxtrack = 16

  parm     = 'Name    '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 6L
;  lbuf     = ntrack*16L ; in bytes
;  LOSname  = STRARR(ntrack)
  lbuf     = maxtrack*16L ; in bytes  ; patch
  LOSname  = STRARR(maxtrack)  ; patch
  cxf_string_pad, LOSname, 16L
  physunit = LONARR(ntrack)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, LOSname, physunit)

if error eq 553844740 then begin
    maxtrack = 16L  ; patch
    parm     = 'Name    '
    IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
    type     = 6L
    lbuf     = maxtrack*16L ; in bytes  ; patch
    LOSname  = STRARR(maxtrack)  ; patch
    cxf_string_pad, LOSname, 16L
    physunit = LONARR(ntrack)
    s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, LOSname, physunit)  
endif

;**********************************************************
                     
  IF error NE 0 THEN GOTO, DDERROR
  LOSname = LOSname[0:ntrack-1]  ; patch
; trim the strings back to their original length
  FOR i=0L,ntrack-1 DO LOSname[i] =  STRTRIM(LOSname[i],2)

  parm     = 'R       '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 2L
  lbuf     = ntrack
  R        = FLTARR(ntrack)
  physunit = LONARR(ntrack)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, R, physunit)
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'z       '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 2L
  lbuf     = ntrack
  z        = FLTARR(ntrack)
  physunit = LONARR(ntrack)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, z, physunit)
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'phi     '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 2L
  lbuf     = ntrack
  phi      = FLTARR(ntrack)
  physunit = LONARR(ntrack)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, phi, physunit)
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'Rorig   '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 2L
  lbuf     = ntrack
  Rorig    = FLTARR(ntrack)
  physunit = LONARR(ntrack)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, Rorig, physunit)
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'zorig   '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 2L
  lbuf     = ntrack
  zorig    = FLTARR(ntrack)
  physunit = LONARR(ntrack)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, zorig, physunit)
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'phiorig '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 2L
  lbuf     = ntrack
  phiorig  = FLTARR(ntrack)
  physunit = LONARR(ntrack)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, phiorig, physunit)
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'costhtor'
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 2L
  lbuf     = ntrack
  costhtor = FLTARR(ntrack)
  physunit = LONARR(ntrack)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, costhtor, physunit)
  IF error NE 0 THEN GOTO, DDERROR

  LOSInfo = { ntrack     : ntrack,   $
              name       : LOSname,  $
                R_pos    : R,        $
                z_pos    : z,        $
              phi_pos    : phi,      $
                R_origin : Rorig,    $
                z_origin : zorig,    $
              phi_origin : phiorig,  $
              costh_tor  : costhtor  }
  indata = CREATE_STRUCT(indata, 'LOSInfo', LOSInfo)

  ; >>> kappatou - start <<<
  IF diagnostic eq 'CAZ' OR diagnostic eq 'CEZ' OR diagnostic eq 'CCZ' or diagnostic eq 'CHZ' or diagnostic eq 'COZ' or diagnostic eq 'CUZ' THEN BEGIN
    error  = 0L
    adim   = lonarr(3)
    asz    = lonarr(3)
    rleng  = 0L
    aname = '        '
    index = 0L &  nta = 0L  &  narea = 0L
    name     = 'Ti      '
    s = Call_External(!libddww, 'ddgetaug', 'ddainfo', error, diaref, $
                          name, asz, adim, index) 
    s = Call_External(!libddww, 'ddgetaug', 'ddainfo2', error, diaref, $
                          name, nta, asz, adim, aname, narea)
    IF asz[2] NE 0 THEN nasz=3 ELSE IF asz[1] NE 0 THEN nasz=2 ELSE nasz=1
    IF debug THEN print, STRTRIM(name,2)
    ; R_time
    type     = 2L
    k1       = 1L
    k2       = long(nta)
    lbuf     = long(total(asz)) 
    R_time   = reform(fltarr(asz[0],nta))
    sizes    = 0L
    s = CALL_EXTERNAL (!libddww,'ddgetaug','ddagroup', $
                       error, diaref, 'R_time  ', $
                       k1,k2, type, lbuf, R_time,sizes)
    IF error NE 0 THEN BEGIN
      ; check if the error is because the area base does not exist
      if error eq 555679747. then begin
              GOTO, NORZPHITIME
      endif else begin
             ; identify if error is only a warning 
             ;(problem appeared May 2016) - kappatou 17052016
             s = CALL_EXTERNAL (!libddww,'ddgetaug','xxwarn', error)
             if s eq 1 then begin
                  err = STRING(' ',format='(a180)')
                  s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerror', error, 3L, ' ')
             endif else begin
                GOTO, DDERROR
             endelse
      endelse
    ENDIF
    dR_time   = reform(fltarr(asz[0],nta))
    s = CALL_EXTERNAL (!libddww,'ddgetaug','ddagroup', $
                       error, diaref, 'dR_time ', $
                       k1,k2, type, lbuf, dR_time,sizes)
    ; z_time
    z_time   = reform(fltarr(asz[0],nta))
    s = CALL_EXTERNAL (!libddww,'ddgetaug','ddagroup', $
                       error, diaref, 'z_time  ', $
                       k1,k2, type, lbuf, z_time,sizes)
    dz_time   = reform(fltarr(asz[0],nta))
    s = CALL_EXTERNAL (!libddww,'ddgetaug','ddagroup', $
                       error, diaref, 'dz_time ', $
                       k1,k2, type, lbuf, dz_time,sizes)
    ; phi_time
    phi_time   = reform(fltarr(asz[0],nta))
    s = CALL_EXTERNAL (!libddww,'ddgetaug','ddagroup', $
                       error, diaref, 'phi_time', $
                       k1,k2, type, lbuf, phi_time,sizes)
    dph_time   = reform(fltarr(asz[0],nta))
    s = CALL_EXTERNAL (!libddww,'ddgetaug','ddagroup', $
                       error, diaref, 'dph_time', $
                       k1,k2, type, lbuf, dph_time,sizes)
    ; costheta
    cth_time   = reform(fltarr(asz[0],nta))
    s = CALL_EXTERNAL (!libddww,'ddgetaug','ddagroup', $
                       error, diaref, 'cth_time', $
                       k1,k2, type, lbuf, cth_time,sizes)
    LOStime = {R_time    : R_time[0:ntrack-1,*],$
                dR_time   : dR_time[0:ntrack-1,*],$
                z_time    : z_time[0:ntrack-1,*],$
                dz_time   : dz_time[0:ntrack-1,*],$
                phi_time  : phi_time[0:ntrack-1,*],$
                dphi_time : dph_time[0:ntrack-1,*],$ 
                costh_time: cth_time[0:ntrack-1,*]}
    indata = CREATE_STRUCT(indata, 'LOStime', LOStime)
    NORZPHITIME:
    if error eq 555679747. then print,'No time-dependent coordinates have been found.'
  ENDIF ; selection CAR, CER
  ; >>> kappatou - end <<<
  
  
  
  ; Shot file parameters: fit line information
  name     = 'LineInfo'
  parm     = 'Number  '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 1L
  lbuf     = 1L
  nlines   = 0L
  physunit = 0L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, nlines, physunit)
  IF error NE 0 THEN GOTO, DDERROR

; here a patch to avoid a bug in the DDPARM
  maxlines = 10L  ; patch
  parm     = 'Name    '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 6L
;  lbuf     = nlines*32L ; in bytes
;  LINEname = STRARR(nlines)
  lbuf     = maxlines*32L ; in bytes  ; patch
  LINEname  = STRARR(maxlines)  ; patch
  cxf_string_pad, LINEname, 32L
  physunit = LONARR(nlines)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, LINEname, physunit)
  IF error NE 0 THEN GOTO, DDERROR
  LINEname = LINEname[0:nlines-1]  ; patch

  parm     = 'mass    '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 2L
  lbuf     = nlines
  mass     = FLTARR(nlines)
  physunit = LONARR(nlines)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, mass, physunit)
  IF error NE 0 THEN GOTO, DDERROR

; The following parameters are not available in some old shot
; files --> set to default (missing) values
  parm     = 'active  '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 1L
  lbuf     = nlines
  active   = LONARR(nlines)
  physunit = LONARR(nlines)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, active, physunit)
;  IF byte(error,0) EQ 33 AND $       ; DD package
;     byte(error,1) EQ  3 AND $       ; ddparm
;     byte(error,2) EQ  0 AND $       ; error
;     byte(error,3) EQ  6 THEN BEGIN  ; code 'parameter not found'
;    s = CALL_EXTERNAL (!libddww,'ddgetaug','xxerror', error, 3L, parm)
;    error = 0L
;    active = LONARR(lbuf)
;  ENDIF
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'cxwavel '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 2L
  lbuf     = nlines
  cxwavel  = FLTARR(nlines)
  physunit = LONARR(nlines)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, cxwavel, physunit)
;  IF byte(error,0) EQ 33 AND $       ; DD package
;     byte(error,1) EQ  3 AND $       ; ddparm
;     byte(error,2) EQ  0 AND $       ; error
;     byte(error,3) EQ  6 THEN BEGIN  ; code 'parameter not found'
;    s = CALL_EXTERNAL (!libddww,'ddgetaug','xxerror', error, 3L, parm)
;    error = 0L
;    cxwavel = FLTARR(lbuf)
;  ENDIF
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'upper   '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 1L
  lbuf     = nlines
  upper    = LONARR(nlines)
  physunit = LONARR(nlines)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, upper, physunit)
;  IF byte(error,0) EQ 33 AND $       ; DD package
;     byte(error,1) EQ  3 AND $       ; ddparm
;     byte(error,2) EQ  0 AND $       ; error
;     byte(error,3) EQ  6 THEN BEGIN  ; code 'parameter not found'
;    s = CALL_EXTERNAL (!libddww,'ddgetaug','xxerror', error, 3L, parm)
;    err_string = string(' ',format='(a80)')
;    s = CALL_EXTERNAL (!libddww,'ddgetaug','xxerrprt', -1L, err_string, error, 3L, parm)
;    print, err_string
;    error = 0L
;    upper = LONARR(lbuf)
;  ENDIF
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'lower   '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 1L
  lbuf     = nlines
  lower    = LONARR(nlines)
  physunit = LONARR(nlines)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, lower, physunit)
;  IF byte(error,0) EQ 33 AND $       ; DD package
;     byte(error,1) EQ  3 AND $       ; ddparm
;     byte(error,2) EQ  0 AND $       ; error
;     byte(error,3) EQ  6 THEN BEGIN  ; code 'parameter not found'
;    s = CALL_EXTERNAL (!libddww,'ddgetaug','xxerror', error, 3L, parm)
;    err_string = string(' ',format='(a80)')
;    s = CALL_EXTERNAL (!libddww,'ddgetaug','xxerrprt', -1L, err_string, error, 3L, parm)
;    print, err_string
;    error = 0L
;    lower = LONARR(lbuf)
;  ENDIF
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'Z0      '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 1L
  lbuf     = nlines
  Z0       = LONARR(nlines)
  physunit = LONARR(nlines)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, Z0, physunit)
;  IF byte(error,0) EQ 33 AND $       ; DD package
;     byte(error,1) EQ  3 AND $       ; ddparm
;     byte(error,2) EQ  0 AND $       ; error
;     byte(error,3) EQ  6 THEN BEGIN  ; code 'parameter not found'
;    s = CALL_EXTERNAL (!libddww,'ddgetaug','xxerror', error, 3L, parm)
;    err_string = string(' ',format='(a80)')
;    s = CALL_EXTERNAL (!libddww,'ddgetaug','xxerrprt', -1L, err_string, error, 3L, parm)
;    print, err_string
;    error = 0L
;    Z0 = LONARR(lbuf)
;  ENDIF
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'Z       '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 1L
  lbuf     = nlines
  Z        = LONARR(nlines)
  physunit = LONARR(nlines)
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, Z, physunit)
;  IF byte(error,0) EQ 33 AND $       ; DD package
;     byte(error,1) EQ  3 AND $       ; ddparm
;     byte(error,2) EQ  0 AND $       ; error
;     byte(error,3) EQ  6 THEN BEGIN  ; code 'parameter not found'
;    s = CALL_EXTERNAL (!libddww,'ddgetaug','xxerror', error, 3L, parm)
;    err_string = string(' ',format='(a80)')
;    s = CALL_EXTERNAL (!libddww,'ddgetaug','xxerrprt', -1L, err_string, error, 3L, parm)
;    print, err_string
;    error = 0L
;    Z = LONARR(lbuf)
;  ENDIF
  IF error NE 0 THEN GOTO, DDERROR

  parm     = 'cxline  '
  IF debug THEN print, STRTRIM(name,2)+': '+STRTRIM(parm,2)
  type     = 1L
  lbuf     = 1L
  cxline   = 0L
  physunit = 0L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddparm', $
                     error, diaref, name, $
                     parm, type, lbuf, cxline, physunit)
  IF error NE 0 THEN GOTO, DDERROR
  cxline = cxline - 1

  LINEInfo = { nlines     : nlines,   $
               name       : LINEname, $
               mass       : mass,     $
               active     : active,   $
               cxwavel    : cxwavel,  $
               upper      : upper,    $
               lower      : lower,    $
               Z0         : Z0,       $
               Z          : Z,        $
               cxline     : cxline    }
  indata = CREATE_STRUCT(indata, 'LINEInfo', LINEinfo)

  
; Data for primary CXRS line
  IF cxline GE 0 THEN BEGIN

; Ion temperature from primary CXRS line
  name   = 'Ti      '
  IF debug THEN print, name
  
  cxf_read_signal,error,diaref,name,time1,time2,data,/raw


  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

; Uncertainty in ion temperature from primary CXRS line
  name   = 'err_Ti  '
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,ddata,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

  ti = {data  :  data[*,0:ntrack-1], $
        error : ddata[*,0:ntrack-1]  }
  indata = CREATE_STRUCT(indata, 'Ti', ti)

; Ion temperature from primary CXRS line corrected for Zeeman/Paschen-Back Effect
  name   = 'Ti_c    '
  IF debug THEN print, name
  
  cxf_read_signal,error,diaref,name,time1,time2,data,/raw

  IF error NE 0 THEN BEGIN
    indata = CREATE_STRUCT(indata, 'corrected_Ti_available', 0l)
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, ignore_err_ti_c
  ENDIF
    indata = CREATE_STRUCT(indata, 'corrected_Ti_available', 1l)

; Uncertainty in ion temperature from primary CXRS line
  name   = 'err_Ti_c'
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,ddata,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

  ti_c = {data  :  data[*,0:ntrack-1], $
        error : ddata[*,0:ntrack-1]  }
  indata = CREATE_STRUCT(indata, 'Ti_c', ti_c)

ignore_err_ti_c:


; Rotation from primary CXRS line corrected for Zeeman/Paschen-Back Effect - RMM Feb 21 2020
  name   = 'vr_c    '
  IF debug THEN print, name
  ;stop
  cxf_read_signal,error,diaref,name,time1,time2,data,/raw

  IF error NE 0 THEN BEGIN
    indata = CREATE_STRUCT(indata, 'corrected_vr_available', 0l)
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, ignore_err_vr_c
  ENDIF
    indata = CREATE_STRUCT(indata, 'corrected_vr_available', 1l)

; Uncertainty in ion rotation from primary CXRS line
  name   = 'err_vr_c'
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,ddata,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

  vr_c = {data  :  data[*,0:ntrack-1], $
          error : ddata[*,0:ntrack-1]  }
  indata = CREATE_STRUCT(indata, 'vr_c', vr_c)

ignore_err_vr_c:




; Toroidal rotation velocity from primary CXRS line
  name   = 'vrot    '
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,data,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

; Uncertainty in toroidal rotation velocity from primary CXRS line
  name   = 'err_vrot'
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,ddata,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

  if diagnostic eq 'CXZ'  then begin
          inds= where(ddata(*,*) ne 0l and ddata(*,*,*) ne -1l)
         ddata(inds)=abs(ddata(inds))
        
  endif

  vrot = {data  :  data[*,0:ntrack-1], $
          error : ddata[*,0:ntrack-1]  }
  indata = CREATE_STRUCT(indata, 'vrot', vrot)

; Intensity of primary CXRS line
  name   = 'inte    '
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,data,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

; Uncertainty in intensity of primary CXRS line
  name   = 'err_inte'
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,ddata,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

  inte = {data  :  data[*,0:ntrack-1], $
          error : ddata[*,0:ntrack-1]  }
  indata = CREATE_STRUCT(indata, 'inte', inte)

  ENDIF ; primary CX line information

; Fitted temperatures
  name   = 'temp    '
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,data,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

; Uncertainty in fitted temperatures
  name   = 'err_temp'
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,ddata,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

  temp = {data  :  data[*,0:ntrack-1,0:nlines-1], $
          error : ddata[*,0:ntrack-1,0:nlines-1]  }
  indata = CREATE_STRUCT(indata, 'temp', temp)

; Fitted intensities
  name   = 'flux    '
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,data,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

; Uncertainty in fitted intensities
  name   = 'err_flux'
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,ddata,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

  intensity = {data  :  data[*,0:ntrack-1,0:nlines-1], $
               error : ddata[*,0:ntrack-1,0:nlines-1]  }
  indata = CREATE_STRUCT(indata, 'intensity', intensity)

; Fitted wavelengths
  name   = 'wave    '
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,data,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

; Uncertainty in fitted wavelengths
  name   = 'err_wave'
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,ddata,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

  wavelength = {data  :  data[*,0:ntrack-1,0:nlines-1], $
               error : ddata[*,0:ntrack-1,0:nlines-1]  }
  indata = CREATE_STRUCT(indata, 'wavelength', wavelength)

; Fitted baseline
  name   = 'base    '
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,data,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

; Uncertainty in fitted baseline
  name   = 'err_base'
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,ddata,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

  baseline = {data  :  data[*,0:ntrack-1], $
          error : ddata[*,0:ntrack-1]  }
  indata = CREATE_STRUCT(indata, 'baseline', baseline)

; Fit status
  name   = 'fit_stat'
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,data,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

  data = data[*,0:ntrack-1]
  indata = CREATE_STRUCT(indata, 'fit_status', data)

; Fit history
  name     = 'history '
  IF debug THEN print, name
  cxf_read_signal,error,diaref,name,time1,time2,data,/raw
  IF error NE 0 THEN BEGIN
    err = STRTRIM(ID+'Error reading signal: '+name,2)
    GOTO, FINISH
  ENDIF

  niteration = (SIZE(data,/DIMENSIONS))[0]
  history = PTRARR(niteration)
  FOR i=0L, niteration-1 DO BEGIN
    cxf_read_switches, REFORM(data[i,*]), iteration, /StrArray, $
                       debug=debug, err
    IF err NE '' THEN GOTO, FINISH
    history[i] = Ptr_New(iteration, /No_Copy)
  ENDFOR

  indata = CREATE_STRUCT(indata, 'history', history)

; List of predecessors
  name       = 'sflist  '
  IF debug THEN print,name
  maxdepth   = 1L
  sfpred, error, diaref, name, maxdepth, leng, $
          pred_exp, pred_diag, pred_shot, pred_edit, $
          depth, level, time, date
  IF error LT 0 THEN BEGIN
    err = 'SFPRED error: '+ STRTRIM(STRING(error),2)
    GOTO, FINISH
  ENDIF ELSE IF error GT 0 THEN GOTO, DDERROR
  IF leng EQ 1 THEN BEGIN ; no predecessors
    sf_list = {length:0L}
  ENDIF ELSE BEGIN
    wh = WHERE(depth GT 0) ; drop self-reference
    sf_list = {length:leng-1, experiment:pred_exp[wh], shot:pred_shot[wh], $
               diagnostic:pred_diag[wh], edition:pred_edit[wh]}
  ENDELSE

  indata = CREATE_STRUCT(indata, 'sf_list', sf_list)

; Close the shot file
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddclose', $
                     error, diaref)
  IF error NE 0 THEN BEGIN
    IF BYTE(error,2) EQ 0 THEN GOTO, DDERROR ; serious error
  ENDIF

  RETURN

DDERROR:

  err = STRING(' ',format='(a180)')
  s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                    -1L, err, error, 3L, ' ')
  err = ID+STRTRIM(err,2)
  IF debug THEN STOP

FINISH:

  IF error NE 0 AND diaref GT 0 THEN BEGIN
    err2 = 0L
    s = CALL_EXTERNAL (!libddww,'ddgetaug','ddclose', err2, diaref)
  ENDIF
;    stop
    
END ; cxf_load_aug_fit
