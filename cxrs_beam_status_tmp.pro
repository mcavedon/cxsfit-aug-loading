;copied this here so I can go through it and make comments to figure out how this works. -RMM July 21 2009

PRO cxrs_beam_status, diagnostic, shot, time, exp, $
                      beam, NI_shotfile, err

; Return a vector of flags showing at which CXRS measurement
; time 'sufficient' beam power is available

  id  =  'CXRS_BEAM_STATUS: '

; Set beam-on if the average beam power in the integration window exceeds:
  power_threshold =  0.5E6  ; [W]

; check if needed system variable exist. Define it if not.
  defsysv, '!LIBDDWW', exists=exists
  if exists eq 0 then defsysv, '!LIBDDWW', '/usr/ads/lib/libddww.so', 1

; Diagnostic-specific switches
  CASE diagnostic OF
    'CER' : BEGIN
       NI_index = [2,0]; PINI 3
     END
    'CHR' : BEGIN
      IF shot LT 18202 THEN NI_index = [2,1] $  ; PINI 7
                       ELSE NI_index = [3,1]    ; PINI 8
    END
    'CXH' : BEGIN
        load_cxh,shot,cxh_tmp,err_tmo,/only_header
        timep = cxh_tmp.data[*].time
        get_ventil3,shot,v3,err
        if v3.active eq 1 then begin
	    dummy = min(abs(v3.ventil_time[0]+reference_offset - timep),weight_ind)
	    beam=where(timep ge (v3.ventil_time[0]+valid_start) and timep le (v3.ventil_time[1]+valid_end))
        endif else begin
            beam = -1
        endelse

    END
    ELSE : BEGIN
      err_string =  'Unrecognised CXRS diagnostic name!'
      GOTO, FINISH
    ENDELSE
  ENDCASE
; intialise
  ier        = 0L
  dia_ref    = 0L
  datum      = string(' ',format='(a18)')
  dim        = 0L
  phys_unit  = 0L
  err_string = string(' ',format='(a80)')

; Open NI shot file
  NI_exp  =  'AUGD'
  NI_diag =  'NIS'
  NI_shot =  shot
  NI_edn  =  0L
  s = call_external(!libddww, 'ddgetaug', 'ddopen', $
                    ier, NI_exp, NI_diag, NI_shot, NI_edn, $
                    dia_ref, datum)
  if (ier gt 0) then begin
    s = call_external(!libddww,'ddgetaug','xxerrprt',     $
                      -1L,err_string,ier,3L,'ddopen:')
    GOTO, FINISH
  endif

; Read power waveforms

  t1 = 0.0
  t2 = 100.0
  name =  'PNIQ    '
  NI_time = 1L ; to force returning time base as well
  read_signal,ier,dia_ref,name,t1,t2,NI_powers,$
              time=NI_time
  IF ier NE 0 THEN BEGIN
    err_string =  'read_signal error: '+STRTRIM(STRING(ier),2)
    GOTO, FINISH
  ENDIF

; Extract the required source
  NI_power = NI_powers[*,NI_index[0],NI_index[1]]

; Integrate over each CXRS exposure
  ntime =  N_ELEMENTS(time)
  NI_ave_power =  FLTARR(ntime)
  FOR i=0L, ntime-1 DO BEGIN
    tstart = time[i] - exp[i]/2.
    tstop  = time[i] + exp[i]/2.
    wh_NI_time =  WHERE(NI_time GT tstart AND NI_time LT tstop, $
                        n_NI_time)

; make sure end times are included
    tab_time  = [tstart, tstop]
    tab_power = INTERPOL(NI_power, NI_time, tab_time, /SPLINE)
    IF n_NI_time GT 0 THEN BEGIN
       tab_time  = [tab_time[0],  NI_time[wh_NI_time],  tab_time[1] ]
       tab_power = [tab_power[0], NI_power[wh_NI_time], tab_power[1]]
    ENDIF
    NI_ave_power[i] = INT_TABULATED(tab_time, tab_power)/(tstop-tstart)
  ENDFOR

  beam = (NI_ave_power GT power_threshold)

  NI_shotfile =  { experiment : NI_exp,  $
                   diagnostic : NI_diag, $
                   shot       : NI_shot, $
                   edition    : NI_edn   }

FINISH:
; Close shotfile
   s = call_external(!libddww,'ddgetaug','ddclose', $
                    ier,dia_ref)
  if (ier gt 0 and err_string EQ '') then begin
    s = call_external(!libddww,'ddgetaug','xxerrprt', $
                      -1L,err_string,ier,3L,'ddclose:')
  endif

  if STRLEN(STRTRIM(err_string, 2)) GT 0 THEN $
    err = id+err_string $
  else err =  ''
END

