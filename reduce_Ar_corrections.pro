;The corrections we are applying to the Ar 16 n=1514 CX line 
;are too big - they correspond to a perfect spectrum where the asymmetry can have its full impact
;In reality the noise, Bremmstrahlung background, He lines, and reduced areas
;we apply limit the impact of the asymmetry, causing the correction to be too big. 
;These corrections might still be too big. 
;But this is the maximum correction taking into account photon noise weighting in the
;presence of a Bremmstrahlung background

pro restore_Arbck_grids,out

    dir='/afs/ipp-garching.mpg.de/home/c/cxrs/idl/cxsfit64/machines/aug/'
    ;restore, dir+'vrot_corr_ar15_1514_dense_wbg.idlsave';,/verb
    ;restore, dir+'ti_corr_ar15_1514_dense_wbg.idlsave';,/verb
    restore, dir+'vrot_corr_ar15_1514_2Gfit_wbg.idlsave';,/verb
    restore, dir+'ti_corr_ar15_1514_2Gfit_wbg.idlsave';,/verb
    
    ;V_shift,T_I,B_ANGLE,B_T,bg,lambda0
    ;T_I_APP_ALL,T_I,B_ANGLE,B_T,bg
    out={V_shift:V_shift,T_I_APP_ALL:t_i_app_all,T_I:t_i,B_ANGLE:b_angle,b_T:b_t,bg:bg}

return
end


pro reduce_Ar_corrections,Arcorinfo,Tiapp,Btor,BANG,BackRatio, $
                          TITRUE,VSHIFTTRUE
                         ;TIUPSHIFT, VUPSHIFT; original code
    set32_64
    
    v_shift=arcorinfo.V_shift
    T_I_APP_ALL=arcorinfo.t_i_app_all
     
    T_I=arcorinfo.t_i
    B_ANGLE=arcorinfo.b_angle
    b_T=arcorinfo.b_t
    bg=arcorinfo.bg
    
    ;Keep it in the grids
    if backratio lt 0. then backratio=0.0
    if backratio gt 1.5 then backratio =1.5
        
    ;dir='/u/rmcdermo/idl/zeeman_fs/'
    ;dir='/afs/ipp-garching.mpg.de/home/c/cxrs/idl/cxsfit64/machines/aug/'
    ;restore, dir+'vrot_corr_ar15_1514_dense_wbg.idlsave';,/verb
    ;restore, dir+'ti_corr_ar15_1514_dense_wbg.idlsave';,/verb
    ;These grids are not NEARY as dense as Thomas's - how long did it take you to run yours?
    
    nti=n_elements(t_i)
    nbt=n_elements(b_t)
    nba=n_elements(b_angle)
    nbg=n_elements(bg)

    btind=interpol(findgen(nbt),b_t,btor)
    bangind=interpol(findgen(nba),b_angle,bang)
    bgind=interpol(findgen(nbg),bg,backratio)
    bgind0=0.0

    ;New answer
    TIAPPINTERP=fltarr(nti)
    vshiftinterp=fltarr(nti)
    
    ;I want to compare these numbers to the zero background case. 
    TIAPPINTERP0=fltarr(nti)
    vshiftinterp0=fltarr(nti)

    for i=0,nti-1 do begin  
        
        tiappinterp[i]=interpolate(reform(t_i_app_all[i,*,*,*]),btind,bangind,bgind)
        vshiftinterp[i]=interpolate(reform(v_shift[i,*,*,*]),btind,bangind,bgind)
         
        tiappinterp0[i]=interpolate(reform(t_i_app_all[i,*,*,*]),btind,bangind,bgind0)
        vshiftinterp0[i]=interpolate(reform(v_shift[i,*,*,*]),btind,bangind,bgind0)
        
    endfor
    
    tiord=sort(t_i) ; just in case - first round of corrections had (by mistake)
    ;a non-monotonic Ti grid! oops 
    TITRUE=interpol(t_i[tiord],tiappinterp[tiord],tiapp)
    VSHIFTTRUE=interpol(vshiftinterp,t_i,titrue)
  
    TITRUE0=interpol(t_i[tiord],tiappinterp0[tiord],tiapp)
    VSHIFTTRUE0=interpol(vshiftinterp0,t_i,titrue0)
    
    ;print,'Ticor',titrue0,titrue
    ;print,'Vcor',vshifttrue0,vshifttrue
    
    ;plot,TIAPPINTERP0/1000.,t_i/1000.,thi=2,psym=-4,xtit='App Ti (keV)',$
    ;    ytit='True Ti (keV)',xran=[0,3],/xsty,/ysty,$
    ;    xticklen=1,yticklen=1,xgridsty=1,ygridsty=1
    ;oplot,TIAPPINTERP/1000.,t_i/1000.,thi=2,col=150,line=2,psym=-5
    ;oplot,[tiapp,tiapp]/1000.,[0,5],line=2,thi=2,col=100
    
    ;print,'Diff Ticor',titrue-titrue0
    ;print,'Diff Vcor',Vshifttrue-Vshifttrue0
    
    
    ;These do not apply to the 2G fit case. 
    ;tiupshift=titrue-titrue0
    ;vupshift=vshifttrue-vshifttrue0
    
    
return
end
