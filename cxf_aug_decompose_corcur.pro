pro cxf_aug_decompose_corcur,experiment,shot,specdata,outdata,specdata_cor,specdata_cur,outdata_cor,outdata_cur

                     print,outdata.spectrometer
                     ;help,outdata
                     print,experiment
                     ;;;;;;;;;

                        spectrometer_original = 'COR+CUR'
                        print,'CUR'

                        if (!version.memory_bits eq 64) then $
                        libddww='/afs/ipp/aug/ads/lib64/@sys/libddww8.so' else $
                        libddww= '/usr/ads/lib/libddww.so'
                        ier        = 0L
                        shot       = long(outdata.shot_nr)
                        ed         = 0L
                        dia_ref    = 0L
                        datum      = string(' ',format='(a18)')
                        phys_unit  = 0L
                        dim=0L
  
                        s = call_external(libddww, 'ddgetaug', 'ddopen', $
                                          ier, experiment,'CUR',shot, ed, dia_ref, datum)
                            ;; Number of channels on the spectrometer----------------------------------
                            nchan = 0L
                            s = call_external(libddww, 'ddgetaug', 'ddparm', $
                                              ier, dia_ref, 'READ_CCD', 'ydim', $
                                              1L, 1L, nchan, phys_unit)   
                            s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror', ier, 3L, ' ')
                            if nchan eq 0 then begin
                               print, 'CCD data not in shotfile!'
                               return
                            endif
                            print,nchan
                            losdum='        '
                            los_name = strarr(nchan)    
                            ;;Read in LOS names per ROI on Camera
                            for i=0,nchan-1 do begin
                                chan_string = 'CHAN_' + string(i+1, format='(I2.2)') ; = 'CHAN_01' etc.
                                s = call_external(libddww, 'ddgetaug', 'ddparm', $
                                              ier, dia_ref, 'PARAM', chan_string, $
                                              6L,8L, losdum, phys_unit) 
                                s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror', ier, 3L, ' ')        
                                if ier eq 0 then los_name[i] = string(byte(losdum))    
                            endfor  
                        print,los_name
                        ;;;;
                        sort_idx = INTARR(n_elements(los_name))
                        sort_cur_idx = INTARR(n_elements(specdata.los_name))
                        for iindex=0, n_elements(los_name)-1 do begin
                            sort_idx[iindex]=UINT(strmid(los_name[iindex],4,2))
                        endfor
                        for iindex=0, n_elements(specdata.los_name)-1 do begin
                            sort_cur_idx[iindex]=UINT(strmid(specdata.los_name[iindex],4,2))
                        endfor
                        ;;;;
                        match,sort_idx,sort_cur_idx,suba,subb,COUNT=count,/SORT,EPSILON=10E-02
                        print,'count =',count
                        print,'suba =',suba
                        print,'subb =',subb
                        print,outdata.spectrometer                                    
                        ;stop

                     ;;;;;;;;;
                     ;;;;;;;;;
                     ntime        = n_elements(specdata.time)
                     ;print,ntime
                     r_pos_new    = FLTARR(count)
                     z_pos_new    = FLTARR(count)
                     los_name_new = STRARR(count)
                     PHI_POS_new      = FLTARR(count)
                     R_ORIG_new       = FLTARR(count)
                     Z_ORIG_new       = FLTARR(count)
                     PHI_ORIG_new     = FLTARR(count)
                     R_POS_TIME_new      = fltarr(count,ntime)
                     Z_POS_TIME_new      = fltarr(count,ntime)
                     PHI_POS_TIME_new    = fltarr(count,ntime)
                     DR_POS_TIME_new     = fltarr(count,ntime)
                     DZ_POS_TIME_new     = fltarr(count,ntime)
                     DPHI_POS_TIME_new   = fltarr(count,ntime)
                     EXTERNALCOSTHETA_new= fltarr(count,ntime)
                     fit_status_new  = intarr(ntime,count)
                     WSHIFT_new      = fltarr(ntime,count)
                     WLFP_new        = fltarr(ntime,count)
                     pixref_new      = fltarr(ntime,count)
                     
                     r_pos_new    = specdata.r_pos[subb]
                     z_pos_new    = specdata.z_pos[subb]
                     los_name_new = specdata.los_name[subb]
                     phi_pos_new  = specdata.phi_pos[subb]
                     r_orig_new   = specdata.r_orig[subb]
                     z_orig_new   = specdata.z_orig[subb]
                     phi_orig_new = specdata.phi_orig[subb]
                     R_POS_TIME_new      = specdata.r_pos_time[subb,*]
                     Z_POS_TIME_new      = specdata.z_pos_time[subb,*]
                     PHI_POS_TIME_new    = specdata.phi_pos_time[subb,*]
                     DR_POS_TIME_new     = specdata.DR_POS_TIME[subb,*]
                     DZ_POS_TIME_new     = specdata.Dz_POS_TIME[subb,*]
                     DPHI_POS_TIME_new   = specdata.DPHI_POS_TIME[subb,*]
                     EXTERNALCOSTHETA_new= specdata.EXTERNALCOSTHETA[subb,*]
                     fit_status_new  = outdata.fit_status[*,subb]
                     WSHIFT_new      = outdata.WSHIFT[*,subb]
                     WLFP_new        = outdata.WLFP[*,subb]
                     pixref_new      = outdata.pixref[*,subb]
                    
                     
                     specdata_cur = {machine: specdata.machine,$
                                     spectrometer: specdata.spectrometer,$
                                     input_file : specdata.input_file,$
                                     shot_nr: specdata.shot_nr,$
                                     npixel: specdata.npixel,$
                                     ntrack: count,$
                                     nframe: specdata.nframe,$
                                     nframe_baseline: specdata.nframe_baseline,$
                                     ninst: specdata.ninst,$
                                     modulation: specdata.modulation,$
                                     wlength: specdata.wlength,$
                                     slit: specdata.slit,$
                                     los_name: specdata.los_name[subb],$
                                     r_pos: r_pos_new,$
                                     z_pos: z_pos_new,$
                                     PHI_POS: phi_pos_new,$
                                     R_ORIG: r_orig_new,$
                                     Z_ORIG: z_orig_new,$
                                     PHI_ORIG: phi_orig_new,$
                                     TIME: specdata.time,$
                                     EXPOSURE: specdata.exposure,$                                                       
                                     BEAM: specdata.beam,$
                                     BASELINE: specdata.baseline,$
                                     INTENSITY: specdata.intensity,$
                                     WAVELENGTH: specdata.wavelength,$
                                     DISPERSION: specdata.dispersion,$
                                     INSTFU: specdata.instfu,$
                                     R_POS_TIME: r_pos_time_new,$
                                     Z_POS_TIME: z_pos_time_new,$
                                     PHI_POS_TIME: phi_pos_time_new,$
                                     DR_POS_TIME: dr_pos_time_new,$
                                     DZ_POS_TIME: dz_pos_time_new,$
                                     DPHI_POS_TIME: dphi_pos_time_new,$
                                     EXTERNALCOSTHETA: externalcostheta_new}
                                     
                                     
                     outdata_cur = {CODE: outdata.code,$
                                    MACHINE: outdata.machine,$
                                    SPECTROMETER: 'CUR',$
                                    SHOT_NR: outdata.shot_nr,$
                                    NTRACK: count,$
                                    NFRAME: outdata.nframe,$
                                    NLINES: outdata.nlines,$
                                    MAX_NGAUSSPERLINE: outdata.max_ngaussperline,$
                                    MAX_NFREEPARAMPERLINE: outdata.max_nfreeparamperline,$
                                    MAX_NPARAMCOUPLING: outdata.MAX_NPARAMCOUPLING,$
                                    GAUSSPERLINE: outdata.gaussperline,$
                                    LINETYPE: outdata.linetype,$
                                    FREEPARAMPERLINE: outdata.freeparamperline,$
                                    MASS: outdata.mass,$
                                    TEMP: {data: outdata.temp.data[*,*,subb],$
                                          error: outdata.temp.error[*,*,subb]},$
                                    INTENSITY: {data: outdata.intensity.data[*,*,subb],$
                                          error: outdata.temp.error[*,*,subb]},$
                                    WAVELENGTH: {data: outdata.wavelength.data[*,*,subb],$
                                          error: outdata.wavelength.error[*,*,subb]},$
                                    VROT: {data: outdata.vrot.data[*,*,subb],$
                                          error: outdata.vrot.error[*,*,subb]},$
                                    BASELINE: {data: outdata.baseline.data[*,subb],$
                                              dataleft: outdata.baseline.dataleft[*,subb],$
                                              dataright: outdata.baseline.dataright[*,subb],$
                                              error: outdata.baseline.error[*,subb]},$
                                    COUPLINGS: outdata.couplings,$
                                    FIT_STATUS: fit_status_new,$
                                    WSHIFT: wshift_new,$
                                    WLFP: wlfp_new,$
                                    PIXREF: pixref_new}
                                    
                                    ;stop
                     ;;;;;;;;;
                     ;;;;;;;;;
                        print,'COR'

                        s = call_external(libddww, 'ddgetaug', 'ddopen', $
                                          ier, experiment,'COR',shot, ed, dia_ref, datum)
                            ;; Number of channels on the spectrometer----------------------------------
                            nchan = 0L
                            s = call_external(libddww, 'ddgetaug', 'ddparm', $
                                              ier, dia_ref, 'READ_CCD', 'ydim', $
                                              1L, 1L, nchan, phys_unit)   
                            s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror', ier, 3L, ' ')
                            if nchan eq 0 then begin
                               print, 'CCD data not in shotfile!'
                               return
                            endif
                            print,nchan
                            losdum='        '
                            los_name = strarr(nchan)    
                            ;;Read in LOS names per ROI on Camera
                            for i=0,nchan-1 do begin
                                chan_string = 'CHAN_' + string(i+1, format='(I2.2)') ; = 'CHAN_01' etc.
                                s = call_external(libddww, 'ddgetaug', 'ddparm', $
                                              ier, dia_ref, 'PARAM', chan_string, $
                                              6L,8L, losdum, phys_unit) 
                                s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror', ier, 3L, ' ')        
                                if ier eq 0 then los_name[i] = string(byte(losdum))    
                            endfor  
                        print,los_name
                        ;;;;
                        sort_idx = INTARR(n_elements(los_name))
                        sort_cor_idx = INTARR(n_elements(specdata.los_name))
                        for iindex=0, n_elements(los_name)-1 do begin
                            sort_idx[iindex]=UINT(strmid(los_name[iindex],4,2))
                        endfor
                        for iindex=0, n_elements(specdata.los_name)-1 do begin
                            sort_cor_idx[iindex]=UINT(strmid(specdata.los_name[iindex],4,2))
                        endfor
                        ;;;;
                        match,sort_idx,sort_cor_idx,suba,subb,COUNT=count,/SORT,EPSILON=10E-02
                        print,'count =',count
                        print,'suba =',suba
                        print,'subb =',subb
                        print,outdata.spectrometer
                        ;help,outdata
                        ;stop

                     ;;;;;;;;;
                     ;;;;;;;;;
                     ntime        = n_elements(specdata.time)
                     r_pos_new    = FLTARR(count)
                     z_pos_new    = FLTARR(count)
                     los_name_new = STRARR(count)
                     PHI_POS_new      = FLTARR(count)
                     R_ORIG_new       = FLTARR(count)
                     Z_ORIG_new       = FLTARR(count)
                     PHI_ORIG_new     = FLTARR(count)
                     R_POS_TIME_new      = fltarr(count,ntime)
                     Z_POS_TIME_new      = fltarr(count,ntime)
                     PHI_POS_TIME_new    = fltarr(count,ntime)
                     DR_POS_TIME_new     = fltarr(count,ntime)
                     DZ_POS_TIME_new     = fltarr(count,ntime)
                     DPHI_POS_TIME_new   = fltarr(count,ntime)
                     EXTERNALCOSTHETA_new= fltarr(count,ntime)
                     fit_status_new  = intarr(ntime,count)
                     WSHIFT_new      = fltarr(ntime,count)
                     WLFP_new        = fltarr(ntime,count)
                     pixref_new      = fltarr(ntime,count)
                     r_pos_new    = specdata.r_pos[subb]
                     z_pos_new    = specdata.z_pos[subb]
                     los_name_new = specdata.los_name[subb]
                     phi_pos_new  = specdata.phi_pos[subb]
                     r_orig_new   = specdata.r_orig[subb]
                     z_orig_new   = specdata.z_orig[subb]
                     phi_orig_new = specdata.phi_orig[subb]
                     R_POS_TIME_new      = specdata.r_pos_time[subb,*]
                     Z_POS_TIME_new      = specdata.z_pos_time[subb,*]
                     PHI_POS_TIME_new    = specdata.phi_pos_time[subb,*]
                     DR_POS_TIME_new     = specdata.DR_POS_TIME[subb,*]
                     DZ_POS_TIME_new     = specdata.Dz_POS_TIME[subb,*]
                     DPHI_POS_TIME_new   = specdata.DPHI_POS_TIME[subb,*]
                     EXTERNALCOSTHETA_new= specdata.EXTERNALCOSTHETA[subb,*]
                     fit_status_new  = outdata.fit_status[*,subb]
                     WSHIFT_new      = outdata.WSHIFT[*,subb]
                     WLFP_new        = outdata.WLFP[*,subb]
                     pixref_new      = outdata.pixref[*,subb]
                     specdata_cor = {machine: specdata.machine,$
                                     spectrometer: specdata.spectrometer,$
                                     input_file : specdata.input_file,$
                                     shot_nr: specdata.shot_nr,$
                                     npixel: specdata.npixel,$
                                     ntrack: count,$
                                     nframe: specdata.nframe,$
                                     nframe_baseline: specdata.nframe_baseline,$
                                     ninst: specdata.ninst,$
                                     modulation: specdata.modulation,$
                                     wlength: specdata.wlength,$
                                     slit: specdata.slit,$
                                     los_name: los_name_new,$
                                     r_pos: r_pos_new,$
                                     z_pos: z_pos_new,$
                                     PHI_POS: phi_pos_new,$
                                     R_ORIG: r_orig_new,$
                                     Z_ORIG: z_orig_new,$
                                     PHI_ORIG: phi_orig_new,$
                                     TIME: specdata.time,$
                                     EXPOSURE: specdata.exposure,$                                                       
                                     BEAM: specdata.beam,$
                                     BASELINE: specdata.baseline,$
                                     INTENSITY: specdata.intensity,$
                                     WAVELENGTH: specdata.wavelength,$
                                     DISPERSION: specdata.dispersion,$
                                     INSTFU: specdata.instfu,$
                                     R_POS_TIME: r_pos_time_new,$
                                     Z_POS_TIME: z_pos_time_new,$
                                     PHI_POS_TIME: phi_pos_time_new,$
                                     DR_POS_TIME: dr_pos_time_new,$
                                     DZ_POS_TIME: dz_pos_time_new,$
                                     DPHI_POS_TIME: dphi_pos_time_new,$
                                     EXTERNALCOSTHETA: externalcostheta_new}
                     outdata_cor = {CODE: outdata.code,$
                                    MACHINE: outdata.machine,$
                                    SPECTROMETER: 'COR',$
                                    SHOT_NR: outdata.shot_nr,$
                                    NTRACK: count,$
                                    NFRAME: outdata.nframe,$
                                    NLINES: outdata.nlines,$
                                    MAX_NGAUSSPERLINE: outdata.max_ngaussperline,$
                                    MAX_NFREEPARAMPERLINE: outdata.max_nfreeparamperline,$
                                    MAX_NPARAMCOUPLING: outdata.MAX_NPARAMCOUPLING,$
                                    GAUSSPERLINE: outdata.gaussperline,$
                                    LINETYPE: outdata.linetype,$
                                    FREEPARAMPERLINE: outdata.freeparamperline,$
                                    MASS: outdata.mass,$
                                    TEMP: {data: outdata.temp.data[*,*,subb],$
                                          error: outdata.temp.error[*,*,subb]},$
                                    INTENSITY: {data: outdata.intensity.data[*,*,subb],$
                                          error: outdata.temp.error[*,*,subb]},$
                                    WAVELENGTH: {data: outdata.wavelength.data[*,*,subb],$
                                          error: outdata.wavelength.error[*,*,subb]},$
                                    VROT: {data: outdata.vrot.data[*,*,subb],$
                                          error: outdata.vrot.error[*,*,subb]},$
                                    BASELINE: {data: outdata.baseline.data[*,subb],$
                                              dataleft: outdata.baseline.dataleft[*,subb],$
                                              dataright: outdata.baseline.dataright[*,subb],$
                                              error: outdata.baseline.error[*,subb]},$
                                    COUPLINGS: outdata.couplings,$
                                    FIT_STATUS: fit_status_new,$
                                    WSHIFT: wshift_new,$
                                    WLFP: wlfp_new,$
                                    PIXREF: pixref_new}
                     ;;;;;;;;;
                     print,experiment
                     help,specdata
                     help,specdata_cur
                     help,specdata_cor
                     help,outdata
                     help,outdata_cur
                     help,outdata_cor
                     stop

end
