; reads the Raus position, integrates over spectrometer integration
; time and recalculates Raus position between 0 and 1 for beam
; modulation tool (so, that signal "active" if Raus > 0.85)
; Version 1 by U.Plank 23.08.2019



PRO Raus,  shot, time, exp, Raus_exp=Raus_exp, Raus_diag=Raus_diag,Raus_shot=Raus_shot, Raus_edn=Raus_edn, Raus_name=Raus_name, Raus_time=Raus_time, $
                      Raus, Raus_shotfile, err
  

  
  id  =  'RAUS_STATUS: '
  
;  
; check if needed system variable exist. Define it if not.
  defsysv, '!LIBDDWW', exists=exists
  if exists eq 0 then defsysv, '!LIBDDWW', '/usr/ads/lib/libddww.so', 1
  
  
; intialise
  ier        = 0L
  dia_ref    = 0L
  datum      = string(' ',format='(a18)')
  dim        = 0L
  phys_unit  = 0L
  err_string = string(' ',format='(a80)')
   
  ntime =  N_ELEMENTS(time)  
  
; Open equilibrium shot file for Raus position
  if not keyword_set(Raus_exp) then Raus_exp  =  'AUGD'
  if not keyword_set(Raus_diag) then Raus_diag =  'IDG'
  if not keyword_set(Raus_shot) then Raus_shot =  shot
  if not keyword_set(Raus_edn) then Raus_edn  =  0L
  if not keyword_set(Raus_name) then Raus_name =  'Raus'
  if not keyword_set(Raus_time) then Raus_time = 1L                ; to force returning time base as well
                                ;read_signal
  
  read_signal_mrm,ier, Raus_shot,Raus_diag,Raus_name,Raus_time,Raus_signal,phys_dim, edition=Raus_edn, exp=Raus_exp
  IF ier NE 0 THEN BEGIN
     
     read_signal_mrm,ier, Raus_shot, 'GQH',Raus_name,Raus_time,Raus_signal,phys_dim, edition=Raus_edn, exp=Raus_exp
     
     if ier ne 0 then begin
        
        read_signal_mrm,ier, Raus_shot, 'GQI',Raus_name,Raus_time,Raus_signal,phys_dim, edition=Raus_edn, exp=Raus_exp
        
        if ier ne 0 then begin
           Raus_time=time
           Raus_signal=fltarr(ntime)
           Raus=Raus_signal+1
           GOTO, FINISH
        endif
     endif
  ENDIF
  if shot eq 37168 then begin   ;there was no plasma and thus, no Raus data
     Raus_time=time
     Raus_signal=fltarr(ntime)
     Raus=Raus_signal+1
     GOTO, FINISH
  endif     
  
  Raus_ave_signal =  FLTARR(ntime)
  
  
  FOR i=0L, ntime-1 DO BEGIN
     tstart = time[i] - exp[i]/2.
     tstop  = time[i] + exp[i]/2.
     wh_Raus_time =  WHERE(Raus_time GT tstart AND Raus_time LT tstop, $
                           n_Raus_time)
     tab_time  = [tstart, tstop]
     tab_signal = INTERPOL(Raus_signal, Raus_time, tab_time, /SPLINE)
     if n_Raus_time GT 0 then begin
        tab_time  = [tab_time[0],  Raus_time[wh_Raus_time],  tab_time[1] ]
        tab_signal = [tab_signal[0], Raus_signal[wh_Raus_time], tab_signal[1]]
        
     endif                                                    
     Raus_ave_signal[i] = INT_TABULATED(tab_time, tab_signal)/(tstop-tstart)
     if Raus_ave_signal[i] lt 0 then Raus_ave_signal[i]=0
     ;if Raus_ave_signal[i] ge 0 and Raus_ave_signal[i] lt 2.1425 then Raus_ave_signal[i]=0.855
     ;if Raus_ave_signal[i] ge 2.1425 and Raus_ave_signal[i] lt 2.1435 then Raus_ave_signal[i]=0.89
     ;if Raus_ave_signal[i] ge 2.1435 and Raus_ave_signal[i] lt 2.1445 then Raus_ave_signal[i]=0.925
     ;if Raus_ave_signal[i] ge 2.1445 and Raus_ave_signal[i] lt 2.1455 then Raus_ave_signal[i]=0.96
     ;if Raus_ave_signal[i] ge 2.1455 and Raus_ave_signal[i] lt 2.1465 then Raus_ave_signal[i]=0.99     
     ;if Raus_ave_signal[i] ge 2.1465 then Raus_ave_signal[i]=1.1     
  ENDFOR
  ;Raus=FLTARR(ntime)
  ;wh_R=WHERE(Raus_ave_signal ge 2.14 and Raus_ave_signal le 2.15)
  ;tmin=wh_Raus_ave_signal[0]
  ;tmax=wh_Raus_ave_signal[-1]
  
  Raus =0.7+0.3* (Raus_ave_signal-2.13)/(2.15-2.13) 

  for i=0L, ntime-1 DO BEGIN
     if Raus[i] lt 0 then Raus[i]=0
     ;if Raus[i] gt 1 then Raus[i]=1
  endfor   
FINISH:

Raus_shotfile =  { experiment : Raus_exp,  $
                   diagnostic : Raus_diag, $
                   shot       : Raus_shot, $
                   edition    : Raus_edn   }

; Close shotfile
     s = call_external(!libddww,'ddgetaug','ddclose', $
                       ier,dia_ref)
     if (ier gt 0 and err_string EQ '') then begin
        s = call_external(!libddww,'ddgetaug','xxerrprt', $
                          -1L,err_string,ier,3L,'ddclose:')
     endif
     
     if STRLEN(STRTRIM(err_string, 2)) GT 0 THEN $
        err = id+err_string $    
     else err =  ''
  
  
 END
  
