pro change_sfh, name, nt, ntracks, nlines, nt_baseline, ncomm, nhistory, nparams, $
    maxlines, maxtracks

    ;Check if sfhlib var. is defined or not
    defsysv, '!LIBSFH', exists=exists
    if exists eq 0 then begin
        defsysv, '!LIBSFH', '/afs/ipp/aug/ads/lib64/@sys/libsfh8.so',0
    endif else begin
        !LIBSFH = '/afs/ipp/aug/ads/lib64/@sys/libsfh8.so'
    endelse

    ;; some variables
    ier        = 0L
    name       = name
    sfhref    = 0L
    err_string = string(' ',format='(a80)')
    nt = long(nt)
    nt_baseline = long(nt_baseline)
    ntracks = long(ntracks)
    nlines = long(nlines)
     
     
    ;; open header
    ;; -------------
    s = call_external(!libsfh, 'sfhidl', 'sfhopen', $
                   ier, name, sfhref)
    if (ier gt 0) then begin
        s = call_external(!libsfh, 'sfhidl', 'sfherror', ier, err_string)
        stop
    endif

    ;; read maxtrack and maxlines from the parameters in the sfh
    ;; --------------------------------------------------------
    object = string('LOSInfo',f='(a-8)')
    param = string('R',f='(a-8)')
    len = 0L
    maxtrack = 0L
    data = fltarr(100)+1
    npar = 0L
    res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfhreadpar', ier, sfhref, $
        object, param, 2L, maxtrack, data, npar)
    stop
    if ier ne 0 then $
        res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfherror', ier, signam )


    ;; Change nchannel/nlines in Area Bases
    ;; -------------
    areaBases = ['LOSNum','R','z','phi']
    for jarea=0,n_elements(areaBases)-1 do begin
        areaBase = areaBases[jarea]
        s = call_external(!libsfh, 'sfhidl', 'sfhmdarea', ier,$
                       sfhref, areaBase, 1L, ntracks, 0L, 0L)
        if (ier gt 0) then begin
            s = call_external(!libsfh, 'sfhidl', 'sfherror', ier, err_string)
            stop
        endif
    endfor
    areaBase = string('LineNum',f='(a-8)')
    s = call_external(!libsfh, 'sfhidl', 'sfhmdarea', ier,$
                   sfhref, areaBase, 1L, nlines, 0L, 0L)
    if (ier gt 0) then begin
        s = call_external(!libsfh, 'sfhidl', 'sfherror', ier, err_string)
        stop
    endif
    areaBase = string('LOSLine',f='(a-8)')
    s = call_external(!libsfh, 'sfhidl', 'sfhmdarea', ier,$
                   sfhref, areaBase, 1L, ntracks, nlines, 0L)
    if (ier gt 0) then begin
        s = call_external(!libsfh, 'sfhidl', 'sfherror', ier, err_string)
        stop
    endif

    ;; Change time points in TimeBase
    ;; -----------------------------------

    signam = string('time',f='(a-8)')
    res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfhmodtim', ier, sfhref, signam, nt)
    if ier ne 0 then $
        res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfherror', ier, signam )
    signam = string('texp',f='(a-8)')
    res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfhmodtim', ier, sfhref, signam, nt)
    if ier ne 0 then $
        res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfherror', ier, signam )
    signam = string('base_tim',f='(a-8)')
    res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfhmodtim', ier, sfhref, signam, nt_baseline)
    if ier ne 0 then $
        res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfherror', ier, signam )



    ;; Change Dimensions of SignalGroups
    ;; ---------------------------------
    SigGroups = ['Ti','Ti_c','err_Ti','err_Ti_c','vrot',$
        'err_vrot','inte','err_inte','fit_stat']
    for jsig=0,n_elements(SigGroups)-1 do begin
        signam = SigGroups[jsig]
        ; change number of time points
        res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfhmodtim', ier, sfhref, signam, nt)
        if ier ne 0 then $
            res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfherror', ier, signam)
        ; change number of channels            
        res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfhmdindex24', ier, sfhref, signam, ntracks, 1L, 1L)
        if ier ne 0 then $
            res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfherror', ier, signam)
    endfor
    SigGroups = ['temp','err_temp','flux','err_flux','wave',$
        'err_wave','base','err_base']
    for jsig=0,n_elements(SigGroups)-1 do begin
        signam = SigGroups[jsig]
        ; change number of time points
        res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfhmodtim', ier, sfhref, signam, nt)
        if ier ne 0 then $
            res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfherror', ier, signam)
        ; change number of channels            
        res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfhmdindex24', ier, sfhref, signam, ntracks, 1L, 1L)
        if ier ne 0 then $
            res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfherror', ier, signam)
    endfor
    SigGroups = ['baseline','baselerr']
    for jsig=0,n_elements(SigGroups)-1 do begin
        signam = SigGroups[jsig]
        ; change number of time points
        res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfhmodtim', ier, sfhref, signam, nt_baseline)
        if ier ne 0 then $
            res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfherror', ier, signam)
        ; change number of channels            
        res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfhmdindex24', ier, sfhref, signam, ntracks, 1L, 1L)
        if ier ne 0 then $
            res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfherror', ier, signam)
    endfor

    ;; Change History dimensions
    signam = 'history'
    res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfhmodtim', ier, sfhref, signam, nhistory)
    if ier ne 0 then $
        res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfherror', ier, signam)
    res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfhmdindex24', ier, sfhref, signam, nparams, 1L, 1L)
    if ier ne 0 then $
        res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfherror', ier, signam)
     
    ;; Change comment dimensions
    signam = 'Comment'
    res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfhmodtim', ier, sfhref, signam, ncomm)
    if ier ne 0 then $
        res = CALL_EXTERNAL(!libsfh, 'sfhidl', 'sfherror', ier, signam)

    ;; close header
    ;; -------------
    s = call_external(!libsfh, 'sfhidl', 'sfhclose', $
                   ier, sfhref)
    if (ier gt 0) then begin
        s = call_external(!libsfh, 'sfhidl', 'sfherror', ier, err_string)
        stop
    endif

end
