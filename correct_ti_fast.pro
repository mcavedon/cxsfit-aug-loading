@/afs/ipp-garching.mpg.de/home/c/cxrs/idl/cxsfit64/machines/aug/reduce_Ar_corrections.pro

pro correct_ti_fast,shot,time_array, los_struct,wvl,t_i_array,t_i_err,t_i_corr_array,t_i_err_corr_array,$
                    v_array,v_err,v_corrected,v_err_corrected,corv,diagnostic,backratio

; -> shot           :   shotnumber, long
; -> time_array     :   times of ti-array, float-array
; -> los_struct     :   structure containing the LOS-geometry (as available in cxf_save_aug_fit.pro)
; -> line_name      :   name of spectral line, string
; -> t_i_array      :   t_i for each time points and LOS, float-array
; <- t_i_corr_array :   corrected t_i for each time points and LOS, float-array
;
;please not that:
;a) the correction is including the fine-structure correction
;b) the correction is including a fully-mixed l and m distribution within each main quantum number
;


; details of LOS structure:
; struct.R_pos      : R positions of measurement
; struct.z_pos      : z positions of measurement
; struct.phi_pos    : phi positions of measurement
;
; struct.R_orig      : R positions of optical head
; struct.z_orig      : z positions of optical head
; struct.phi_orig    : phi positions of optical head
;

; first get angles between LOS and magnetic field line at point of measurement
; for each time point: 1) get field line geometry 2) determine angle between LOS 3) store in array

; Read magnetic equilibrium data directly

;RMM - 01 2020 - 99% of things don't have a rotation correction
CORV=0.
;djcruz - 09-2020 High resolution tables for low Ti only available for N and B
low=0.


;eqiread, shot, min(time_array), max(time_array), diag='IDE', ed=0, exp='AUGD',$
;                surfaces=surfaces

;eqiread, shot, min(time_array), max(time_array), diag='FPP', ed=0, exp='AUGD',$
;                surfaces=surfaces
   

if shot eq 40148 then begin
   eqiread, shot, min(time_array), max(time_array), diag='EQI', ed=0, exp='AUGD',$
               surfaces=surfaces 
endif else begin
   if shot eq 40140 or shot eq 40124 or shot gt 40224 and shot lt 40231 or shot gt 40333 and shot lt 40400  then begin
      eqiread, shot, min(time_array), max(time_array), diag='EQH', ed=0, exp='AUGD',$
               surfaces=surfaces                
      
   endif else begin
      eqiread, shot, min(time_array), max(time_array), diag='EQR', ed=0, exp='AUGD',$
               surfaces=surfaces
   endelse
endelse
;stop
t_i_corr_array=t_i_array*0.
t_i_err_corr_array=t_i_array*0.

v_corr_geo=v_array*0.
v_corrected=v_array*0.
v_err_corrected=v_err*0.


intersect_angles = fltarr(n_elements(time_array), n_elements(los_struct.r_pos))
intersec_angles=intersect_angles
b_absolute = fltarr(n_elements(time_array), n_elements(los_struct.r_pos))
los_vector1 = fltarr(n_elements(los_struct.r_pos),3)
los_vector2 = fltarr(n_elements(los_struct.r_pos),3)
los_vector= fltarr(n_elements(los_struct.r_pos),3)
torvec=fltarr(n_elements(los_struct.r_pos),3)
los_name=strarr(n_elements(los_struct.r_pos))
rvec=fltarr(n_elements(time_array), n_elements(los_struct.r_pos),3)

for i_los = 0, n_elements(los_struct.r_pos)-1 do begin

    dm=strsplit(los_struct.los_name[i_los],'-',/extract)
    los_name[i_los]=dm[0]

    los_vector1(i_los,0)=los_struct.r_pos(i_los)*cos(los_struct.phi_pos(i_los)/180.*!Pi)
    los_vector1(i_los,1)=los_struct.r_pos(i_los)*sin(los_struct.phi_pos(i_los)/180.*!Pi)
    los_vector1(i_los,2)= los_struct.z_pos(i_los)

    los_vector2(i_los,0)=los_struct.r_orig(i_los)*cos(los_struct.phi_orig(i_los)/180.*!Pi)
    los_vector2(i_los,1)=los_struct.r_orig(i_los)*sin(los_struct.phi_orig(i_los)/180.*!Pi)
    los_vector2(i_los,2)= los_struct.z_orig(i_los)

    los_vector(i_los,0)=los_vector2(i_los,0)-los_vector1(i_los,0)
    los_vector(i_los,1)=los_vector2(i_los,1)-los_vector1(i_los,1)
    los_vector(i_los,2)= los_vector2(i_los,2)-los_vector1(i_los,2)
    los_vector(i_los,*)=los_vector(i_los,*)/sqrt(los_vector(i_los,0)^2+los_vector(i_los,1)^2+los_vector(i_los,2)^2)

    ;I also want to know the unit vector in the toroidal direction ...
    ;This is a unit vector from the origin to to the measurement position
    org2pos=los_vector1[i_los,0:1]/sqrt(los_vector1[i_los,0]^2.+los_vector1[i_los,1]^2.) ; discounting z

    dm=[-1.*org2pos[1],org2pos[0],0]
    dm=dm/sqrt(total(dm^2.))
    torvec[i_los,*]=dm
    ;Now CMR LOS should give similar directions for toroidal and los vectors
    ;And CER LOS should give opposite directions.

 endfor


field_line_vector1=fltarr(3)
field_line_vector2=fltarr(3)
field_line_vector=fltarr(3)
alter_shot = 0l

polvec=fltarr(n_elements(time_array),n_elements(los_struct.r_pos),3)
usevec=fltarr(n_elements(time_array),n_elements(los_struct.r_pos),3)

spawn,'date'
; ulp: put this outside  of the loop as it is already calculated for all
; times in time_array
rmag=interpol(surfaces.rmag,surfaces.time,time_array)
zmag=interpol(surfaces.zmag,surfaces.time,time_array)

for i_time = 0., n_elements(time_array)-1 do begin
    for i_los = 0, n_elements(los_struct.r_pos)-1 do begin
        trace_field_line_fast,surfaces,time_array(i_time),los_struct.r_pos(i_los),$
                        los_struct.z_pos(i_los),los_struct.phi_pos(i_los), $
                        5.,0.002,r,z,phi,b_abs=b_abs,/no_plot

        field_line_vector1(0)=r(0)*cos(phi(0)/180.*!Pi)
        field_line_vector1(1)=r(0)*sin(phi(0)/180.*!Pi)
        field_line_vector1(2)= z(0)

        field_line_vector2(0)=r(3)*cos(phi(3)/180.*!Pi)
        field_line_vector2(1)=r(3)*sin(phi(3)/180.*!Pi)
        field_line_vector2(2)= z(3)

        field_line_vector(0)=field_line_vector2(0)-field_line_vector1(0)
        field_line_vector(1)=field_line_vector2(1)-field_line_vector1(1)
        field_line_vector(2)=field_line_vector2(2)-field_line_vector1(2)
        field_line_vector(*)=field_line_vector(*)/sqrt(field_line_vector(0)^2+$
                             field_line_vector(1)^2+field_line_vector(2)^2)
        
        intersect_angles(i_time,i_los) = acos(abs(transpose(field_line_vector)#reform(los_vector(i_los,*))))/!Pi*180.
        b_absolute(i_time,i_los) = b_abs(2)
        if intersect_angles(i_time,i_los) lt 0.0 or intersect_angles(i_time,i_los) gt 90.0 then begin
           print,'There is a problem with the intersection angle!'
            print,'It should be smaller than 90deg and larger than 0deg. It is, however,'
            print,intersect_angles(i_time,i_los),'deg'

        endif

        ;Get the angle between the LOS and the Poloidal direction
        philos=los_struct.phi_pos[i_los]*!pi/180.
        rlos=los_struct.r_pos[i_los]    &   zlos=los_struct.z_pos[i_los]

        ;ulp 18032020 was changed so that the resulting rvec is time dependent
        rvec[i_time,i_los,*]=[rmag[i_time]*cos(philos)-rlos*cos(philos),$
                              rmag[i_time]*sin(philos)-rlos*sin(philos),$
                              zmag[i_time]-zlos]

        rvec[i_time,i_los,*]=rvec[i_time,i_los,*]/sqrt(total(rvec[i_time, i_los, *]^2))
        ;and I already know the toroidal vector.
        polvec[i_time,i_los,*]=crossp(rvec[i_time, i_los, *],reform(torvec[i_los,*]))


                                ;ulp if we also want to correct the
                                ;poloidal los for geometry effects and
                                ;haven't done so far then we
                                ;can use v_corr_geo instead of v_array
        if los_name[i_los] eq 'CPR' or los_name[i_los] eq 'CNR'  or los_name[i_los] eq 'CVH'  then begin
           
            v_corr_geo[i_time, i_los]=v_array[i_time, i_los]/abs(total(polvec[i_time,i_los,*]*los_vector[i_los,*]))
            
            usevec[i_time,i_los,*]=polvec[i_time,i_los,*]
        endif else begin
            v_corr_geo[i_time, i_los]=v_array[i_time, i_los]
           usevec[i_time,i_los,*]=torvec[i_los,*]
        endelse
        
        ;intersec_angles(i_time,i_los) = acos(abs(transpose(transpose(usevec[i_time, i_los, *]))#reform(los_vector(i_los,*))))/!Pi*180.
        ;print, intersect_angles(i_time,i_los), intersec_angles(i_time,i_los), intersect_angles(i_time, i_los)+intersec_angles(i_time,i_los)
       endfor

 endfor

;stop
spawn,'date'

proper_filename = ' '

;I have not calculated rotation corrections for these lines: RMM Feb 2020
if wvl gt 655.0 and wvl lt 657.0 then proper_filename= 'ti_corr_d23_dense.idlsave'
if wvl gt 447.0 and wvl lt 453.0 then proper_filename= 'ti_corr_li45_dense.idlsave'
if wvl gt 513.0 and wvl lt 519.0 then proper_filename= 'ti_corr_li57_dense.idlsave'
if wvl gt 603.0 and wvl lt 609.0 then proper_filename= 'ti_corr_o910_dense.idlsave'

if wvl gt 466.0 and wvl lt 471.0 then begin
    proper_filename= 'ti_corr_he34_dense.idlsave'
    ;rot_correct_file='vrot_corr_He43_dense.idlsave'
    corv=0
endif
if wvl gt 493.0 and wvl lt 498.0 then begin
    proper_filename= 'ti_corr_b67_dense.idlsave'
    rot_correct_file='vrot_corr_B76_dense.idlsave'
    corv=1
    lowti_ticorr = 'ti_corr_B76_lowTi.idlsave'
    lowti_rotcorr = 'vrot_corr_B76_lowTi.idlsave'
    low=1
endif
if wvl gt 527.0 and wvl lt 531.0 then begin
    proper_filename= 'ti_corr_c78_dense.idlsave'
    rot_correct_file='vrot_corr_C87_dense.idlsave'
    corv=1
    lowti_ticorr = 'ti_corr_C87_lowTi.idlsave'
    lowti_rotcorr = 'vrot_corr_C87_lowTi.idlsave'
    low=1    
endif
if wvl gt 563.0 and wvl lt 569.0 then begin
    proper_filename= 'ti_corr_n89_dense.idlsave'
    rot_correct_file='vrot_corr_N98_dense.idlsave'
    corv=1
    lowti_ticorr = 'ti_corr_N98_lowTi.idlsave'
    lowti_rotcorr = 'vrot_corr_N98_lowTi.idlsave'
    low=1
endif
 ; this is too close to the Argon 18+ line - adjust wavelength range RMM 20/09/19
if wvl gt 523.0 and wvl lt 526.0 then begin
    proper_filename= 'ti_corr_ne1011_dense.idlsave'
    rot_correct_file='vrot_corr_Ne1110_dense.idlsave'
    corv=1
    lowti_ticorr = 'ti_corr_Ne1110_lowTi.idlsave'
    lowti_rotcorr = 'vrot_corr_Ne1110_lowTi.idlsave'
    low=1
endif

CXwAr16=0.0
if wvl gt 539.0 and wvl lt 543.0 then begin
    proper_filename='ti_corr_ar15_1514_dense.idlsave';
    ; For Argon the Zeeman/FS corrections to the rotation are not negligible. RMM Jan2020
    rot_correct_file='vrot_corr_ar15_1514_dense.idlsave'
    ;These variables get restored and are identical to the Ti variables
    ;Variables: v_shift,T_I,B_ANGLE,B_T
    CORV=1.
    CXwAr16=1.
endif

if wvl gt 434.5 and wvl lt 438.5 then begin
    proper_filename='ti_corr_ar15_1413_dense.idlsave'
    rot_correct_file='vrot_corr_ar15_1413_dense.idlsave'
    CORV=1.
endif
if wvl gt 477.3 and wvl lt 481.3 then begin
    proper_filename='ti_corr_ar16_1514_dense.idlsave'
    rot_correct_file='vrot_corr_ar16_1514_dense.idlsave'
    corv=1
endif
if wvl gt 520.2 and wvl lt 523.0 then begin
    proper_filename='ti_corr_ar17_1615_dense.idlsave'
    rot_correct_file='vrot_corr_ar17_1615_dense.idlsave'
    corv=1
endif
if wvl gt 433.5 and wvl lt 434.45 then begin
    proper_filename='ti_corr_D52_lowTi.idlsave'
    lowti_ticorr = 'ti_corr_D52_lowTi.idlsave'
    print,'WARNING'
    print,'D gamma correction only valid for parallel views'
    corv=0
    low=1
    stop
endif
if proper_filename eq ' ' then begin
    t_i_corr_array=t_i_array
    t_i_err_corr_array=t_i_err
    goto,ende
endif

if low then begin
    restore,lowti_ticorr
    b_angle_low = b_angle
    t_i_low = t_i
    b_t_low = b_t
    t_i_app_all_low = t_i_app_all
    if corv then begin
        restore,lowti_rotcorr
        v_shift_low = v_shift*1000.0
        wdif_low=wvl-lambda0
        c=2.998d8
        vdif_low=(wdif_low/lambda0)*c
    endif
    print,'Specific tables for low Ti will be used'
endif

if corv then begin
    restore,rot_correct_file
    ti_vc=t_i
    ;Need to check if the wavelength used to fit the data matches the center of mass of the zeeman/fs fit.
    ;mostly they don't.
    c=2.998d8
    wdif=wvl-lambda0 ; This is the difference between the rest wavelength used in the fit and the true
                  ; True here being defined as the COM of the Zeeman/FS split distribution
    vdif=(wdif/lambda0)*c ; this is the velocity difference from using the wrong rest wavelength. This along LOS!

endif

print,proper_filename,' is used for Ti correction!'
restore,proper_filename
;stop
for i_time = 0., n_elements(time_array)-1 do begin

    for i_los = 0, n_elements(los_struct.r_pos)-1 do begin

        if finite(t_i_array(i_time,i_los)) then begin
            if t_i_array(i_time,i_los) le 1000.0 and low eq 1 then begin

                    index_bt = where(abs(b_t_low-b_absolute(i_time,i_los)) eq min(abs(b_t_low-b_absolute(i_time,i_los))))
                    index_bangle = where(abs(b_angle_low-intersect_angles(i_time,i_los)) eq $
                                         min(abs(b_angle_low-intersect_angles(i_time,i_los))))
;stop
                    index_ti_app = where(abs(t_i_app_all_low(index_bt[0],index_bangle[0],*)-t_i_array(i_time,i_los)) eq $
                                     min(abs(t_i_app_all_low(index_bt[0],index_bangle[0],*)-t_i_array(i_time,i_los))))
                    t_i_corr_array(i_time,i_los) = t_i_array(i_time,i_los)*t_i_low(index_ti_app[0])/$
                                                   t_i_app_all_low(index_bt[0],index_bangle[0],index_ti_app[0])

                    ;If corv set then correct the rotation - Jan 2020 RMM
                    if corv then begin

                        ;Find the near point in the vshift array
                        ZFS_VELSHIFT=interpol(v_shift_low[index_bt[0],index_bangle[0],*],t_i_low,t_i_corr_array[i_time,i_los])

                        ;Need the projection of this in the toroidal/poloidal direction
                        ZFS_VELSHIFT_TORPOL=ZFS_VELSHIFT/total(usevec[i_time,i_los,*]*los_vector[i_los,*])

                        ;Need the projection of vdif (correction for wrong rest wavelength)
                        ;into the toroidal/poloidal direction as well
                        VDIF_TORPOL=VDIF_low/total(usevec[i_time,i_los,*]*los_vector[i_los,*])

                        v_corrected(i_time,i_los) = v_array[i_time,i_los]+(ZFS_VELSHIFT_TORPOL-VDIF_TORPOL)
                    endif
            endif else begin
                ; 100% accurate but too slow - use with sparse set of idlsave-file 'proper_filename'
                ;t_i_app_arr_here = make_ti_app_arr_here(b_absolute(i_time,i_los),intersect_angles(i_time,i_los),$
                ;                                        b_angle,b_t,T_I_APP_ALL)
                ;t_i_corr_array(i_time,i_los) =  interpol(t_i,t_i_app_arr_here,abs(t_i_array(i_time,i_los)))

                ;faster implementation use only with dense set of idlsave-file 'proper_filename'
                    index_bt = where(abs(b_t-b_absolute(i_time,i_los)) eq min(abs(b_t-b_absolute(i_time,i_los))))
                    index_bangle = where(abs(b_angle-intersect_angles(i_time,i_los)) eq $
                                         min(abs(b_angle-intersect_angles(i_time,i_los))))
;stop
                    index_ti_app = where(abs(T_I_APP_ALL(*,index_bt[0],index_bangle[0])-t_i_array(i_time,i_los)) eq $
                                     min(abs(T_I_APP_ALL(*,index_bt[0],index_bangle[0])-t_i_array(i_time,i_los))))
                    t_i_corr_array(i_time,i_los) = t_i_array(i_time,i_los)*t_i(index_ti_app[0])/$
                                                   T_I_APP_ALL(index_ti_app[0],index_bt[0],index_bangle[0])

                    ;If corv set then correct the rotation - Jan 2020 RMM
                if corv then begin
                   
                    ;Find the near point in the vshift array
                    ZFS_VELSHIFT=interpol(v_shift[*,index_bt[0],index_bangle[0]],ti_vc,t_i_corr_array[i_time,i_los])

                    ;Need the projection of this in the toroidal/poloidal direction
                    ZFS_VELSHIFT_TORPOL=ZFS_VELSHIFT/total(usevec[i_time,i_los,*]*los_vector[i_los,*])

                    ;Need the projection of vdif (correction for wrong rest wavelength)
                    ;into the toroidal/poloidal direction as well
                    VDIF_TORPOL=VDIF/total(usevec[i_time,i_los,*]*los_vector[i_los,*])

                    v_corrected(i_time,i_los) = v_array[i_time,i_los]+(ZFS_VELSHIFT_TORPOL-VDIF_TORPOL)

                    if CXwAr16 then begin
                        ;If Ar then the "full" corrections we have applied are too big.
                        restore_Arbck_grids,Arout
                        reduce_Ar_corrections,Arout,T_i_array[i_time,i_los],B_absolute[i_time,i_los],$
                                              intersect_angles[i_time,i_los],BackRatio[i_time,i_los],$
                                              titrue,vshifttrue
                                              ;TIUPSHIFT, VUPSHIFT ; original code, not 2G Ar fit.

                        ;New Ti correction - 2G Ar fit
                        t_i_corr_array(i_time,i_los)=titrue;t_i_corr_array(i_time,i_los)+TIUPSHIFT

                        ;New rotation correction - 2G Ar fit
                        ZFS_VELSHIFT_TORPOL=VSHIFTTRUE/total(usevec[i_time,i_los,*]*los_vector[i_los,*])
                        v_corrected(i_time,i_los) = v_array[i_time,i_los]+(ZFS_VELSHIFT_TORPOL-VDIF_TORPOL)

                    endif
                endif
            endelse
        endif else begin
            t_i_corr_array(i_time,i_los) = 0.
            v_corrected(i_time,i_los)=0.0
        endelse

        if (finite(t_i_array(i_time,i_los)) and finite(t_i_err(i_time,i_los))) then begin
            if t_i_array(i_time,i_los) le 1000.0 and low eq 1 then begin

                index_ti_app_err = where(abs(t_i_app_all_low(index_bt[0],index_bangle[0],*)-t_i_array(i_time,i_los)-t_i_err(i_time,i_los)) eq $
                                     min(abs(t_i_app_all_low(index_bt[0],index_bangle[0],*)-t_i_array(i_time,i_los)-t_i_err(i_time,i_los))))
                t_i_corr_array_err_tmp= (abs(t_i_array(i_time,i_los))+ $
                                         abs(T_I_err(i_time,i_los))) * $
                                             t_i_low(index_ti_app_err[0])/t_i_app_all_low(index_bt[0],index_bangle[0],index_ti_app_err[0])
                t_i_err_corr_array(i_time,i_los) = abs(t_i_corr_array(i_time,i_los)-t_i_corr_array_err_tmp)

                ;If corv then correct the rotation - Jan 2020 RMM
                if corv then begin
                    ;Find the near point in the vshift array
                    ZFS_VELSHIFT_ERR=interpol(v_shift_low[index_bt[0],index_bangle[0],*], t_i_low , T_i_err_corr_array[i_time,i_los])
                    ZFS_VELSHIFT_ERR_TORPOL= ZFS_VELSHIFT_ERR/total(usevec[i_time,i_los,*]*los_vector[i_los,*])
                    v_err_corrected(i_time,i_los) =v_err[i_time,i_los]
                endif
            endif else begin
                ;100% accurate but too slow - use with sparse set of idlsave-file 'proper_filename'
                ;t_i_err_corr_array(i_time,i_los) = abs(t_i_corr_array(i_time,i_los)-$
                ;                                   interpol(t_i,t_i_app_arr_here,abs(t_i_array(i_time,i_los))$
                ;                                   +abs(t_i_err(i_time,i_los))))

                ;faster implementation use only with dense set of idlsave-file 'proper_filename'
                index_ti_app_err = where(abs(T_I_APP_ALL(*,index_bt[0],index_bangle[0])-t_i_array(i_time,i_los)-t_i_err(i_time,i_los)) eq $
                                     min(abs(T_I_APP_ALL(*,index_bt[0],index_bangle[0])-t_i_array(i_time,i_los)-t_i_err(i_time,i_los))))
                t_i_corr_array_err_tmp= (abs(t_i_array(i_time,i_los))+ $
                                         abs(T_I_err(i_time,i_los))) * $
                                             T_I(index_ti_app_err[0])/T_I_APP_ALL(index_ti_app_err[0],index_bt[0],index_bangle[0])
                t_i_err_corr_array(i_time,i_los) = abs(t_i_corr_array(i_time,i_los)-t_i_corr_array_err_tmp)

                ;If corv then correct the rotation - Jan 2020 RMM
                if corv then begin
                   
                    ;Find the near point in the vshift array
                    ZFS_VELSHIFT_ERR=interpol(v_shift[*,index_bt[0],index_bangle[0]], Ti_vc , T_i_err_corr_array[i_time,i_los])
                    ZFS_VELSHIFT_ERR_TORPOL= ZFS_VELSHIFT_ERR/total(usevec[i_time,i_los,*]*los_vector[i_los,*])
                    v_err_corrected(i_time,i_los) =v_err[i_time,i_los]
                endif
            endelse
        endif else begin
            t_i_err_corr_array(i_time,i_los) = 0.
            v_err_corrected[i_time,i_los]=0.
        endelse

    endfor

endfor


if ~corv then begin             ; If no correction then the corrected equals the non corrected
   ;stop
    v_corrected=v_corr_geo
    v_err_corrected=v_err
    corv=1
endif


ende:


end
