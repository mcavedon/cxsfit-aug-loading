pro cxf_external_shotfile_e,event
 
  widget_control,event.top,get_uvalue=info

  if event.id eq info.cancel_button then begin
    widget_control,event.top,/destroy
    return
  end

  if event.id eq info.ok_button then begin
    widget_control,info.shotfile_widget,get_value=shotfile
    widget_control,/hourglass
    cxf_load_aug_fit,shotfile.experiment, $
                     shotfile.shot, $
                     shotfile.diagnostic, $
                     shotfile.edition, $
                     indata, err
;    cxf_load_aug_fit,'allanw',17148L,'CEZ',0l,indata,err
    widget_control,hourglass=0
    if err ne '' then begin
      a=dialog_message(err,/error)
    endif else if indata.lineinfo.cxline eq -1 then begin
      a=dialog_message('No primary CX line specifed in this shot file!', $
                       /error)
    endif else begin
      shotfile.shot_found    = indata.shot_f
      shotfile.edition_found = indata.edition_f
      widget_control,info.topinfo_base,get_uvalue=topinfo
      if not ptr_valid(topinfo.externaldata) then begin
        externaldata={ti_src:ptr_new(),af_src:ptr_new(),ti_data:ptr_new(),af_data:ptr_new()}
      endif else begin
        externaldata=*topinfo.externaldata
      endelse

      if info.ti_or_af eq 0 then begin
        if ptr_valid(externaldata.ti_data) then ptr_free,externaldata.ti_data
        if ptr_valid(externaldata.ti_src) then ptr_free,externaldata.ti_src

        ti_data={t:indata.time,r:indata.losinfo.r_pos,d:indata.ti.data}			
        ti_src=shotfile

        externaldata.ti_data=ptr_new(ti_data)
        externaldata.ti_src=ptr_new(ti_src)
      endif else begin
        if ptr_valid(externaldata.af_data) then ptr_free,externaldata.af_data
        if ptr_valid(externaldata.af_src) then ptr_free,externaldata.af_src

        af_data={t:indata.time,r:indata.losinfo.r_pos,d:indata.vrot.data}			
        af_src=shotfile

        externaldata.af_data=ptr_new(af_data)
        externaldata.af_src=ptr_new(af_src)
      endelse

      ptr_free,topinfo.externaldata
      topinfo.externaldata=ptr_new(externaldata)
      widget_control,info.topinfo_base,set_uvalue=topinfo
      widget_control,event.top,/destroy

    endelse
		
  endif
end
