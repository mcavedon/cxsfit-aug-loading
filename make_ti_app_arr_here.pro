function make_ti_app_arr_here,b_abs_here,b_angle_here,b_angle,b_t,T_I_APP


t_i_app_here=dblarr(n_elements(T_I_APP[*,0,0]))



for i_ti = 0, n_elements(T_I_APP(*,0,0))-1 do begin

        t_i_app_here(i_ti) = bilin_interpol(T_I_APP(i_ti,*,*),b_t,b_angle,b_abs_here, b_angle_here )

endfor


return,t_i_app_here


end
