; Routine written by A. Kappatou

function check_sfh,shot,exp,diag,ed,verb=verb

if n_params() eq 0 then begin
    print,'USAGE: check_sfh,shot,exp,diag,ed,verb=verb'
    goto,theend
endif

ier        = 0L
shot       = long(shot)
ed         = long(ed)
dia_ref    = 0L
datum      = string(' ',format='(a18)')
dim        = 0L
phys_unit  = 0L
err_string = string(' ',format='(a80)')

exists=1 ; it exists
; OPEN SHOTFILE
s = call_external(!libddww, 'ddgetaug', 'ddopen', $
                 ier, exp, diag, shot, ed, dia_ref, datum)
if (ier gt 0) then begin
   s = call_external(!libddww,'ddgetaug','xxerrprt',     $
                     -1L,err_string,ier,3L,'ddopen:')
   exists = 0 ; it doesn't exist
   GOTO, FINISH
endif

FINISH:
; CLOSE SHOTFILE
s = call_external(!libddww,'ddgetaug','ddclose', $
                  ier,dia_ref)
if (ier gt 0 and STRTRIM(err_string,2) EQ '') then begin
   s = call_external(!libddww,'ddgetaug','xxerrprt', $
                     -1L,err_string,ier,3L,'ddclose:')
endif

theend:
if keyword_set(verb) then begin
    print,''
    print,'  '+strtrim(shot,2)+' '+exp+' '+diag+' '+strtrim(ed,2)
    if exists eq 1 then print,'  Exists.' else print,'  Does not exist.'
 endif
return, exists
end 
