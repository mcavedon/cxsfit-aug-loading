
; Get input specifying an AUG shot file via a widget interface

FUNCTION cxf_get_aug_shotfile_cw, top, shotfile=shotfile

;
; ldh 22.03.06 - first version
;
;  shotfile   : I/O : structure containing the requested shot file
;                     definition on successful completion.  At input
;                     the tags are used for initial widget settings
;       .experiment : experiment name of shot file [e.g.: 'AUGD']
;       .shot       : shot number of shot file [e.g.: 17148L]
;       .shot_found : shot number returned when opening shot file.
;                     Not used here but included for compatibility
;                     with using 0 as most recent shot number.
;       .diagnostic : shot file diagnostic name [e.g.: 'CEZ']
;       .edition    : edition number of shot file [e.g.: 0L for most recent]
;       .edition_found : edition number returned when opening shot file
;                     Not used here but included for compatibility
;                     with using 0 as most recent edition.

; check to see if the shotfile structure is already defined  
  IF SIZE(shotfile,/TNAME) NE 'STRUCT' THEN $
    shotfile = {type          : 'SF',       $
                experiment    : 'AUGD',     $
                diagnostic    : '',         $
                shot          : 0L,         $
                shot_found    : 0L,         $
                edition       : 0L,         $
                edition_found : 0L          }

; build compound widget

  tlb    = Widget_Base(top, Column=2, /Grid_Layout, /Frame)

  cexp   = shotfile.experiment
  cdiag  = shotfile.diagnostic
  cshot  = STRING(shotfile.shot_found,   Format='(i6)')
  cedn   = STRING(shotfile.edition_found,Format='(i6)')

  ExpID  = CW_Field(tlb, Value=cexp,  Title='Experiment ', XSize=6)
  DiagID = CW_Field(tlb, Value=cdiag, Title='Diagnostic ', XSize=6)
  ShotID = CW_Field(tlb, Value=cshot, Title='Shot       ', XSize=6)
  EdnID  = CW_Field(tlb, Value=cedn,  Title='Edition    ', XSize=6)

; Setup structure for passing to event handler
  info = {tlb:tlb, $
          ExpID:ExpID, DiagID:DiagID, $
          ShotID:ShotID, EdnID:EdnID  }
  Widget_Control, tlb, set_uvalue=info

  Widget_Control, tlb, event_pro='cxf_get_aug_shotfile_ce', $
                       func_get_value='cxf_get_aug_shotfile_gv'

  RETURN, tlb

END
