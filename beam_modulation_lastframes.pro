; AKAPPATOU 
; This is a modification of Rachael's beam modulation code.
; Differences:
; -- only the last frames of the beam-on phase are used and the subsequent 
; off-frames for subtraction, as the first frames after the switch on of Q3 
; are almost never reliable.
; -- there is an option to define the delta_t of on-frames to be used
; -- there is an option to average the off-frames to be subtracted
;**********************************************************************************


;RMM - July 2009

;This code will take the spectrometer_data array created in
;cxf_load_aug_data.pro (datain) as well as the shotfile information structure from load_c(e/h)r_pi (diagdata) and create a new structure to be given to
;cxsfit.  The data in this structure will be limited to "beam-on"
;times and will have the passive portion of the spectra pre-subtracted
;from it. This code is general enough to handle continuous beam
;modulation, beam blips, and "beam dips" (steady state beam operations
;with short "off" periods to enable a measure of the passive
;background spectra.)
         
;INPUT INFORMATION
;datain = structure containing spectrometer_Data created for cxsfit
;diagdata -structure contianing shotfile information from a load_cer_pi routine or similar
;whtrack - track information for the case where a row is not being used see cxf_load_aug_data.pro line 60-70ish
;waveL - calculated wavelength interval (assumed continuous pixels) from cxf_load_aug_data.pro (line 112 for example)

;**********************************************************************************
     
PRO beam_modulation_lastframes, datain, diagdata, whtrack,waveL,dataout
shot=diagdata.header.shot
;raw CXRS data
    data=datain.intensity.data
    dataraw=diagdata.data.intens
    dataerror=datain.intensity.error
    
;Diagnostic information needed for error analysis
readout_noise=diagdata.header.noise.r_noise
photoncount=diagdata.header.noise.ph_count
background=diagdata.header.background
brightcal=diagdata.header.cal_mW
expose_t=diagdata.header.exptime
dispersion=diagdata.header.dispersion

if shot ge 25000 then intenserror=diagdata.data.intenserr ; error in photons for each frame

;now that teh CHR is in the same format - this code should be more unified

  ; speed of light
  c = 2.9979D8      ; [m/s]

; Planck constant   ; [J s]
  h = 6.6261D-34
          
;The time base for the CXRS diagnostic - This time represents the center of the frame.
    cxrstime=datain.time
    ntm=n_elements(cxrstime)     
    
;Flags for neutral beam power.  0 - beam is off, 1 beam is on
;These flags correspond to an average over the CXRS exposure time.
    beam=datain.beam
    
;Get an array of times when the beam is off
    offtimes=cxrstime(where(beam eq 0))

;define variables to store frame information in
    Goodframes=fltarr(ntm)
    
;this should only have values for goodframe=3 data - i.e. data that is in the middle of a pulse and not on the edge. 
    Mframe_backtime=fltarr(ntm) ; which frame to use as a background for 'middle frames'
    
;Iterate through number of frames (not including first and last)
;to find frames for which there is a useable background 
    for i=2, ntm-2 do begin  
        ;First frames in a beam on period
        if (beam[i] eq 1 and beam[i-1] eq 0) then begin
            Goodframes[i]=1
        endif
            
        ;Last frame in a beam on period 
        ;These two definitions should work for beam pulses as well as beam dips
        if(beam[i] eq 1 and beam[i+1] eq 0) then begin
            Goodframes[i]=4
        endif
      
        ;Frames when the beam is on - but not on a leading or trailing edge.
        ;In this case - we only want to consider frames that are within a short
        ;time of a background frame.  say 100ms - completely arbitrary choice
        delta=(min(abs(cxrstime[i]-offtimes))*1000.)
    
        delta_on = 40. ;ms ; how many frames from the blip-end should be considered
        nbefore=ceil(delta/(expose_t*1000.))
        if (beam[i-1] eq 1 and beam[i] eq 1 and beam[i+1] eq 1 and delta le delta_on) then begin
            ;if beam[i] eq 1 and beam[i-2] eq 0 then begin
            if beam[i-nbefore] eq 0 and beam[i] eq 1 then begin 
                Goodframes[i]=2 
            endif else begin 
                Goodframes[i]=3
            endelse
            ;location of the nearest background frame - in the offtime base --> we want the "after" frame
            minL=where(abs(cxrstime[i]-offtimes)*1000. eq delta,npts)
            ;use before or after time
            if npts eq 1 then Mframe_backtime[i]=offtimes[minL[0]]
            ;save both before and after time
            if npts eq 2 then stop ;Mframe_backtime[i,*]=offtimes[minL]
        endif       
        
    endfor    ;finish iterating through frames
 
 window,1
 plot,goodframes,psym=-6
 stop
;Array of indicies for usable frames
    usefrm=where(goodframes eq 3,Nframes) ; only "last" frames without the very last
;If the diagnostic is CER then for beam 3 the first two frames in the beam pulse are NOT useable - they are crap!
;shitty ass POS beam
;    if diag eq 'CER'  then usefrm=where(goodframes gt 2,Nframes)
;-RMM - took this out for 2010-2011 campaign because timing/signal got so much better

;*temporary line dealing with 4ms L-mode beam blips - eliminate late frame in the blip
;    if diag eq 'CER'  then usefrm=where(goodframes ge 2 and goodframes le 4,Nframes)

;stop
;Time base for the usable frames
    Time=cxrstime(usefrm)

;New data array to temporarily store the background subtracted data
    dmdata=datain.intensity.data
    dmdata=dmdata*0.0
    
    dmerror=datain.intensity.error

;Iterate through all of the tracks (rows on the camera)
    ntrack=datain.ntrack
    
    delta_off=40. ; set delta_off=expose_t for 1 frame
    n_off=floor(delta_off/(expose_t*1000.))
    print,n_off
    stop
    
    for T=0, ntrack-1 do begin
        TT=whtrack[T]

;For last frames use the frame after as the background
;Indicies on the cxrs time base for the "last frames"
    lastframes = where(goodframes eq 4, nLframes)
        for LN=0, nLframes -1 do begin
            L=lastframes[LN]
            ;Do the background subtraction
            dmdata[*,T,L]=data[*,T,L]-avg(data[*,T,L+1:L+n_off],2)   
            ;Because of noise - this can lead to negative brightnesses on some pixels
            ;Not sure if this is a problem in CXSFIT or not. Adding an offset.
            dmdata[*,T,L]=dmdata[*,T,L]+5e16;0.25*max(data[*,T,L])
            
            ;get the error on this data
            
            dmerror[*,T,L] = sqrt(intenserror[*,tt,L]^2+avg(intenserror[*,tt,L+1:L+n_off],2)^2.)

       endfor ; end iteration through last frames
        
;For middle frames need to be a bit more careful how we do this. 
;Indicies on the cxrs time base for the "middle frames"    
    middleframes=where(goodframes eq 2 or goodframes eq 3,nMframes)
    ;iterate through these frames
        for MN=0,nMframes-1 do begin
            M=middleframes[MN]
            ;create a background frame from average of two time frames
            FRM1=where(cxrstime eq mframe_backtime[M]) ; this is the index of the backfrm1
          ;  FRM2=where(cxrstime eq mframe_backtime[M,1]) ; this is the index of the backfrm2
            
          ;  B1=reform(data[*,T,frm1])
          ;  B2=reform(data[*,T,frm2])
          ;  dmbackgrnd=(b1+b2)/2.
            
          ;  ;are the two backgrounds the same?
          ;  AREQ=array_equal(b1,b2)
            
            ;Do the background subtraction
            dmdata[*,T,M]=data[*,T,M]-avg(data[*,T,frm1:frm1+n_off-1],2)
            ;Because of noise - this can lead to negative brightnesses on some pixels
            ;Not sure if this is a problem in CXSFIT or not. Adding an offset.
            dmdata[*,T,M]=dmdata[*,T,M]+5e16;0.25*max(data[*,T,M])
            
           dmerror[*,T,M] = sqrt(intenserror[*,tt,M]^2+avg(intenserror[*,tt,frm1:frm1+n_off-1],2)^2.)

        endfor
    endfor ; end iteration through tracks
    
    ;Most irritatingly I need to completely redefine the entire structure at the moment.
    ;In the future the original structure will be defined with pointers so that the values 
    ;can be changed - but first I need to find the rest of the code so I can propagate the 
    ;the consequences of that change forward consistently.  Of course - making the changes
    ;and seeing where the code breaks is another way to go =) - But, some other day.
    
    cxf_define_spectrometer_data,dataout,err
        dataout.machine.name =  datain.machine.name
        dataout.shot_nr = datain.shot_nr
        dataout.spectrometer.name = datain.spectrometer.name
        dataout.npixel = datain.npixel
        dataout.ntrack = datain.ntrack
        dataout.nframe = nframes
        dataout.ninst  = datain.ninst

        cxf_define_spectrometer_data,dataout,err
        IF err NE '' THEN RETURN
        dataout.LOS_name = datain.LOS_name
        dataout.R_pos    = datain.R_pos
        dataout.z_pos    = datain.z_pos
        dataout.phi_pos  = datain.phi_pos
        ; additional location data
        dataout = create_struct(dataout,'R_pos_time',datain.R_pos_time[*,usefrm],'z_pos_time',datain.z_pos_time[*,usefrm],'phi_pos_time',datain.phi_pos_time[*,usefrm],'dR_pos_time',datain.dR_pos_time[*,usefrm],'dz_pos_time',datain.dz_pos_time[*,usefrm],'dphi_pos_time',datain.dphi_pos_time[*,usefrm],'externalcostheta',datain.externalcostheta[*,usefrm])

        dataout.R_orig   = datain.R_orig
        dataout.z_orig   = datain.z_orig
        dataout.phi_orig = datain.phi_orig
        dataout.wlength  = datain.wlength
        dataout.slit     = datain.slit

        dataout.time = time
        dataout.exposure = datain.exposure[usefrm]

        dataout.wavelength.data      = datain.wavelength.data
        dataout.wavelength.error     = datain.wavelength.error
        dataout.wavelength.reference = datain.wavelength.reference

        dataout.dispersion.data      = datain.dispersion.data
        dataout.dispersion.error     = datain.dispersion.error
        dataout.dispersion.reference = datain.dispersion.reference
        
        dataout.intensity.validity = datain.intensity.validity[*,*,usefrm]
        dataout.intensity.reference = datain.intensity.reference

        dataout.instfu.y0 = datain.instfu.y0
        dataout.instfu.xw = datain.instfu.xw
        dataout.instfu.xs = datain.instfu.xs
        dataout.instfu.reference = datain.instfu.reference

        dataout.beam = datain.beam[usefrm]
        dataout.intensity.data=dmdata[*,*,usefrm]
        dataout.intensity.error=dmerror[*,*,usefrm]
           
    ;stop
return
end
