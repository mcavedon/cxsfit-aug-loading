;RMM - August 2012 - pixel by pixel interpolation of the background before and after a beam blip

;This code will take the spectrometer_data array created in
;cxf_load_aug_data.pro (datain) as well as the shotfile information structure from load_c(e/h)r_pi (diagdata) and create a new structure to be given to
         
;INPUT INFORMATION
;datain = structure containing spectrometer_Data created for cxsfit
;diagdata -structure contianing shotfile information from a load_cer_pi routine or similar
;whtrack - track information for the case where a row is not being used see cxf_load_aug_data.pro line 60-70ish

;**********************************************************************************

PRO beam_blips_cer_addup, datain,dataout
    shot=datain.shot_nr
    if shot lt 25000 then begin
        print,'Background subtraction method only for data after 25000'
        return
    endif

;raw CXRS data
    data=datain.intensity.data ; fltarr(pixel,track,frame)
    ntrack=n_elements(data[0,*,0])
    dataerror=datain.intensity.error
    cxrstime=datain.time
    npix=n_elements(data[*,0,0])
    ntm=n_elements(cxrstime)
    
;Flags for neutral beam power.  0 - beam is off, 1 beam is on
;These flags correspond to an average over the CXRS exposure time.
    beam=datain.beam
    
    if shot eq 37454 or shot eq 37214 or shot eq 37488 then begin
        ;I am lying to cxsfit about when the beam is on so I can have two time points. 
        ;and then I can write a shotfile - yay. for Klara H.
        dmblipind=where(cxrstime ge 4.05 and cxrstime le 4.05+.02)
        beam[dmblipind]=1.0    
    endif
    
    
    ;For shots with only one beam blip - I have a problem because the time base
    ;is only size 1 - and cxsfit doesn't like this
    ;in that case - add up some fake data!
    
    sfr=cxrstime*0.0
    efr=cxrstime*0.0
    for i=2,ntm-3 do if beam[i] eq 1 and beam[i-1] eq 0 then sfr[i]=1
    for i=3,ntm-3 do if beam[i] eq 1 and beam[i+1] eq 0 then efr[i]=1    
    
    sframes=where(sfr eq 1,nblips1)
    eframes=where(efr eq 1,nblips2)

    
    if nblips2-nblips1 eq 0 then nblips=nblips1 else stop
        
    ;distringuish between a beam turning on and staying on for a while and a blip. 
    bliplen=cxrstime[eframes]-cxrstime[sframes]

    ;Anything longer than 200 ms I'm saying is NOT a blip
    shortblip=where(bliplen lt 0.200,nblips)
    sframes=sframes[shortblip]
    eframes=eframes[shortblip]
 
    ;Also let's only accept a blip if there are at least two frames in between
    ;so that a decent passive can be found
    if nblips ge 2 then begin
        pfs=sframes[1:nblips-1]-eframes[0:nblips-2]
        pfs=[3,pfs,3]
        gdpas=pfs*0.0   
        for i=0,nblips-1 do if pfs[i] ge 3 and pfs[i+1] ge 3 then gdpas[i]=1
        gdpasi=where(gdpas[0:nblips-1] eq 1,nblips)
        sframes=sframes[gdpasi]
        eframes=eframes[gdpasi]
    
    endif
    

    ;dmonindex
    onindex=fix((sframes+eframes)/2.)

    ;arrays for data
    datanew=fltarr(npix,ntrack,nblips)
    dataerr=fltarr(npix,ntrack,nblips)

    device,decomposed=0
    loadct,39
    !P.color=0
    !P.background=255
    
;iterate through the on points
    
    wavelength=datain.wavelength.data ; 512,24
   
    for i=0, nblips -1 do begin
        
        ;find the indicies of the nearest frames before and after
        ;Actually, scratch that - skip one because this frame might still
        ;have some active signal in it
        bframe=sframes[i]-2
        aframe=eframes[i]+2
        
        ;times of the before and after before and after frames
        btime=cxrstime[bframe]
        atime=cxrstime[aframe]
        
        ;Sometimes after frames no good
        ;aframe=bframe
        ;atime=btime
        
        ;now get the data
        ;iterate through number of tracks
        for t=0,ntrack-1 do begin       
            
            bframes=[bframe-2,bframe-1,bframe,aframe,aframe+1,aframe+2]
            bdata=reform(data[*,t,bframes])
            b_err=reform(dataerror[*,t,bframes])
            
            back=total(bdata,2)/n_elements(bframes)
            berr=sqrt(total(b_err,2)^2.)/n_elements(bframes)
                                    
            nfrm_in=eframes[i]-sframes[i]+1
            
            if nfrm_in eq 1 then begin
                dmdata=data[*,t,sframes[i]]
                dmdataerr=dataerror[*,t,sframes[i]]           
            endif else begin
                dmdata=total(data[*,t,sframes[i]:eframes[i]],3)/nfrm_in
                dmdataerr=sqrt(total(dataerror[*,t,sframes[i]:eframes[i]]^2.,3))/nfrm_in     
            endelse
            
            ;propagation of errors ... berr, dmdataerr          
            newerr=   sqrt((berr/1e16)^2.+(dmdataerr/1e16)^2.)        
            newerr=newerr*1e16
                     
;            !p.multi=[0,1,3]
;            plot,bdata[*,0],xran=[200,300]
;            for i=0,n_elements(bframes)-1 do oplot,bdata[*,i]
;            oplot,back,col=50.
;            errplot,back-berr,back+berr,col=50
           
;;           ; plot,data[*,t,sframes[i]],xran=[200,300]
;            oplot,data[*,t,eframes[i]-1]            
;            oplot,data[*,t,eframes[i]]
;            oplot,dmdata,col=250
;            errplot,dmdata-dmdataerr,dmdata+dmdataerr,col=250
;            oplot,back,col=50.
;            errplot,back-berr,back+berr,col=50 
                       
;            plot,dmdata-back,col=150,thi=2,xran=[200,300]
;            errplot,dmdata-back-newerr,dmdata-back+newerr,col=150
                        
            
            datanew[*,t,i]=dmdata-back
            dataerr[*,t,i]=newerr
            
            ;wait,.1          
        endfor
    endfor

    cxf_define_spectrometer_data,dataout,err
        dataout.machine.name =  datain.machine.name
        dataout.shot_nr = datain.shot_nr
        dataout.spectrometer.name = datain.spectrometer.name
        dataout.npixel = datain.npixel
        dataout.ntrack = datain.ntrack
        dataout.nframe = nblips
        dataout.ninst  = datain.ninst

        cxf_define_spectrometer_data,dataout,err
        
        IF err NE '' THEN RETURN
        dataout.LOS_name = datain.LOS_name
        dataout.R_pos    = datain.R_pos
        dataout.z_pos    = datain.z_pos
        dataout.phi_pos  = datain.phi_pos
        dataout.R_orig   = datain.R_orig
        dataout.z_orig   = datain.z_orig
        dataout.phi_orig = datain.phi_orig
        dataout.wlength  = datain.wlength
        dataout.slit     = datain.slit

         dataout = create_struct(dataout,'R_pos_time',datain.R_pos_time[*,onindex],$
                                         'z_pos_time',datain.z_pos_time[*,onindex],$
                                         'phi_pos_time',datain.phi_pos_time[*,onindex],$
                                         'dR_pos_time',datain.dR_pos_time[*,onindex],$
                                         'dz_pos_time',datain.dz_pos_time[*,onindex],$
                                         'dphi_pos_time',datain.dphi_pos_time[*,onindex],$
                                         'externalcostheta',datain.externalcostheta[*,onindex])

        dataout.time = cxrstime[onindex]
        dataout.exposure = datain.exposure[onindex]

        dataout.wavelength.data      = datain.wavelength.data
        dataout.wavelength.error     = datain.wavelength.error
        dataout.wavelength.reference = datain.wavelength.reference

        dataout.dispersion.data      = datain.dispersion.data
        dataout.dispersion.error     = datain.dispersion.error
        dataout.dispersion.reference = datain.dispersion.reference
        
        dataout.intensity.validity = datain.intensity.validity[*,*,onindex]
        dataout.intensity.reference = datain.intensity.reference

        dataout.instfu.y0 = datain.instfu.y0
        dataout.instfu.xw = datain.instfu.xw
        dataout.instfu.xs = datain.instfu.xs
        dataout.instfu.reference = datain.instfu.reference

        dataout.beam = datain.beam[onindex]
        dataout.intensity.data=datanew
        dataout.intensity.error=dataerr
           
return
end

