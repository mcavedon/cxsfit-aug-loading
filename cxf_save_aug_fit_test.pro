
; Save fit results to an AUG shot file

PRO cxf_save_aug_fit_test, experiment, comment, specdata, fitparam, extdata, outdata, $
                      history, err

  debug = 1 

  c = 2.9979E8                  ; speed of light

  err = ''
  ID  = 'CXF_SAVE_AUG_FIT: '

  IF outdata.machine NE 'AUG' THEN BEGIN
    err = ID+'Please save only AUG data to shotfiles!'
    RETURN
  ENDIF

; Warn the user if there is no cxline set
  IF (*fitparam.fparam).cxline+1 LT 1 THEN BEGIN
    text = ['This fit does not contain a primary CX line.', $
            'Corresponding signals will not be written.', $
            'Do you wish to proceed?']
    title = 'AUG Shot File Query'
    ans =  DIALOG_MESSAGE(text, title=title, /Question, /Center)
    IF ans EQ 'No' THEN RETURN
  ENDIF

; Set maximum sizes - these must match definitions in
; the shot file header!!!

  ntrack = outdata.ntrack
  nframe = outdata.nframe
  nframe_baseline = specdata.baseline.nframe
  nline = outdata.nlines
  ncomm = n_elements(comment)
  nhistory = N_ELEMENTS(history)
  nparams = 0
  FOR k=0L, nhistory-1 DO BEGIN
    cxf_write_switches, switches, *history[k], /StrArray, err
    tmpparams = (SIZE(switches))[1]
    IF tmpparams GT nparams THEN BEGIN
        nparams = tmpparams
    ENDIF
  ENDFOR

  CASE strupcase(outdata.spectrometer) OF
    'CER' : diagnostic = 'CEZ'
    'CHR' : diagnostic = 'CHZ'
    'CMR' : diagnostic = 'CMZ'
    'CPR' : diagnostic = 'CPZ'    
    'CNR' : diagnostic = 'CNZ'    
    'LIA' : diagnostic = 'LIT'
    'CXH' : diagnostic = 'CXZ'
    'CVH' : diagnostic = 'CVZ'
    'COR' : diagnostic = 'COZ'
    'BOS' : diagnostic = 'BOZ'
    'CUR' : diagnostic = 'CUZ'
    'CAR' : diagnostic = 'CAZ'
    'CBR' : diagnostic = 'CBZ'
    'CCR' : diagnostic = 'CCZ'
    ELSE : BEGIN
      err = ID+'Unrecognised spectrometer name!'
      RETURN
    END
  ENDCASE

; Check that link to shot file software library is defined
  ;defsysv, '!libddww', exist=exist
  ;if not exist then $
  ;  defsysv, '!libddww', '/usr/ads/lib/libddww.so.6.1',1
  set32_64; - RMM changed on April 1 2014


; Move to directory containing the shotfile header
  wkdir = (routine_info('CXF_SAVE_AUG_FIT_TEST',/source)).path
  wkdir = strmid(wkdir,0,rstrpos(wkdir,'/')) 
  PUSHD, wkdir

; mcavedon 16 April 2017
; Change dimensions of the shotfile header accordingly to actual ntracks and ntimes
  ; to avoid problems with sy,bolic links
  sfhdir = '/afs/ipp/u/cxrs/idl/cxsfit64/machines/aug'
  change_sfh, sfhdir+'/'+diagnostic+'00000.sfh', nframe, ntrack, nline, nframe_baseline,$
      ncomm, nhistory, nparams, maxlines, maxtrack


; Make a directory if necessary
if (file_info("~/shotfiles")).exists eq 0 then begin
	spawn,'mkdir ~/shotfiles'
endif

if (file_info("~/shotfiles/"+diagnostic)).exists eq 0 then begin
	spawn,'mkdir ~/shotfiles/'+diagnostic
endif

; Check which times contain at least one valid fit
; fit_status: <0 failed fit
;              0 no fit attempted
;              1 good fit available
;             >1 available fit may not be converged
; ==> valid fit where fit_status GE 1

; for edge system sort out 'bad' fits by criterium
; for edge system sort out 'bad' fits by criterium
; keep case for CMR 2007-2010 as well as for passive analysis ELV 2009
if outdata.shot_nr lt 25900 then begin
    if diagnostic eq 'CMZ' then begin
        act_pass_fact=0.0
        if outdata.mass(0) gt 3 and outdata.mass(0) lt 5 then act_pass_fact=0.50
        if outdata.mass(0) gt 9 and outdata.mass(0) lt 11 then act_pass_fact=0.3
        if outdata.mass(0) gt 11 and outdata.mass(0) lt 13 then act_pass_fact=1.0
        if outdata.mass(0) gt 13 and outdata.mass(0) lt 15 then act_pass_fact=0.0
        if outdata.mass(0) gt 19 and outdata.mass(0) lt 21 then act_pass_fact=0.0
        if outdata.mass(0) gt 38 and outdata.mass(0) lt 41 then act_pass_fact=0.0
        if act_pass_fact ne 0.0 and outdata.nlines ne 1 and strupcase(experiment) ne 'ELV' and strupcase(experiment) ne 'MCAVEDON' then begin     
            for i_chan = 0., outdata.ntrack-1 do begin
                for i_frame = 0, outdata.nframe-1 do begin
                    if outdata.temp.error(i_frame,0,i_chan) gt 120. or    $
                        outdata.intensity.data(i_frame,0,i_chan)/outdata.intensity.data(i_frame,1,i_chan) lt act_pass_fact then begin
                        outdata.fit_status(i_frame,i_chan)=-1.
                        outdata.intensity.data(i_frame,*,i_chan)=!values.f_nan
                        outdata.intensity.error(i_frame,*,i_chan)=!values.f_nan
                        outdata.temp.data(i_frame,*,i_chan)=!values.f_nan
                        outdata.temp.error(i_frame,*,i_chan)=!values.f_nan
                        outdata.vrot.data(i_frame,*,i_chan)=!values.f_nan
                        outdata.vrot.error(i_frame,*,i_chan)=!values.f_nan
                        outdata.wavelength.data(i_frame,*,i_chan)=!values.f_nan
                        outdata.wavelength.error(i_frame,*,i_chan)=!values.f_nan
                        outdata.baseline.data(i_frame,i_chan)=!values.f_nan
                        outdata.baseline.error(i_frame,i_chan)=!values.f_nan
                    endif
                endfor
            endfor
        endif else begin
            for i_chan = 0., outdata.ntrack-1 do begin

                if (outdata.mass(0) gt 13 and outdata.mass(0) lt 15 or outdata.mass(0) gt 38 and outdata.mass(0) lt 41 or outdata.mass(0) gt 19 and outdata.mass(0) lt 21) and strupcase(experiment) ne 'ELV' and strupcase(experiment) ne 'MCAVEDON' then begin    ;special case for investigations on passive N2-emissions
                
                    for i_frame = 0, outdata.nframe-1 do begin

                        if outdata.temp.error(i_frame,0,i_chan) gt 900. or outdata.intensity.data(i_frame,0,i_chan) lt  1.5d13 then begin
                            outdata.fit_status(i_frame,i_chan)=-1.
                            outdata.intensity.data(i_frame,*,i_chan)=!values.f_nan
                            outdata.intensity.error(i_frame,*,i_chan)=!values.f_nan
                            outdata.temp.data(i_frame,*,i_chan)=!values.f_nan
                            outdata.temp.error(i_frame,*,i_chan)=!values.f_nan
                            outdata.vrot.data(i_frame,*,i_chan)=!values.f_nan
                            outdata.vrot.error(i_frame,*,i_chan)=!values.f_nan
                            outdata.wavelength.data(i_frame,*,i_chan)=!values.f_nan
                            outdata.wavelength.error(i_frame,*,i_chan)=!values.f_nan
                            outdata.baseline.data(i_frame,i_chan)=!values.f_nan
                            outdata.baseline.error(i_frame,i_chan)=!values.f_nan
                        endif

                    endfor

                endif else begin

                    for i_frame = 0, outdata.nframe-1 do begin

                        if outdata.temp.error(i_frame,0,i_chan) gt 90. or outdata.intensity.data(i_frame,0,i_chan) lt  1.5d15  and strupcase(experiment) ne 'ELV' and strupcase(experiment) ne 'MCAVEDON' then begin
                            outdata.fit_status(i_frame,i_chan)=-1.
                            outdata.intensity.data(i_frame,*,i_chan)=!values.f_nan
                            outdata.intensity.error(i_frame,*,i_chan)=!values.f_nan
                            outdata.temp.data(i_frame,*,i_chan)=!values.f_nan
                            outdata.temp.error(i_frame,*,i_chan)=!values.f_nan
                            outdata.vrot.data(i_frame,*,i_chan)=!values.f_nan
                            outdata.vrot.error(i_frame,*,i_chan)=!values.f_nan
                            outdata.wavelength.data(i_frame,*,i_chan)=!values.f_nan
                            outdata.wavelength.error(i_frame,*,i_chan)=!values.f_nan
                            outdata.baseline.data(i_frame,i_chan)=!values.f_nan
                            outdata.baseline.error(i_frame,i_chan)=!values.f_nan
                        endif

                    endfor

                endelse
            endfor

        endelse
    endif
endif else begin    
; now apply same for both edge systems - 'new' CMR and CPR -> shots > 25900
; for now only for new CMR
   ; if diagnostic eq 'CMZ' or diagnostic eq 'CPZ' then begin
   if diagnostic eq 'CMZ' and strupcase(experiment) eq 'AUGD' then begin
        act_pass_fact=0.0
        if outdata.mass(0) gt 3 and outdata.mass(0) lt 5 then act_pass_fact=0.50
        if outdata.mass(0) gt 9 and outdata.mass(0) lt 11 then act_pass_fact=0.3
        if outdata.mass(0) gt 11 and outdata.mass(0) lt 13 then act_pass_fact=1.0
        if outdata.mass(0) gt 13 and outdata.mass(0) lt 15 then act_pass_fact=0.0
        if outdata.mass(0) gt 19 and outdata.mass(0) lt 21 then act_pass_fact=0.0
        if outdata.mass(0) gt 38 and outdata.mass(0) lt 41 then act_pass_fact=0.0
        if act_pass_fact ne 0.0 and outdata.nlines ne 1 then begin    
            for i_chan = 0., outdata.ntrack-1 do begin
                for i_frame = 0, outdata.nframe-1 do begin
                    if outdata.temp.error(i_frame,0,i_chan) gt 100. or    $
                        outdata.intensity.data(i_frame,0,i_chan)/outdata.intensity.data(i_frame,1,i_chan) lt act_pass_fact then begin
                        outdata.fit_status(i_frame,i_chan)=-1.
                        outdata.intensity.data(i_frame,*,i_chan)=!values.f_nan
                        outdata.intensity.error(i_frame,*,i_chan)=!values.f_nan
                        outdata.temp.data(i_frame,*,i_chan)=!values.f_nan
                        outdata.temp.error(i_frame,*,i_chan)=!values.f_nan
                        outdata.vrot.data(i_frame,*,i_chan)=!values.f_nan
                        outdata.vrot.error(i_frame,*,i_chan)=!values.f_nan
                        outdata.wavelength.data(i_frame,*,i_chan)=!values.f_nan
                        outdata.wavelength.error(i_frame,*,i_chan)=!values.f_nan
                        outdata.baseline.data(i_frame,i_chan)=!values.f_nan
                        outdata.baseline.error(i_frame,i_chan)=!values.f_nan
                    endif
                endfor
            endfor
        endif
   endif
endelse

; sort out 'bad fits' for CPR by criterium
if diagnostic eq 'CPZ' and strupcase(experiment) eq 'AUGD' then begin
    act_pass_fact=0.0
    if outdata.mass(0) gt 3 and outdata.mass(0) lt 5 then act_pass_fact=0.50
    if outdata.mass(0) gt 9 and outdata.mass(0) lt 11 then act_pass_fact=0.3
    if outdata.mass(0) gt 11 and outdata.mass(0) lt 13 then act_pass_fact=1.0
    if outdata.mass(0) gt 13 and outdata.mass(0) lt 15 then act_pass_fact=0.0
    if outdata.mass(0) gt 19 and outdata.mass(0) lt 21 then act_pass_fact=0.0
    if outdata.mass(0) gt 38 and outdata.mass(0) lt 41 then act_pass_fact=0.0
    if act_pass_fact ne 0.0 and outdata.nlines ne 1 then begin    
        for i_chan = 0., outdata.ntrack-1 do begin
            for i_frame = 0, outdata.nframe-1 do begin
                if outdata.temp.error(i_frame,0,i_chan) gt 100. or    $
                    outdata.intensity.data(i_frame,0,i_chan)/outdata.intensity.data(i_frame,1,i_chan) lt act_pass_fact then begin
                    outdata.fit_status(i_frame,i_chan)=-1.
                    outdata.intensity.data(i_frame,*,i_chan)=!values.f_nan
                    outdata.intensity.error(i_frame,*,i_chan)=!values.f_nan
                    outdata.temp.data(i_frame,*,i_chan)=!values.f_nan
                    outdata.temp.error(i_frame,*,i_chan)=!values.f_nan
                    outdata.vrot.data(i_frame,*,i_chan)=!values.f_nan
                    outdata.vrot.error(i_frame,*,i_chan)=!values.f_nan
                    outdata.wavelength.data(i_frame,*,i_chan)=!values.f_nan
                    outdata.wavelength.error(i_frame,*,i_chan)=!values.f_nan
                    outdata.baseline.data(i_frame,i_chan)=!values.f_nan
                    outdata.baseline.error(i_frame,i_chan)=!values.f_nan
                endif
            endfor
        endfor
    endif
endif

    
;rotation error bars are negative due to some mismatch within cxsfit in combination with negative dispersion

    if diagnostic eq 'CXZ' then begin
        inds= where(outdata.vrot.error(*,*,*) ne 0l and outdata.vrot.error(*,*,*) ne -1l)
         outdata.vrot.error(inds)=outdata.vrot.error(inds)*(-1.0)
    endif

  whtime = WHERE(TOTAL(outdata.fit_status > 0,2) GT 0, ntime)
  IF ntime EQ 0 THEN BEGIN
    err = ID+'No valid fits available!'
    RETURN
  ENDIF






; Open a shot file for writing
  error      = 0L
  shot       = outdata.shot_nr
  mode       = 'new '
  edition    = -1L
  diaref     = 0L
  tim        = STRING(' ',format='(a18)')
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwopen', $
                     error, experiment, diagnostic, $
                     shot, mode, edition, diaref, tim)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',format='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; General information: Code name
  IF debug THEN print,'Info.CodeName'
  name       = 'Info    '
  parm       = 'CodeName'
  type       = 6L
  lbuf       = 16L              ; now in bytes
  buffer     = outdata.code
  cxf_string_pad,buffer,16      ; pad with blanks
  stride     = 16L              ; 16 character long strings
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; General information: Code version
  IF debug THEN print,'Info.Version'
  name       = 'Info    '
  parm       = 'Version '
  type       = 2L
  lbuf       = 1L
  buffer     = long(cxf_version(/number))
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; General information: UserID
  IF debug THEN print,'Info.UserID'
  name       = 'Info    '
  parm       = 'UserID  '
  type       = 6L
  lbuf       = 16L              ; now in bytes
  buffer     = GETENV('USER')
  cxf_string_pad,buffer,16      ; pad with blanks
  stride     = 16L              ; 16 character long strings
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; General information: Wavelength reference
  IF debug THEN print,'Info.wave_ref'
  name       = 'Info    '
  parm       = 'wave_ref'
  type       = 6L
  lbuf       = 72L
  buffer     = specdata.wavelength.reference
  cxf_string_pad,buffer,72      ; pad with blanks
  stride     = 72L              ; 72 character long strings
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; General information: Dispersion reference
  IF debug THEN print,'Info.disp_ref'
  name       = 'Info    '
  parm       = 'disp_ref'
  type       = 6L
  lbuf       = 72L
  buffer     = specdata.dispersion.reference
  cxf_string_pad,buffer,72      ; pad with blanks
  stride     = 72L              ; 72 character long strings
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; General information: Intensity reference
  IF debug THEN print,'Info.inte_ref'
  name       = 'Info    '
  parm       = 'inte_ref'
  type       = 6L
  lbuf       = 72L
  buffer     = specdata.intensity.reference
  cxf_string_pad,buffer,72      ; pad with blanks
  stride     = 72L              ; 72 character long strings
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; General information: Instrument function reference
  IF debug THEN print,'Info.inst_ref'
  name       = 'Info    '
  parm       = 'inst_ref'
  type       = 6L
  lbuf       = 72L
  buffer     = specdata.instfu.reference
  cxf_string_pad,buffer,72      ; pad with blanks
  stride     = 72L              ; 72 character long strings
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Comment field
  IF debug THEN print,'Comment'
  name       = 'Comment '
  type       = 6L
  lbuf       = N_ELEMENTS(comment) * 72L ; now in bytes
  stride     = 72L ; 72 character long strings
  buffer = comment
  cxf_string_pad,buffer,72 ; pad with blanks
  indices    = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                     error, diaref, name, type, $
                     lbuf, buffer, stride, indices)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Write time base
  IF debug THEN print,'time'
  name       = 'time    '
  type       = 2L
  length     = ntime
  timebuf    = specdata.time[whtime]
  stride     = 1L
  s = CALL_EXTERNAL(!libddww,'ddgetaug','wwtbase', $
                    error, diaref, name, type, $
                    length, timebuf, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; exposure time
  IF debug THEN print,'texp'
  name       = 'texp    '
  type       = 2L
  lbuf       = ntime
  buffer     = specdata.exposure[whtime]
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwsignal', $
                     error, diaref, name, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    s = CALL_EXTERNAL (!libddww,'ddgetaug','xxerror', error, 3L, ' ')
    GOTO, FINISH
  ENDIF

; Line-of-sight area base
  IF debug THEN print,'LOSNum'
  name       = 'LOSNum  '
  k1         = 1L
  k2         = 1L
  type       = 1L
  ntrack     = outdata.ntrack
  buffer     = LINDGEN(ntrack)+1
  sizes      = [outdata.ntrack,0L,0L]
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwainsert', $
                     error, diaref, name, k1, k2, type, $
                     buffer, sizes)
                                         
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Line-of-sight information: number
  IF debug THEN print,'LOSInfo.Number'
  name       = 'LOSInfo '
  parm       = 'Number  '
  type       = 1L
  lbuf       = 1L
  buffer     = ntrack
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
                    
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Line-of-sight information: names
  IF debug THEN print,'LOSInfo.Name'
  name       = 'LOSInfo '
  parm       = 'Name    '
  type       = 6L
  lbuf       = ntrack * 16L   ; now in bytes
  buffer     = STRARR(maxtrack)
  buffer[0:ntrack-1] = specdata.los_name
  cxf_string_pad,buffer,30      ; pad with blanks
  stride     = 16L              ; 16 character long strings
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Line-of-sight information: R location
  IF debug THEN print,'LOSInfo.R'
  name       = 'LOSInfo '
  parm       = 'R       '
  type       = 2L
  lbuf       = maxtrack
  buffer     = FLTARR(maxtrack)
  buffer[0:ntrack-1] = specdata.R_pos
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)

  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Line-of-sight information: R location
  IF debug THEN print,'LOSInfo.z'
  name       = 'LOSInfo '
  parm       = 'z       '
  type       = 2L
  lbuf       = maxtrack
  buffer     = FLTARR(maxtrack)
  buffer[0:ntrack-1] = specdata.z_pos
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Line-of-sight information: phi location
  IF debug THEN print,'LOSInfo.phi'
  name       = 'LOSInfo '
  parm       = 'phi     '
  type       = 2L
  lbuf       = maxtrack
  buffer     = FLTARR(maxtrack)
  buffer[0:ntrack-1] = specdata.phi_pos
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Line-of-sight information: R location of origin
  IF debug THEN print,'LOSInfo.Rorig'
  name       = 'LOSInfo '
  parm       = 'Rorig   '
  type       = 2L
  lbuf       = maxtrack
  buffer     = FLTARR(maxtrack)
  buffer[0:ntrack-1] = specdata.R_orig
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Line-of-sight information: z location of origin
  IF debug THEN print,'LOSInfo.zorig'
  name       = 'LOSInfo '
  parm       = 'zorig   '
  type       = 2L
  lbuf       = maxtrack
  buffer     = FLTARR(maxtrack)
  buffer[0:ntrack-1] = specdata.z_orig
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Line-of-sight information: phi location of origin
  IF debug THEN print,'LOSInfo.phiorig'
  name       = 'LOSInfo '
  parm       = 'phiorig '
  type       = 2L
  lbuf       = maxtrack
  buffer     = FLTARR(maxtrack)
  buffer[0:ntrack-1] = specdata.phi_orig
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Projection of LOS on toroidal direction (cos(theta))
  IF debug THEN print,'LOSInfo.costhtor'
  R_pos  = specdata.R_pos
  x_pos  = specdata.R_pos*cos(specdata.phi_pos/!RADEG)
  y_pos  = specdata.R_pos*sin(specdata.phi_pos/!RADEG)
  z_pos  = specdata.z_pos
  x_orig = specdata.R_orig*cos(specdata.phi_orig/!RADEG)
  y_orig = specdata.R_orig*sin(specdata.phi_orig/!RADEG)
  z_orig = specdata.z_orig
  l_LOS  = SQRT((x_pos-x_orig)^2+(y_pos-y_orig)^2+(z_pos-z_orig)^2)
  c_LOS  = [[(x_pos-x_orig)/l_LOS], $
            [(y_pos-y_orig)/l_LOS], $
            [(z_pos-z_orig)/l_LOS]]
  c_tor  = [[-y_pos/R_pos],[+x_pos/R_pos],[FLTARR(ntrack)]]
  costheta = TOTAL(c_LOS*c_tor,2)
  name       = 'LOSInfo '
  parm       = 'costhtor'
  type       = 2L
  lbuf       = maxtrack
  buffer     = FLTARR(maxtrack)
  buffer[0:ntrack-1] = costheta
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Spectral line area base
  IF debug THEN print,'LineNum'
  name       = 'LineNum '
  k1         = 1L
  k2         = 1L
  type       = 1L
  nlines     =  outdata.nlines
  nlines     = LONG(outdata.nlines)
  buffer[0:nlines-1] = LINDGEN(nlines)+1
  sizes      = [maxlines,0L,0L]
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwainsert', $
                     error, diaref, name, k1, k2, type, $
                     buffer, sizes)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Spectral line information: number
  IF debug THEN print,'LineInfo.Number'
  name       = 'LineInfo'
  parm       = 'Number  '
  type       = 1L
  lbuf       = 1L
  buffer     = nlines
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Spectral line information: names
  IF debug THEN print,'LineInfo.Name'
  name       = 'LineInfo'
  parm       = 'Name    '
  type       = 6L
  lbuf       = nlines * 32L     ; now in bytes
  buffer     = (*fitparam.initial).line.description
  cxf_string_pad,buffer,32      ; pad with blanks
  stride     = 32L              ; 32 character long strings
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Spectral line information: Mass associated with each line
  IF debug THEN print,'LinInfo.mass'
  name               = 'LineInfo'
  parm               = 'mass    '
  type               = 2L
  lbuf               = maxlines
  buffer             = FLTARR(maxlines)
  buffer[0:nlines-1] = outdata.mass
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Spectral line information: Active CX label
  IF debug THEN print,'LinInfo.active'
  name               = 'LineInfo'
  parm               = 'active  '
  type               = 1L
  lbuf               = maxlines
  buffer             = LONARR(maxlines)
  buffer[0:nlines-1] = (*fitparam.initial).line.active
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Spectral line information: CX transition wavelength
  IF debug THEN print,'LinInfo.cxwavel'
  name               = 'LineInfo'
  parm               = 'cxwavel '
  type               = 2L
  lbuf               = maxlines
  buffer             = FLTARR(maxlines)
  buffer[0:nlines-1] = (*fitparam.initial).line.wavelength
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Spectral line information: CX transition upper N
  IF debug THEN print,'LinInfo.upper'
  name               = 'LineInfo'
  parm               = 'upper   '
  type               = 1L
  lbuf               = maxlines
  buffer             = LONARR(maxlines)
  buffer[0:nlines-1] = (*fitparam.initial).line.upper
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Spectral line information: CX transition lower N
  IF debug THEN print,'LinInfo.lower'
  name               = 'LineInfo'
  parm               = 'lower   '
  type               = 1L
  lbuf               = maxlines
  buffer             = LONARR(maxlines)
  buffer[0:nlines-1] = (*fitparam.initial).line.lower
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Spectral line information: CX transition imp. nuclear charge
  IF debug THEN print,'LinInfo.Z0'
  name               = 'LineInfo'
  parm               = 'Z0      '
  type               = 1L
  lbuf               = maxlines
  buffer             = LONARR(maxlines)
  buffer[0:nlines-1] = (*fitparam.initial).line.z0
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Spectral line information: CX transition imp. ion charge
  IF debug THEN print,'LinInfo.Z'
  name               = 'LineInfo'
  parm               = 'Z       '
  type               = 1L
  lbuf               = maxlines
  buffer             = LONARR(maxlines)
  buffer[0:nlines-1] = (*fitparam.initial).line.z
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Spectral line information: Primary CX line
  IF debug THEN print,'LinInfo.cxline'
  name               = 'LineInfo'
  parm               = 'cxline  '
  type               = 1L
  lbuf               = 1L
  buffer             = (*fitparam.fparam).cxline+1
  stride     = 1L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwparm', $
                     error, diaref, name, parm, type, $
                     lbuf, buffer, stride)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Two dimensional LOS / Spectral line area base
  IF debug THEN print,'LOSLine'
  name       = 'LOSLine '
  k1         = 1L
  k2         = 1L
  type       = 1L
  buffer     = LONARR(maxtrack+maxlines)
  buffer[0:ntrack-1] = LINDGEN(ntrack)+1
  buffer[maxtrack:maxtrack+nlines-1] = LINDGEN(nlines)+1
  sizes      = [maxtrack,maxlines,0L]
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwainsert', $
                     error, diaref, name, k1, k2, type, $
                     buffer, sizes)
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; Primary CXRS line
  cxline = (*fitparam.fparam).cxline

  IF cxline GE 0 THEN BEGIN

; Area bases (R,z & phi) for primary CXRS line
    IF debug THEN print,'R'
    name       = 'R       '
    k1         = 1L
    k2         = 1L
    type       = 2L
    buffer     = FLTARR(maxtrack)
    buffer[0:ntrack-1] = specdata.R_pos
    sizes      = [maxtrack,0,0]
    s = CALL_EXTERNAL (!libddww,'ddgetaug','wwainsert', $
                       error, diaref, name, k1, k2, type, $
                       buffer, sizes)
    IF error NE 0 THEN BEGIN
      err = STRING(' ',FORMAT='(a80)')
      s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                        -1L, err, error, 3L, ' ')
      err = ID+STRTRIM(err,2)
      IF debug THEN STOP
      GOTO, FINISH
    ENDIF
    IF debug THEN print,'z'
    name       = 'z       '
    k1         = 1L
    k2         = 1L
    type       = 2L
    buffer     = FLTARR(maxtrack)
    buffer[0:ntrack-1] = specdata.z_pos
    sizes      = [maxtrack,0,0]
    s = CALL_EXTERNAL (!libddww,'ddgetaug','wwainsert', $
                       error, diaref, name, k1, k2, type, $
                       buffer, sizes)
    IF error NE 0 THEN BEGIN
      err = STRING(' ',FORMAT='(a80)')
      s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                        -1L, err, error, 3L, ' ')
      err = ID+STRTRIM(err,2)
      IF debug THEN STOP
      GOTO, FINISH
    ENDIF
    IF debug THEN print,'phi'
    name       = 'phi     '
    k1         = 1L
    k2         = 1L
    type       = 2L
    buffer     = FLTARR(maxtrack)
    buffer[0:ntrack-1] = specdata.phi_pos
    sizes      = [maxtrack,0,0]
    s = CALL_EXTERNAL (!libddww,'ddgetaug','wwainsert', $
                       error, diaref, name, k1, k2, type, $
                       buffer, sizes)
    IF error NE 0 THEN BEGIN
      err = STRING(' ',FORMAT='(a80)')
      s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                        -1L, err, error, 3L, ' ')
      err = ID+STRTRIM(err,2)
      IF debug THEN STOP
      GOTO, FINISH
    ENDIF

     ; >>> Kappatou - start <<<
    IF strupcase(outdata.spectrometer) EQ 'CAR' OR $
       strupcase(outdata.spectrometer) EQ 'CER' OR $
       strupcase(outdata.spectrometer) EQ 'CCR' OR $
       strupcase(outdata.spectrometer) EQ 'COR' OR $
       strupcase(outdata.spectrometer) EQ 'CUR' OR $
       strupcase(outdata.spectrometer) EQ 'CHR' then begin
       
    ; time-dependent area bases (R,z & phi and errors) for primary CXRS line
    IF debug THEN print,'R_time'
    name       = 'R_time  '
    k1         = 1L
    k2         = long(ntime)
    type       = 2L
    buffer     = FLTARR(maxtrack,ntime)
    buffer[0:ntrack-1,*] = specdata.R_pos_time[*,whtime]
    sizes      = [maxtrack,0,0]
    s = CALL_EXTERNAL (!libddww,'ddgetaug','wwainsert', $
                       error, diaref, name, k1, k2, type, $
                       buffer, sizes)
    IF error NE 0 THEN BEGIN
      err = STRING(' ',FORMAT='(a80)')
      s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                        -1L, err, error, 3L, ' ')
      err = ID+STRTRIM(err,2)
      IF debug THEN STOP
      GOTO, FINISH
    ENDIF
    IF debug THEN print,'dR_time'
    name       = 'dR_time '
    k1         = 1L
    k2         = long(ntime)
    type       = 2L
    buffer     = FLTARR(maxtrack,ntime)
    buffer[0:ntrack-1,*] = specdata.dR_pos_time[*,whtime]
    sizes      = [maxtrack,0,0]
    s = CALL_EXTERNAL (!libddww,'ddgetaug','wwainsert', $
                       error, diaref, name, k1, k2, type, $
                       buffer, sizes)
    IF error NE 0 THEN BEGIN
      err = STRING(' ',FORMAT='(a80)')
      s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                        -1L, err, error, 3L, ' ')
      err = ID+STRTRIM(err,2)
      IF debug THEN STOP
      GOTO, FINISH
    ENDIF
    IF debug THEN print,'z_time'
    name       = 'z_time  '
    k1         = 1L
    k2         = long(ntime)
    type       = 2L
    buffer     = FLTARR(maxtrack,ntime)
    buffer[0:ntrack-1,*] = specdata.z_pos_time[*,whtime]
    sizes      = [maxtrack,0,0]
    s = CALL_EXTERNAL (!libddww,'ddgetaug','wwainsert', $
                       error, diaref, name, k1, k2, type, $
                       buffer, sizes)
    IF error NE 0 THEN BEGIN
      err = STRING(' ',FORMAT='(a80)')
      s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                        -1L, err, error, 3L, ' ')
      err = ID+STRTRIM(err,2)
      IF debug THEN STOP
      GOTO, FINISH
    ENDIF
    IF debug THEN print,'dz_time'
    name       = 'dz_time '
    k1         = 1L
    k2         = long(ntime)
    type       = 2L
    buffer     = FLTARR(maxtrack,ntime)
    buffer[0:ntrack-1,*] = specdata.dz_pos_time[*,whtime]
    sizes      = [maxtrack,0,0]
    s = CALL_EXTERNAL (!libddww,'ddgetaug','wwainsert', $
                       error, diaref, name, k1, k2, type, $
                       buffer, sizes)
    IF error NE 0 THEN BEGIN
      err = STRING(' ',FORMAT='(a80)')
      s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                        -1L, err, error, 3L, ' ')
      err = ID+STRTRIM(err,2)
      IF debug THEN STOP
      GOTO, FINISH
    ENDIF
    IF debug THEN print,'phi_time'
    name       = 'phi_time'
    k1         = 1L
    k2         = long(ntime)
    type       = 2L
    buffer     = FLTARR(maxtrack,ntime)
    buffer[0:ntrack-1,*] = specdata.phi_pos_time[*,whtime]
    sizes      = [maxtrack,0,0]
    s = CALL_EXTERNAL (!libddww,'ddgetaug','wwainsert', $
                       error, diaref, name, k1, k2, type, $
                       buffer, sizes)
    IF error NE 0 THEN BEGIN
      err = STRING(' ',FORMAT='(a80)')
      s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                        -1L, err, error, 3L, ' ')
      err = ID+STRTRIM(err,2)
      IF debug THEN STOP
      GOTO, FINISH
    ENDIF
    IF debug THEN print,'dphi_time'
    name       = 'dph_time'
    k1         = 1L
    k2         = long(ntime)
    type       = 2L
    buffer     = FLTARR(maxtrack,ntime)
    buffer[0:ntrack-1,*] = specdata.dphi_pos_time[*,whtime]
    sizes      = [maxtrack,0,0]
    s = CALL_EXTERNAL (!libddww,'ddgetaug','wwainsert', $
                       error, diaref, name, k1, k2, type, $
                       buffer, sizes)
    IF error NE 0 THEN BEGIN
      err = STRING(' ',FORMAT='(a80)')
      s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                        -1L, err, error, 3L, ' ')
      err = ID+STRTRIM(err,2)
      IF debug THEN STOP
      GOTO, FINISH
    ENDIF
    ; costheta
    IF debug THEN print,'cth_time'
    name       = 'cth_time'
    k1         = 1L
    k2         = long(ntime)
    type       = 2L
    buffer     = FLTARR(maxtrack,ntime)
    buffer[0:ntrack-1,*] = specdata.externalcostheta[*,whtime]
    sizes      = [maxtrack,0,0]
    s = CALL_EXTERNAL (!libddww,'ddgetaug','wwainsert', $
                       error, diaref, name, k1, k2, type, $
                       buffer, sizes)
    IF error NE 0 THEN BEGIN
      err = STRING(' ',FORMAT='(a80)')
      s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                        -1L, err, error, 3L, ' ')
      err = ID+STRTRIM(err,2)
      IF debug THEN STOP
      GOTO, FINISH
    ENDIF
    
    ENDIF ; CAR,CER CCR, CHR selection
    ; >>> kappatou - end <<<


; Ion temperature (from primary CXRS line)
    IF debug THEN print,'Ti'
    name       = 'Ti      '
    type       = 2L
    lbuf       = ntime
    stride     = 1L
    FOR l=0L,ntrack-1 DO BEGIN
      buffer = outdata.temp.data[whtime,cxline,l]
      indices    = [l+1]
      s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                         error, diaref, name, type, $
                         lbuf, buffer, stride, indices)
      IF error NE 0 THEN BEGIN
        err = STRING(' ',FORMAT='(a80)')
        s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                          -1L, err, error, 3L, ' ')
        err = ID+STRTRIM(err,2)
        IF debug THEN STOP
        GOTO, FINISH
      ENDIF
    ENDFOR

; Error in ion temperature (from primary CXRS line)
    IF debug THEN print,'err_Ti'
    name       = 'err_Ti  '
    type       = 2L
    lbuf       = ntime
    stride     = 1L
    FOR l=0L,ntrack-1 DO BEGIN
      buffer = outdata.temp.error[whtime,cxline,l]
      indices    = [l+1]
      s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                         error, diaref, name, type, $
                         lbuf, buffer, stride, indices)
      IF error NE 0 THEN BEGIN
        err = STRING(' ',FORMAT='(a80)')
        s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                          -1L, err, error, 3L, ' ')
        err = ID+STRTRIM(err,2)
        IF debug THEN STOP
        GOTO, FINISH
      ENDIF
    ENDFOR

;bring in corrected Ti_c (correction due to Zeemann/Paschen-Back and finestructure splitting)----------------------------------------------------------------------------
tmp_arr = reform(outdata.wavelength.data(*,cxline,*))
wvl=mean(tmp_arr(where(tmp_arr gt 200.0 and finite(tmp_arr))))

correct_ti_fast,shot,timebuf,specdata,wvl,reform(outdata.temp.data[whtime,cxline,*]),reform(outdata.temp.error[whtime,cxline,*]),t_i_corrected,t_i_err_corrected

if n_elements(timebuf) eq 1 then begin
    temp = fltarr(1,ntrack)
    temp[0,*] = t_i_corrected
    t_i_corrected = temp
    temp[0,*] = t_i_err_corrected
    t_i_err_corrected = temp
endif

; corrected Ion temperature (from primary CXRS line)
    IF debug THEN print,'Ti_c'
    name       = 'Ti_c    '
    type       = 2L
    lbuf       = ntime
    stride     = 1L
    FOR l=0L,ntrack-1 DO BEGIN
      buffer = t_i_corrected[*,l]
      indices    = [l+1]
      s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                         error, diaref, name, type, $
                         lbuf, buffer, stride, indices)
      IF error NE 0 THEN BEGIN
        err = STRING(' ',FORMAT='(a80)')
        s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                          -1L, err, error, 3L, ' ')
        err = ID+STRTRIM(err,2)
        IF debug THEN STOP
        GOTO, FINISH
      ENDIF
    ENDFOR

; Error in corrected ion temperature (from primary CXRS line)
    IF debug THEN print,'err_Ti_c'
    name       = 'err_Ti_c'
    type       = 2L
    lbuf       = ntime
    stride     = 1L
    FOR l=0L,ntrack-1 DO BEGIN
      buffer = t_i_err_corrected[*,l]
      indices    = [l+1]
      s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                         error, diaref, name, type, $
                         lbuf, buffer, stride, indices)
      IF error NE 0 THEN BEGIN
        err = STRING(' ',FORMAT='(a80)')
        s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                          -1L, err, error, 3L, ' ')
        err = ID+STRTRIM(err,2)
        IF debug THEN STOP
        GOTO, FINISH
      ENDIF
    ENDFOR

;end of corrected Ti_c -----------------------------------------------------------------------------------------------------------------------------------------------------------

; Toroidal rotation velocity (from primary CXRS line)
    IF debug THEN print,'vrot'
    name       = 'vrot    '
    type       = 2L
    lbuf       = ntime
    stride     = 1L
    FOR l=0L,ntrack-1 DO BEGIN
      buffer = outdata.vrot.data[whtime,cxline,l]
      indices    = [l+1]
      s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                         error, diaref, name, type, $
                         lbuf, buffer, stride, indices)
      IF error NE 0 THEN BEGIN
        err = STRING(' ',FORMAT='(a80)')
        s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                          -1L, err, error, 3L, ' ')
        err = ID+STRTRIM(err,2)
        IF debug THEN STOP
        GOTO, FINISH
      ENDIF
    ENDFOR

; Error in toroidal rotation velocity (from primary CXRS line)
    IF debug THEN print,'err_vrot'
    name       = 'err_vrot'
    type       = 2L
    lbuf       = ntime
    stride     = 1L
    FOR l=0L,ntrack-1 DO BEGIN
      buffer = outdata.vrot.error[whtime,cxline,l]
      indices    = [l+1]
      s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                         error, diaref, name, type, $
                         lbuf, buffer, stride, indices)
      IF error NE 0 THEN BEGIN
        err = STRING(' ',FORMAT='(a80)')
        s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                          -1L, err, error, 3L, ' ')
        err = ID+STRTRIM(err,2)
        IF debug THEN STOP
        GOTO, FINISH
      ENDIF
    ENDFOR

; Intensity of primary CXRS line
    IF debug THEN print,'inte'
    name       = 'inte    '
    type       = 2L
    lbuf       = ntime
    stride     = 1L
    FOR l=0L,ntrack-1 DO BEGIN
      buffer = outdata.intensity.data[whtime,cxline,l]
      indices    = [l+1]
      s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                         error, diaref, name, type, $
                         lbuf, buffer, stride, indices)
      IF error NE 0 THEN BEGIN
        err = STRING(' ',FORMAT='(a80)')
        s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                          -1L, err, error, 3L, ' ')
        err = ID+STRTRIM(err,2)
        IF debug THEN STOP
        GOTO, FINISH
      ENDIF
    ENDFOR

; Error in intensity of primary CXRS line
    IF debug THEN print,'err_inte'
    name       = 'err_inte'
    type       = 2L
    lbuf       = ntime
    stride     = 1L
    FOR l=0L,ntrack-1 DO BEGIN
      buffer = outdata.intensity.error[whtime,cxline,l]
      indices    = [l+1]
      s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                         error, diaref, name, type, $
                         lbuf, buffer, stride, indices)
      IF error NE 0 THEN BEGIN
        err = STRING(' ',FORMAT='(a80)')
        s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                          -1L, err, error, 3L, ' ')
        err = ID+STRTRIM(err,2)
        IF debug THEN STOP
        GOTO, FINISH
      ENDIF
    ENDFOR

  ENDIF                         ; data for primary CX line

; Line temperatures
  IF debug THEN print,'temp'
  name       = 'temp    '
  type       = 2L
  lbuf       = ntime
  stride     = 1L
  FOR k=0L,nlines-1 DO BEGIN
    FOR l=0L,ntrack-1 DO BEGIN
      buffer = outdata.temp.data[whtime,k,l]
      indices    = [l+1,k+1]
      s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                         error, diaref, name, type, $
                         lbuf, buffer, stride, indices)
      IF error NE 0 THEN BEGIN
        err = STRING(' ',FORMAT='(a80)')
        s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                          -1L, err, error, 3L, ' ')
        err = ID+STRTRIM(err,2)
        IF debug THEN STOP
        GOTO, FINISH
      ENDIF
    ENDFOR
  ENDFOR

; Error in line temperatures
  IF debug THEN print,'err_temp'
  name       = 'err_temp'
  type       = 2L
  lbuf       = ntime
  stride     = 1L
  FOR k=0L,nlines-1 DO BEGIN
    FOR l=0L,ntrack-1 DO BEGIN
      buffer = outdata.temp.error[whtime,k,l]
      indices    = [l+1,k+1]
      s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                         error, diaref, name, type, $
                         lbuf, buffer, stride, indices)
      IF error NE 0 THEN BEGIN
        err = STRING(' ',FORMAT='(a80)')
        s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                          -1L, err, error, 3L, ' ')
        err = ID+STRTRIM(err,2)
        IF debug THEN STOP
        GOTO, FINISH
      ENDIF
    ENDFOR
  ENDFOR

; Line intensities
  IF debug THEN print,'flux'
  name       = 'flux    '
  type       = 2L
  lbuf       = ntime
  stride     = 1L
  FOR k=0L,nlines-1 DO BEGIN
    FOR l=0L,ntrack-1 DO BEGIN
      buffer = outdata.intensity.data[whtime,k,l]
      indices    = [l+1,k+1]
      s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                         error, diaref, name, type, $
                         lbuf, buffer, stride, indices)
      IF error NE 0 THEN BEGIN
        err = STRING(' ',FORMAT='(a80)')
        s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                          -1L, err, error, 3L, ' ')
        err = ID+STRTRIM(err,2)
        IF debug THEN STOP
        GOTO, FINISH
      ENDIF
    ENDFOR
  ENDFOR

; Error in line intensities
  IF debug THEN print,'err_flux'
  name       = 'err_flux'
  type       = 2L
  lbuf       = ntime
  stride     = 1L
  FOR k=0L,nlines-1 DO BEGIN
    FOR l=0L,ntrack-1 DO BEGIN
      buffer = outdata.intensity.error[whtime,k,l]
      indices    = [l+1,k+1]
      s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                         error, diaref, name, type, $
                         lbuf, buffer, stride, indices)
      IF error NE 0 THEN BEGIN
        err = STRING(' ',FORMAT='(a80)')
        s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                          -1L, err, error, 3L, ' ')
        err = ID+STRTRIM(err,2)
        IF debug THEN STOP
        GOTO, FINISH
      ENDIF
    ENDFOR
  ENDFOR

; Line centre
  IF debug THEN print,'wave'
  name       = 'wave    '
  type       = 2L
  lbuf       = ntime
  stride     = 1L
  FOR k=0L,nlines-1 DO BEGIN
    FOR l=0L,ntrack-1 DO BEGIN
      buffer = outdata.wavelength.data[whtime,k,l]
      indices    = [l+1,k+1]
      s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                         error, diaref, name, type, $
                         lbuf, buffer, stride, indices)
      IF error NE 0 THEN BEGIN
        err = STRING(' ',FORMAT='(a80)')
        s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                          -1L, err, error, 3L, ' ')
        err = ID+STRTRIM(err,2)
        IF debug THEN STOP
        GOTO, FINISH
      ENDIF
    ENDFOR
  ENDFOR

; Error in line centre
  IF debug THEN print,'err_wave'
  name       = 'err_wave'
  type       = 2L
  lbuf       = ntime
  stride     = 1L
  FOR k=0L,nlines-1 DO BEGIN
    FOR l=0L,ntrack-1 DO BEGIN
      buffer = outdata.wavelength.error[whtime,k,l]
      indices    = [l+1,k+1]
      s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                         error, diaref, name, type, $
                         lbuf, buffer, stride, indices)
      IF error NE 0 THEN BEGIN
        err = STRING(' ',FORMAT='(a80)')
        s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                          -1L, err, error, 3L, ' ')
        err = ID+STRTRIM(err,2)
        IF debug THEN STOP
        GOTO, FINISH
      ENDIF
    ENDFOR
  ENDFOR

; Base line
  IF debug THEN print,'base'
  name       = 'base    '
  type       = 2L
  lbuf       = ntime
  stride     = 1L
  FOR l=0L,ntrack-1 DO BEGIN
    ; Check if the channel was fitted (check intensity of main CX line)
    ; if not fitted don't save baseline
    if total(outdata.intensity.data(*,cxline,l)) eq 0. then continue

    buffer = outdata.baseline.data[whtime,l]
    indices    = [l+1]
    s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                       error, diaref, name, type, $
                       lbuf, buffer, stride, indices)
    IF error NE 0 THEN BEGIN
      err = STRING(' ',FORMAT='(a80)')
      s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                        -1L, err, error, 3L, ' ')
      err = ID+STRTRIM(err,2)
      IF debug THEN STOP
      GOTO, FINISH
    ENDIF
  ENDFOR

; Error in base line
  IF debug THEN print,'err_base'
  name       = 'err_base'
  type       = 2L
  lbuf       = ntime
  stride     = 1L
  FOR l=0L,ntrack-1 DO BEGIN
    ; Check if the channel was fitted (check intensity of main CX line)
    ; if not fitted don't save baseline
    if total(outdata.intensity.data(*,cxline,l)) eq 0. then continue

    buffer = outdata.baseline.error[whtime,l]
    indices    = [l+1]
    s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                       error, diaref, name, type, $
                       lbuf, buffer, stride, indices)
    IF error NE 0 THEN BEGIN
      err = STRING(' ',FORMAT='(a80)')
      s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                        -1L, err, error, 3L, ' ')
      err = ID+STRTRIM(err,2)
      IF debug THEN STOP
      GOTO, FINISH
   ENDIF
 ENDFOR

  ;; ATTENTION, ben was here!
  if max(specdata.baseline.data) ne 0 then begin
     IF debug THEN print,'bens baseline'
     ntime_baseline=specdata.baseline.nframe
     name       = 'base_tim' 
     type       = 2L
     length     = ntime_baseline
     timebuf    = specdata.baseline.time
     stride     = 1L
     s = CALL_EXTERNAL(!libddww,'ddgetaug','wwtbase', $
                       error, diaref, name, type, $
                       length, timebuf, stride)
     IF error NE 0 THEN BEGIN
        err = STRING(' ',FORMAT='(a80)')
        s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                          -1L, err, error, 3L, ' ')
        err = ID+STRTRIM(err,2)
        IF debug THEN STOP
        GOTO, FINISH
     ENDIF
     name       = 'baseline'
     type       = 2L
     lbuf       = ntime_baseline
     stride     = 1L
     FOR l=0L,ntrack-1 DO BEGIN
        buffer = specdata.baseline.data[l,*]
        indices    = [l+1]
        s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                           error, diaref, name, type, $
                           lbuf, buffer, stride, indices)
        IF error NE 0 THEN BEGIN
           err = STRING(' ',FORMAT='(a80)')
           s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                             -1L, err, error, 3L, ' ')
           err = ID+STRTRIM(err,2)
           IF debug THEN STOP
           GOTO, FINISH
        ENDIF
     ENDFOR
     ;; store central wavelength of spectrometer in the first bin
     ;; of the error array (not nice but works!!)
     specdata.baseline.error[0,0]=specdata.wlength ;;[nm]
     name       = 'baselerr'
     type       = 2L
     lbuf       = ntime_baseline
     stride     = 1L
     FOR l=0L,ntrack-1 DO BEGIN
        buffer = specdata.baseline.error[l,*]
        indices    = [l+1]
        s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                           error, diaref, name, type, $
                           lbuf, buffer, stride, indices)
        IF error NE 0 THEN BEGIN
           err = STRING(' ',FORMAT='(a80)')
           s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                             -1L, err, error, 3L, ' ')
           err = ID+STRTRIM(err,2)
           IF debug THEN STOP
           GOTO, FINISH
        ENDIF
     ENDFOR
  endif ;; !! ben

; Fit flags: <0 failed; 0 no fit done; 1 OK;
;            2 possible converge problems
  IF debug THEN print,'fit_stat'
  name       = 'fit_stat'
  type       = 1L
  lbuf       = ntime
  stride     = 1L
  FOR l=0L,ntrack-1 DO BEGIN
    buffer = LONG(outdata.fit_status[whtime,l])
    indices    = [l+1]
    s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                       error, diaref, name, type, $
                       lbuf, buffer, stride, indices)
    IF error NE 0 THEN BEGIN
      err = STRING(' ',FORMAT='(a80)')
      s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                        -1L, err, error, 3L, ' ')
      err = ID+STRTRIM(err,2)
      IF debug THEN STOP
      GOTO, FINISH
    ENDIF
  ENDFOR

; Fit parameter history
  IF debug THEN print,'history'
  name       = 'history'
  nhistory   = (SIZE(history))[1]
  buffer = STRARR(maxparams,nhistory)
  FOR k=0L, nhistory-1 DO BEGIN
    cxf_write_switches, switches, *history[k], debug=debug, /StrArray, err
    IF err NE '' THEN BEGIN
      IF debug THEN STOP
      GOTO, FINISH
    ENDIF
    nparams = (SIZE(switches))[1]
    IF nparams GT maxparams THEN BEGIN
      err = ID+'Number of parameters exceeds size allocated in SFH: ' $
            + 'Increase maxparams!'
      IF debug THEN STOP
      GOTO, FINISH
    ENDIF
    buffer[0:nparams-1,k] = switches
  ENDFOR
  type       = 6L
  lbuf       = nhistory * 72L   ; now in bytes
  stride     = 72L              ; 72 character long strings
  cxf_string_pad, buffer, 72L
  FOR k=0L,maxparams-1 DO BEGIN
    indices    = [k+1]
    s = CALL_EXTERNAL (!libddww,'ddgetaug','wwinsert', $
                       error, diaref, name, type, $
                       lbuf, buffer[k,*], stride, indices)
    IF error NE 0 THEN BEGIN
      err = STRING(' ',FORMAT='(a80)')
      s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                        -1L, err, error, 3L, ' ')
      err = ID+STRTRIM(err,2)
      IF debug THEN STOP
      GOTO, FINISH
    ENDIF
  ENDFOR

; List of predecessors:
; - Level 0 CXRS SF
  sf_exper = 'AUGD'
  sf_diag  = outdata.spectrometer
  ;; 25.03.2014 L0 shotfiles 
  if outdata.spectrometer eq 'CVH' or outdata.spectrometer eq 'CXH' then sf_diag = 'FVS'
  sf_shot  = shot
  sf_edit  = 0L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','wwpred', $
                     error, diaref, $
                     sf_exper,sf_diag,sf_shot,sf_edit)
; If the level 0 shot file doesn't exist this might be a special
; (i.e. a calibration file) - ask the user if she really wants to
; write a shot file and, if so, proceed without the predecessors
;  IF byte(error,0) EQ 32 AND $       ; WW package
;     byte(error,1) EQ 13 AND $       ; wwpred
;     byte(error,2) EQ  0 AND $       ; error
;     byte(error,3) EQ  7 THEN BEGIN  ; code 'Open- or I/O error in DD-file'
;; 25.03.2014 elv byte(error) order changed on 64-bit machines!!!!
  IF byte(error,3) EQ 32 AND $       ; WW package
     byte(error,2) EQ 13 AND $       ; wwpred
     byte(error,1) EQ  0 AND $       ; error
     byte(error,0) EQ  7 THEN BEGIN  ; code 'Open- or I/O error in DD-file'
    text = ['Missing Level 0 Shot File!', $
            'Proceed to write Level 1 shot file?']
    ans =  DIALOG_MESSAGE(text, title=title, /Question, /Center)
    IF ans EQ 'Yes' THEN error = 0
  ENDIF
  IF error NE 0 THEN BEGIN
    err = STRING(' ',FORMAT='(a80)')
    s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                      -1L, err, error, 3L, ' ')
    err = ID+STRTRIM(err,2)
    IF debug THEN STOP
    GOTO, FINISH
  ENDIF

; - Level 1 CXRS SF for Ti external estimates
  sfti_exper =  '' & sfti_diag = '' & sfti_shot = 0L & sfti_edit = 0L
  IF PTR_VALID(extdata) THEN BEGIN
    IF PTR_VALID((*extdata).ti_src) AND $
       (*fitparam.globsw).external_ti_estimates AND $
       MAX((*fitparam.initial).line.wid_fixed AND (*fitparam.initial).line.active) $
       THEN BEGIN
      sfti_exper = ((*extdata).ti_src).experiment
      sfti_diag  = ((*extdata).ti_src).diagnostic
      sfti_shot  = ((*extdata).ti_src).shot_found
      sfti_edit  = ((*extdata).ti_src).edition_found
      IF sfti_exper EQ 'AUGD' THEN BEGIN ; only public shotfiles allowed in predecessor list
        s = CALL_EXTERNAL (!libddww,'ddgetaug','wwpred', $
                           error, diaref, $
                           sfti_exper,sfti_diag,sfti_shot,sfti_edit)
        IF error NE 0 THEN BEGIN
          err = STRING(' ',FORMAT='(a80)')
          s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                            -1L, err, error, 3L, ' ')
          err = ID+STRTRIM(err,2)
          IF debug THEN STOP
          GOTO, FINISH
        ENDIF
      ENDIF
    ENDIF
  ENDIF
; - Level 1 CXRS SF for AF external estimates
  IF PTR_VALID(extdata) THEN BEGIN
    IF PTR_VALID((*extdata).af_src) AND $
       (*fitparam.globsw).external_af_estimates AND $
       MAX((*fitparam.initial).line.pos_fixed AND (*fitparam.initial).line.active) $
       THEN BEGIN
      sfaf_exper = ((*extdata).af_src).experiment
      sfaf_diag  = ((*extdata).af_src).diagnostic
      sfaf_shot  = ((*extdata).af_src).shot_found
      sfaf_edit  = ((*extdata).af_src).edition_found
      IF sfaf_exper EQ 'AUGD' AND $ ; only public shotfiles allowed in predecessor list
         (sfaf_exper NE sfti_exper OR sfaf_diag NE sfti_diag OR $
          sfaf_shot  NE sfti_shot  OR sfaf_edit NE sfti_edit) THEN BEGIN
        s = CALL_EXTERNAL (!libddww,'ddgetaug','wwpred', $
                           error, diaref, $
                           sfaf_exper,sfaf_diag,sfaf_shot,sfaf_edit)
        IF error NE 0 THEN BEGIN
          err = STRING(' ',FORMAT='(a80)')
          s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                            -1L, err, error, 3L, ' ')
          err = ID+STRTRIM(err,2)
          IF debug THEN STOP
          GOTO, FINISH
        ENDIF
      ENDIF
    ENDIF
  ENDIF

; Close the shot file
  disp  = 'lock'
  space = 'compress'
  s = CALL_EXTERNAL(!libddww,'ddgetaug','wwclose', $
                    error, diaref, disp, space)
  IF error NE 0 THEN BEGIN
; Ignore warnings and errors from DDB
    IF BYTE(error,0) EQ 24 THEN error = 0L
    IF error NE 0L THEN BEGIN
      err = STRING(' ',FORMAT='(a80)')
      s = CALL_EXTERNAL(!libddww,'ddgetaug','xxerrprt', $
                        -1L, err, error, 3L, ' ')
      err = ID+STRTRIM(err,2)
      IF debug THEN STOP
    ENDIF
  ENDIF

FINISH:
; Move back to original directory
  POPD

  IF error NE 0 AND diaref NE 0 THEN BEGIN
    disp  = 'lock'
    space = 'compress'
    error2 = 0L
    s = CALL_EXTERNAL (!libddww,'ddgetaug','wwclose', $
                       error2, diaref, disp, space)
  ENDIF

END
