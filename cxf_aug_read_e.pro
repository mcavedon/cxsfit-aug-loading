pro cxf_aug_read_e,event

  widget_control,event.top,get_uvalue=info

  if event.id eq info.cancel_button then begin
    widget_control,event.top,/destroy
    return
  endif

;This asks upfront is the chosen diagnostic CMR - if so activate the extra widget base corresponding to this diagnostic
    diagnostic=widget_info(info.diagnostic,/droplist_select)
if (diagnostic eq 2) or (diagnostic eq 3) or (diagnostic eq 9) then $ 
    widget_control,info.extrabase_edge,sensitive=1 else $
    widget_control,info.extrabase_edge,sensitive=0
  
    
    if (diagnostic eq 4) or (diagnostic eq 5) then widget_control,info.extrabase_cxh,sensitive=1 else widget_control,info.extrabase_cxh,sensitive=0


   
    widget_control,info.pulse,get_value=pulse
    
    ;This now includes CER, old CHR, CAR, CBR, CCR, CFR (for old shots)
    if (diagnostic eq 0 or (diagnostic eq 1 and pulse le 30150) or (diagnostic ge 11 and diagnostic le 13)) then begin
         widget_control,info.extrabase_core,sensitive=1

         if (diagnostic eq 0) then begin
             widget_control,info.core_options,sensitive=1
             widget_control,info.core_options,get_value=add_up_frames
             if (add_up_frames[0]) then $
                widget_control,info.core_options_nframes,sensitive=1 else $
                widget_control,info.core_options_nframes,sensitive=0
         endif else begin
            widget_control,info.core_options,sensitive=0
         endelse

     endif else begin
         widget_control,info.extrabase_core,sensitive=0
     endelse

    

     ;; activate widget for CHR, COR and BOS diagnostics
     if (diagnostic eq 7 or diagnostic eq 8 or diagnostic eq 1 or diagnostic eq 10 or diagnostic eq 11 or diagnostic eq 15) then begin
        widget_control,info.extrabase_ben,sensitive=1
        widget_control,info.pulse,get_value=pulse
        if (pulse lt 30410 and diagnostic eq 1) then begin
           widget_control,info.extrabase_ben,sensitive=0
           widget_control,info.extrabase_core,sensitive=1
        endif   
        widget_control,info.beam_buttons_ben2,get_value=los_choice
        if (los_choice[1] eq 1) then $
           widget_control,info.nframes_text,sensitive=1 else $
           widget_control,info.nframes_text,sensitive=0   
     endif else begin
        widget_control,info.extrabase_ben,sensitive=0
     endelse
  
     
  
  if event.id eq info.ok_button then begin
    widget_control,/hourglass
    widget_control,info.pulse,get_value=pulse
    diagnostic=widget_info(info.diagnostic,/droplist_select)
    
    pulse=long(pulse)
    
    case diagnostic of
      0:begin
        signalname='CER'
        widget_control,info.beam_buttons_core,get_value=beam_choice
        widget_control,info.beam_buttons_core1,get_value=roi_choice
        widget_control,info.beam_buttons_core2,get_value=cal_choice
        widget_control,info.core_options,get_value=addupframes
        ; this is for testing - kappatou
        ; I hard-code the option for time-dependent R,z,phi,costheta 
        ; if the others agree we can have either a widget option or define
        ; all core systems with time-dep and all edge systems without
        ; the time-dependence.
        timeloc=1
        print,'TIME-DEPENDENT R,Z,Phi - Under testing!!!'
        if beam_choice eq 0 then begin
            print,'Using All Time Frames'
            BeamMod=0
            ;cfrdata=0
        endif else begin
            if beam_choice eq 1 then begin
                print,'Using Beam Modulation'
                BeamMod=1
                ;cfrdata=0
            endif else begin
                if beam_choice eq 3 then begin
                    print,'Using Beam Blips'
                    beammod=3 ; 2 is the last frames!
                endif else begin
                    if beam_choice eq 4 then begin
                        print,'Using Beam Blips, adding up frames'
                        beammod=4
                    endif else begin
                        beammod=0
                    endelse
                endelse
            endelse
        endelse 
         if roi_choice eq 0 then begin
            print,'Using All ROI'
            ROIADD=0
        endif else begin
            print,'Adding ROI with like Major R'
            ROIADD=1
        endelse        
         if cal_choice eq 0 then begin
            print,'Using default calibrations plus Ne after shot if available'
            passivecal=0
            neon=0
        endif else begin
            if cal_choice eq 1 then begin
                print,'Loading ne calibration file'
                passivecal=0
                neon=1
            endif else begin
                print,'Calibrating with passive line'
                passivecal=1
                neon=0
            endelse
        endelse   
        if addupframes then begin
           widget_control,info.core_options_nframes,get_value=nframes
           frames=UINT(nframes[0])
           if frames lt 2 then begin
              addupframes=0
              frames=1
              print,'!!!!!!!!! Warning: Incorrect number of frames has been set'
              print,'Continuing with addupframes = 1 thus no frames are added up'
              print,'From cxf_aug_read_e.pro line ~ 124'
           endif else print,'Adding up #frames:',frames
        endif else begin
           frames=1
        endelse

        CER={addupframes:addupframes,nframes:frames}

        extraoptions={CER:CER,timeloc:timeloc}
        

      end

      
      1:begin
        if pulse le 30150 then begin
            signalname='CHR'
            widget_control,info.beam_buttons_core,get_value=beam_choice
            widget_control,info.beam_buttons_core1,get_value=roi_choice
            widget_control,info.beam_buttons_core2,get_value=cal_choice
    
            if beam_choice eq 0 then begin
                print,'Using All Time Frames'
                BeamMod=0
            endif else begin
                print,'Using Beam Modulation'
                BeamMod=1
            endelse 
            if roi_choice eq 0 then begin
                print,'Using All ROI'
                ROIADD=0
            endif else begin
                print,'Adding ROI with like Major R'
                ROIADD=1
            endelse        
            if cal_choice eq 0 then begin
                print,'using default calibrations plus neon in shot if available'
                passivecal=0
                neon=0
            endif else begin
                if cal_choice eq 1 then begin
                    print,'Loading Ne calibration file'
                    passivecal=0
                    neon=1
                endif else begin
                    print,'Calibrating with passive line'
                    passivecal=1
                    neon=0
                endelse
            endelse          
        endif else begin
            ;THE NEW CHR 2014 - same format as BOS/COR/CUR
            signalname='CHR'
            timeloc=1
            widget_control,info.beam_buttons_ben1,get_value=beam_choice
            widget_control,info.beam_buttons_ben2,get_value=los_choice 
            ;;This gives a zero -which is Ne fit plus line fit (default)in the 
            ;;COR/CUR/BOS calibration scheme. 
            widget_control,info.beam_buttons_core2,get_value=fitting_choice
        
            if beam_choice eq 0 then begin
                print,'Using All Time Frames'
                BeamMod=0
             endif else begin
                print,'Using Beam Modulation'
                BeamMod=1
             endelse
             if los_choice[0] eq 0 then begin
                print,'ALL LOS'
                selhead=0
             endif else begin
                print,'ONLY LFS LOS'
                selhead=1
             endelse

             if los_choice[1] eq 1 then addupframes=1 else addupframes=0

             widget_control,info.nframes_text,get_value=nframes
             frames=UINT(nframes[0])
        
             CHR={selhead:temporary(selhead),$
             addupframes:temporary(addupframes),$
             nframes:temporary(frames),$
             fitting_method:temporary(fitting_choice)}
             extraoptions={CHR:temporary(CHR),timeloc:timeloc}             
           
        endelse 
      end
      
      2:begin
        signalname='CMR'
        widget_control,info.time_choice_buttons_edge,get_value=time_choice
        if time_choice eq 2 then begin
            widget_control,info.starttime_edge,get_value=time_start
            widget_control,info.endtime_edge,get_value=time_end
        endif else begin
            time_start=0.
            time_end=0.   
        endelse       
        CMR={time_choice:time_choice,time_intervall:[time_start,time_end]}
        extraoptions={CMR:CMR}
      end

      3:begin
        signalname='CPR'
        widget_control,info.time_choice_buttons_edge,get_value=time_choice
        if time_choice eq 2 then begin
            widget_control,info.starttime_edge,get_value=time_start
            widget_control,info.endtime_edge,get_value=time_end
        endif else begin
            time_start=0.
            time_end=0.   
        endelse       

        CPR={time_choice:time_choice,time_intervall:[time_start,time_end]}
        extraoptions={CPR:CPR}
      end 
           
      4:begin
        signalname='CXH'
        ;widget_control,info.time_choice_buttons_cxh,get_value=time_choice
        for i=0,N_ELEMENTS(info.button_array)-1 do $
         if widget_info(info.button_array[i],/button_set) then time_choice=i
        print,time_choice
        widget_control,info.molecular_choice_buttons_cxh,get_value=molecular_choice
        CXH={time_choice:time_choice,molecular_choice:molecular_choice}
        extraoptions={CXH:CXH}
      end

      5:begin
        signalname='CVH'
        ;widget_control,info.time_choice_buttons_cxh,get_value=time_choice
        for i=0,N_ELEMENTS(info.button_array)-1 do IF $
        WIDGET_INFO(info.button_array[i],/button_set) THEN time_choice=i
        print,time_choice
        widget_control,info.molecular_choice_buttons_cxh,get_value=molecular_choice
        CVH={time_choice:time_choice,molecular_choice:molecular_choice}
        extraoptions={CVH:CVH}
      end

      6:begin
        signalname='LIA'
      end
      7:begin
         signalname='COR'
         widget_control,info.beam_buttons_ben1,get_value=beam_choice
         widget_control,info.beam_buttons_ben2,get_value=los_choice 
         widget_control,info.fitting_buttons,get_value=fitting_choice
         timeloc=1
         if beam_choice eq 0 then begin
            print,'Using All Time Frames'
            BeamMod=0
            BeamAv=0
            beamdip=0
         endif else begin
             if beam_choice eq 1 then begin
                 print,'Using Beam Modulation'
                 BeamMod=1
                 BeamAv=0
                 beamdip=0
             endif else begin
                    if beam_choice eq 2 then begin
                        print, 'Using Beam Modulation with Averaging'
                        BeamMod=0
                        BeamAv=1
                        beamdip=0
                    endif else begin
                        BeamMod=0
                        BeamAv=0
                        beamdip=1
                    endelse
             endelse
         endelse
         if los_choice[0] eq 0 then begin
            print,'ALL LOS'
            selhead=0
         endif else begin
            print,'ONLY LFS LOS'
            selhead=1
         endelse

         if los_choice[1] eq 1 then addupframes=1 else addupframes=0

         widget_control,info.nframes_text,get_value=nframes
         frames=UINT(nframes[0])
         
         COR={selhead:temporary(selhead),$
              addupframes:temporary(addupframes),$
              nframes:temporary(frames),$
              fitting_method:temporary(fitting_choice),$
              BeamAv:temporary(BeamAv),$ 
              beamdip:temporary(beamdip),$
              timeloc:temporary(timeloc)} 
         extraoptions={COR:temporary(COR)}
      end
      
      8:begin
         signalname='BOS'
         widget_control,info.beam_buttons_ben1,get_value=beam_choice
         widget_control,info.beam_buttons_ben2,get_value=los_choice 
         widget_control,info.fitting_buttons,get_value=fitting_choice
         if beam_choice eq 0 then begin
            print,'Using All Time Frames'
            BeamMod=0
            BeamAv=0
            beamdip=0
         endif else begin
            if beam_choice eq 1 then begin
                 print,'Using Beam Modulation'
                 BeamMod=1
                 BeamAv=0
                 beamdip=0
            endif else begin
                if beam_choice eq 2 then begin
                    print, 'Using Beam Modulation with Averaging'
                    BeamMod=0
                    BeamAv=1
                    beamdip=0
                endif else begin             
                    BeamMod=0
                    BeamAv=0
                    beamdip=1
               endelse
            endelse
         endelse
         if los_choice[0] eq 0 then begin
            print,'ALL LOS'
            selhead=0
         endif else begin
            print,'ONLY LFS LOS'
            selhead=1
         endelse
         if los_choice[1] eq 1 then addupframes=1 else addupframes=0

         widget_control,info.nframes_text,get_value=nframes
         frames=UINT(nframes[0])
         
         BOS={selhead:temporary(selhead),$
              addupframes:temporary(addupframes),$
              nframes:temporary(frames),$
              fitting_method:temporary(fitting_choice),$
              BeamAv:temporary(BeamAv),$ 
              beamdip:temporary(beamdip)}
         extraoptions={BOS:temporary(BOS)}
      end

      9:begin
         signalname='CNR'
         widget_control,info.time_choice_buttons_edge,get_value=time_choice
         
         
         if time_choice eq 2 then begin
            widget_control,info.starttime_edge,get_value=time_start
            widget_control,info.endtime_edge,get_value=time_end
         endif else begin
            time_start=0.
            time_end=0.   
         endelse       
         widget_control,info.molecular_choice_buttons_edge,get_value=molecular_choice
         
         CNR={time_choice:time_choice,time_intervall:[time_start,time_end], molecular_choice:molecular_choice}
         extraoptions={CNR:CNR} 
      end 
      
      10:begin
         signalname='CUR'
         widget_control,info.beam_buttons_ben1,get_value=beam_choice
         widget_control,info.beam_buttons_ben2,get_value=los_choice 
         widget_control,info.fitting_buttons,get_value=fitting_choice
         timeloc=1
         if beam_choice eq 0 then begin
            print,'Using All Time Frames'
            BeamMod=0
            BeamAv=0
            beamdip=0
         endif else begin
            if beam_choice eq 1 then begin
                 print,'Using Beam Modulation'
                 BeamMod=1
                 BeamAv=0
                 beamdip=0
            endif else begin
                if beam_choice eq 2 then begin
                    print, 'Using Beam Modulation with Averaging'
                    BeamMod=0
                    BeamAv=1
                    beamdip=0
                endif else begin
                    BeamMod=0
                    BeamAv=0
                    beamdip=1
                 endelse
            endelse
         endelse
         if los_choice[0] eq 0 then begin
            print,'ALL LOS'
            selhead=0
         endif else begin
            print,'ONLY LFS LOS'
            selhead=1
         endelse

         if los_choice[1] eq 1 then addupframes=1 else addupframes=0

         widget_control,info.nframes_text,get_value=nframes
         frames=UINT(nframes[0])
         
         CUR={selhead:temporary(selhead),$
              addupframes:temporary(addupframes),$
              nframes:temporary(frames),$
              fitting_method:temporary(fitting_choice),$
              BeamAv:temporary(BeamAv),$ 
              beamdip:temporary(beamdip),$
              timeloc:temporary(timeloc)}
         extraoptions={CUR:temporary(CUR)}
      end

      11:begin
        signalname='CAR'
        print,'in CAR in cxf_aug_read_e'
        widget_control,info.beam_buttons_core,get_value=beam_choice
        widget_control,info.beam_buttons_core1,get_value=roi_choice
        widget_control,info.beam_buttons_core2,get_value=cal_choice
       ; widget_control,info.beam_buttons_core3,get_value=head_choice ; AK
         widget_control,info.core_options,get_value=addupframes
       ; this is for testing
        ; I hard-code the option for time-dependent R,z,phi,costheta 
        ; if the others agree we can have either a widget option or define
        ; all core systems with time-dep and all edge systems without
        ; the time-dependence.
        timeloc=1
        print,'TIME-DEPENDENT R,Z,Phi - Under construction!!!'
      ;   if head_choice eq 0 then begin  ;AK
      ;      print,'Using all tracks'
      ;      SelHead=0
      ;  endif else begin
      ;      if head_choice eq 1 then begin
      ;          print,'Using CER head data'
      ;          SelHead=1
      ;      endif else begin
      ;          print,'Using CFP head data'
      ;          SelHead=2
      ;      endelse
      ;  endelse  
        if beam_choice eq 0 then begin
            print,'Using All Time Frames'
            BeamMod=0
        endif else begin
            if beam_choice eq 1 then begin
                print,'Using Beam Modulation'
                BeamMod=1
            endif else begin
                print,'Using Beam Modulation (only last frames)'
                BeamMod=2
            endelse
        endelse 
         if roi_choice eq 0 then begin
            print,'Using All ROI'
            ROIADD=0
        endif else begin
            print,'Adding ROI with like Major R'
            ROIADD=1
        endelse        
         if cal_choice eq 0 then begin
            print,'calibrating with neon file'
            passivecal=0
            neon=1
        endif else begin
            if cal_choice eq 1 then begin
                print,'using default calibrations'
                passivecal=0
                neon=0
            endif else begin
                print,'Calibrating with passive line'
                passivecal=1
                neon=0
            endelse
        endelse   
        
        if addupframes then begin
           widget_control,info.core_options_nframes,get_value=nframes
           frames=UINT(nframes[0])
           if frames lt 2 then begin
              addupframes=0
              frames=1
              print,'!!!!!!!!! Warning: Incorrect number of frames has been set'
              print,'Continuing with addupframes = 1 thus no frames are added up'
              print,'From cxf_aug_read_e.pro line ~ 394'
           endif else print,'Adding up #frames:',frames
        endif else begin
           frames=1
        endelse
       CAR={addupframes:addupframes,nframes:frames}
       extraoptions={CAR:CAR,timeloc:timeloc}

      end

      12:begin
        signalname='CBR'
        widget_control,info.beam_buttons_core,get_value=beam_choice
        widget_control,info.beam_buttons_core1,get_value=roi_choice
        widget_control,info.beam_buttons_core2,get_value=cal_choice
         widget_control,info.core_options,get_value=addupframes
        if beam_choice eq 0 then begin
            print,'Using All Time Frames'
            BeamMod=0
        endif else begin
            print,'Using Beam Modulation'
            BeamMod=1
        endelse 
         if roi_choice eq 0 then begin
            print,'Using All ROI'
            ROIADD=0
        endif else begin
            print,'Adding ROI with like Major R'
            ROIADD=1
        endelse        
         if cal_choice eq 0 then begin
            print,'calibrating with neon file'
            passivecal=0
            neon=1
        endif else begin
            if cal_choice eq 1 then begin
                print,'using default calibrations'
                passivecal=0
                neon=0
            endif else begin
                print,'Calibrating with passive line'
                passivecal=1
                neon=0
            endelse
        endelse           
      end   
      
      13:begin
        signalname='CCR'
        widget_control,info.beam_buttons_core,get_value=beam_choice
        widget_control,info.beam_buttons_core1,get_value=roi_choice
        widget_control,info.beam_buttons_core2,get_value=cal_choice
         widget_control,info.core_options,get_value=addupframes
        timeloc=1
        print,'TIME-DEPENDENT R,Z,Phi - Under construction!!!'
        extraoptions={timeloc:timeloc}
        if beam_choice eq 0 then begin
            print,'Using All Time Frames'
            BeamMod=0
        endif else begin
            print,'Using Beam Modulation'
            BeamMod=1
        endelse 
         if roi_choice eq 0 then begin
            print,'Using All ROI'
            ROIADD=0
        endif else begin
            print,'Adding ROI with like Major R'
            ROIADD=1
        endelse        
         if cal_choice eq 0 then begin
            print,'calibrating with neon file'
            passivecal=0
            neon=1
        endif else begin
            if cal_choice eq 1 then begin
                print,'using default calibrations'
                passivecal=0
                neon=0
            endif else begin
                print,'Calibrating with passive line'
                passivecal=1
                neon=0
            endelse
        endelse             
        if addupframes then begin
           widget_control,info.core_options_nframes,get_value=nframes
           frames=UINT(nframes[0])
           if frames lt 2 then begin
              addupframes=0
              frames=1
              print,'!!!!!!!!! Warning: Incorrect number of frames has been set'
              print,'Continuing with addupframes = 1 thus no frames are added up'
              print,'From cxf_aug_read_e.pro line ~ 394'
           endif else print,'Adding up #frames:',frames
        endif else begin
           frames=1
        endelse      
       CCR={addupframes:addupframes,nframes:frames}
       extraoptions={CCR:CCR,timeloc:timeloc}
      end 
      
      14:begin
      signalname = 'CFR'
       widget_control,info.beam_buttons_core,get_value=beam_choice
        widget_control,info.beam_buttons_core1,get_value=roi_choice
        widget_control,info.beam_buttons_core2,get_value=cal_choice
         widget_control,info.core_options,get_value=addupframes
        timeloc=1
        print,'TIME-DEPENDENT R,Z,Phi - Under construction!!!'
        if beam_choice eq 0 then begin
            print,'Using All Time Frames'
            BeamMod=0
        endif else begin
            if beam_choice eq 1 then begin
                print,'Using Beam Modulation'
                BeamMod=1
            endif else begin
                print,'Using Beam Modulation (only last frames)'
                BeamMod=2
            endelse
        endelse 
         if roi_choice eq 0 then begin
            print,'Using All ROI'
            ROIADD=0
        endif else begin
            print,'Adding ROI with like Major R'
            ROIADD=1
        endelse        
         if cal_choice eq 0 then begin
            print,'calibrating with neon file'
            passivecal=0
            neon=1
        endif else begin
            if cal_choice eq 1 then begin
                print,'using default calibrations'
                passivecal=0
                neon=0
            endif else begin
                print,'Calibrating with passive line'
                passivecal=1
                neon=0
            endelse
        endelse   
        
        if addupframes then begin
           widget_control,info.core_options_nframes,get_value=nframes
           frames=UINT(nframes[0])
           if frames lt 2 then begin
              addupframes=0
              frames=1
              print,'!!!!!!!!! Warning: Incorrect number of frames has been set'
              print,'Continuing with addupframes = 1 thus no frames are added up'
              print,'From cxf_aug_read_e.pro line ~ 394'
           endif else print,'Adding up #frames:',frames
        endif else begin
           frames=1
        endelse
       CFR={addupframes:addupframes,nframes:frames}
       extraoptions={CFR:CFR,timeloc:timeloc}
      end
      
      16:begin 
      signalname = 'FVS'
      end
      
      17:begin
      signalname = 'EVS'
      end  

      20:begin
      signalname = 'CEF'
      end 
              
      ;11:begin
         ;signalname='COR+CUR'
         ;widget_control,info.beam_buttons_ben1,get_value=beam_choice
         ;widget_control,info.beam_buttons_ben2,get_value=los_choice 
         ;widget_control,info.fitting_buttons,get_value=fitting_choice
         ;if beam_choice eq 0 then begin
            ;print,'Using All Time Frames'
            ;BeamMod=0
         ;endif else begin
            ;print,'Using Beam Modulation'
            ;BeamMod=1
         ;endelse
         ;if los_choice[0] eq 0 then begin
            ;print,'ALL LOS'
            ;selhead=0
         ;endif else begin
            ;print,'ONLY LFS LOS'
            ;selhead=1
         ;endelse

         ;if los_choice[1] eq 1 then addupframes=1 else addupframes=0

         ;widget_control,info.nframes_text,get_value=nframes
         ;frames=UINT(nframes[0])
         
         ;CORCUR={selhead:temporary(selhead),$
              ;addupframes:temporary(addupframes),$
              ;nframes:temporary(frames),$
              ;fitting_method:temporary(fitting_choice)}
         ;extraoptions={CORCUR:temporary(CORCUR)}
      ;end


      else:
      
    endcase

    cxf_define_spectrometer_data,specdata,err
    if err ne '' then begin
      print,err
      stop
    endif

    specdata.shot_nr=pulse
    specdata.spectrometer.name=signalname
    specdata.machine.name='AUG'
    ;stop
    cxf_load_aug_data,specdata,err,extraoptions=extraoptions,Beammod=Beammod,passivecal=passivecal,roiadd=roiadd,neon=neon,cfrdata=cfrdata
    if err ne '' then begin
      res = dialog_message(err,/ERROR)
      return
    endif else begin
      widget_control,info.topinfo_base,get_uvalue=topinfo

      ptr_free,topinfo.expdata
      topinfo.expdata=ptr_new(specdata)
    
      widget_control,info.topinfo_base,set_uvalue=topinfo
      cxf_data_loaded,topinfo
      ;widget_control,topinfo.data_loaded,send_event={id:topinfo.data_loaded,top:info.topinfo_base,handler:info.topinfo_base}
      widget_control,event.top,/destroy

      return
    endelse
  endif





end
