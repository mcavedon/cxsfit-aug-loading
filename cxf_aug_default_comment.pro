function cxf_aug_default_comment,specdata,fitparam,outdata
  title = cxf_version()+' '+strupcase(specdata.machine.name) $
         +':'+strupcase(specdata.spectrometer.name) $
         +' #'+strtrim(string(specdata.shot_nr),2)
  nlines = (*fitparam.initial).nlines
  line_desc = ((*fitparam.initial).line.description)[0:nlines-1]
  IF nlines EQ 1 THEN $
    lines = 'Fit with 1 gaussian: "'+STRTRIM(line_desc[0],2)+'"' $
  ELSE BEGIN
    lines = 'Fit with '+STRTRIM(STRING(nlines),2)+' gaussians: "' $
           +STRTRIM(line_desc[0],2)+'"'
    j = 0L
    FOR i=1,nlines-1 DO BEGIN
      IF STRLEN(lines[j]+', "'+STRTRIM(line_desc[i],2)+'"') LE 71L THEN $
        lines[j] = lines[j]+', "'+STRTRIM(line_desc[i],2)+'"' $
      ELSE BEGIN
        lines[j] = lines[j]+','
        j = j+1
        lines = [lines,'']
        lines[j] = '   "'+STRTRIM(line_desc[i],2)+'"'
      ENDELSE
    ENDFOR
  ENDELSE
  whtime = WHERE(TOTAL(outdata.fit_status > 0,2) GT 0, ntime)
  IF ntime EQ 0 THEN BEGIN
    print,'No valid fits available!'
    first_time = 0.0 & last_time = 0.0
  ENDIF ELSE BEGIN
    first_time = MIN(specdata.time[whtime])
     last_time = MAX(specdata.time[whtime])
  ENDELSE
  times = 'First fit time: '+STRTRIM(STRING(first_time,FORMAT='(F7.3)'),2) $
        +', Last fit time: '+STRTRIM(STRING( last_time,FORMAT='(F7.3)'),2)
  comment = [title,lines,times,'']
  return,comment
end  
