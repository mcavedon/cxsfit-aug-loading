@/afs/ipp-garching.mpg.de/home/c/cxrs/idl/cxsfit64/machines/aug/cxf_aug_decompose_corcur.pro

pro cxf_aug_save_e,event

  widget_control,event.top,get_uvalue=info

  if event.id eq info.cancel_button then begin
    widget_control,event.top,/destroy
    return
  endif
  
  if event.id eq info.ok_button then begin
    widget_control,/hourglass
    widget_control,info.experiment,get_value=experiments
    experiment_id=widget_info(info.experiment,/droplist_select)
    experiment = experiments[experiment_id]
    widget_control,info.comment_text,get_value=comment

    widget_control, info.topinfo_base, get_uvalue=topinfo
    specdata = *topinfo.expdata
    ;extdata  =  topinfo.externaldata ; might be a null pointer
    outdata  = *topinfo.outdata
    if outdata.spectrometer eq 'COR+CUR' then begin
       help,specdata
       help,outdata
       stop
    endif
    widget_control, topinfo.panel_window, get_value=fitparam
    history  = *topinfo.historydata
    sel=cxf_lastclear_index(history)
    history=history[sel:*]
    
    CASE strupcase(outdata.spectrometer) OF
         'COR+CUR' : begin
                     print,outdata.spectrometer
                     print,experiment
                     print,comment
                     print,fitparam
                     ;;;;;;;;;
                     cxf_aug_decompose_corcur,'AUGD',outdata.shot_nr,specdata,outdata,specdata_cor,specdata_cur,outdata_cor,outdata_cur

                     cxf_save_aug_fit,experiment,comment,specdata_cor,fitparam,extdata,outdata_cor,history,err
                     print,err
                     cxf_save_aug_fit,experiment,comment,specdata_cur,fitparam,extdata,outdata_cur,history,err
                     print,err
                     end
         'CXMR' : begin
                  cxf_save_aug_fit_test,experiment,comment,specdata,fitparam,extdata,outdata,history,err
                  end
    ELSE : BEGIN
           cxf_save_aug_fit,experiment,comment,specdata,fitparam,extdata,outdata,history,err
    END
    ENDCASE
    
    ;if strupcase(outdata.spectrometer) EQ 'CXMR' then begin
    ;    cxf_save_aug_fit_test,experiment,comment,specdata,fitparam,extdata,outdata,history,err
    ;endif else begin
    ;    cxf_save_aug_fit,experiment,comment,specdata,fitparam,extdata,outdata,history,err
    ;endelse
    if err ne '' then begin
      res = dialog_message(err,/ERROR)
      return
    endif else begin
      widget_control,event.top,/destroy
      return
    endelse
  endif

end
