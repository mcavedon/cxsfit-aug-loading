pro cxf_external_shotfile_w,topinfo_base,ti=ti,af=af,err=err

	err=''

	; check ti/af switches, return err if stupid

	if keyword_set(ti) eq keyword_set(af) then begin
		err='Please specify either /ti or /af'
		return
	endif

	if keyword_set(ti) then begin
		strsw='Ti'
		ti_or_af=0
	endif

	if keyword_set(af) then begin
		strsw='AF'
		ti_or_af=1
	endif

	pulse=-1

        Widget_Control, topinfo_base, get_uvalue=topinfo

	tlb=widget_base(title='External '+strsw+' Read',/column)

        if ti_or_af eq 0 then begin
		IF Ptr_Valid(topinfo.externaldata) THEN $
	          IF Ptr_Valid((*topinfo.externaldata).ti_src) THEN $
        	    IF (*(*topinfo.externaldata).ti_src).type EQ 'SF' THEN $
	              shotfile = (*(*topinfo.externaldata).ti_src)
	endif else begin
		IF Ptr_Valid(topinfo.externaldata) THEN $
	          IF Ptr_Valid((*topinfo.externaldata).af_src) THEN $
        	    IF (*(*topinfo.externaldata).af_src).type EQ 'SF' THEN $
	              shotfile = (*(*topinfo.externaldata).af_src)
	endelse
        
	shotfile_widget = cxf_get_aug_shotfile_cw(tlb, shotfile=shotfile)

        base=widget_base(tlb,/row)

        ok_button=widget_button(base,value='Ok')
  	cancel_button=widget_button(base,value='Cancel')

  	info={ 	topinfo_base:topinfo_base , $
		pulse:pulse, $
		ti_or_af:ti_or_af, $
                shotfile_widget:shotfile_widget, $
	 	ok_button:ok_button, $
	 	cancel_button:cancel_button $
       		}

	widget_control,tlb,set_uvalue=info      								
  	widget_control, tlb, /realize
  	xmanager, 'cxf_external_shotfile_w',tlb,event_handler='cxf_external_shotfile_e'



end
