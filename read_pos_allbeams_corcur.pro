; C.Bruhn - 31032017
; Old/other program for reading this 
; (R,z,phi locations for the different beam combinations) for the COR and CUR
pro read_pos_allbeams_corcur,file,nlos,los_str,bm1,bm2,bm3,bm4,$
                            bm12,bm13,bm14,bm23,bm24,bm34,$
                            bm123,bm124,bm134,bm234,bm1234

; read the file
restore,'/afs/ipp/home/c/cxrs/idl/cxsfit/machines/aug/pos_allbeams_template.sav'
res = read_ascii(file,template=pos_allbeams_template)
los=res.los[0:nlos-1]

bm1_R    = res.R[0:nlos-1]          &  bm1_dR    = res.dR[0:nlos-1]          
bm2_R    = res.R[nlos:2*nlos-1]     &  bm2_dR    = res.dR[nlos:2*nlos-1]     
bm3_R    = res.R[2*nlos:3*nlos-1]   &  bm3_dR    = res.dR[2*nlos:3*nlos-1]   
bm4_R    = res.R[3*nlos:4*nlos-1]   &  bm4_dR    = res.dR[3*nlos:4*nlos-1]   
bm12_R   = res.R[4*nlos:5*nlos-1]   &  bm12_dR   = res.dR[4*nlos:5*nlos-1]   
bm13_R   = res.R[5*nlos:6*nlos-1]   &  bm13_dR   = res.dR[5*nlos:6*nlos-1]   
bm14_R   = res.R[6*nlos:7*nlos-1]   &  bm14_dR   = res.dR[6*nlos:7*nlos-1]   
bm23_R   = res.R[7*nlos:8*nlos-1]   &  bm23_dR   = res.dR[7*nlos:8*nlos-1]   
bm24_R   = res.R[8*nlos:9*nlos-1]   &  bm24_dR   = res.dR[8*nlos:9*nlos-1]   
bm34_R   = res.R[9*nlos:10*nlos-1]  &  bm34_dR   = res.dR[9*nlos:10*nlos-1]  
bm123_R  = res.R[10*nlos:11*nlos-1] &  bm123_dR  = res.dR[10*nlos:11*nlos-1] 
bm124_R  = res.R[11*nlos:12*nlos-1] &  bm124_dR  = res.dR[11*nlos:12*nlos-1] 
bm134_R  = res.R[12*nlos:13*nlos-1] &  bm134_dR  = res.dR[12*nlos:13*nlos-1] 
bm234_R  = res.R[13*nlos:14*nlos-1] &  bm234_dR  = res.dR[13*nlos:14*nlos-1] 
bm1234_R = res.R[14*nlos:15*nlos-1] &  bm1234_dR = res.dR[14*nlos:15*nlos-1] 
bm1_z    = res.z[0:nlos-1]          &  bm1_dz    = res.dz[0:nlos-1]          
bm2_z    = res.z[nlos:2*nlos-1]     &  bm2_dz    = res.dz[nlos:2*nlos-1]     
bm3_z    = res.z[2*nlos:3*nlos-1]   &  bm3_dz    = res.dz[2*nlos:3*nlos-1]   
bm4_z    = res.z[3*nlos:4*nlos-1]   &  bm4_dz    = res.dz[3*nlos:4*nlos-1]   
bm12_z   = res.z[4*nlos:5*nlos-1]   &  bm12_dz   = res.dz[4*nlos:5*nlos-1]   
bm13_z   = res.z[5*nlos:6*nlos-1]   &  bm13_dz   = res.dz[5*nlos:6*nlos-1]   
bm14_z   = res.z[6*nlos:7*nlos-1]   &  bm14_dz   = res.dz[6*nlos:7*nlos-1]   
bm23_z   = res.z[7*nlos:8*nlos-1]   &  bm23_dz   = res.dz[7*nlos:8*nlos-1]   
bm24_z   = res.z[8*nlos:9*nlos-1]   &  bm24_dz   = res.dz[8*nlos:9*nlos-1]   
bm34_z   = res.z[9*nlos:10*nlos-1]  &  bm34_dz   = res.dz[9*nlos:10*nlos-1]  
bm123_z  = res.z[10*nlos:11*nlos-1] &  bm123_dz  = res.dz[10*nlos:11*nlos-1] 
bm124_z  = res.z[11*nlos:12*nlos-1] &  bm124_dz  = res.dz[11*nlos:12*nlos-1] 
bm134_z  = res.z[12*nlos:13*nlos-1] &  bm134_dz  = res.dz[12*nlos:13*nlos-1] 
bm234_z  = res.z[13*nlos:14*nlos-1] &  bm234_dz  = res.dz[13*nlos:14*nlos-1] 
bm1234_z = res.z[14*nlos:15*nlos-1] &  bm1234_dz = res.dz[14*nlos:15*nlos-1] 
bm1_phi    = res.phi[0:nlos-1]          &  bm1_dphi    = res.dphi[0:nlos-1]          
bm2_phi    = res.phi[nlos:2*nlos-1]     &  bm2_dphi    = res.dphi[nlos:2*nlos-1]     
bm3_phi    = res.phi[2*nlos:3*nlos-1]   &  bm3_dphi    = res.dphi[2*nlos:3*nlos-1]   
bm4_phi    = res.phi[3*nlos:4*nlos-1]   &  bm4_dphi    = res.dphi[3*nlos:4*nlos-1]   
bm12_phi   = res.phi[4*nlos:5*nlos-1]   &  bm12_dphi   = res.dphi[4*nlos:5*nlos-1]   
bm13_phi   = res.phi[5*nlos:6*nlos-1]   &  bm13_dphi   = res.dphi[5*nlos:6*nlos-1]   
bm14_phi   = res.phi[6*nlos:7*nlos-1]   &  bm14_dphi   = res.dphi[6*nlos:7*nlos-1]   
bm23_phi   = res.phi[7*nlos:8*nlos-1]   &  bm23_dphi   = res.dphi[7*nlos:8*nlos-1]   
bm24_phi   = res.phi[8*nlos:9*nlos-1]   &  bm24_dphi   = res.dphi[8*nlos:9*nlos-1]   
bm34_phi   = res.phi[9*nlos:10*nlos-1]  &  bm34_dphi   = res.dphi[9*nlos:10*nlos-1]  
bm123_phi  = res.phi[10*nlos:11*nlos-1] &  bm123_dphi  = res.dphi[10*nlos:11*nlos-1] 
bm124_phi  = res.phi[11*nlos:12*nlos-1] &  bm124_dphi  = res.dphi[11*nlos:12*nlos-1] 
bm134_phi  = res.phi[12*nlos:13*nlos-1] &  bm134_dphi  = res.dphi[12*nlos:13*nlos-1] 
bm234_phi  = res.phi[13*nlos:14*nlos-1] &  bm234_dphi  = res.dphi[13*nlos:14*nlos-1] 
bm1234_phi = res.phi[14*nlos:15*nlos-1] &  bm1234_dphi = res.dphi[14*nlos:15*nlos-1] 

; group things together... I know...
los_str = los
bm1 = {R:bm1_R, dr:bm1_dR, z:bm1_z, dz:bm1_dz, phi:bm1_phi, dphi:bm1_dphi}
bm2 = {R:bm2_R, dr:bm2_dR, z:bm2_z, dz:bm2_dz, phi:bm2_phi, dphi:bm2_dphi}
bm3 = {R:bm3_R, dr:bm3_dR, z:bm3_z, dz:bm3_dz, phi:bm3_phi, dphi:bm3_dphi}
bm4 = {R:bm4_R, dr:bm4_dR, z:bm4_z, dz:bm4_dz, phi:bm4_phi, dphi:bm4_dphi}
bm12 = {R:bm12_R, dr:bm12_dR, z:bm12_z, dz:bm12_dz, phi:bm12_phi, dphi:bm12_dphi}
bm13 = {R:bm13_R, dr:bm13_dR, z:bm13_z, dz:bm13_dz, phi:bm13_phi, dphi:bm13_dphi}
bm14 = {R:bm14_R, dr:bm14_dR, z:bm14_z, dz:bm14_dz, phi:bm14_phi, dphi:bm14_dphi}
bm23 = {R:bm23_R, dr:bm23_dR, z:bm23_z, dz:bm23_dz, phi:bm23_phi, dphi:bm23_dphi}
bm24 = {R:bm24_R, dr:bm24_dR, z:bm24_z, dz:bm24_dz, phi:bm24_phi, dphi:bm24_dphi}
bm34 = {R:bm34_R, dr:bm34_dR, z:bm34_z, dz:bm34_dz, phi:bm34_phi, dphi:bm34_dphi}
bm123 = {R:bm123_R, dr:bm123_dR, z:bm123_z, dz:bm123_dz, phi:bm123_phi, dphi:bm123_dphi}
bm124 = {R:bm124_R, dr:bm124_dR, z:bm124_z, dz:bm124_dz, phi:bm124_phi, dphi:bm124_dphi}
bm134 = {R:bm134_R, dr:bm134_dR, z:bm134_z, dz:bm134_dz, phi:bm134_phi, dphi:bm134_dphi}
bm234 = {R:bm234_R, dr:bm234_dR, z:bm234_z, dz:bm234_dz, phi:bm234_phi, dphi:bm234_dphi}
bm1234 = {R:bm1234_R, dr:bm1234_dR, z:bm1234_z, dz:bm1234_dz, phi:bm1234_phi, dphi:bm1234_dphi}

end
