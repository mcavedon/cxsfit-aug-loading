
import numpy as np
import struct

def Valve_information(HEB, shot_Nr):

    ## --- read the SIMATIC part of the shotfile ----------------------------------------------------------------
    if 'SimSig' in HEB.getObjectNames().values(): # check if simatic brick exists
        print HEB('SimSig').data.astype(np.uint8)
        simatic = HEB('SimSig').data.astype(np.uint8)[:148]
        # translates the bitstring into readable data
        simatic_clear = struct.unpack('>BBBBff8sfffIIfIIfffffff8sfffIIfIIfffffff', simatic)
        # assigns the entries to the specific values
        Valve_dict = {}
        Valve_dict['seg13_pressure'] = round(simatic_clear[4],3)
        Valve_dict['seg13_t_start'] = simatic_clear[10]/1000.   # s
        Valve_dict['seg13_periode'] = simatic_clear[11]         # ms
        Valve_dict['seg13_length'] = simatic_clear[12]          # ms
        Valve_dict['seg13_N_pulse'] = int(simatic_clear[14])    # performed puffs
        Valve_dict['seg13_t_stop'] = Valve_dict['seg13_t_start']+\
            Valve_dict['seg13_periode']/1000.*Valve_dict['seg13_N_pulse'] #s
        Valve_dict['seg13_temperature'] = float('{:0.1f}'.format(simatic_clear[17]))
        Valve_dict['seg13_flux_par'] = float('{:0.2e}'.format(simatic_clear[18]))
        Valve_dict['seg13_flux_el'] = float('{:0.2e}'.format(simatic_clear[21]))
        Valve_dict['seg13_gas'] = simatic_clear[6].replace('\x00','')

        Valve_dict['seg16_pressure'] = round(simatic_clear[5],3)
        Valve_dict['seg16_t_start'] = simatic_clear[26]/1000.   # s
        Valve_dict['seg16_periode'] = simatic_clear[27]         # ms
        Valve_dict['seg16_length'] = simatic_clear[29]          # ms
        Valve_dict['seg16_N_pulse'] = int(simatic_clear[30])    # performed puffs
        Valve_dict['seg16_t_stop'] = Valve_dict['seg13_t_start']+\
            Valve_dict['seg16_periode']/1000.*Valve_dict['seg13_N_pulse'] #s
        Valve_dict['seg16_temperature'] = float('{:0.1f}'.format(simatic_clear[33]))
        Valve_dict['seg16_flux_par'] = float('{:0.2e}'.format(simatic_clear[34]))
        Valve_dict['seg16_flux_el'] = float('{:0.2e}'.format(simatic_clear[37]))
        Valve_dict['seg16_gas'] = simatic_clear[22].replace('\x00','')

        print('   ---> Valve data read from shotfile')

    # --- in case no simatic object exists (for older discharges) -------------------------------------------------
    else: # try to laod a logfile
        import os
        import sys
        log_filepath = '/afs/ipp/home/m/mgrien/public/public_write/HEB_log_files/'
        all_filenames = os.listdir(log_filepath)

        # check if log file exists for shot_Nr
        filename = [x for x in all_filenames if ('seg13-'+str(shot_Nr)) in x]
        if len(filename) == 0:
            print('No match in the log files for valve parameters!')
            Valve_dict = {}
        elif len(filename) >1 :
            print('Too many files found for valve parameters!')
        else: # file found
            # --- create a dictionary of the entries for Se 13 ---
            log_information = np.loadtxt(log_filepath+filename[0],dtype = str, delimiter = ':')
            log_dict = {}
            for ii in range(len(log_information)):
                log_dict[log_information[ii,0].strip()] = log_information[ii,1]

            # --- read the pressure ---
            if 'Pressure (0..0.5 bar)' in log_dict.keys():
                pressure = float(log_dict['Pressure (0..0.5 bar)'].strip().replace(',','.').replace('bar',''))
                if pressure > 0.48: # use other sensor
                    pressure = float(log_dict['Pressure (0..5 bar)'].strip().replace(',','.').replace('bar',''))
            elif 'Pressure (0..1 bar)' in log_dict.keys(): # for Se16 valve
                pressure = float(log_dict['Pressure (0..1 bar)'].strip().replace(',','.').replace('bar',''))
                if pressure > 0.95: # use other sensor
                    pressure = float(log_dict['Pressure (0..20 bar)'].strip().replace(',','.').replace('bar',''))
            else:
                pressure = float(log_dict['Pressure'].strip().replace(',','.').replace('bar',''))

            # --- read or calc the flux ---
            if 'Parts per second' in log_dict.keys(): # flux info exists
                seg13_flux_par =  float(log_dict['Parts per second'].strip().replace(',','.'))
                seg13_flux_el  =  float(log_dict['Electrons per second'].strip().replace(',','.'))
                print('   ---> Valve data read from log_file')
            else: # calcualte the flux from pressure
                sys.path.append('/afs/ipp/home/m/mgrien/Python/HEB/')
                import Valve_pressure_to_flowrate
                reload(Valve_pressure_to_flowrate)

                # --- dictionary for gas correction factors ---
                gas_cor = {}
                gas_names = ['He', 'D', 'H', 'Ne', 'N', 'Ar']
                flux_corr = [1, 0.943, 1.33, 0.445, 0.356, 0.316]
                el_per_part = [2, 2, 2, 10, 14, 18]
                for ii in range(len(gas_names)):
                    gas_cor[gas_names[ii]] = [flux_corr[ii],  el_per_part[ii]]
                # -----------36182-----------------------------------
                gas = log_dict['Gas'].strip()
                if gas in gas_cor.keys():
                    seg13_flux_par = Valve_pressure_to_flowrate.S13_p_to_flowrate(pressure*1e3)*gas_cor[gas][0]
                    seg13_flux_el = seg13_flux_par*gas_cor[gas][1]
                    print('   ---> Valve data calculated from log_file ')
                else:
                    print('Wrong Gas, flux can not be calculated!')

            if seg13_flux_par == 0:
                print('WARNING, Gas flux is 0!')
                seg13_flux_par = 1
                seg13_flux_el = 1

            # --- fill in the dictionarry ---
            Valve_dict = {}
            Valve_dict['seg13_pressure'] = pressure
            Valve_dict['seg13_t_start'] = float(log_dict['Delay'].strip().replace(',','.').replace('ms',''))/1e3    # s
            Valve_dict['seg13_periode'] = float(log_dict['Interval'].strip().replace(',','.').replace('ms',''))     # ms
            Valve_dict['seg13_length'] = float(log_dict['Pulse length'].strip().replace(',','.').replace('ms',''))  # ms
            if 'Number of pulses' in log_dict.keys():
                Valve_dict['seg13_N_pulse'] = int(log_dict['Number of pulses'].strip().replace(',','.').replace('ms',''))
            else:
                Valve_dict['seg13_N_pulse'] = int(log_dict['Number of pulses done'].strip().replace(',','.').replace('ms',''))
            Valve_dict['seg13_t_stop'] = Valve_dict['seg13_t_start']+\
                Valve_dict['seg13_periode']/1000.*Valve_dict['seg13_N_pulse'] #s
            if 'Temperature' in log_dict.keys():
                Valve_dict['seg13_temperature'] = float(log_dict['Temperature'].strip().\
                    replace(',','.').replace('\xb0C',''))
            else:
                Valve_dict['seg13_temperature'] = -100

            Valve_dict['seg13_flux_par'] = float('{:0.2e}'.format(seg13_flux_par))
            Valve_dict['seg13_flux_el'] = float('{:0.2e}'.format(seg13_flux_el))
            Valve_dict['seg13_gas'] = log_dict['Gas'].strip()

    # --- check the dictionary ---
    if Valve_dict is None:
        print('No valve information are found for the gas flux, in "valve_information.py"')
        sys.exit()
    # temperature of the valve and gas is the same, if valve temperature does not exist, write one for the gas
    if Valve_dict['seg13_temperature'] < 5:
        Valve_dict['seg13_temp_gas'] = 300
    else:
        Valve_dict['seg13_temp_gas'] = Valve_dict['seg13_temperature']+273

    # --- return the generated Valve dictionary ---
    return Valve_dict



if __name__ == "__main__":
    import dd

    shots=np.arange( 35738, 35762)
    experiments=['augd', 'mgrien', 'ulp']
    for shot_Nr in shots:
		for experiment in experiments:
			try:
				HEB = dd.shotfile('HEB', shot_Nr, experiment = experiment)
				Valve_dict = Valve_information(HEB, shot_Nr)
				print str(shot_Nr) +': ' +str(Valve_dict['seg16_gas'])+', ' +str(Valve_dict\
				['seg16_pressure'])+'\n'
			except:
				pass





