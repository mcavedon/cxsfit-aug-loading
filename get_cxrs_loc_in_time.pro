; A.Kappatou 28012014 
; This routine reads in the beams and defines which beams are on at any given time.
; Then it restores the values for R,z,phi for beam combinations 
; and finally creates the R,z,phi,costheta time dependent arrays.

@/afs/ipp-garching.mpg.de/u/cxrs/idl/cxsfit64/machines/aug/get_nbi_on.pro 
@/afs/ipp-garching.mpg.de/u/cxrs/idl/cxsfit64/machines/aug/read_pos_allbeams.pro
pro get_cxrs_loc_in_time,shot,diag,spectrometer_data,time_loc

; read which beams are on
; -----------------------
cxrstime=spectrometer_data.time
ntm=n_elements(cxrstime)
; define beam box by diagnostic:
if diag eq 'CAR' OR diag eq 'CBR' OR diag eq 'CCR' OR diag eq 'CFR'  or $
   diag eq 'CER' OR diag eq 'CMR' OR diag eq 'CPR' or diag eq 'CHR' then beambox=1
if diag eq 'car' or diag eq 'cbr' or diag eq 'ccr' or $
   diag eq 'cer' or diag eq 'cmr' or diag eq 'cpr' or diag eq 'chr' then beambox=1
if diag eq 'COR' OR diag eq 'CUR' then beambox=2
if diag eq 'cor' or diag eq 'cur' then beambox=2
if diag eq 'COR+CUR' OR diag eq 'CORCUR' then beambox=2
if diag eq 'cor+cur' or diag eq 'corcur' then beambox=2
if diag eq 'CEF' and shot lt 38200 then beambox = 1
if diag eq 'CEF' and shot gt 38200 then beambox = 2

losname=spectrometer_data.los_name
if (diag eq 'CHR') OR (diag eq 'CAR' and shot gt 34995) then begin
    ;need to add the "--" to the end of the LOS names!
   for i=0,n_elements(spectrometer_data.LOS_name)-1 do losname[i]=strtrim(losname[i],2)+'--'
endif


nbion=fltarr(ntm,4)
if beambox eq 1 then begin
    for isource=1,4 do begin
        ; NBI box 1
        get_nbi_on,long(shot),isource,cxrstime,timeout,PNBIout,nbionout
        nbion[*,isource-1]=nbionout
        defsource = 3 ; default source for box 1
    endfor
endif
if beambox eq 2 then begin
    for isource=5,8 do begin
        ; NBI box 2
        get_nbi_on,long(shot),isource,cxrstime,timeout,PNBIout,nbionout
        nbion[*,isource-5]=nbionout
        defsource = 8 ; default source for box 2
    endfor
endif

; initialise location arrays
; --------------------------
R_pos_time   =fltarr(spectrometer_data.ntrack,spectrometer_data.nframe)
z_pos_time   =fltarr(spectrometer_data.ntrack,spectrometer_data.nframe)
phi_pos_time =fltarr(spectrometer_data.ntrack,spectrometer_data.nframe)
costheta_time=fltarr(spectrometer_data.ntrack,spectrometer_data.nframe)
dR_pos_time   =fltarr(spectrometer_data.ntrack,spectrometer_data.nframe)
dz_pos_time   =fltarr(spectrometer_data.ntrack,spectrometer_data.nframe)
dphi_pos_time =fltarr(spectrometer_data.ntrack,spectrometer_data.nframe)

; read the file that contains the different locations
; for the different combinations of beams

; ==================== BOX 1 ==================================================
if beambox eq 1 AND  shot ge 25890 then begin

p='/afs/ipp-garching.mpg.de/home/c/cxrs/idl/CER_ausw/load_cer_2009_routines/calib_files/'

if shot ge 38201 then file = p+'CER_RZ_position_allbeams_2021.txt'  
;keeping this file for 2022, LOS positions unchanged - RMM Nov. 2021.
if shot ge 36800 and shot le 38200 then file = p+'CER_RZ_position_allbeams_2020.txt'
if shot ge 34996 and shot le 36799 then file = p+'CER_RZ_position_allbeams_2018.txt'
if shot ge 33725 and shot le 34995 then file = p+'CER_RZ_position_allbeams_2017.txt'
if shot ge 31890 and shot le 33724 then file = p+'CER_RZ_position_allbeams_2015.txt'
if shot ge 30150 and shot le 31889 then file = p+'CER_RZ_position_allbeams_2014.txt'
if shot ge 27393 and shot le 30149 then file=p+'CER_RZ_position_allbeams_2013.txt'
if shot ge 25891 and shot le 27392 then file=p+'CER_RZ_position_allbeams_2010_2011.txt'


read_pos_allbeams,file,los_str,bm1,bm2,bm3,bm4,$
                       bm12,bm13,bm14,bm23,bm24,bm34,$
                       bm123,bm124,bm134,bm234,bm1234

; define the tracks used
nfibers = n_elements(spectrometer_data.LOS_name)
whlos = fltarr(nfibers)
for i=0,nfibers-1 do begin
    whlos[i]=where(LOS_str eq LOSname[i])
endfor

;loop through time points
for i=0,ntm-1 do begin
    ; -- all --
    ; 1 & 2 & 3 & 4
    if nbion[i,0] eq 1 AND nbion[i,1] eq 1 AND nbion[i,2] eq 1  AND nbion[i,3] eq 1 then begin
        R_pos_time[*,i]=bm1234.R[whlos]
        z_pos_time[*,i]=bm1234.z[whlos]
        phi_pos_time[*,i]=bm1234.phi[whlos]
        dR_pos_time[*,i]=bm1234.dR[whlos]
        dz_pos_time[*,i]=bm1234.dz[whlos]
        dphi_pos_time[*,i]=bm1234.dphi[whlos]
    endif 
    ; -- three sources --
    ; 1 & 2 & 3
    if nbion[i,0] eq 1 AND nbion[i,1] eq 1 AND nbion[i,2] eq 1  AND nbion[i,3] eq 0 then begin
        R_pos_time[*,i]=bm123.R[whlos]
        z_pos_time[*,i]=bm123.z[whlos]
        phi_pos_time[*,i]=bm123.phi[whlos]
        dR_pos_time[*,i]=bm123.dR[whlos]
        dz_pos_time[*,i]=bm123.dz[whlos]
        dphi_pos_time[*,i]=bm123.dphi[whlos]
    endif 
    ; 2 & 3 & 4
    if nbion[i,0] eq 0 AND nbion[i,1] eq 1 AND nbion[i,2] eq 1  AND nbion[i,3] eq 1 then begin
        R_pos_time[*,i]=bm234.R[whlos]
        z_pos_time[*,i]=bm234.z[whlos]
        phi_pos_time[*,i]=bm234.phi[whlos]
        dR_pos_time[*,i]=bm234.dR[whlos]
        dz_pos_time[*,i]=bm234.dz[whlos]
        dphi_pos_time[*,i]=bm234.dphi[whlos]
    endif 
    ; 1 & 3 & 4
    if nbion[i,0] eq 1 AND nbion[i,1] eq 0 AND nbion[i,2] eq 1  AND nbion[i,3] eq 1 then begin
        R_pos_time[*,i]=bm134.R[whlos]
        z_pos_time[*,i]=bm134.z[whlos]
        phi_pos_time[*,i]=bm134.phi[whlos]
        dR_pos_time[*,i]=bm134.dR[whlos]
        dz_pos_time[*,i]=bm134.dz[whlos]
        dphi_pos_time[*,i]=bm134.dphi[whlos]
    endif 
    ; 1 & 2 & 4
    if nbion[i,0] eq 1 AND nbion[i,1] eq 1 AND nbion[i,2] eq 0  AND nbion[i,3] eq 1 then begin
        R_pos_time[*,i]=bm124.R[whlos]
        z_pos_time[*,i]=bm124.z[whlos]
        phi_pos_time[*,i]=bm124.phi[whlos]
        dR_pos_time[*,i]=bm124.dR[whlos]
        dz_pos_time[*,i]=bm124.dz[whlos]
        dphi_pos_time[*,i]=bm124.dphi[whlos]
    endif 
    ; -- two sources --
    ; 1 & 2 
    if nbion[i,0] eq 1 AND nbion[i,1] eq 1 AND nbion[i,2] eq 0  AND nbion[i,3] eq 0 then begin
        R_pos_time[*,i]=bm12.R[whlos]
        z_pos_time[*,i]=bm12.z[whlos]
        phi_pos_time[*,i]=bm12.phi[whlos]
        dR_pos_time[*,i]=bm12.dR[whlos]
        dz_pos_time[*,i]=bm12.dz[whlos]
        dphi_pos_time[*,i]=bm12.dphi[whlos]
    endif 
    ; 1 & 3 
    if nbion[i,0] eq 1 AND nbion[i,1] eq 0 AND nbion[i,2] eq 1  AND nbion[i,3] eq 0 then begin
        R_pos_time[*,i]=bm13.R[whlos]
        z_pos_time[*,i]=bm13.z[whlos]
        phi_pos_time[*,i]=bm13.phi[whlos]
        dR_pos_time[*,i]=bm13.dR[whlos]
        dz_pos_time[*,i]=bm13.dz[whlos]
        dphi_pos_time[*,i]=bm13.dphi[whlos]
    endif 
    ; 1 & 4 
    if nbion[i,0] eq 1 AND nbion[i,1] eq 0 AND nbion[i,2] eq 0  AND nbion[i,3] eq 1 then begin
        R_pos_time[*,i]=bm14.R[whlos]
        z_pos_time[*,i]=bm14.z[whlos]
        phi_pos_time[*,i]=bm14.phi[whlos]
        dR_pos_time[*,i]=bm14.dR[whlos]
        dz_pos_time[*,i]=bm14.dz[whlos]
        dphi_pos_time[*,i]=bm14.dphi[whlos]
    endif 
    ; 2 & 3 
    if nbion[i,0] eq 0 AND nbion[i,1] eq 1 AND nbion[i,2] eq 1  AND nbion[i,3] eq 0 then begin
        R_pos_time[*,i]=bm23.R[whlos]
        z_pos_time[*,i]=bm23.z[whlos]
        phi_pos_time[*,i]=bm23.phi[whlos]
        dR_pos_time[*,i]=bm23.dR[whlos]
        dz_pos_time[*,i]=bm23.dz[whlos]
        dphi_pos_time[*,i]=bm23.dphi[whlos]
    endif 
    ; 2 & 4 
    if nbion[i,0] eq 0 AND nbion[i,1] eq 1 AND nbion[i,2] eq 0  AND nbion[i,3] eq 1 then begin
        R_pos_time[*,i]=bm24.R[whlos]
        z_pos_time[*,i]=bm24.z[whlos]
        phi_pos_time[*,i]=bm24.phi[whlos]
        dR_pos_time[*,i]=bm24.dR[whlos]
        dz_pos_time[*,i]=bm24.dz[whlos]
        dphi_pos_time[*,i]=bm24.dphi[whlos]
    endif 
    ; 3 & 4 
    if nbion[i,0] eq 0 AND nbion[i,1] eq 0 AND nbion[i,2] eq 1  AND nbion[i,3] eq 1 then begin
        R_pos_time[*,i]=bm34.R[whlos]
        z_pos_time[*,i]=bm34.z[whlos]
        phi_pos_time[*,i]=bm34.phi[whlos]
        dR_pos_time[*,i]=bm34.dR[whlos]
        dz_pos_time[*,i]=bm34.dz[whlos]
        dphi_pos_time[*,i]=bm34.dphi[whlos]
    endif 
    ; -- one source --
    ; 1 
    if nbion[i,0] eq 1 AND nbion[i,1] eq 0 AND nbion[i,2] eq 0  AND nbion[i,3] eq 0 then begin
        R_pos_time[*,i]=bm1.R[whlos]
        z_pos_time[*,i]=bm1.z[whlos]
        phi_pos_time[*,i]=bm1.phi[whlos]
        dR_pos_time[*,i]=bm1.dR[whlos]
        dz_pos_time[*,i]=bm1.dz[whlos]
        dphi_pos_time[*,i]=bm1.dphi[whlos]
    endif 
    ; 2 
    if nbion[i,0] eq 0 AND nbion[i,1] eq 1 AND nbion[i,2] eq 0  AND nbion[i,3] eq 0 then begin
        R_pos_time[*,i]=bm2.R[whlos]
        z_pos_time[*,i]=bm2.z[whlos]
        phi_pos_time[*,i]=bm2.phi[whlos]
        dR_pos_time[*,i]=bm2.dR[whlos]
        dz_pos_time[*,i]=bm2.dz[whlos]
        dphi_pos_time[*,i]=bm2.dphi[whlos]
    endif 
    ; 3 
    if nbion[i,0] eq 0 AND nbion[i,1] eq 0 AND nbion[i,2] eq 1  AND nbion[i,3] eq 0 then begin
        R_pos_time[*,i]=bm3.R[whlos]
        z_pos_time[*,i]=bm3.z[whlos]
        phi_pos_time[*,i]=bm3.phi[whlos]
        dR_pos_time[*,i]=bm3.dR[whlos]
        dz_pos_time[*,i]=bm3.dz[whlos]
        dphi_pos_time[*,i]=bm3.dphi[whlos]
    endif 
    ; 4 
    if nbion[i,0] eq 0 AND nbion[i,1] eq 0 AND nbion[i,2] eq 0  AND nbion[i,3] eq 1 then begin
        R_pos_time[*,i]=bm4.R[whlos]
        z_pos_time[*,i]=bm4.z[whlos]
        phi_pos_time[*,i]=bm4.phi[whlos]
        dR_pos_time[*,i]=bm4.dR[whlos]
        dz_pos_time[*,i]=bm4.dz[whlos]
        dphi_pos_time[*,i]=bm4.dphi[whlos]
    endif 
    ; all beams off - set to standard Q3
    if nbion[i,0] eq 0 AND nbion[i,1] eq 0 AND nbion[i,2] eq 0  AND nbion[i,3] eq 0 then begin
        if defsource eq 3 then begin
            R_pos_time[*,i]=bm3.R[whlos]
            z_pos_time[*,i]=bm3.z[whlos]
            phi_pos_time[*,i]=bm3.phi[whlos]
            dR_pos_time[*,i]=bm3.dR[whlos]
            dz_pos_time[*,i]=bm3.dz[whlos]
            dphi_pos_time[*,i]=bm3.dphi[whlos]
        endif else begin
            if defsource eq 8 then begin
                R_pos_time[*,i]=bm8.R[whlos]
                z_pos_time[*,i]=bm8.z[whlos]
                phi_pos_time[*,i]=bm8.phi[whlos]
                dR_pos_time[*,i]=bm8.dR[whlos]
                dz_pos_time[*,i]=bm8.dz[whlos]
                dphi_pos_time[*,i]=bm8.dphi[whlos]
            endif
        endelse
    endif
    
    if diag eq 'CHR' then phi_pos_time[*,i]=phi_pos_time[*,i]-67.5 ; old coords. to new (2014) coords.
    
    ; now calculate costhetas
    x_pos  = reform(R_pos_time[*,i])*cos(reform(phi_pos_time[*,i])/!RADEG)
    y_pos  = reform(R_pos_time[*,i])*sin(reform(phi_pos_time[*,i])/!RADEG)
    z_pos  = reform(z_pos_time[*,i])
    x_orig = spectrometer_data.R_orig*cos(spectrometer_data.phi_orig/!RADEG)
    y_orig = spectrometer_data.R_orig*sin(spectrometer_data.phi_orig/!RADEG)
    z_orig = spectrometer_data.z_orig
    l_LOS  = SQRT((x_pos-x_orig)^2+(y_pos-y_orig)^2+(z_pos-z_orig)^2)
    c_LOS  = 	[[(x_pos-x_orig)/l_LOS], $
                 [(y_pos-y_orig)/l_LOS], $
                 [(z_pos-z_orig)/l_LOS]]
    c_tor  = [[-y_pos/reform(R_pos_time[*,i])],[+x_pos/reform(R_pos_time[*,i])],[FLTARR(spectrometer_data.ntrack)]]
    costheta = TOTAL(c_LOS*c_tor,2)
    costheta_time[*,i] = costheta
endfor

endif 

; ==================== BOX 2 ==================================================
if beambox eq 2 AND shot ge 31890 then begin
;p='/afs/ipp-garching.mpg.de/home/a/aleb/cxsfit/trunk/machines/aug/'
;if shot ge 31890 and shot le 33724 then file = p+'RZ_COR_position_allbeams_2015.txt'
;if shot ge 33725 then file = p+'RZ_COR_position_allbeams_2017.txt'
;changed this directory to one in CXRS account July 25 2018 - RMM
;And updated the calculations with the CHICA improvements backward filling to 2014

p='/afs/ipp-garching.mpg.de/home/c/cxrs/idl/COR_BOS_ausw/calib/RZ_pos_files/'
if shot ge 36800 and shot le 38200 then file = p+'COR_RZ_position_allbeams_2020.txt'
if shot ge 34996 and shot le 36799 then file = p+'COR_RZ_position_allbeams_2018.txt'
if shot ge 33725 and shot le 34995 then file = p+'COR_RZ_position_allbeams_2017.txt'
if shot ge 31890 and shot le 33724 then file = p+'COR_RZ_position_allbeams_2015.txt'
if shot ge 30150 and shot le 31889 then file = p+'COR_RZ_position_allbeams_2014.txt'

nlos=70 ; COR optical head

;Starting in 2021 include CMR 8 head
if shot ge 38201 then begin
    nlos=100
    if shot ge 38201 and shot le 39790 then file = p+'COR_RZ_position_allbeams_2021_CMR8_7mm.txt'
    if shot gt 39790 then file = p+'COR_RZ_position_allbeams_2022.txt'   
endif

read_pos_allbeams_corcur,file,nlos,los_str,bm5,bm6,bm7,bm8,$
                       bm56,bm57,bm58,bm67,bm68,bm78,$
                       bm567,bm568,bm578,bm678,bm5678

; define the tracks used
nfibers = n_elements(spectrometer_data.LOS_name)
whlos = fltarr(nfibers)
cmr8flag=fltarr(nfibers)
for i=0,nfibers-1 do begin
    whlos[i]=where(LOS_str+'  ' eq spectrometer_data.LOS_name[i]) 
    ;check for LOS like CMR8 with only one space after their name!
    if whlos[i]  eq -1 then begin
        whlos[i]=where(LOS_str+' ' eq spectrometer_data.LOS_name[i])
        cmr8flag[i]=1
    endif
endfor
cmr8los=where(cmr8flag)

;loop through time points
for i=0,ntm-1 do begin
    ; -- all --
    ; 5 & 6 & 7 & 8
    if nbion[i,0] eq 1 AND nbion[i,1] eq 1 AND nbion[i,2] eq 1  AND nbion[i,3] eq 1 then begin
        R_pos_time[*,i]=bm5678.R[whlos]
        z_pos_time[*,i]=bm5678.z[whlos]
        phi_pos_time[*,i]=bm5678.phi[whlos]
        dR_pos_time[*,i]=bm5678.dR[whlos]
        dz_pos_time[*,i]=bm5678.dz[whlos]
        dphi_pos_time[*,i]=bm5678.dphi[whlos]
    endif 
    ; -- three sources --
    ; 5 & 6 & 7
    if nbion[i,0] eq 1 AND nbion[i,1] eq 1 AND nbion[i,2] eq 1  AND nbion[i,3] eq 0 then begin
        R_pos_time[*,i]=bm567.R[whlos]
        z_pos_time[*,i]=bm567.z[whlos]
        phi_pos_time[*,i]=bm567.phi[whlos]
        dR_pos_time[*,i]=bm567.dR[whlos]
        dz_pos_time[*,i]=bm567.dz[whlos]
        dphi_pos_time[*,i]=bm567.dphi[whlos]
    endif 
    ; 2 & 3 & 4
    if nbion[i,0] eq 0 AND nbion[i,1] eq 1 AND nbion[i,2] eq 1  AND nbion[i,3] eq 1 then begin
        R_pos_time[*,i]=bm678.R[whlos]
        z_pos_time[*,i]=bm678.z[whlos]
        phi_pos_time[*,i]=bm678.phi[whlos]
        dR_pos_time[*,i]=bm678.dR[whlos]
        dz_pos_time[*,i]=bm678.dz[whlos]
        dphi_pos_time[*,i]=bm678.dphi[whlos]
    endif 
    ; 1 & 3 & 4
    if nbion[i,0] eq 1 AND nbion[i,1] eq 0 AND nbion[i,2] eq 1  AND nbion[i,3] eq 1 then begin
        R_pos_time[*,i]=bm578.R[whlos]
        z_pos_time[*,i]=bm578.z[whlos]
        phi_pos_time[*,i]=bm578.phi[whlos]
        dR_pos_time[*,i]=bm578.dR[whlos]
        dz_pos_time[*,i]=bm578.dz[whlos]
        dphi_pos_time[*,i]=bm578.dphi[whlos]
    endif 
    ; 1 & 2 & 4
    if nbion[i,0] eq 1 AND nbion[i,1] eq 1 AND nbion[i,2] eq 0  AND nbion[i,3] eq 1 then begin
        R_pos_time[*,i]=bm568.R[whlos]
        z_pos_time[*,i]=bm568.z[whlos]
        phi_pos_time[*,i]=bm568.phi[whlos]
        dR_pos_time[*,i]=bm568.dR[whlos]
        dz_pos_time[*,i]=bm568.dz[whlos]
        dphi_pos_time[*,i]=bm568.dphi[whlos]
    endif 
    ; -- two sources --
    ; 1 & 2 
    if nbion[i,0] eq 1 AND nbion[i,1] eq 1 AND nbion[i,2] eq 0  AND nbion[i,3] eq 0 then begin
        R_pos_time[*,i]=bm56.R[whlos]
        z_pos_time[*,i]=bm56.z[whlos]
        phi_pos_time[*,i]=bm56.phi[whlos]
        dR_pos_time[*,i]=bm56.dR[whlos]
        dz_pos_time[*,i]=bm56.dz[whlos]
        dphi_pos_time[*,i]=bm56.dphi[whlos]
    endif 
    ; 1 & 3 
    if nbion[i,0] eq 1 AND nbion[i,1] eq 0 AND nbion[i,2] eq 1  AND nbion[i,3] eq 0 then begin
        R_pos_time[*,i]=bm57.R[whlos]
        z_pos_time[*,i]=bm57.z[whlos]
        phi_pos_time[*,i]=bm57.phi[whlos]
        dR_pos_time[*,i]=bm57.dR[whlos]
        dz_pos_time[*,i]=bm57.dz[whlos]
        dphi_pos_time[*,i]=bm57.dphi[whlos]
    endif 
    ; 1 & 4 
    if nbion[i,0] eq 1 AND nbion[i,1] eq 0 AND nbion[i,2] eq 0  AND nbion[i,3] eq 1 then begin
        R_pos_time[*,i]=bm58.R[whlos]
        z_pos_time[*,i]=bm58.z[whlos]
        phi_pos_time[*,i]=bm58.phi[whlos]
        dR_pos_time[*,i]=bm58.dR[whlos]
        dz_pos_time[*,i]=bm58.dz[whlos]
        dphi_pos_time[*,i]=bm58.dphi[whlos]
    endif 
    ; 2 & 3 
    if nbion[i,0] eq 0 AND nbion[i,1] eq 1 AND nbion[i,2] eq 1  AND nbion[i,3] eq 0 then begin
        R_pos_time[*,i]=bm67.R[whlos]
        z_pos_time[*,i]=bm67.z[whlos]
        phi_pos_time[*,i]=bm67.phi[whlos]
        dR_pos_time[*,i]=bm67.dR[whlos]
        dz_pos_time[*,i]=bm67.dz[whlos]
        dphi_pos_time[*,i]=bm67.dphi[whlos]
    endif 
    ; 2 & 4 
    if nbion[i,0] eq 0 AND nbion[i,1] eq 1 AND nbion[i,2] eq 0  AND nbion[i,3] eq 1 then begin
        R_pos_time[*,i]=bm68.R[whlos]
        z_pos_time[*,i]=bm68.z[whlos]
        phi_pos_time[*,i]=bm68.phi[whlos]
        dR_pos_time[*,i]=bm68.dR[whlos]
        dz_pos_time[*,i]=bm68.dz[whlos]
        dphi_pos_time[*,i]=bm68.dphi[whlos]
    endif 
    ; 3 & 4 
    if nbion[i,0] eq 0 AND nbion[i,1] eq 0 AND nbion[i,2] eq 1  AND nbion[i,3] eq 1 then begin
        R_pos_time[*,i]=bm78.R[whlos]
        z_pos_time[*,i]=bm78.z[whlos]
        phi_pos_time[*,i]=bm78.phi[whlos]
        dR_pos_time[*,i]=bm78.dR[whlos]
        dz_pos_time[*,i]=bm78.dz[whlos]
        dphi_pos_time[*,i]=bm78.dphi[whlos]
    endif 
    ; -- one source --
    ; 1 
    if nbion[i,0] eq 1 AND nbion[i,1] eq 0 AND nbion[i,2] eq 0  AND nbion[i,3] eq 0 then begin
        R_pos_time[*,i]=bm5.R[whlos]
        z_pos_time[*,i]=bm5.z[whlos]
        phi_pos_time[*,i]=bm5.phi[whlos]
        dR_pos_time[*,i]=bm5.dR[whlos]
        dz_pos_time[*,i]=bm5.dz[whlos]
        dphi_pos_time[*,i]=bm5.dphi[whlos]
    endif 
    ; 2 
    if nbion[i,0] eq 0 AND nbion[i,1] eq 1 AND nbion[i,2] eq 0  AND nbion[i,3] eq 0 then begin
        R_pos_time[*,i]=bm6.R[whlos]
        z_pos_time[*,i]=bm6.z[whlos]
        phi_pos_time[*,i]=bm6.phi[whlos]
        dR_pos_time[*,i]=bm6.dR[whlos]
        dz_pos_time[*,i]=bm6.dz[whlos]
        dphi_pos_time[*,i]=bm6.dphi[whlos]
    endif 
    ; 3 
    if nbion[i,0] eq 0 AND nbion[i,1] eq 0 AND nbion[i,2] eq 1  AND nbion[i,3] eq 0 then begin
        R_pos_time[*,i]=bm7.R[whlos]
        z_pos_time[*,i]=bm7.z[whlos]
        phi_pos_time[*,i]=bm7.phi[whlos]
        dR_pos_time[*,i]=bm7.dR[whlos]
        dz_pos_time[*,i]=bm7.dz[whlos]
        dphi_pos_time[*,i]=bm7.dphi[whlos]
    endif 
    ; 4 
    if nbion[i,0] eq 0 AND nbion[i,1] eq 0 AND nbion[i,2] eq 0  AND nbion[i,3] eq 1 then begin
        R_pos_time[*,i]=bm8.R[whlos]
        z_pos_time[*,i]=bm8.z[whlos]
        phi_pos_time[*,i]=bm8.phi[whlos]
        dR_pos_time[*,i]=bm8.dR[whlos]
        dz_pos_time[*,i]=bm8.dz[whlos]
        dphi_pos_time[*,i]=bm8.dphi[whlos]
    endif 
    ; all beams off - set to standard Q3
    if nbion[i,0] eq 0 AND nbion[i,1] eq 0 AND nbion[i,2] eq 0  AND nbion[i,3] eq 0 then begin
        if defsource eq 3 then begin
            R_pos_time[*,i]=bm3.R[whlos]
            z_pos_time[*,i]=bm3.z[whlos]
            phi_pos_time[*,i]=bm3.phi[whlos]
            dR_pos_time[*,i]=bm3.dR[whlos]
            dz_pos_time[*,i]=bm3.dz[whlos]
            dphi_pos_time[*,i]=bm3.dphi[whlos]
        endif else begin
            if defsource eq 8 then begin
                R_pos_time[*,i]=bm8.R[whlos]
                z_pos_time[*,i]=bm8.z[whlos]
                phi_pos_time[*,i]=bm8.phi[whlos]
                dR_pos_time[*,i]=bm8.dR[whlos]
                dz_pos_time[*,i]=bm8.dz[whlos]
                dphi_pos_time[*,i]=bm8.dphi[whlos]
            endif
        endelse
    endif

    phi_pos_time[*,i] = phi_pos_time[*,i]-67.5
    iphi = WHERE(phi_pos_time[*,i] lt 0.,nphi)
    if nphi gt 0 then phi_pos_time[iphi,i] = phi_pos_time[iphi,i]+180. 

    ; now calculate costhetas
    x_pos  = reform(R_pos_time[*,i])*cos(reform(phi_pos_time[*,i])/!RADEG)
    y_pos  = reform(R_pos_time[*,i])*sin(reform(phi_pos_time[*,i])/!RADEG)
    z_pos  = reform(z_pos_time[*,i])
    x_orig = spectrometer_data.R_orig*cos(spectrometer_data.phi_orig/!RADEG)
    y_orig = spectrometer_data.R_orig*sin(spectrometer_data.phi_orig/!RADEG)
    z_orig = spectrometer_data.z_orig
    l_LOS  = SQRT((x_pos-x_orig)^2+(y_pos-y_orig)^2+(z_pos-z_orig)^2)
    c_LOS  = 	[[(x_pos-x_orig)/l_LOS], $
                 [(y_pos-y_orig)/l_LOS], $
                 [(z_pos-z_orig)/l_LOS]]
    c_tor  = [[-y_pos/reform(R_pos_time[*,i])],[+x_pos/reform(R_pos_time[*,i])],[FLTARR(spectrometer_data.ntrack)]]
    costheta = TOTAL(c_LOS*c_tor,2)
    costheta_time[*,i] = costheta
endfor

endif


; ==================== OLD SHOTS ==================================================
if (beambox eq 1 AND shot lt 25890) OR (beambox eq 2 AND shot lt 31890) then begin
; ---- for older discharges for which time-dependent information has not been analysed yet
    ; get R,z,phi's fixed in time
    r_pos_fix   = spectrometer_data.R_pos 
    z_pos_fix   = spectrometer_data.z_pos
    phi_pos_fix = spectrometer_data.phi_pos
    dr_pos_fix  = fltarr(spectrometer_data.ntrack) 
    dz_pos_fix  = fltarr(spectrometer_data.ntrack) 
    dphi_pos_fix= fltarr(spectrometer_data.ntrack) 
    ; replicate in a [tracks,frames] array
    for i=0,ntm-1 do begin
        R_pos_time[*,i]   = r_pos_fix
        z_pos_time[*,i]   = z_pos_fix
        phi_pos_time[*,i] = phi_pos_fix
        dR_pos_time[*,i]  = dr_pos_fix
        dz_pos_time[*,i]  = dz_pos_fix
        dphi_pos_time[*,i]= dphi_pos_fix
        ; now calculate costhetas
        x_pos  = reform(R_pos_time[*,i])*cos(reform(phi_pos_time[*,i])/!RADEG)
        y_pos  = reform(R_pos_time[*,i])*sin(reform(phi_pos_time[*,i])/!RADEG)
        z_pos  = reform(z_pos_time[*,i])
        x_orig = spectrometer_data.R_orig*cos(spectrometer_data.phi_orig/!RADEG)
        y_orig = spectrometer_data.R_orig*sin(spectrometer_data.phi_orig/!RADEG)
        z_orig = spectrometer_data.z_orig
        l_LOS  = SQRT((x_pos-x_orig)^2+(y_pos-y_orig)^2+(z_pos-z_orig)^2)
        c_LOS  = 	[[(x_pos-x_orig)/l_LOS], $
                        [(y_pos-y_orig)/l_LOS], $
                        [(z_pos-z_orig)/l_LOS]]
        c_tor  = [[-y_pos/reform(R_pos_time[*,i])],[+x_pos/reform(R_pos_time[*,i])],[FLTARR(spectrometer_data.ntrack)]]
        costheta = TOTAL(c_LOS*c_tor,2)
        costheta_time[*,i] = costheta       
    endfor
endif

; create structure to pass
time_loc = {R_pos_time   :R_pos_time,$
            z_pos_time   :z_pos_time,$
            phi_pos_time :phi_pos_time,$
            dR_pos_time  :dR_pos_time,$
            dz_pos_time  :dz_pos_time,$
            dphi_pos_time:dphi_pos_time,$
            costheta_time:costheta_time}
            
            print,'COSTHETA CALC CER CHECK'
            help,costheta
            ;print,costheta_time
            
;if cmr8los[0] ne -1 then begin
;    time_loc.r_pos_time[cmr8los,*]=time_loc.r_pos_time[cmr8los,*]+0.01    
;endif        
;stop

end
