@/afs/ipp/home/s/sprd/idlpro/fit_spec/read_xvs_diag.pro
@/afs/ipp/home/c/cxrs/idl/cxsfit64/machines/aug/read_princeton.pro

;@/afs/ipp/home/d/djcruz/repository/cxsfit_djcruz/amd64_sles15/machines/aug/rem_D2_lines.pro
;@/afs/ipp/home/d/djcruz/repository/cxsfit_djcruz/amd64_sles15/machines/aug/hfs_puff_status.pro

@/afs/ipp/home/c/cxrs/idl/cxsfit64/machines/aug/rem_D2_lines.pro
@/afs/ipp/home/c/cxrs/idl/cxsfit64/machines/aug/hfs_puff_status.pro

pro read_hfs_data,shot,spectrometer_data,extraoptions,head_name=head_name


err  = ''
id   = 'CXF_LOAD_AUG_DATA: '
ierr = 0L

; speed of light
c = 2.9979D8      ; [m/s]

; Planck constant   ; [J s]
h = 6.6261D-34

; Read losNames of FVS and EVS

los_names=''
los_index=''
spectr='xxx'

; Open EVS shotfile
error=0L
diaref=0L
edition=0L
date='123456789012345668'	; String with 18 characters
result=call_external(!libddww,'ddgetaug','ddopen', $
       error,'AUGD','EVS',shot,edition,diaref,date)
; Read the LOS name
type=6L
losname='        '
physunit=0L
signalname='PARAM'
CHANEL_ID='CHAN_xx'
n_los=25L
for i=0,(n_los-1) do begin
    if i lt 9 then begin
        nr=strcompress('0'+string((i+1)),/remove_all)
        endif else begin
        nr=strcompress(string((i+1)),/remove_all)
        endelse
    strput,CHANEL_ID,nr,5
    result=call_external(!libddww,'ddgetaug','ddparm', $
            error,diaref,'PARAM',CHANEL_ID,6L,8L,losname,physunit)
    det_name = strmid(losname,0,3)
    if strcmp(det_name,head_name) eq 1 then begin
        los_index=[los_index,i]
        los_names=los_names+losname
        spectr = 'EVS'
    endif
endfor
; Close the shotfile
result=call_external(!libddww,'ddgetaug','ddclose', $
       error,diaref)


; Open FVS shotfile
if strcmp(spectr,'EVS') eq 0 then begin
error=0L
diaref=0L
edition=0L
date='123456789012345668'	; String with 18 characters
result=call_external(!libddww,'ddgetaug','ddopen', $
       error,'AUGD','FVS',shot,edition,diaref,date)
; Read the LOS name
type=6L
losname='        '
physunit=0L
signalname='PARAM'
CHANEL_ID='CHAN_xx'
n_los=24L

for i=0,(n_los-1) do begin
    if i lt 9 then begin
        nr=strcompress('0'+string((i+1)),/remove_all)

    endif else begin
        nr=strcompress(string((i+1)),/remove_all)

    endelse
    strput,CHANEL_ID,nr,5
    result=call_external(!libddww,'ddgetaug','ddparm', $
            error,diaref,'PARAM',CHANEL_ID,6L,8L,losname,physunit)
    det_name = strmid(losname,0,3)

    if strcmp(det_name,head_name) eq 1 then begin
        los_index=[los_index,i]
        los_names=los_names+losname
        spectr = 'FVS'
    endif

endfor

; Close the shotfile
result=call_external(!libddww,'ddgetaug','ddclose', $
       error,diaref)
endif

 los_names= strsplit(los_names,/EXTRACT)
 los_index=los_index(1:(n_elements(los_index)-1))
 print,'Spectrometer used:   ',spectr
 print,'Channels used:', los_index+1L

; Input switches
shot     = spectrometer_data.shot_nr
spectrometer_data.spectrometer.name = spectr
diagnosticL1 = STRSPLIT(spectr, 'S', /EXTRACT)+'L'
error = 0L

if (shot eq 38568) or (shot eq 38569) then begin
    smear_opt = 1
    read_xvs_diag, 38570, spectr, dwdp, exptime, sad2, ctsph, good_time, lam, offset, sens, spec, wlen, slit, $
           gratcons, op_ang, pixw, magnification, fwhm_pix, neon_done, neon, lambda_neon, r1, phi1, z1, $
           r2, phi2, z2, los_name=los_name, smear_opt=smear_opt, /read_again
endif


smear_opt = 1
read_xvs_diag, shot, spectr, dwdp, exptime, sad2, ctsph, time, lam, offset, sens, spec, wlen, slit, $
       gratcons, op_ang, pixw, magnification, fwhm_pix, neon_done, neon, lambda_neon, r1, phi1, z1, $
       r2, phi2, z2, los_name=los_name, smear_opt=smear_opt, /read_again
print,'SMEAR_OPT=', smear_opt

;dwdp is in nm

if (shot eq 38568) or (shot eq 38569) then begin
    time = good_time
endif

if  (shot eq 38571) or (shot eq 38587) then begin
    error=0L
    diaref=0L
    edition=0L
    date='123456789012345668'	; String with 18 characters
    ; open shotfile
    result=call_external(!libddww,'ddgetaug','ddopen', $
           error,'AUGD','FVS',shot,edition,diaref,date)

    ; Get delta time trigger
    delta_trigger=0l ;length of the trigger in ns
    err_string = string(' ',format='(a80)')
    result = call_external(!libddww, 'ddgetaug', 'ddparm', $
                   error, diaref,'PPG ', 'TPL', $
                   1L, 1L, delta_trigger, physunit)
    if (error gt 0) then begin
        ier = 0L
        result = call_external(!libddww,'ddgetaug','xxerrprt',     $
                      -1L,err_string,ier,3L,'TPL:')
        stop
    endif
    delta_trigger *= 1.e-9

    ; Get trigger pulse array
    pulses=intarr(14) ;pulses triggered
    err_string = string(' ',format='(a80)')
    result = call_external(!libddww, 'ddgetaug', 'ddparm', $
                   error, diaref,'PPG', 'PULS', $
                   1L, 7L, pulses, physunit)
    if (error gt 0) then begin
        ier = 0L
        result = call_external(!libddww,'ddgetaug','xxerrprt',     $
                      -1L,err_string,ier,3L,'PULS:')
        stop
    endif

    ; Get trigger frequency array
    freq=intarr(14) ;length of the trigger pulses
    err_string = string(' ',format='(a80)')
    result = call_external(!libddww, 'ddgetaug', 'ddparm', $
                   error, diaref,'PPG ', 'FREQ', $
                   1L, 7L, freq, physunit)
    if (error gt 0) then begin
        ier = 0L
        result = call_external(!libddww,'ddgetaug','xxerrprt',     $
                      -1L,err_string,ier,3L,'FREQ:')
        stop
    endif

    ; Get units of trigger freq
    res=intarr(14) ;1 = micros, 2 = mils
    err_string = string(' ',format='(a80)')
    result = call_external(!libddww, 'ddgetaug', 'ddparm', $
                   error, diaref,'PPG ', 'RES', $
                   1L, 7L, res, physunit)
    if (error gt 0) then begin
        ier = 0L
        result = call_external(!libddww,'ddgetaug','xxerrprt',     $
                      -1L,err_string,ier,3L,'RES:')
        stop
    endif

    ; Get if pulses are sent to the camera or not
    out=intarr(14) ; 1 = ON, 2 = OFF
    err_string = string(' ',format='(a80)')
    result = call_external(!libddww, 'ddgetaug', 'ddparm', $
                   error, diaref,'PPG ', 'OUT', $
                   1L, 7L, out, physunit)
    if (error gt 0) then begin
        ier = 0L
        result = call_external(!libddww,'ddgetaug','xxerrprt',     $
                      -1L,err_string,ier,3L,'OUT:')
        stop
    endif

    ; Close the shotfile
    result=call_external(!libddww,'ddgetaug','ddclose', $
           error,diaref)

    if res[0] eq 1 then factor = 1.d-6
    if res[0] eq 2 then factor = 1.d-3

    timebase = findgen(pulses(0))*freq(0)*factor
    time = timebase

    ;Do wvl calibration using a different .spe file
    read_princeton,'/afs/ipp/home/d/djcruz/repository/cxsfit_djcruz/Calibration38571_HgLamp.SPE',hg
    hg = reform(hg[*,0,1:99])
    hg = mean(hg,dimension=2)
    whg = lam[*,0]
    yfit=gaussfit(whg,hg,coeff)
    shift = 435.8335 - coeff[1]
    lam = lam + shift
    print, 'Applied shift to wvl [nm]', shift

endif


if (shot eq 37761) or (shot eq 37760) then time+=5.0e-3



;Select frames with Ip > 0
timebase = 1L
read_signal_mrm,0L,shot,'MAG','Ipa',timebase,variable,phys_dim,edition=0L,exp='AUGD'
threshold = 2.0e5
timebase = timebase(where((variable gt threshold) or (variable lt -1.0*threshold)))
interval_ip_on = [timebase(0),timebase(n_elements(timebase)-1)]

spec = spec (*,where((time gt interval_ip_on(0)) and (time lt interval_ip_on(1))),*)
time = time(where((time gt interval_ip_on(0)) and (time lt interval_ip_on(1))))

if extraoptions.(head_name).time_choice eq 1 then begin ; Select frames [t_ini_gaspuff, t_end_gaspuff]
    timebase = 1L
    read_signal_mrm,0L,shot,'HEB','S01VALVE',timebase,variable,phys_dim,edition=0L,exp='AUGD'
    threshold  = 80 ; [V] the valve is considered opened at 80 V
    timebase = timebase(where( variable gt threshold))
    interval_gaspuff_on = [timebase(0)-0.005,(timebase(n_elements(timebase)-1)+0.005)]
    spec = spec (*,where((time gt interval_gaspuff_on(0)) and (time lt interval_gaspuff_on(1))),*)
    time = time(where((time gt interval_gaspuff_on(0)) and (time lt interval_gaspuff_on(1))))
endif
if n_elements(los_index) eq 1 then begin
    npixel = n_elements(spec[*,0,0])
    nframe  = n_elements(spec[0,*,0])

    spec = spec[*,*,los_index]
    sens = sens[*,los_index]
    lam = lam[*,los_index]
    offset = offset[*,los_index]

    spec = reform(spec,npixel,nframe,1L)
    sens = reform(sens,npixel,1L)
    lam = reform(lam,npixel,1L)
    offset = reform(offset,npixel,1L)

endif else begin
    los_index = los_index(reverse(sort(r2(los_index))))
    stop
    spec = spec[*,*,los_index]
    sens = sens[*,los_index]
    lam = lam[*,los_index]
    offset = offset[*,los_index]
endelse
los_name = los_name[los_index]

spectrometer_data.npixel = n_elements(spec[*,0,0])
spectrometer_data.nframe  = n_elements(spec[0,*,0])
spectrometer_data.ntrack = n_elements(spec[0,0,*])
spectrometer_data.ninst  = 1
cxf_define_spectrometer_data,spectrometer_data,err
spectrometer_data.LOS_name = los_name
spectrometer_data.R_orig = r1[los_index]
spectrometer_data.Z_orig = z1[los_index]
spectrometer_data.PHI_orig = phi1[los_index]
spectrometer_data.R_pos = r2[los_index]
spectrometer_data.Z_pos = z2[los_index]
spectrometer_data.PHI_pos = phi2[los_index]
; Reference wavelength [A]
spectrometer_data.wlength = wlen*10

; Slit width [microns]
spectrometer_data.slit = slit

; Time vector [s]
spectrometer_data.time     = time
spectrometer_data.exposure = exptime + FLTARR(spectrometer_data.nframe)

; Calculate the wavelength intervals assuming the pixels are contiguous
wav_vect  = lam
dwav_vect = FLTARR(spectrometer_data.npixel,spectrometer_data.ntrack)
dwav_vect[0,*] = wav_vect[1,*]-wav_vect[0,*]
dwav_vect[1:spectrometer_data.npixel-2,*] = 0.5*(wav_vect[2:spectrometer_data.npixel-1,*] $
                               -wav_vect[0:spectrometer_data.npixel-3,*])
dwav_vect[spectrometer_data.npixel-1,*] = wav_vect[spectrometer_data.npixel-1,*] $
                        -wav_vect[spectrometer_data.npixel-2,*]
spectrometer_data.wavelength.data      = wav_vect
spectrometer_data.wavelength.error     = FLTARR(spectrometer_data.npixel,$
                            spectrometer_data.ntrack)
spectrometer_data.wavelength.reference = ' '
spectrometer_data.dispersion.data      = dwav_vect
spectrometer_data.dispersion.error     = FLTARR(spectrometer_data.npixel,$
                            spectrometer_data.ntrack)
spectrometer_data.dispersion.reference = ' '

; Calculate error bars and calibrated data

ppc = 1./ctsph
ron = sad2
err_bars = dblarr(spectrometer_data.npixel,spectrometer_data.ntrack,$
    spectrometer_data.nframe)
err_bars_counts = sqrt(abs(spec)*ctsph+sad2)
data_calib = dblarr(spectrometer_data.npixel,spectrometer_data.ntrack,$
    spectrometer_data.nframe)
for jtime = 0,spectrometer_data.nframe-1 do begin
    err_bars[*,*,jtime] = reform(err_bars_counts[*,jtime,*])/sens/exptime/dwdp
    data_calib[*,*,jtime] = reform(spec[*,jtime,*])/sens/exptime/dwdp
endfor

spectrometer_data.intensity.data = data_calib
spectrometer_data.intensity.error = err_bars
spectrometer_data.intensity.reference = ' '

spectrometer_data.instfu.y0 = replicate(1.,spectrometer_data.ntrack)
spectrometer_data.instfu.xw = fwhm_pix[los_index]*dwdp*10
spectrometer_data.instfu.xs = replicate(0.,spectrometer_data.ntrack)
spectrometer_data.instfu.reference = ' '

mass = 0.0
if (wlen lt 568.0) and (wlen gt 566.0) then mass = 14.0
if (wlen lt 496.0) and (wlen gt 492.0) then mass = 10.0
if (wlen lt 436.0) and (wlen gt 432.0) then mass = 2.0

instfunc_eV = mean(spectrometer_data.instfu.xw/10.0/wlen)^2.0*mass*938.3e6/(8.0*alog(2.0))
print,'Instrument function in eV is ',instfunc_eV
;externalcostheta = fltarr(spectrometer_data.ntrack,spectrometer_data.nframe)+1.
;spectrometer_data = create_struct(spectrometer_data,'externalcostheta',externalcostheta)

; Load flag showing if 'sufficient' beam =puff is available for a
; given frame
  if shot ge 35313 then begin
    if shot ne 35396 then begin
     hfs_puff_status, head_name, shot, spectrometer_data.time, $
                       spectrometer_data.exposure, $
                       beam_orig, powerbeam, puff_shotfile, err
     IF STRTRIM(err,2) NE '' THEN BEGIN
                            ; If the shot file is missing then assume no beams (this is default in
                            ; cxf_define_spectrometer_data)!!!
        IF STRPOS(err,'shotfile does not exist') NE -1 THEN BEGIN
           spectrometer_data.beam = spectrometer_data.time*0.
           err = ''
        ENDIF ELSE BEGIN
           RETURN
        ENDELSE
     ENDIF ELSE BEGIN
; read ElM signal in order to remove elms from background subtraction
       elm_exp='AUGD'
       read_signal_mrm, ier,shot,'ELM','t_endELM',telm_begin,telm_end,phys_dim,exp=elm_exp
       if ier eq 0 then begin
          print, ''
          print, 'Exclude ELMs from data'
          print, ''
          elmfree = indgen(n_elements(spectrometer_data.time))*0+1
          preELM = -1.0*(0.001 + mean(spectrometer_data.exposure)/2.0)
          postELM = 0.001 + mean(spectrometer_data.exposure)/2.0
          time = spectrometer_data.time
          for t=0L, n_elements(telm_begin)-1 do begin
              elmfree(where((time ge (telm_begin(t)+preELM)) and (time le (telm_end(t)+postELM)))) = 0
          endfor

          powerbeam(where(elmfree eq 0)) = 0.5
          beam_orig(where(elmfree eq 0)) = 0.0 ;I do not want to fit frames in ELMs. This is used when no beam modulation is applied
       endif else begin
          ier = 0
          elm_exp = 'djcruz'
          read_signal_mrm, ier,shot,'ELM','t_endELM',telm_begin,telm_end,phys_dim,exp=elm_exp
          if ier eq 0 then begin
             print, ''
             print, 'Exclude ELMs from data'
             print, ''
             elmfree = indgen(n_elements(spectrometer_data.time))*0+1
             preELM = -1.0*(0.001 + mean(spectrometer_data.exposure)/2.0)
             postELM = 0.001 + mean(spectrometer_data.exposure)/2.0
             time = spectrometer_data.time
             for t=0L, n_elements(telm_begin)-1 do begin
                 elmfree(where((time ge (telm_begin(t)+preELM)) and (time le (telm_end(t)+postELM)))) = 0
             endfor
             powerbeam(where(elmfree eq 0)) = 0.5
             beam_orig(where(elmfree eq 0)) = 0.0 ;I do not want to fit frames in ELMs. This is used when no beam modulation is applied
         endif else begin
              print, ''
              print, 'ELM shotfile does not exist!'
              print, ''
          endelse
       endelse
    spectrometer_data.beam = beam_orig
    spectrometer_data.modulation.flag=0
    powerbeam = reform(powerbeam,1,n_elements(powerbeam))
    names = ['GasPuffSe01']
    *spectrometer_data.modulation.beampower=powerbeam
    *spectrometer_data.modulation.beamlabels =names
    *spectrometer_data.modulation.orig_beam =spectrometer_data.beam
    *spectrometer_data.modulation.orig_data =spectrometer_data.intensity.data
    *spectrometer_data.modulation.orig_error = spectrometer_data.intensity.error
     ENDELSE
     endif else begin ;shot = 35396

        beam = dblarr(n_elements(spectrometer_data.time))
        ini_beam = 3.018 ;[s]
        delta_t = 0.020 ; [s]
        for i = 0,15 do begin
            index_on = where( (time gt ini_beam) and (time le (ini_beam+delta_t)))
            beam(index_on) = 1.0
            beam(index_on(0)-1) = 0.5
            beam(index_on(n_elements(index_on)-1)+1) = 0.5
            ini_beam = ini_beam + 0.1
        endfor

; read ElM signal in order to remove elms from background subtraction
       elm_exp='AUGD'
       read_signal_mrm, ier,shot,'ELM','t_endELM',telm_begin,telm_end,phys_dim,exp=elm_exp
       if ier eq 0 then begin
          print, ''
          print, 'Exclude ELMs from data'
          print, ''
          for t=0L, n_elements(telm_begin)-1 do begin
             min_begin=min(abs(spectrometer_data.time-(telm_begin[t]-0.001)),location_begin)
             min_end=min(abs(spectrometer_data.time-(telm_end[t]+0.001)),location_end)
             for k=location_begin, location_end+1 do begin
                if beam[k] ne 1.0 then beam[k]=0.7
             endfor
          endfor
       endif else begin
          print, ''
          print, 'ELM shotfile does not exist!'
          print, ''
       endelse

        spectrometer_data.beam = beam
        spectrometer_data.modulation.flag=0
        names = ['GasPuffSe01']
        *spectrometer_data.modulation.beampower=reform(beam,1,n_elements(beam))
        *spectrometer_data.modulation.beamlabels =names
        *spectrometer_data.modulation.orig_beam =spectrometer_data.beam
        *spectrometer_data.modulation.orig_data =spectrometer_data.intensity.data
        *spectrometer_data.modulation.orig_error = spectrometer_data.intensity.error

    endelse
  endif

spectrometer_data.spectrometer.name = head_name
if head_name eq 'CXH' then spectrometer_data.PHI_orig = (spectrometer_data.PHI_orig + 2*!Pi)*180/!Pi
if head_name eq 'CVH' then spectrometer_data.PHI_orig = (spectrometer_data.PHI_orig)*180/!Pi
spectrometer_data.PHI_pos = spectrometer_data.PHI_pos*180/!Pi

externalcostheta = fltarr(spectrometer_data.ntrack,spectrometer_data.nframe)+1.
x_pos  = spectrometer_data.R_pos*cos(spectrometer_data.phi_pos/!RADEG)
y_pos  = spectrometer_data.R_pos*sin(spectrometer_data.phi_pos/!RADEG)
z_pos  = spectrometer_data.z_pos
x_orig = spectrometer_data.R_orig*cos(spectrometer_data.phi_orig/!RADEG)
y_orig = spectrometer_data.R_orig*sin(spectrometer_data.phi_orig/!RADEG)
z_orig = spectrometer_data.z_orig
l_LOS  = SQRT((x_pos-x_orig)^2+(y_pos-y_orig)^2+(z_pos-z_orig)^2)
c_LOS  = 	[[(x_pos-x_orig)/l_LOS], $
        [(y_pos-y_orig)/l_LOS], $
         [(z_pos-z_orig)/l_LOS]]
c_tor  = [[-y_pos/spectrometer_data.R_pos],[+x_pos/spectrometer_data.R_pos],[FLTARR(spectrometer_data.ntrack)]]

if head_name eq 'CXH' then begin
    print,'HEAD IS CXH'
    costheta_tor = TOTAL(c_LOS*c_tor,2)
    for jlos=0,spectrometer_data.ntrack-1 do begin
        externalcostheta[jlos,*] = abs(costheta_tor[jlos])
    endfor
endif

if head_name eq 'CVH' then begin
    print,'HEAD IS CVH'
    rvec=fltarr(n_elements(time), n_elements(spectrometer_data.R_pos),3)
    polvec=fltarr(n_elements(time), n_elements(spectrometer_data.R_pos),3)
    eqiread, shot, min(time), max(time), diag='EQH', ed=0, exp='AUGD',surfaces=surfaces
    rmag=interpol(surfaces.rmag,surfaces.time,time)
    zmag=interpol(surfaces.zmag,surfaces.time,time)
    for i_time = 0., spectrometer_data.nframe-1 do begin
        for i_los = 0, spectrometer_data.ntrack-1 do begin
            trace_field_line_fast,surfaces,time(i_time),spectrometer_data.R_pos(i_los),$
                            spectrometer_data.z_pos(i_los),spectrometer_data.phi_pos(i_los), $
                            5.,0.002,r,z,phi,b_abs=b_abs,/no_plot
                            philos=spectrometer_data.phi_pos[i_los]*!pi/180.
                            rlos=spectrometer_data.R_pos[i_los]
                            zlos=spectrometer_data.z_pos[i_los]
            rvec[i_time,i_los,*]=[rmag[i_time]*cos(philos)-rlos*cos(philos),$
                                  rmag[i_time]*sin(philos)-rlos*sin(philos),$
                                  zmag[i_time]-zlos]
            rvec[i_time,i_los,*]=rvec[i_time,i_los,*]/sqrt(total(rvec[i_time, i_los, *]^2))
            polvec[i_time,i_los,*]=crossp(rvec[i_time, i_los, *],reform(c_tor[i_los,*]))
            costheta_pol = total(c_LOS(i_los,*)*polvec(i_time,i_los,*))
            externalcostheta[i_los,i_time] = abs(costheta_pol)
        endfor
    endfor
endif

spectrometer_data = create_struct(spectrometer_data,'externalcostheta',externalcostheta)


if extraoptions.(head_name).molecular_choice eq 1 then begin
    rem_D2_lines,spectrometer_data,data_calib,wlen,reform(powerbeam)
endif

kill_var,dwdp
kill_var,exptime
kill_var,sad2
kill_var,ctsph
kill_var,time
kill_var,lam
kill_var,offset
kill_var,sens
kill_var,spec
kill_var,los_name
kill_var,data_calib
kill_var,err_bars
kill_var,err_bars_counts
kill_var,dwav_vect
kill_var,wav_vect

end
