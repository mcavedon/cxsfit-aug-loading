;copied this here so I can go through it and make comments to figure out how this works. -RMM July 21 2009

PRO cxrs_beam_status, diagnostic, shot, time, exp, $
                      beam, NI_shotfile, err

; Return a vector of flags showing at which CXRS measurement
; time 'sufficient' beam power is available

  id  =  'CXRS_BEAM_STATUS: '

; Set beam-on if the average beam power in the integration window exceeds:
  power_threshold =  0.5E6  ; [W]

    ;power_threshold=0.01e6
    ; stop
  ;endif
; check if needed system variable exist. Define it if not.
  defsysv, '!LIBDDWW', exists=exists
  if exists eq 0 then defsysv, '!LIBDDWW', '/usr/ads/lib/libddww.so', 1

  
; Diagnostic-specific switches
  CASE diagnostic OF
     'CER' : BEGIN
       
        NI_index = [[1,2,3],[0,0,0]] ; PINI 2, 3,4
        nb=3
        if shot eq 40060 or shot eq 40061 then stop
        
        ;NI_index = [[2],[0]]
        ;nb=1
        ;endif
     END
     'CFR' : BEGIN
       
        NI_index = [[1,2,3],[0,0,0]] ; PINI 2, 3,4
        nb=3
                                ;NI_index = [2,0]     
     END
     'CPR' : BEGIN
        NI_index = [[1,2],[0,0]] ; PINI 2,3
        nb=2
                                ;NI_index = [2,0] 
     END
     'CMR' : BEGIN
        NI_index = [[2],[0]] ; PINI 3
        nb=1
                                ;NI_index = [2,0] 
     END
;CNR puff is handled in cxrs_puff_status
     
     'CHR' : BEGIN
        NI_index = [2,3,[0,0]]
        nb=2                    ; PINI 3 - AK: 03022014
                                ;IF shot LT 18202 THEN NI_index = [2,1] $  ; PINI 7
                                ;                 ELSE NI_index = [3,1]    ; PINI 8
     END
     'CXH' : BEGIN
                                ; define some parameters, maybe elsewhere
        reference_offset = -0.0001 ; offset to v3 start time for background weight frames
        valid_start = 0.0          ; offset to v3 start time for valid frames
        valid_end = 0.1            ; offset to v3 end time for valid frames
                                ; end of definition
                                ;load_cxh,shot,cxh_tmp,err_tmo,/only_header,/ip_on
        get_ventil3,shot,v3,err
        
        beam= time * 0.
        for i_time = 0., n_elements(time)-1 do begin
           if time(i_time) ge v3.ventil_time[0] and time(i_time) le v3.mano_time[1] then beam[i_time]=1
        endfor    
        
        
        goto, finish2
     END
     'CXH_PUFF' : BEGIN
                                ; define some parameters, maybe elsewhere
        reference_offset = -0.0001 ; offset to v3 start time for background weight frames
        valid_start = 0.0          ; offset to v3 start time for valid frames
        valid_end = 0.1            ; offset to v3 end time for valid frames
                                ; end of definition
        load_cxh,shot,cxh_tmp,err_tmo,/only_header,/cxpuff
        
        
        beam= time * 0.
        
        beam(cxh_tmp.header.puff_on) = 1.0
        
        goto, finish2
     END
     'CVH' : BEGIN
                                ; define some parameters, maybe elsewhere
        reference_offset = -0.0001 ; offset to v3 start time for background weight frames
        valid_start = 0.0          ; offset to v3 start time for valid frames
        valid_end = 0.1            ; offset to v3 end time for valid frames
                                ; end of definition
                                ;load_cxh,shot,cxh_tmp,err_tmo,/only_header,/ip_on
        get_ventil3,shot,v3,err
        
        beam= time * 0.
        for i_time = 0., n_elements(time)-1 do begin
           if time(i_time) ge v3.ventil_time[0] and time(i_time) le v3.mano_time[1] then beam[i_time]=1
        endfor    
        
        
        goto, finish2
     END
     'CVH_PUFF' : BEGIN
                                ; define some parameters, maybe elsewhere
        reference_offset = -0.0001 ; offset to v3 start time for background weight frames
        valid_start = 0.0          ; offset to v3 start time for valid frames
        valid_end = 0.1            ; offset to v3 end time for valid frames
                                ; end of definition
        load_cvh_fvs,shot,cvh_tmp,err_tmo,/only_header,/cxpuff
        
        
        beam= time * 0.
        
        beam(cvh_tmp.header.puff_on) = 1.0
        
        goto, finish2
     END    
     'CAR' : BEGIN
        NI_index = [[2,3],[0,0]] ; PINI 3, 4
        nb=2
        ;NI_index = [2,0]     
        ;nb=1
        ;print,'I have set up only PINI 3 for beam-on, for Elis shot 17072018'
        ;stop                        
     END
     'CBR' : BEGIN
        NI_index = [[2,3],[0,0]] ; PINI 3, 4
        nb=2
                                ;NI_index = [2,0]     
     END
     'CCR' : BEGIN
        NI_index = [[2,3],[0,0]] ; PINI 3, 4
        nb=2
                                ;NI_index = [2,0]     
     END
     'CEF' : BEGIN
        if shot lt 38200 then NI_index = [[2],[0]] $ ; PINI 3  
        else NI_index = [[3],[1]]
        nb=1
        ;if getenv('USER') eq 'pcano' then stop
     END

     ELSE : BEGIN
        err_string =  'Unrecognised CXRS diagnostic name!'
        GOTO, FINISH
     ENDELSE
  ENDCASE
; intialise
  ier        = 0L
  dia_ref    = 0L
  datum      = string(' ',format='(a18)')
  dim        = 0L
  phys_unit  = 0L
  err_string = string(' ',format='(a80)')
  
; Open NI shot file
  NI_exp  =  'AUGD'
  NI_diag =  'NIS'
  NI_shot =  shot
  NI_edn  =  0L
;  s = call_external(!libddww, 'ddgetaug', 'ddopen', $
;                    ier, NI_exp, NI_diag, NI_shot, NI_edn, $
;                    dia_ref, datum)
;  if (ier gt 0) then begin
;    s = call_external(!libddww,'ddgetaug','xxerrprt',     $
;                      -1L,err_string,ier,3L,'ddopen:')
;    GOTO, FINISH
;  endif
  
; Read power waveforms
  
;  t1 = 0.0
;  t2 = 100.0
  name =  'PNIQ    '
  NI_time = 1L                  ; to force returning time base as well
                                ;read_signal,ier,dia_ref,name,t1,t2,NI_powers,$
                                ;            time=NI_time,quiet=quiet
  read_signal_mrm,ier, NI_shot,NI_diag,name,NI_time,NI_powers,phys_dim,edition=Ni_edn, exp=Ni_exp
;stop
;mcavedon: Problem with err code interpretation, needs to be fixed for
;          libddww8.so
;akappato: this  causes an error when no beams shf (NIS) is there
  IF ier NE 0 THEN BEGIN
     err_string =  'read_signal error: '+STRTRIM(STRING(ier),2)
     beam= fltarr( N_ELEMENTS(time)) ;UPlank: this is needed for beam modulation tool of CNR diagnostics
     GOTO, FINISH
  ENDIF
 
; Integrate over each CXRS exposure
  ntime =  N_ELEMENTS(time)
  NI_ave_power =  FLTARR(ntime)
  
  
  if nb eq 1 then begin
                                ; Extract the required source   
     NI_power = NI_powers[*,NI_index[0],NI_index[1]]
     FOR i=0L, ntime-1 DO BEGIN
        tstart = time[i] - exp[i]/2.
        tstop  = time[i] + exp[i]/2.
        
        
        wh_NI_time =  WHERE(NI_time GT tstart AND $
                            NI_time LT tstop, n_NI_time)
                                ; make sure end times are included
        tab_time  = [tstart, tstop]
        tab_power = INTERPOL(NI_power, NI_time, tab_time, /SPLINE)
        IF n_NI_time GT 0 THEN BEGIN
           tab_time  = [tab_time[0],  NI_time[wh_NI_time],  tab_time[1] ]
           tab_power = [tab_power[0], NI_power[wh_NI_time], tab_power[1]]
        ENDIF
        
        NI_ave_power[i] = INT_TABULATED(tab_time, tab_power)/(tstop-tstart)
        
     ENDFOR
     ;stop
     beam = (NI_ave_power GT power_threshold)
  endif
  
  if nb gt 1 then begin
; Extract the required source
     for ni=0,nb-1 do begin
        NI_power = NI_powers[*,NI_index[ni,0],NI_index[ni,1]]
       
        FOR i=0L, ntime-1 DO BEGIN
         
           
           tstart = time[i] - exp[i]/2.
           tstop  = time[i] + exp[i]/2.
           
           wh_NI_time =  WHERE(NI_time GT tstart AND NI_time LT tstop, $
                               n_NI_time)
           
                                ; make sure end times are included
           tab_time  = [tstart, tstop]
           tab_power = INTERPOL(NI_power, NI_time, tab_time, /SPLINE)
           IF n_NI_time GT 0 THEN BEGIN
              tab_time  = [tab_time[0],  NI_time[wh_NI_time],  tab_time[1] ]
              tab_power = [tab_power[0], NI_power[wh_NI_time], tab_power[1]]
           ENDIF
           
           NI_ave_power[i] = ni_ave_power[i]+INT_TABULATED(tab_time, tab_power)/(tstop-tstart)
        ENDFOR
        ;stop
        beam = (NI_ave_power GT power_threshold)        
     endfor
  endif
 
  NI_shotfile =  { experiment : NI_exp,  $
                   diagnostic : NI_diag, $
                   shot       : NI_shot, $
                   edition    : NI_edn   }
  
 ;stop
FINISH:
; Close shotfile
  s = call_external(!libddww,'ddgetaug','ddclose', $
                    ier,dia_ref)
  if (ier gt 0 and err_string EQ '') then begin
     s = call_external(!libddww,'ddgetaug','xxerrprt', $
                       -1L,err_string,ier,3L,'ddclose:')
  endif
  
  if STRLEN(STRTRIM(err_string, 2)) GT 0 THEN $
     err = id+err_string $
  else err =  ''
FINISH2:

END

