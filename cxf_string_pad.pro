PRO cxf_string_pad, string_matrix, length

; Pad/truncate a matrix of strings to a given length

  IF SIZE(string_matrix,/TYPE) NE 7 THEN BEGIN
    string_matrix=strtrim(string(string_matrix),2)
  ENDIF

  nelements   = SIZE(string_matrix,/N_ELEMENTS)
  ndimensions = SIZE(string_matrix,/N_DIMENSIONS)
  dimensions  = SIZE(string_matrix,/DIMENSIONS)

  IF ndimensions GE 1 THEN $
    string_matrix = REFORM(string_matrix,nelements)
  FOR i = 0L, nelements-1 DO BEGIN
    str_len = STRLEN(string_matrix[i])
    IF str_len GT length THEN BEGIN
      string_matrix[i] = STRMID(string_matrix[i],0,length)
    ENDIF ELSE IF STRLEN(string_matrix[i]) LT length THEN BEGIN
      blanks = STRING(' ', $
                FORMAT='(a'+STRTRIM(STRING(length-str_len),2)+')')
      string_matrix[i] = string_matrix[i]+blanks
    ENDIF
  ENDFOR

  IF ndimensions GE 1 THEN $
    string_matrix = REFORM(string_matrix,dimensions)

END ; cxf_string_pad
