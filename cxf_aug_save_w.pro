pro cxf_aug_save_w,topinfo_base

  usernm = Getenv('USER')
  experiment_list = ['AUGD',usernm]
  experiment_index = 0L ; private shot files
  
; build widget
  tlb=widget_base(title='Save Data',/column)

  base=widget_base(tlb,/row)
  label=widget_label(base,value='Experiment:')
  experiment=widget_droplist(base,value=experiment_list)
  widget_control,experiment,set_droplist_select=experiment_index

; comment text
; default comment is title, names of lines used and time range of fit:
  widget_control, topinfo_base, get_uvalue=topinfo
  specdata = *topinfo.expdata
  widget_control, topinfo.panel_window, get_value=fitparam
  outdata  = *topinfo.outdata
  
  ;if strupcase(outdata.spectrometer) EQ 'COR+CUR' then begin
  ;   ;help,topinfo
  ;   help,specdata
  ;   ;help,specdata.los_name
  ;   print,specdata.los_name
  ;   ;stop
  ;   help,outdata
  ;   ;stop
  ;endif
  
  comment=cxf_aug_default_comment(specdata,fitparam,outdata)

  base=widget_base(tlb,/row)
  comment_text  = Widget_Text(base, Value=comment, /Editable, $
                              XSize=72, YSize=32)

  base=widget_base(tlb,/row)
  ok_button=widget_button(base,value='Ok')
  cancel_button=widget_button(base,value='Cancel')


  info={ topinfo_base:topinfo_base , $
	 experiment:experiment, $
         comment_text:comment_text, $
	 ok_button:ok_button, $
	 cancel_button:cancel_button $
       }

  widget_control,tlb,set_uvalue=info      								
  widget_control, tlb, /realize
  xmanager, 'cxf_aug_save_w',tlb,event_handler='cxf_aug_save_e'

end
