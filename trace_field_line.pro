@/afs/ipp-garching.mpg.de/u/cxrs/idl/cxfit64/machines/aug/ask_for_new_shot.pro

pro trace_field_line,shot,time,r_start,z_start,phi_start,npnts,delta,r_line,z_line,phi_line, $
                    edition=edition,exper=exper,diag=diag,no_plot=no_plot,b_abs=b_abs,alter_shot=alter_shot


;   -> shot         : shotnumber
;   -> time         : time in discharge
;   -> r_start      : r-coordinate in m at middle point of magnetic field line
;   -> z_start      : z-coordinate in m at middle point of magnetic field line 
;   -> phi_start    : phi-coordinate in degrees at middle point of magnetic field line 
;   -> npnts        : number of points along magnetic field line
;   -> delta        : distance of points in m
;   <- r_line       : array of size npnts containing r-coordinates (in m) of points along magnetic field line
;   <- z_line       : array of size npnts containing z-coordinates (in m) of points along magnetic field line
;   <- phi_line     : array of size npnts containing phi-coordinates (in deg) of points along magnetic field line
;   keywords for FPP/EQU diagnostic
;

defsysv, '!LIBKK', exists=exists
if exists eq 0 then defsysv, '!LIBKK','/usr/ads/lib/libkk.so', 1

libkk=!libkk
error=long(0)
eq_edition = long(0)
eq_exper = 'AUGD'
eq_diag =  'FPP'

if keyword_set(exper) then eq_exper = exper
if keyword_set(diag) then eq_diag =  diag
if keyword_set(edition) then eq_edition = edition
shot_old=shot
if keyword_set(alter_shot) then shotl = long(alter_shot) else shotl = long(shot)
n=2l
r0=[r_start,r_start]
z0=[z_start,z_start]
phi0=[phi_start,phi_start]
fpf=r0
fJp=r0
B_r=r0*0.
B_z=r0*0.
B_t=r0*0.
B_norm=r0*0.

r_line=fltarr(npnts)
z_line=fltarr(npnts)
phi_line=fltarr(npnts)
b_abs=fltarr(npnts)


for i_pnt = npnts/2., npnts-1 do begin 

    r_line(i_pnt)=r0(0)
    z_line(i_pnt)=z0(0)
    phi_line(i_pnt)=phi0(0)
    b_abs(i_pnt) = b_norm

    result = CALL_EXTERNAL(libkk,'kkidl', 'kkrzBrzt', error ,$
           eq_exper, eq_diag , shotl , eq_edition, time, $
                    r0, z0 , n , B_r,B_z,B_t,fPf,fJp)
    print,'r0',r0,'z0',z0
    print,'Br ',B_r,' Bz ',B_z,' Bt ',B_t
    reask:
    if error then begin
        print,'-1 will cancel the writing of the shotfile'
        shot = ask_for_new_shot()
        shotl=long(shot)
        if shot eq -1 then begin
            print,'Danger! - retall'
            shot = shot_old
            retall      
        endif
        result = CALL_EXTERNAL(libkk,'kkidl', 'kkrzBrzt', error ,$
		       eq_exper, eq_diag , shotl , eq_edition, time, $
                        r0, z0 , n , B_r,B_z,B_t,fPf,fJp)
    endif 
    if error then goto, reask

    B_norm=sqrt(B_r^2+B_z^2+B_t^2)
    r0(0)=r0(0)-B_r/B_norm*delta
    z0(0)=z0(0)-B_z/B_norm*delta
    phi0(0)=(phi0(0)/180.*!Pi*r0(0)-B_t/B_norm*delta)/r0(0)/!Pi*180.
endfor

r0=[r_start,r_start]
z0=[z_start,z_start]
phi0=[phi_start,phi_start]
fpf=r0
fJp=r0
B_r=r0*0.
B_z=r0*0.
B_t=r0*0.

for i_pnt = npnts/2., 0,-1 do begin 

    r_line(i_pnt)=r0(0)
    z_line(i_pnt)=z0(0)
;    oplot,[r_line(i_pnt)],[z_line(i_pnt)],psym=4
    phi_line(i_pnt)=phi0(0)
    b_abs(i_pnt) = b_norm
    
    result = CALL_EXTERNAL(libkk,'kkidl', 'kkrzBrzt', error ,$
		       eq_exper, eq_diag , shotl , eq_edition, time, $
                        r0, z0 , n , B_r,B_z,B_t,fPf,fJp)


    B_norm=sqrt(B_r^2+B_z^2+B_t^2)

    r0(0)=r0(0)+B_r/B_norm*delta
    z0(0)=z0(0)+B_z/B_norm*delta
    phi0(0)=(phi0(0)/180.*!Pi*r0(0)+B_t/B_norm*delta)/r0(0)/!Pi*180.
    
endfor

if not keyword_set(no_plot) then begin
    !P.multi=[0.,2.,1.]
    set_color_right

    plot_asdup,shot=shotl,/new_coord
    oplot,r_line,z_line,psym=3,color=!mycol.blue
    oplot,r_line[0:n_elements(r_line)/2],z_line[0:n_elements(r_line)/2],psym=4,color=!mycol.blue
    oplot,[r_start],[z_start],psym=1,thick=3
    fluss,shotl,time
    plot_asdup,shot=shotl,/top_view,/new_coord
    oplot,r_line*cos(phi_line/180.*!Pi),r_line*sin(phi_line/180.*!Pi),psym=3,color=!mycol.blue
    oplot,r_line[0:n_elements(r_line)/2]*cos(phi_line[0:n_elements(r_line)/2]/180.*!Pi),r_line[0:n_elements(r_line)/2]*sin(phi_line[0:n_elements(r_line)/2]/180.*!Pi),psym=4,color=!mycol.blue
    oplot,[r_start*cos(phi_start/180.*!Pi)],[r_start*sin(phi_start/180.*!Pi)],psym=1,thick=3

endif
edition=eq_edition
shot = shot_old
;from web-documentation
;     ___________________ calculate  for (r,z)-values  --> Br,z,t_values
;                kkrzBrzt (iERR  ,expnam,dianam,nSHOT,nEDIT,tSHOT,
;               >                 rin,zin,lin,
;               <                 Br,Bz,Bt,   fPF,fJp)
;                -------------------------------------------------------



;dummy=' '
;read,dummy

end
