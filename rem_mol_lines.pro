pro rem_mol_lines,spectrometer_data,data_calib,wlen,beam

los_number = strarr(spectrometer_data.ntrack)

if spectrometer_data.spectrometer.name eq 'CVH' then begin
   for i=0,(spectrometer_data.ntrack-1) do begin
      los_number(i) = STRMID(spectrometer_data.los_name(i),4, 2)
   endfor
   los_number = float(los_number)
   last_LOS = where(los_number eq max(los_number))
   print,'Last LOS is'+' '+spectrometer_data.los_name(last_LOS)


   threshold=0.85
    print,'CVH'
    if (wlen lt 500) and (wlen gt 490) then begin
        print, 'Removing D2 lines'
        print, 'B V case'
        ;deu_low_wvl = 495.18
        ;deu_hig_wvl = 495.77
        ;act_low_wvl = 494.23
        ;act_hig_wvl = 494.78
        deu_low_wvl = 496.0
        deu_hig_wvl = 498.0
        act_low_wvl = 490.0
        act_hig_wvl = 499.0
    endif
    if (wlen lt 570) and (wlen gt 560) then begin
        print, 'Removing D2 lines'
        print, 'N VII case'
        ;deu_low_wvl = 568.1
        ;deu_hig_wvl = 568.52
        ;act_low_wvl = 566.49
        ;act_hig_wvl = 567.23
        deu_low_wvl = 567.26
        deu_hig_wvl = 567.51
        act_low_wvl = 564.0
        act_hig_wvl = 570.0   
    endif
endif

if spectrometer_data.spectrometer.name eq 'CXH' then begin
   for i=0,(spectrometer_data.ntrack-1) do begin
      los_number(i) = STRMID(spectrometer_data.los_name(i),4, 2)
   endfor
   los_number = float(los_number)
   last_LOS = where(los_number eq max(los_number))
   print,'Last LOS is'+' '+spectrometer_data.los_name(last_LOS)
   
   threshold=0.85
    print,'CXH'
    if (wlen lt 500) and (wlen gt 490) then begin
        print, 'Removing D2 lines'
        print, 'B V case'
        ;deu_low_wvl = 495.18
        ;deu_hig_wvl = 495.77
        ;act_low_wvl = 494.23
        ;act_hig_wvl = 494.78
        deu_low_wvl = 496.0
        deu_hig_wvl = 498.0
        act_low_wvl = 490.0
        act_hig_wvl = 499.0
    endif
    if (wlen lt 570) and (wlen gt 560) then begin
        print, 'Removing D2 lines'
        print, 'N VII case'
        deu_low_wvl = 567.26
        deu_hig_wvl = 567.51
        act_low_wvl = 564.0
        act_hig_wvl = 570.0       
        ;act_low_wvl = 566.80
        ;act_hig_wvl = 567.23
    endif
endif

if spectrometer_data.spectrometer.name eq 'CNR' then begin
   for i=0,(spectrometer_data.ntrack-1) do begin
      los_number(i) = STRMID(spectrometer_data.los_name(i),4, 2) ; here the distinction between pol. and tor. LoS has to be implemented ...
   endfor
   los_number = float(los_number)
   last_LOS = where(los_number eq max(los_number))
   print,'Last LOS is'+' '+spectrometer_data.los_name(last_LOS) 
   
   threshold=0.65
   print,'CNR'
   ;last_LOS=17
   ;print, 'Last LOS is'+' '+spectrometer_data.los_name(last_LOS) 
   if (wlen lt 500) and (wlen gt 490) then begin
        print, 'Removing D2 lines'
        print, 'B V case'
        deu_low_wvl = 496.0
        deu_hig_wvl = 498.0
        ;act_low_wvl = 480.23
        ;act_hig_wvl = 500.78
        act_low_wvl = 490.0
        act_hig_wvl = 499.0     
    endif
    if (wlen lt 570) and (wlen gt 560) then begin
        print, 'Removing D2 lines'
        print, 'N VII case'
        ;deu_low_wvl = 568.6
        ;deu_hig_wvl = 569.4
        ;act_low_wvl = 560.49
        ;act_hig_wvl = 572.23
        deu_low_wvl = 567.26
        deu_hig_wvl = 567.51
        act_low_wvl = 564.0
        act_hig_wvl = 570.0   
    endif
endif


if spectrometer_data.spectrometer.name eq 'CMR' or spectrometer_data.spectrometer.name eq 'CPR' then begin
   last_LOS=spectrometer_data.ntrack-1  
   print,'Last LOS is'+' '+spectrometer_data.los_name(last_LOS)
   
   threshold=0.
   print,'CMR or CPR'
   ;last_LOS=17
   ;print, 'Last LOS is'+' '+spectrometer_data.los_name(last_LOS) 
   if (wlen lt 500) and (wlen gt 490) then begin
        print, 'Removing D2 lines'
        print, 'B V case'
        deu_low_wvl = 496.0
        deu_hig_wvl = 498.0
        ;act_low_wvl = 480.23
        ;act_hig_wvl = 500.78
        act_low_wvl = 490.0
        act_hig_wvl = 499.0     
    endif
    if (wlen lt 570) and (wlen gt 560) then begin
        print, 'Removing D2 lines'
        print, 'N VII case'
        ;deu_low_wvl = 568.6
        ;deu_hig_wvl = 569.4
        ;act_low_wvl = 560.49
        ;act_hig_wvl = 572.23
        deu_low_wvl = 570.
        deu_hig_wvl = 570.25
        act_low_wvl = 562.0
        act_hig_wvl = 572.0   
    endif
endif



deu_low_index = indgen(spectrometer_data.ntrack)
deu_hig_index = indgen(spectrometer_data.ntrack)
act_low_index = indgen(spectrometer_data.ntrack)
act_hig_index = indgen(spectrometer_data.ntrack)
for i = 0,(spectrometer_data.ntrack -1) do begin
    deu_low_index(i) = where(abs(spectrometer_data.wavelength.data(*,i)-deu_low_wvl) eq min(abs(spectrometer_data.wavelength.data(*,i)-deu_low_wvl)))
    deu_hig_index(i) = where(abs(spectrometer_data.wavelength.data(*,i)-deu_hig_wvl) eq min(abs(spectrometer_data.wavelength.data(*,i)-deu_hig_wvl)))
    act_low_index(i) = where(abs(spectrometer_data.wavelength.data(*,i)-act_low_wvl) eq min(abs(spectrometer_data.wavelength.data(*,i)-act_low_wvl)))
    act_hig_index(i) = where(abs(spectrometer_data.wavelength.data(*,i)-act_hig_wvl) eq min(abs(spectrometer_data.wavelength.data(*,i)-act_hig_wvl)))
endfor
;ulp use while loop, if difference between high and low index is larger than 1
while min(deu_hig_index - deu_low_index) ne max(deu_hig_index - deu_low_index) do begin
    dummy = where((deu_hig_index - deu_low_index) eq min(min(deu_hig_index - deu_low_index)))
    deu_hig_index(dummy) = deu_hig_index(dummy) + 1
 endwhile

while min(act_hig_index - act_low_index) ne max(act_hig_index - act_low_index) do begin
    dummy = where((act_hig_index - act_low_index) eq min(min(act_hig_index - act_low_index)))
    act_hig_index(dummy) = act_hig_index(dummy) + 1
endwhile

for j=0,(spectrometer_data.ntrack - 1) do begin
    for i=0,(spectrometer_data.nframe - 1) do begin
       if beam(i) ge threshold then begin

            spectrometer_data.intensity.data(act_low_index(j):act_hig_index(j),j,i) = $
            data_calib(act_low_index(j):act_hig_index(j),j,i) - $
            data_calib(act_low_index(last_LOS):act_hig_index(last_LOS),last_LOS,i)* $
            abs(total(data_calib(deu_low_index(j):deu_hig_index(j),j,i)))/ $
            abs(total(data_calib(deu_low_index(last_LOS):deu_hig_index(last_LOS),last_LOS,i))) ;ulp: maybe it's better to use total (summation or integration) instead of the mean value?
        endif
endfor
endfor

stop

end
