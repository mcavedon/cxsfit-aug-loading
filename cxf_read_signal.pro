;
;   read_signal.pro - read any shot file signal/-group and its timebase
;
;
;	Wolfgang Suttrop, wls@slcwls
;	E1, Tel. 1466, L6, Zi. 156
;
;	27-Sep-94  wls  extracted from read_cec
;	09-Dec-94  wls	time can be any dimension (follow relations)
;	21-Dec-94  wls	time and area base read optional,
;			making use of DDAINFO and DDAGROUP
;	03-Apr-98  wls  allow for time dependent area base
;	16-Mar-99  wls	caution multiple time bases, absent signal
;       12-Sep-06  ldh  copy with name change to cxsfit library
;                       (name clashes were occuring with user_contrib)
;       08-Nov-05  wls  multidimensional area base fixed
;       18-Sep-06  ldh  include wls' fix

PRO cxf_read_signal,error,diaref,name,t1,t2,data,$
                    time=time,area=area,raw=raw

;	error	(<=)	libddww error code
;	diaref	(=>)	ddww file handle, shot file must be open!
;	name	(=>)	name of signal (group) of interest
;	t1,t2	(=>)	desired time range
;	data	(<=)	signal (group) data, memory allocated by 'read_signal'.
;			'data' assumes the dimensions given in shot file
;			exept for the time index which is sized according to
;			the t1, t2 range specified
;	time	(<=)	optional time base, memory allocated inside function
;	area	(<=)	optional area base, memory allocated inside function
;	raw	(=>)	optional keyword: if set (raw=1 or /raw) raw data is
;			read, i.e. no calibration steps are performed. 
;			By default, calibration is performed.

; Shot file access library
  defsysv, '!libddww', exists=exists
  if exists eq 0 then defsysv, '!libddww', '/usr/ads/lib/libddww.so', 1

; make sure parameters are of correct type

  error = LONG(error)
  diaref = LONG(diaref)

  ctrl = 3L
  k1=0L
  k2=0L
  rleng = 0L
  type = 2L
  ncal = 0L

  rname = string('',format='(a8)')
  buf=lonarr(26)
  rbuf=lonarr(26)
  text=string('',format='(a80)')
  physdim  = string('',format='(a12)')

  ;stop

; get time interval index range

; -------------------------------------------------------
; this doen't work if time is not first dimension !!!
;
;  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddtindex',$
;	error, diaref, name, FLOAT(t1), FLOAT(t2), k1, k2)
;  IF error NE 0 THEN BEGIN
;    print, 'cannot find time interval in signal ', name
;    error = -2L
;    RETURN
;  ENDIF
; -------------------------------------------------------



; get object header of this object for size & type information

  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddobjhdr',$
                      error, diaref, name, buf, text)

    ;stop

  IF error NE 0 THEN GOTO,quit
;debug
;  print,'buf  ', buf

  IF buf(2) EQ -1 THEN BEGIN
    print,'%READ_SIGNAL: signal '+name+' not found'
    error = -1L
    RETURN
  ENDIF

  IF buf(16) LT 1 OR buf(16) GT 4 THEN BEGIN
    print,'%READ_SIGNAL: unsupported dimension ',buf(16),' encountered'
    error = -1L
    RETURN
  ENDIF

  IF buf(0) LT 6 OR buf(0) GT 7 THEN BEGIN
    print,'%READ_SIGNAL: '+name+' must be signal or signal group'
    error = -1L
    RETURN
  ENDIF

  fi=[0L,0L,0L,0L]			; default index range: from start ...
  li=LONG(reverse(buf(18:21)-1))	; ... to actual length in each dim.
  ld=li-fi+1				; length in dimensions
; dim=(SIZE(WHERE(ld GT 1)))(1)		; number of dimensions


; ----------------------------------------------------------------------
; follow all relations and read time base

  ant=1   ; read area base only at one point if no time base found
  ak1=1   ; start with 1st index
  dim=0	  ; dimension teller

  tdim = -1 ; time base dimension
  adim = -1 ; area base dimension

  FOR i=4,11 DO BEGIN
    IF (buf(i) GT 0) AND (buf(i) NE 65535) THEN BEGIN	; is a defined relation
      s = CALL_EXTERNAL (!libddww,'ddgetaug','ddobjname',$
                      error,diaref,buf(i),rname)
      IF error NE 0 THEN GOTO,quit
      s = CALL_EXTERNAL (!libddww,'ddgetaug','ddobjhdr',$
                      error, diaref, rname, rbuf, text)
      IF error NE 0 THEN GOTO,quit
;debug
;      print,'rbuf ', rbuf

; evaluate relations

      CASE rbuf(0) OF
        8: BEGIN		

; read time base array

	     IF tdim LT 0 THEN BEGIN
	       IF(ld(dim) NE rbuf(21)) THEN BEGIN
		  print,'Time Base Dimensionen passen nicht zum Signal'
		  print,'oder das Programm hat noch Macken'
		  error = -2
                  GOTO,quit
	       ENDIF

	       length = LONG (rbuf(21))
	       k1=1L  &  k2= LONG(length)
               time = fltarr(length)
               s = CALL_EXTERNAL (!libddww,'ddgetaug','ddtbase', $
                       error,diaref,rname,k1,k2,type,length,time,rleng)
               IF error NE 0 THEN GOTO,quit

; determine desired subrange

	       wh=WHERE(time GE t1 AND time LE t2)
	       IF wh(0) EQ -1 THEN BEGIN
    	         print,'%READ_SIGNAL: signal ',name,' has time range t=',$
		  time(0),' ... ',time(rleng-1),' s.'
                 print,'There are NO DATA POINTS between ',t1,' s and ',t2,' s'
	         error= -2
	         GOTO,quit	
	       ENDIF

	       IF N_ELEMENTS(wh) NE rleng THEN BEGIN   ; adjust to subrange
		  time = TEMPORARY(time(wh))
	          fi(dim) = MIN(wh)
	          li(dim) = MAX(wh)
;	          ld(dim)=li(dim)-fi(dim)+1
	       ENDIF
	       ant = li(dim)-fi(dim)+1  ; # time indices for area base
	       ak1 = fi(dim)+1          ; start time index for area base
	       tdim = dim
	       dim=dim+1
	    ENDIF
	  END   ; 8=time base

       13: BEGIN

; check area base dimensions
  	     adim=0	 ; area base dimension
  	     FOR j=0,2 DO IF rbuf(18+j) GT 0 THEN BEGIN
                 IF ld(dim) NE rbuf(18+j) THEN BEGIN
		   print,'Area Base Dimensionen passen nicht zum Signal'
		   print,'oder das Programm hat noch Macken'
                   error= -2
		   GOTO,quit
                 ENDIF
                 adim=adim+1
                 dim=dim+1
               ENDIF
             END   ; 13=area base

        ELSE:  ; do nothing (yet)

      ENDCASE
    ENDIF
  ENDFOR


; ----------------------------------------------------------------------
; follow all relations and read area base

  FOR i=4,11 DO BEGIN
    IF (buf(i) GT 0) AND (buf(i) NE 65535) THEN BEGIN	; is a defined relation
      s = CALL_EXTERNAL (!libddww,'ddgetaug','ddobjname',$
                      error,diaref,buf(i),rname)
      IF error NE 0 THEN GOTO,quit
      s = CALL_EXTERNAL (!libddww,'ddgetaug','ddobjhdr',$
                      error, diaref, rname, rbuf, text)
      IF error NE 0 THEN GOTO,quit

; evaluate relations

      CASE rbuf(0) OF
        8: BEGIN		; ignore time base here
	   END

       13: BEGIN

; read area base
	     IF KEYWORD_SET(area) THEN BEGIN
	       IF rbuf(21) LT ant THEN ant = 1
               CASE adim OF 
                 1: BEGIN 
	              areabuf=fltarr(rbuf(18))
	              IF ant GT 1 THEN area=fltarr(ant, rbuf(18))
	            END
		 2: BEGIN
	              areabuf=fltarr(rbuf(18)+rbuf(19))
	              IF ant GT 1 THEN area=fltarr(ant, rbuf(18)+rbuf(19))
                    END
		 3: BEGIN
	              areabuf=fltarr(rbuf(18)+rbuf(19)+rbuf(20))
	              IF ant GT 1 THEN area=fltarr(ant, rbuf(18)+rbuf(19)+rbuf(20))
	 	    END
	       ENDCASE
	       sa=SIZE(areabuf)
	       alength=LONG(sa((SIZE(sa))(1)-1))

               FOR j=0,ant-1 DO BEGIN	; for every time index
	         IF ant EQ 1 THEN ak=1L ELSE ak=LONG(j+ak1)
		 s = CALL_EXTERNAL (!libddww,'ddgetaug','ddagroup', error,$
              	      diaref, name,ak,ak,type,alength,areabuf,rleng)
                 IF error NE 0 THEN GOTO,quit

	         IF ant EQ 1 THEN area=areabuf $
	         ELSE area(j,*)=areabuf

               ENDFOR
             ENDIF
	   END
        ELSE:  ; do nothing
      ENDCASE
    ENDIF
  ENDFOR



; ------------------------------------------------
; allocate data storage space


  ld(0)=li(0)-fi(0)+1

;  IF KEYWORD_SET(raw) AND $
;       ((buf(0) EQ 7 AND buf(14) EQ 3)   OR   (buf(0) EQ 6 AND buf(4) EQ 0)) $
;       THEN BEGIN
  IF KEYWORD_SET(raw) THEN BEGIN
    CASE buf[14] OF
      2: BEGIN  ; character*1
        type=6L
        string_length = 1L ; one byte long
        CASE buf(16) OF
          1: data=strarr(ld(0))
          2: data=strarr(ld(0), ld(1))
          3: data=strarr(ld(0), ld(1), ld(2))
          4: data=strarr(ld(0), ld(1), ld(2), ld(3))
        ENDCASE
        data = data + ' '
      END
      3: BEGIN  ; short integer
        type=0L
        CASE buf(16) OF
          1: data=intarr(ld(0))
          2: data=intarr(ld(0), ld(1))
          3: data=intarr(ld(0), ld(1), ld(2))
          4: data=intarr(ld(0), ld(1), ld(2), ld(3))
        ENDCASE
      END
      4: BEGIN  ; long integer
        type=1L
        CASE buf(16) OF
          1: data=lonarr(ld(0))
          2: data=lonarr(ld(0), ld(1))
          3: data=lonarr(ld(0), ld(1), ld(2))
          4: data=lonarr(ld(0), ld(1), ld(2), ld(3))
        ENDCASE
      END
      5: BEGIN  ; float
        type=2L
        CASE buf(16) OF
          1: data=fltarr(ld(0))
          2: data=fltarr(ld(0), ld(1))
          3: data=fltarr(ld(0), ld(1), ld(2))
          4: data=fltarr(ld(0), ld(1), ld(2), ld(3))
        ENDCASE
      END
      6: BEGIN  ; double
        type=3L
        CASE buf(16) OF
          1: data=dblarr(ld(0))
          2: data=dblarr(ld(0), ld(1))
          3: data=dblarr(ld(0), ld(1), ld(2))
          4: data=dblarr(ld(0), ld(1), ld(2), ld(3))
        ENDCASE
      END
      1794: BEGIN  ; character*8
        type=6L
        string_length = 8L ; eight bytes long
        CASE buf(16) OF
          1: data=strarr(ld(0))
          2: data=strarr(ld(0), ld(1))
          3: data=strarr(ld(0), ld(1), ld(2))
          4: data=strarr(ld(0), ld(1), ld(2), ld(3))
        ENDCASE
        data = data + STRING('',FORMAT='(a8)')
      END
      3842: BEGIN  ; character*16
        type=6L
        string_length = 16L ; sixteen bytes long
        CASE buf(16) OF
          1: data=strarr(ld(0))
          2: data=strarr(ld(0), ld(1))
          3: data=strarr(ld(0), ld(1), ld(2))
          4: data=strarr(ld(0), ld(1), ld(2), ld(3))
        ENDCASE
        data = data + STRING('',FORMAT='(a16)')
      END
      7938: BEGIN  ; character*32
        type=6L
        string_length = 32L ; thirty two bytes long
        CASE buf(16) OF
          1: data=strarr(ld(0))
          2: data=strarr(ld(0), ld(1))
          3: data=strarr(ld(0), ld(1), ld(2))
          4: data=strarr(ld(0), ld(1), ld(2), ld(3))
        ENDCASE
        data = data + STRING('',FORMAT='(a32)')
      END
      12034: BEGIN  ; character*48
        type=6L
        string_length = 48L ; forty eight bytes long
        CASE buf(16) OF
          1: data=strarr(ld(0))
          2: data=strarr(ld(0), ld(1))
          3: data=strarr(ld(0), ld(1), ld(2))
          4: data=strarr(ld(0), ld(1), ld(2), ld(3))
        ENDCASE
        data = data + STRING('',FORMAT='(a48)')
      END
      16130: BEGIN  ; character*64
        type=6L
        string_length = 64L ; sixty four bytes long
        CASE buf(16) OF
          1: data=strarr(ld(0))
          2: data=strarr(ld(0), ld(1))
          3: data=strarr(ld(0), ld(1), ld(2))
          4: data=strarr(ld(0), ld(1), ld(2), ld(3))
        ENDCASE
        data = data + STRING('',FORMAT='(a64)')
      END
      18178: BEGIN  ; character*72
        type=6L
        string_length = 72L ; seventy two bytes long
        CASE buf(16) OF
          1: data=strarr(ld(0))
          2: data=strarr(ld(0), ld(1))
          3: data=strarr(ld(0), ld(1), ld(2))
          4: data=strarr(ld(0), ld(1), ld(2), ld(3))
        ENDCASE
        data = data + STRING('',FORMAT='(a72)')
      END
    ENDCASE
  ENDIF ELSE BEGIN
    CASE buf[14] OF
      6: BEGIN  ; double
        type=3L
        CASE buf(16) OF
          1: data=dblarr(ld(0))
          2: data=dblarr(ld(0), ld(1))
          3: data=dblarr(ld(0), ld(1), ld(2))
          4: data=dblarr(ld(0), ld(1), ld(2), ld(3))
        ENDCASE
      END
      ELSE: BEGIN  ; float
        type=2L
        CASE buf(16) OF
          1: data=fltarr(ld(0))
          2: data=fltarr(ld(0), ld(1))
          3: data=fltarr(ld(0), ld(1), ld(2))
          4: data=fltarr(ld(0), ld(1), ld(2), ld(3))
        ENDCASE
      END
    ENDCASE
  ENDELSE

; read signal (group) array

  k1=LONG(fi(0)+1)
  k2=LONG(li(0)+1)
  length=LONG(ld(0))
  IF type EQ 6L THEN length = length*string_length ; now in bytes

; Here, I'm having trouble with reading the entire signal group
; ==> try DDxtrsignal (as in ISIS) and ask Annedore!
  IF buf(0) EQ 6 THEN   $      ; signal group
    IF KEYWORD_SET(raw) THEN $
      s = CALL_EXTERNAL (!libddww,'ddgetaug','ddsgroup',$
                     error,diaref,name,k1,k2,type,length,data,rleng)  $
    ELSE  $
      s = CALL_EXTERNAL (!libddww,'ddgetaug','ddcsgrp',$
            error,diaref,name,k1,k2,type,length,data,rleng,ncal,physdim) $

;  IF buf[0] EQ 6 THEN BEGIN    ; signal group
;    temp = data[*,0,0,0]
;    FOR k = 0L, ld[3]-1 DO BEGIN
;      FOR j = 0L, ld[2]-1 DO BEGIN
;        FOR i = 0L, ld[1]-1 DO BEGIN
;          indices = [i+1,j+1,k+1]
;          IF KEYWORD_SET(raw) THEN $
;            s = CALL_EXTERNAL (!libddww,'ddgetaug','ddxtrsignal',$
;                               error,diaref,name,k1,k2,indices, $
;                               type,length,temp,rleng)  $
;          ELSE  $
;            s = CALL_EXTERNAL (!libddww,'ddgetaug','ddcxsig',$
;                               error,diaref,name,k1,k2,indices, $
;                               type,length,temp,rleng,ncal,physdim)
;          data[*,i,j,k] = temp
;        ENDFOR
;      ENDFOR
;    ENDFOR
;  ENDIF $

  ELSE IF buf(0) EQ 7 THEN  $  ; signal
    IF KEYWORD_SET(raw) THEN $
      s = CALL_EXTERNAL (!libddww,'ddgetaug','ddsignal',$
                     error,diaref,name,k1,k2,type,length,data,rleng)  $
    ELSE  $
      s = CALL_EXTERNAL (!libddww,'ddgetaug','ddcsgnl',$
            error,diaref,name,k1,k2,type,length,data,rleng,ncal,physdim)

    if error ne 0 then begin
        ;Ignore warnings
        s = CALL_EXTERNAL (!libddww,'ddgetaug','xxwarn', error, ctrl, ' ')
        if s eq 1 then begin
            s = CALL_EXTERNAL (!libddww,'ddgetaug','xxerror', error, ctrl, ' ')
            error=0L
        endif
        ;Stop at errors
        s = CALL_EXTERNAL (!libddww,'ddgetaug','xxsev', error, ctrl, ' ')
        if s eq 1 then GOTO,quit
    endif

; Ignore warnings
   ; IF BYTE(error,2) EQ 1 THEN error = 0L
;  IF    BYTE(error,0) EQ 33 $ ; DD-library
;    AND BYTE(error,1) EQ 27 $ ; ddcsgrp
;    AND BYTE(error,2) EQ  1 $ ; warning
;    AND BYTE(error,3) EQ  2 $ ; "No PARAM_SET found"
;  THEN error = 0L

  ;IF error NE 0L THEN GOTO,quit

  CASE buf(16) OF
    1: ;
    2: data=TEMPORARY(data(*,fi(1):li(1)))
    3: data=TEMPORARY(data(*,fi(1):li(1),fi(2):li(2)))
    4: data=TEMPORARY(data(*,fi(1):li(1),fi(2):li(2),fi(3):li(3)))
 ENDCASE

quit:
  IF error GT 0 THEN $
    s = CALL_EXTERNAL (!libddww,'ddgetaug','xxerror', error, ctrl, ' ')

END
