
; Get input specifying an AUG shot file via a widget interface

FUNCTION cxf_get_aug_shotfile_gv, id

;
; ldh 22.03.06 - first version
;
;
; adw 12.09.06 - If an id of -1 is given then return an empty structure
;

  if id eq -1 then begin
    	shotfile = {	type          : 'SF',           $
              		experiment    : 'AUGD',		$
              		diagnostic    : '',       	$
              		shot          : 0L, 		$
              		shot_found    : 0L,             $
              		edition       : 0L,  		$
              		edition_found : 0L		}
	  RETURN, shotfile
  endif

  Widget_Control, id, get_uvalue=info

  Widget_Control, info.ExpID,  get_value=cexp
  Widget_Control, info.DiagID, get_value=cdiag
  Widget_Control, info.ShotID, get_value=cshot
  Widget_Control, info.EdnID,  get_value=cedn

  shotfile = {type          : 'SF',           $
              experiment    : cexp[0],        $
              diagnostic    : cdiag[0],       $
              shot          : LONG(cshot[0]), $
              shot_found    : 0L,             $
              edition       : LONG(cedn[0]),  $
              edition_found : 0L          }

  RETURN, shotfile

END
