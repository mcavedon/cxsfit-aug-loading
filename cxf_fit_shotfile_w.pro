pro cxf_fit_shotfile_w,topinfo_base

	widget_control,topinfo_base,get_uvalue=topinfo

	tlb=widget_base(title='Read settings from shotfile',/column)

	shotfile=cxf_get_aug_shotfile_gv(-1)


	if ptr_valid(topinfo.expdata) then begin
		shotfile.shot=(*topinfo.expdata).shot_nr
		shotfile.shot_found=(*topinfo.expdata).shot_nr
	
		if strupcase((*topinfo.expdata).spectrometer.name) eq 'CER' then begin
			shotfile.diagnostic='CEZ'
		endif
	
		if strupcase((*topinfo.expdata).spectrometer.name) eq 'CHR' then begin
			shotfile.diagnostic='CHZ'
		endif
	
		if strupcase((*topinfo.expdata).spectrometer.name) eq 'CMR' then begin
			shotfile.diagnostic='CMZ'
		endif
                
		if strupcase((*topinfo.expdata).spectrometer.name) eq 'CPR' then begin
			shotfile.diagnostic='CPZ'
		endif
                
		if strupcase((*topinfo.expdata).spectrometer.name) eq 'CNR' then begin
			shotfile.diagnostic='CNZ'
		endif
                
		if strupcase((*topinfo.expdata).spectrometer.name) eq 'CXH' then begin
			shotfile.diagnostic='CXZ'
		endif 
                               	
		if strupcase((*topinfo.expdata).spectrometer.name) eq 'CVH' then begin
			shotfile.diagnostic='CVZ'
		endif
                                
		if strupcase((*topinfo.expdata).spectrometer.name) eq 'LIA' then begin
			shotfile.diagnostic='LIT'
		endif
	        
                if strupcase((*topinfo.expdata).spectrometer.name) eq 'CAR' then begin
			shotfile.diagnostic='CAZ'
		endif
                
                if strupcase((*topinfo.expdata).spectrometer.name) eq 'CBR' then begin
			shotfile.diagnostic='CBZ'
		endif
                
                if strupcase((*topinfo.expdata).spectrometer.name) eq 'CCR' then begin
			shotfile.diagnostic='CCZ'
		endif
                if strupcase((*topinfo.expdata).spectrometer.name) eq 'CEF' then begin
			shotfile.diagnostic='CMI'
		endif
	endif

	shotfile_widget = cxf_get_aug_shotfile_cw(tlb,shotfile=shotfile)

        base=widget_base(tlb,/row)

        ok_button=widget_button(base,value='Ok')
  	cancel_button=widget_button(base,value='Cancel')

  	info={ 	topinfo_base:topinfo_base , $
                shotfile_widget:shotfile_widget, $
	 	ok_button:ok_button, $
	 	cancel_button:cancel_button $
       		}

	widget_control,tlb,set_uvalue=info      								
  	widget_control, tlb, /realize
  	xmanager, 'cxf_fit_shotfile_w',tlb,event_handler='cxf_fit_shotfile_e'



end
