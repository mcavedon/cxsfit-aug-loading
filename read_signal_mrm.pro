; $Id: //afs/ipp/u/mrm/idl/user_contrib/mrm/readdata/read_signal.pro#1 $
;
; Copyright (c)  2010, Max-Planck-Istitut fuer Plasmaphysik, EURATOM Association
;       All rights reserved.
;+
; NAME:
;	READ_SIGNAL
;
; PURPOSE:
;	Read level-0 SSX data from hotlink based Soft X-Ray camera data
;	at ASDEX Upgrade in the AUG envoronment.
;
; CATEGORY:
;	AUG data retrieval routines.
;
; CALLING SEQUENCE:
;	read_signal,ier,shot,diag_name,sig_name,time,sig,phys_dim
;
; INPUTS, OTUPUTS, KEYWORD PARAMETERS:
;	according to programme header:
;
;pro read_signal,ier,       $			; -> error level on exit
;		shot,      $			;<-  requested shot
;		diag_name, $			;<-  requested diagnostic
;		sig_name,  $			;<-  requested signal/signal group name
;		time,      $			; -> time vector (empty, if suppressed, SIO !)
;		sig,       $			; -> data array or matrix
;		phys_dim,  $ 			; -> physical unit
;		edition = edition, $		;<-> requested edition, real opened edition
;		time_ft = time_ft, $		; -> restricted time window request
;		indices = indices,  $		; -> requested indices for signal groups
;		exp=exp,   $			; -> alternatively specified experiment (default: AUGD)
;		raw_data=raw_data, $		;<-  keyword to request raw ADC values
;		text=text, $			; -> named variable for signal description from sfh
;		t_index=t_index, $		;<-> time indices for read data,
;						;    reuse for reading with keyword no_time set
;		no_time=no_time, $		;<-  suppress time base for SIO diagnostics
;		dia_ref_ext=dia_ref_ext, $	;<-  diref of already open shotfile 
;		double=double, $		;<-  force double data and time vector
;		debug=debug, $			;<-  request additionl debug information during runtime
;		help=help, $			;<-  show detailed help
;		area_base = area_base		;<-> returns the area-base of the signal in named variable
;
; RESTRICTIONS:
;	Works only in the AUG afs environment togehter with the libddww.
;
; EXAMPLE:
;	read_signal,ier,25520,'SSX','J_053',time,data,dim,time_ft=[1.,4.]
;	plot,time,data
;	read_signal,ier,25520,'CEC','Trad-A',time,data,dim,time_ft=[1.,4.]
;	plot_wid,data,time
;	read_signal,ier,25520,'CEC','Trad-A',time,data,dim,time_ft=[1.,4.],indices=[50l,1l,1l]
;	plot,time,data
;
; MODIFICATION HISTORY:
;	Written by:	many IPP authors, mainly sdp, over the years, Mon Jul 26 09:53:07 2010
;	26.7.2010	first documentation added
;
;-

pro read_signal_aug,ier,       $			; -> error level on exit
		shot,      $			;<-  requested shot
		diag_name, $			;<-  requested diagnostic
		sig_name,  $			;<-  requested signal/signal group name
		time,      $			; -> time vector (empty, if suppressed, SIO !)
		sig,       $			; -> data array or matrix
		phys_dim,  $ 			; -> physical unit
		edition = edition, $		;<-> requested edition, real opened edition
		time_ft = time_ft, $		; -> restricted time window request
		indices = indices,  $		; -> requested indices for signal groups
		exp=exp,   $			; -> alternatively specified experiment (default: AUGD)
		raw_data=raw_data, $		;<-  keyword to request raw ADC values
		text=text, $			; -> named variable for signal description from sfh
		t_index=t_index, $		;<-> time indices for read data,
						;    reuse for reading with keyword no_time set
		no_time=no_time, $		;<-  suppress time base for SIO diagnostics
		dia_ref_ext=dia_ref_ext, $	;<-  diref of already open shotfile 
		double=double, $		;<-  force double data and time vector
		debug=debug, $			;<-  request additionl debug information during runtime
		help=help, $			;<-  show detailed help
		area_base = area_base		;<-> returns the area-base of the signal in named variable

;		format_ext=format_ext		;<-  force specific time base format


if (n_params() le 6) then begin
	print,'usage:    read_signal_aug, ier,shot,diag_name,sig_name,time,sig,phys_dim,edition=edition,time_ft=time_ft,indices=indices,exp=exp'
	if (keyword_set(help)) then doc_library,'read_signal'
	return
endif


read_signal,ier,       $			; -> error level on exit
		shot,      $			;<-  requested shot
		diag_name, $			;<-  requested diagnostic
		sig_name,  $			;<-  requested signal/signal group name
		time,      $			; -> time vector (empty, if suppressed, SIO !)
		sig,       $			; -> data array or matrix
		phys_dim,  $ 			; -> physical unit
		edition = edition, $		;<-> requested edition, real opened edition
		time_ft = time_ft, $		; -> restricted time window request
		indices = indices,  $		; -> requested indices for signal groups
		exp=exp,   $			; -> alternatively specified experiment (default: AUGD)
		raw_data=raw_data, $		;<-  keyword to request raw ADC values
		text=text, $			; -> named variable for signal description from sfh
		t_index=t_index, $		;<-> time indices for read data,
						;    reuse for reading with keyword no_time set
		no_time=no_time, $		;<-  suppress time base for SIO diagnostics
		dia_ref_ext=dia_ref_ext, $	;<-  diref of already open shotfile 
		double=double, $		;<-  force double data and time vector
		debug=debug, $			;<-  request additionl debug information during runtime
		help=help, $			;<-  show detailed help
		area_base = area_base		;<-> returns the area-base of the signal in named variable

;		format_ext=format_ext		;<-  force specific time base format

end




pro read_signal_mrm,ier,       $			; -> error level on exit
		shot,      $			;<-  requested shot
		diag_name, $			;<-  requested diagnostic
		sig_name,  $			;<-  requested signal/signal group name
		time,      $			; -> time vector (empty, if suppressed, SIO !)
		sig,       $			; -> data array or matrix
		phys_dim,  $ 			; -> physical unit
		edition = edition, $		;<-> requested edition, real opened edition
		time_ft = time_ft, $		; -> restricted time window request
		indices = indices,  $		; -> requested indices for signal groups
		exp=exp,   $			; -> alternatively specified experiment (default: AUGD)
		raw_data=raw_data, $		;<-  keyword to request raw ADC values
		text=text, $			; -> named variable for signal description from sfh
		t_index=t_index, $		;<-> time indices for read data,
						;    reuse for reading with keyword no_time set
		no_time=no_time, $		;<-  suppress time base for SIO diagnostics
		dia_ref_ext=dia_ref_ext, $	;<-  diref of already open shotfile 
		double=double, $		;<-  force double data and time vector
		debug=debug, $			;<-  request additionl debug information during runtime
		help=help, $			;<-  show detailed help
		area_base = area_base		;<-> returns the area-base of the signal in named variable

;		format_ext=format_ext		;<-  force specific time base format


if (n_params() le 6) then begin
	print,'usage:    read_signal, ier,shot,diag_name,sig_name,time,sig,phys_dim,edition=edition,time_ft=time_ft,indices=indices,exp=exp'
	if (keyword_set(help)) then doc_library,'read_signal'
	return
endif



 defsysv, '!libddww', exists=exists
 if exists ne 1 then begin
	;defsysv, '!libddww', '/usr/ads/lib/libddww.so'
	if (!version.memory_bits eq 64) then $
		lib_path='/usr/ads/lib64' $
	else $
		lib_path='/usr/ads/lib'
	libddww = lib_path+'/libddww8.so'
 endif else begin
	libddww = !libddww
 endelse


 ier = 0l
 if (keyword_set(debug)) then begin
	print,format='("called: read_signal,",i0,",",i0,",",a,",",a,",time,data")', $
		ier,shot,diag_name,sig_name	
 endif

 ;
 ; General information about data types:
 ;
 ; text description according to /usr/ads/codes/formats :
 format_txt = ['BYTE','CHAR','SHORT_INT','INTEGER','IEEE_FLOAT','IEEE_DOUBLE', $
               'LOGICAL','CHAR_REAL','U_SHORT','IBM_REAL','IBM_DOUBLE', $
;	       'CONDENSED_TB','LONGLONG','U_INTEGER','U_LONGLONG', $
               'CHAR_8','CHAR_16','CHAR_32','CHAR_48','CHAR_64','CHAR_72']
 ; DDWW array type at return (ddobjval,'format'>format[0]): 
 format_val = long([1,2,3,4,5,6,7,8,9,10,11, $
;	12,13,14,15, $
	1794,3842,7938,12034,16130,18178])
 ; type which is used for reading in IDL and translated to DDWW
 ; according to /usr/ads/include/usertype.h :
 ;type_val   = [2,6,2,2,2,2,1,2,2, 2, 3,   6,   6,   6,    6,    6,    6]
 type_val   = long([2,6,2,1,2,3,1,2,2,2,3,   $
;	14, 10, 2, 3, $	
;	14, 10, 13, 14, $	
	6,   6,   6,    6,    6,    6])
 ; IDL array type for allocating memory within IDL (idlhelp -v 6.4, size command):
 ;array_type = [4,7,4,4,4,4,3,4,4, 4, 5,   7,   7,   7,    7,    7,    7]       
 array_type = long([4,7,4,3,4,5,3,4,4,4,5,   $
;	15, 14, 4, 5, $	
;	15, 14, 13, 15, $	
	7,   7,   7,    7,    7,    7])




 if (not keyword_set(edition)) then ed = 0l else ed = long(edition)
  
 shot      = long(shot)
 ier       = 0L
 k1        = 0L
 k2        = 0L
 dim       = 0L
 ctrl      = 3L
 ncal      = 0L
 ntval     = 0L
 leng      = 0L
 npretrig  = 0L
 tbeg      = 0.0
 tend      = 0.0
 if KEYWORD_SET(exp) then begin
    exp    = STRUPCASE(STRTRIM(exp, 2))
 endif else begin
    exp    = 'AUGD'
 endelse
 datum     = string('',format='(a18)')
 phys_dim  = string('',format='(a12)')

;_______________________________________________________________________________

;OPEN SHOTFILE

 if (keyword_set(dia_ref_ext)) then begin
	dia_ref=long(dia_ref_ext)
 endif else begin
	dia_ref=0L
	s = call_external(libddww,'ddgetaug','ddopen',         $
                   ier,exp,diag_name,shot,ed,dia_ref,datum)
	if (ier gt 0) then begin
	    s = call_external(libddww,'ddgetaug','xxerror',ier,ctrl,'ddopen: '+ $
		exp+':'+diag_name+strtrim(shot,2)+'.'+strtrim(ed,2))
	    return
	endif
;	dia_ref_ext=dia_ref
 endelse

    
;GET INFORMATION

 text	= '                                                                 '
 obuf	= lonarr(26)
 ier	= 0L
 s = CALL_EXTERNAL(libddww,'ddgetaug','ddobjhdr', $
                   ier,dia_ref,sig_name,obuf,text)
 if (ier ne 0) then begin
    s = CALL_EXTERNAL(libddww,'ddgetaug','xxerror',ier, ctrl, 'ddobjhdr: '+sig_name)
    goto, close
 endif  
  
 obj_type = 0l
 s = CALL_EXTERNAL(libddww,'ddgetaug','ddobjval', $
                   ier,dia_ref,sig_name,'objtype',obj_type)
 if (ier ne 0) then begin
    s = CALL_EXTERNAL(libddww,'ddgetaug','xxerror',ier, ctrl, ' ')
    goto, close
 endif  
 
 obj_size = 0l
 s = CALL_EXTERNAL(libddww,'ddgetaug','ddobjval', $
                   ier,dia_ref,sig_name,'size',obj_size)
 if (ier ne 0) then begin
    s = CALL_EXTERNAL(libddww,'ddgetaug','xxerror',ier, ctrl, 'ddobjval: '+sig_name+', size')
 endif
 k1 = 1l
 k2 = long(obj_size)

; indices in call to ddobjval only valid for sig_group
; => moved to sig_group only branch
;
; dim_arr = lonarr(3)   
; if (obj_type eq 6) then begin	; read for SIG_GROUP the size of the dimensions:
;   s = CALL_EXTERNAL (libddww,'ddgetaug','ddobjval', $
;                    ier,dia_ref,sig_name,'indices',dim_arr)
;   if (ier ne 0) then begin
;     s = CALL_EXTERNAL(libddww,'ddgetaug','xxerror',ier, ctrl, 'ddobjval: '+sig_name+'indices')
;     print,dim_arr
;   endif
; endif

 ; check if valid relation can be found and assume
 ; that a timebase is included. Otherwise it will crash!
 relations = lonarr(8)
 s = CALL_EXTERNAL(libddww,'ddgetaug','ddobjval', $
                   ier,dia_ref,sig_name,'relations',relations)
 if (ier ne 0) then $
    s = CALL_EXTERNAL(libddww,'ddgetaug','xxerror',ier, ctrl, ' ')
 tmp = where(relations gt 0,count)




 if ((not keyword_set(no_time)) and (not keyword_set(t_index))) then begin
 ; try only to read a time base if the timebase is
 ; not explicitly deselected by no_time and a valid
 ; index array from an previous call to read_signal
 ; is used.
 ; This neccassary for the SIO cards, where the retrieval
 ; of the timebase takes ages!!!!
 ; mrm, 23.04.2009

 if (count gt 0) then begin

    ; READ TIME-BASE, if a valid relation has been found.
    ; Otherwise the timebase is an index array!
    ;
    ; read format of timebase:
    ;
    ier = 0L
    typ = 0L
    tname = '        '
    ind = lonarr(4)
    ; get timebase name for signal:
    s = CALL_EXTERNAL(libddww,'ddgetaug','ddsinfo', $
                   ier,dia_ref,sig_name,typ,tname,ind)
    if (ier ne 0) then s=CALL_EXTERNAL(libddww,'ddgetaug','xxerror',ier, ctrl, 'dsinfo')
    if(keyword_set(debug)) then begin
	print,format='("ddsinfo = ",a,",name=",a,",type=",i0,",time_name=",a,",len=",i0)', $
		s,sig_name,typ,tname,strjoin(ind,',')
    endif

    ; get format for timebase itself:
    ier = 0L
    time_format = lonarr(3)
;    s = CALL_EXTERNAL(libddww,'ddgetaug','ddobjval', $
;                   ier,dia_ref,tname,'format',time_format)
    time_format[0] = 6	; force timebase always to DOUBLE !!!!
;    message,'Forcing timebase to double format !!!',/cont
    if (ier ne 0) then s=CALL_EXTERNAL(libddww,'ddgetaug','xxerror',ier, ctrl, ' ')
    time_ind = type_val(where(time_format(0) eq format_val) > 0)
    time_type = (long(type_val(where(time_format(0) eq format_val) > 0)))(0)
    time_arr_typ = (array_type(where(time_format(0) eq format_val) > 0))(0)
    time_format_txt = (format_txt(where(time_format(0) eq format_val) > 0))(0)

    if(keyword_set(debug)) then begin
	print,'time_base format:'
        print,'time_format = ',time_format
        print,'time_ind = ',time_ind
        print,'time_typ = ',time_type
        print,'time_arr_typ = ',time_arr_typ
        print,'time_format_txt = ',time_format_txt
    endif

    ; get information for the entire time range:
    ier = 0L
    _tbeg = 0.0	; double !!
    _tend = 0.0	; double !!
    _ntval = 0L
    _npretrig = 0L
    ; replaced ddtrange with dddtrange for double time base:
    s = CALL_EXTERNAL (libddww,'ddgetaug','ddtrange', $
                       ier,dia_ref,tname,_tbeg,_tend,_ntval,_npretrig)
    if (ier ne 0) then begin
	s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror',ier,ctrl,' ')
	goto, close
    endif
    if(keyword_set(debug)) then begin
	print,'t=',_tbeg,_tend,'size=',_ntval,_npretrig
      _k1 = 0L
      _k2 = 0L
      ier = 0L
      s = CALL_EXTERNAL (libddww,'ddgetaug','ddtindex', $
      ;s = CALL_EXTERNAL (libddww,'ddgetaug','dddtindex', $
                       ier,dia_ref,tname,_tbeg,_tend,_k1,_k2)
;      ntval = long(k2-k1+1)
      print,'size = ',_ntval,_k1,_k2,obj_size,_tbeg,_tend
    endif

;    if (keyword_set(format_ext) ) then begin
;	case format_ext of
;	    'float': begin
;		time_type = 2L
;		time_si = long([1,1,4,1])
;		end
;	    'double': begin
;		time_type = 3L
;		time_si = long([1,1,5,1])
;		end
;	    else: begin
;		time_type = 2L
;		time_si = long([1,1,4,1])
;		end
;	endcase
;    endif else begin
;	time_type = 2L
;	time_si = long([1,1,4,1])
;    endelse

;;    max_ntval = 5000000l
;    ier = 0L
;    max_ntval = _ntval
;    time_si = [1,max_ntval,time_arr_typ,max_ntval]
;    time = make_array(size=time_si)
;    s = CALL_EXTERNAL (libddww,'ddgetaug','ddtbase',  $
;                       ier,dia_ref,tname,1l,max_ntval,time_type,max_ntval,time,dim)
;    if (ier ne 0) then s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror',ier,ctrl,' ')
;    k1 = 1l
;    tbeg = time(k1-1)
;    tmp = max(time(0:dim-1),ntval)
;    tend = time(ntval)
;    ntval = long(ntval+1)
;    k2 = ntval

    ; store time vector information for entire time base
    ; in local variables:
    k1		= 1L
    k2		= long(_ntval)
    ntval	= long(_ntval)
    tbeg	= _tbeg
    tend	= _tend

; indices in call to ddobjval only valid for sig_group
; => moved to sig_group only branch
;
;    tb_index = (where(_ntval eq [obj_size,dim_arr]))(0)
;    if (tb_index ne 0) then message,'Zeitbasis nicht als erster Index abgespeichert',/inform
;    if(keyword_set(debug)) then begin
;	print,'size = ',_ntval,obj_size,ntval
;	print, 'tb_index = ',where(_ntval eq [obj_size,dim_arr])
;	print,'ind=',k1, k2, ntval, 'size=',obj_size, dim_arr, 'tb_index = ',tb_index
;	print, obuf
;    endif

    if (keyword_set(time_ft) and (obj_size eq ntval)) then begin
       ; restrict time base, if time_ft is provided:
       time_ft = float(time_ft)
       tbeg = (time_ft(0) > tbeg)
       tend = (tbeg > time_ft(1) < tend)
;       k1 = long((where(time ge tbeg))(0))+1l
;       k2 = long((where(time ge tend))(0))+1l
;       obj_size = ntval  
       if(keyword_set(debug)) then print,'before ddtindex: t=',tbeg,tend,',ind=',k1,k2,ntval
       s = CALL_EXTERNAL (libddww,'ddgetaug','ddtindex', $
       ;s = CALL_EXTERNAL +(libddww,'ddgetaug','dddtindex', $
                          ier,dia_ref,tname,tbeg,tend,k1,k2)
       if (ier ne 0) then begin
	  s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror', $
		ier,ctrl,'ddtindex '+tname+':'+strtrim(tbeg,2)+'-'+strtrim(tend,2))
       endif
       ntval = long(k2-k1+1)
    endif
;    else begin
;	print, 'NOT reducing time ......'
;	k1 = 1l
;	k2 = obj_size
;    endelse

    ; really read the time base now:
    time_si = [1,ntval,time_arr_typ,ntval]
    if (keyword_set(double)) then begin
	my_time_type = 3l
	time = dblarr(ntval)
    endif else begin
	my_time_type = time_type
	time = make_array(size=time_si)
    endelse
    s = CALL_EXTERNAL (libddww,'ddgetaug','ddtbase',  $
                       ier,dia_ref,tname,k1,k2,my_time_type,ntval,time,dim)
    if (ier ne 0) then begin
       s = CALL_EXTERNAL(libddww,'ddgetaug','xxerror',ier, ctrl, 'ddtbase:'+tname+', ier='+strtrim(ier,2))
       if (call_external(libddww,'ddgetaug','xxsev',ier) ne 0) then begin
       goto, close
       endif
    endif   

 endif else begin

    ; no valid relation and hence timebase has been found.
    ; Index array is stored as timebase:
;    time = dindgen(obj_size)+1
    time = findgen(obj_size)+1
    ntval = n_elements(time)
    k1 = 1l
    k2 = long(ntval)
    _ntval = ntval

 endelse

 ; new indices k1,k2 are stored in keyword t_index
 ; for later call from external program with
 ; this keywords in order to suppress the lengthy
 ; timebase reading for SIO cards:
 t_index = [k1,k2]

 endif else begin

	; reading the time base has been externally suppressed and
	; valid indices k1, k2 are provided in t_index:
	k1 = long(t_index[0])
	k2 = long(t_index[1])
	ntval = k2-k1+1
	;_ntval = ntval

 endelse



 ;
 ; Read format type of data array
 ; for signal or signalgroup:
 ;
 format = lonarr(3)
 s = CALL_EXTERNAL(libddww,'ddgetaug','ddobjval', $
                   ier,dia_ref,sig_name,'format',format)
 if (ier ne 0) then s = CALL_EXTERNAL(libddww,'ddgetaug','xxerror',ier, ctrl, ' ')

 ; DDWW data type:
 type = (long(type_val(where(format(0) eq format_val) > 0)))(0)
 ; IDL data type from / for size:
 arr_typ = (array_type(where(format(0) eq format_val) > 0))(0)
 ; text describtion:
 data_format_txt = (format_txt(where(format(0) eq format_val) > 0))(0)

 if(keyword_set(debug)) then begin
	print,'data format:'
	help,format_val,type_val,array_type,format_txt
	for k=0l,n_elements(type_val)-1 do begin
		print,k,format_val[k],type_val[k],array_type[k],format_txt[k]
	endfor
	print, 'index=',(where(format(0) eq format_val) > 0)
	print, 'format=',format
	print, 'type=',type
	print, 'arr_typ=',arr_typ
	print, 'data_format_txt=',data_format_txt
 endif


 ;
 ;READ DATA
 ;
 case obj_type of
    
    6 : begin  ;signalgroup
        dim_arr = lonarr(3)   
        s = CALL_EXTERNAL (libddww,'ddgetaug','ddobjval', $
                           ier,dia_ref,sig_name,'indices',dim_arr)
	if (ier ne 0) then begin
		s = CALL_EXTERNAL(libddww,'ddgetaug','xxerror', $
			ier, ctrl, 'ddobjval: '+sig_name+'indices')
		print,'dim_arr=',dim_arr
	endif
	tb_index = (where(_ntval eq [obj_size,dim_arr]))(0)
	if (tb_index ne 0) then message,'Zeitbasis nicht als erster Index abgespeichert',/inform
	if(keyword_set(debug)) then begin
		print,'size(_ntval,obj_size,ntval) = ',_ntval,obj_size,ntval
		print, 'tb_index = ',where(_ntval eq [obj_size,dim_arr])
		print,'ind=',k1, k2, ntval, 'size=',obj_size, dim_arr, 'tb_index = ',tb_index
		print, 'obuf=',obuf
	endif

        if (keyword_set(indices)) then begin
           indices = long(1 > [indices,1,1,1] < dim_arr)  
;           si = [1,obj_size,arr_typ,obj_size]
           si = [1,ntval,arr_typ,ntval]
           sig = make_array(size=si)
;           s = CALL_EXTERNAL (libddww,'ddgetaug','ddcxsig',    $
;                    ier,dia_ref,sig_name,k1,k2,indices,type,obj_size,sig,dim, $
;                    ncal,phys_dim)
           if (tb_index eq 0) then begin	; timebase is first index:
              ;s = CALL_EXTERNAL (libddww,'ddgetaug','ddccxsig',    $
              ;                   ier,dia_ref,sig_name,k1,k2,indices,type,ntval,sig,dim, $
              ;                   ncal,phys_dim)
              s = CALL_EXTERNAL (libddww,'ddgetaug','ddcxsig',    $
                                 ier,dia_ref,sig_name,k1,k2,indices,type,ntval,sig,dim, $
                                 ncal,phys_dim)
           endif else begin	; timebase is higher index:
              tmp = make_array(size=[1,1,arr_typ,1])
              k11 = indices(tb_index-1)
              indi = indices
              for i=k1,k2 do begin
                 indi(tb_index-1) = i                 
                 ;s = CALL_EXTERNAL (libddww,'ddgetaug','ddccxsig',    $
                 ;                   ier,dia_ref,sig_name,k11,k11,indi,type,1L,tmp,dim, $
                 ;                   ncal,phys_dim)
                 s = CALL_EXTERNAL (libddww,'ddgetaug','ddcxsig',    $
                                    ier,dia_ref,sig_name,k11,k11,indi,type,1L,tmp,dim, $
                                    ncal,phys_dim)
                 sig(i-k1) = tmp(0)
              endfor
           endelse
           
        endif else begin   
           n_el_ind = 1l
	   _tb_index = (where(_ntval eq [obj_size,dim_arr]))(0)
	   if (_tb_index ne 0) then begin
		message,'Zeitbasis nicht als erster Index abgespeichert',/inform
		if (keyword_set(debug)) then begin
			help,obj_size,dim_arr
			print,dim_arr
		endif
;		for i=0,n_elements(dim_arr)-1 do n_el_ind = n_el_ind*dim_arr(i)
;		si=[1+n_elements(dim_arr),ntval,obj_size,1l,1l,arr_typ,ntval*obj_size]
;		sig = make_array(size=si)
;	   endif else begin
	   endif
		for i=0,n_elements(dim_arr)-1 do n_el_ind = n_el_ind*dim_arr(i)
		si=[1+n_elements(dim_arr),ntval,dim_arr,arr_typ,ntval*n_el_ind]
		sig = make_array(size=si)
;	   endelse
	   if (keyword_set(raw_data)) then begin
		if (keyword_set(debug)) then begin
			print,'raw'
			help,sig_name,type,sig,ntval,leng,k1,k2
		endif
		ier = 0l
		leng = 0l
		s = CALL_EXTERNAL (libddww,'ddgetaug','ddsgroup', $
			ier, dia_ref, sig_name, k1, k2, type, ntval, sig, leng)
		s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror',ier,ctrl, $
			'ddsgroup: '+exp+':'+diag_name+string(shot,format='(i5.5)')+'.'+strtrim(ed,2)+':'+sig_name)
		ncal = 0l
	   	phys_dim = 'a.u.'
	   endif else begin
		if (keyword_set(debug)) then begin
			print,'calibrated'
			help,sig_name,k1,k2,type,ntval,sig,dim,ncal,phys_dim
		endif
		s = CALL_EXTERNAL (libddww,'ddgetaug','ddcsgrp',ier,    $
			dia_ref,sig_name,k1,k2,type,ntval,sig,dim,ncal,phys_dim)
		if (keyword_set(debug)) then begin
			print,ier,ncal,dim,phys_dim
		endif
		s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror',ier,ctrl, $
			'ddcsgr: '+exp+':'+diag_name+string(shot,format='(i5.5)')+'.'+strtrim(ed,2)+':'+sig_name)
		;
		; included for rem for reading uint = uint32_t
		; in RRC:losECE data.
		; mrm, 28.06.2012
		;
;		if (ier ne 0) then begin
;			message,'Trying to read data as raw data, as calirated data failed',/cont
;			ier = 0l
;			leng = 0l
;			s = CALL_EXTERNAL (libddww,'ddgetaug','ddsgroup', $
;				ier, dia_ref, sig_name, k1, k2, type, ntval, sig, leng)
;			s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror',ier,ctrl, $	
;				'ddsgroup: '+exp+':'+diag_name+string(shot,format='(i5.5)')+'.'+strtrim(ed,2)+':'+sig_name)
;		endif
	   endelse
	   if (_tb_index ne 0) then begin
		message,'re-arranging data ...!!!!',/cont
		if (keyword_set(debug)) then begin
			help,sig,si
			print,si
			print,_tb_index,obj_size,k1,k2
		endif
		sig = sig[0:obj_size-1,*]	; ??????
		sig = transpose(sig)
	   endif

;           s = CALL_EXTERNAL (libddww,'ddgetaug','ddccsgrp',ier,    $
;                    dia_ref,sig_name,k1,k2,type,ntval,sig,dim,ncal,phys_dim)
                    
	endelse
        end
    
    7 : begin  ;signal
        si = [1,ntval,arr_typ,ntval]
;        si = [1,obj_size,arr_typ,obj_size]
        sig = make_array(size=si)
;        s = CALL_EXTERNAL (libddww,'ddgetaug','ddcsgnl',ier,dia_ref, $
;
;sig_name,k1,k2,type,obj_size,sig,dim,ncal,phys_dim)
	if (keyword_set(raw_data)) then begin
		if (keyword_set(debug)) then print,'raw'
		ier = 0l
                s = CALL_EXTERNAL (libddww,'ddgetaug','ddsignal',ier,dia_ref, $
                           sig_name,k1,k2,type,ntval,sig,dim)
		s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror',ier,ctrl,'ddsignal:'+sig_name)
	endif else begin
		if (keyword_set(debug)) then print,'calibrated'
		ier = 0l
                s = CALL_EXTERNAL (libddww,'ddgetaug','ddccsgnl',ier,dia_ref, $
                           sig_name,k1,k2,type,ntval,sig,dim,ncal,phys_dim)
		if (ier ne 0) then begin
			if (ier ne 555352323) then begin
				s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror',ier,ctrl,'ddccsgnl:'+sig_name)
			endif
		endif
	endelse
;	my_local_len = 0l
;        s = CALL_EXTERNAL (libddww,'ddgetaug','ddsignal',ier,dia_ref, $
;                           sig_name,k1,k2,type,ntval,sig,my_local_len)
        end                    
    
    else: begin
	message, ' Invalid data type', /continue
	end

 endcase                         

 if (ier eq 555352323) then ier=0  ;warning: no calibration
 if (ier eq 555483394) then ier=0  ;warning: no PARAM_SET found
 if (ier eq 555417858) then ier=0  ;warning: no PARAM_SET found
 if (keyword_set(debug) and (ier ne 0)) then $
	s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror',ier, ctrl, ' ')




;if (arg_present(area_base)) then begin

 ;; copied from hsm/sdp version of read_signal:
 ;; get area base
 ier     = 0l
 a_sizes = lonarr(3)
 a_dim   = lonarr(3)
 a_n     = 0l
 a_nt    = 0l
 index   = 0l
 a_name  = string(' ', format='(a8)')

 s = call_external(libddww, 'ddgetaug', 'ddainfo', $
                   ier,  dia_ref, sig_name, a_sizes, a_dim, index)
 if (ier ne 0) then begin
    if (ier ne 555745537) then begin
      s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror',ier, ctrl, $
	diag_name+':'+sig_name)
    endif
 endif

 a_ind = where(a_sizes gt 0, a_ndim)
 if (keyword_set(debug)) then begin
	help, a_sizes, a_dim, index,a_ndim
	print, 'a_sizes=',a_sizes
	print, 'a_dim=',a_dim
	print, 'index=',index,a_ndim
	print, 'a_ndim=',a_ndim
 endif
 if a_ndim gt 0l then begin
 
    s = call_external(libddww, 'ddgetaug', 'ddainfo2', $
                      ier,  dia_ref, sig_name, a_nt, a_sizes, a_dim, a_name, a_n)
    if (ier ne 0) then begin
      s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror',ier, ctrl, $
	diag_name+':'+sig_name)
    endif
    
    if (keyword_set(debug)) then begin
	help,a_nt, a_sizes, a_dim, a_name, a_n
	print,a_nt, a_sizes, a_dim, a_name, a_n
    endif
    if strcompress(a_name) ne ' ' then begin
       a_sizes   = a_sizes[a_ind]
       leng      = 0l
       albuf     = long(total(a_sizes))
       buf       = make_array(a_sizes, /float)
       area_base = make_array([a_nt, a_sizes], /float)
       
       for k = 1l, a_nt do begin
          s = call_external(libddww, 'ddgetaug', 'ddagroup', $
                            ier, dia_ref, sig_name, k, k, 2l, albuf, buf, leng)
          if (ier ne 0) then begin
             s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror',ier, ctrl, $
		diag_name+':'+sig_name)
             help, a_nt, k
          endif 
          
          area_base[k-1, *] = buf
       endfor
       
       ;; in case of time-independent area-base:
       area_base = reform(area_base)
    endif else message, 'Did not get the name of the area-base!'
 endif else begin
;	if (a_ndim eq 0) then ier=0l
	ier = 0l
	area_base = -1l
 endelse

;endif
 


;CLOSE SHOTFILE
close:
 if (not keyword_set(dia_ref_ext)) then begin
    ierc = 0l
    s = CALL_EXTERNAL (libddww,'ddgetaug','ddclose',ierc, dia_ref)                   
    if (ierc ne 0) then begin
       s = CALL_EXTERNAL (libddww,'ddgetaug','xxerror',ier, ctrl, ' ')
    endif
 endif
    
 edition=ed   


return
end
