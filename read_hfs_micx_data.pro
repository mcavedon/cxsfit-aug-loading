@/afs/ipp/home/c/cxrs/idl/cxsfit64/machines/aug/read_princeton.pro
@/afs/ipp/home/c/cxrs/idl/cxsfit64/machines/aug/rem_D2_lines.pro
@/afs/ipp/home/c/cxrs/idl/cxsfit64/machines/aug/hfs_puff_status.pro

pro read_hfs_micx_data,shot,spectrometer_data,data,head_name=head_name
;; Adapted from read_hfs_data, to load cgf data
;; CGF data already loaded in load_cgf. This shall load the gas puff
;; And adapt the data structure into the spectrometer-data

err  = ''
id   = 'CXF_LOAD_AUG_DATA: '
ierr = 0L

; speed of light
c = 2.9979D8      ; [m/s]

; Planck constant   ; [J s]
h = 6.6261D-34


 los_names= strsplit(data.header.los_name,/EXTRACT)
 print,'Spectrometer used:   ',spectrometer_data.spectrometer.name
 
; Input switches
shot     = data.header.shot
diagnosticL1 = STRSPLIT(spectrometer_data.spectrometer.name, 'S', /EXTRACT)+'L'
error = 0L

time = data.data.time
spec = data.data.intens

dim = size(spec,/dimensions)
npix = dim(0)
nlos = dim(1)
nframes = dim(2)
los_index = findgen(nlos)

;Selection of time window
timebase = 1L
read_signal_mrm,0L,shot,'HEB','S01VALVE',timebase,variable,phys_dim,edition=0L,exp='AUGD'
threshold  = 80 ; [V] the valve is considered opened at 80 V
timebase = timebase(where( variable gt threshold))
interval_gaspuff_on = [timebase(0)-0.005,(timebase(n_elements(timebase)-1)+0.005)]
tim_ix = where((time gt interval_gaspuff_on(0)) and (time lt interval_gaspuff_on(1)), nframe_gp)
;spec = spec(*,*,tim_ix)
;time = time(tim_ix)

; Sorting of LOS
los_index = los_index(reverse(sort(data.header.r_pos)))
spec = spec[*,los_index,*]
lam = data.header.cor_wavel[*,los_index]
los_name_list = []
for i =0,nlos-1 do los_name_list = [los_name_list, los_names[los_index[i]]]
; doubt
;offset = offset[*,los_index]
;sens = sens[*,los_index]

spectrometer_data.npixel = npix
spectrometer_data.nframe  = nframes
spectrometer_data.ntrack = nlos
spectrometer_data.ninst  = 1
cxf_define_spectrometer_data,spectrometer_data,err
spectrometer_data.los_name = los_name_list
spectrometer_data.R_orig = data.header.r_opt[los_index]
spectrometer_data.Z_orig =  data.header.z_opt[los_index]
spectrometer_data.PHI_orig =  data.header.phi_opt[los_index]
spectrometer_data.R_pos = data.header.r_pos[los_index]
spectrometer_data.Z_pos = data.header.z_pos[los_index]
spectrometer_data.PHI_pos = data.header.phi_pos[los_index]

; Reference wavelength [A]
wlen = data.header.wav_mid
spectrometer_data.wlength = data.header.wav_mid*10

; Slit width [microns]
spectrometer_data.slit = 100

; Time vector [s]
spectrometer_data.time     = time
spectrometer_data.exposure = data.header.exptime

; Calculate the wavelength intervals assuming the pixels are contiguous
wav_vect  = lam
dwav_vect = FLTARR(spectrometer_data.npixel,spectrometer_data.ntrack)
dwav_vect[0,*] = wav_vect[1,*]-wav_vect[0,*]
dwav_vect[1:spectrometer_data.npixel-2,*] = 0.5*(wav_vect[2:spectrometer_data.npixel-1,*] $
                               -wav_vect[0:spectrometer_data.npixel-3,*])
dwav_vect[spectrometer_data.npixel-1,*] = wav_vect[spectrometer_data.npixel-1,*] $
                        -wav_vect[spectrometer_data.npixel-2,*]
spectrometer_data.wavelength.data      = wav_vect
spectrometer_data.wavelength.error     = FLTARR(spectrometer_data.npixel,$
                            spectrometer_data.ntrack)
spectrometer_data.wavelength.reference = ' '
spectrometer_data.dispersion.data      = dwav_vect
spectrometer_data.dispersion.error     = FLTARR(spectrometer_data.npixel,$
                            spectrometer_data.ntrack)
spectrometer_data.dispersion.reference = ' '

; error bars already been calibrated
spectrometer_data.intensity.data = data.data.intens
spectrometer_data.intensity.error = data.data.intenserr
spectrometer_data.intensity.reference = ' '

mass = 0.0
if (wlen lt 568.0) and (wlen gt 566.0) then mass = 14.0
if (wlen lt 496.0) and (wlen gt 492.0) then mass = 10.0
if (wlen lt 436.0) and (wlen gt 432.0) then mass = 2.0
if (wlen lt 657.0) and (wlen gt 655.0) then mass = 2.0

; Load gas puff
hfs_puff_status, head_name, shot, spectrometer_data.time, $
spectrometer_data.exposure, beam_orig, powerbeam, puff_shotfile, err
exc_elm = 1

if STRTRIM(err,2) NE '' then begin
    ; If the shot file is missing then assume no beams (this is default in
    ; cxf_define_spectrometer_data)!!!
    if STRPOS(err,'shotfile does not exist') NE -1 then begin
      spectrometer_data.beam = spectrometer_data.time*0.
      err = ''
    endif else RETURN
endif else begin
    
    ; read ElM signal in order to remove elms from background subtraction
    if exc_elm eq 1 then begin 
      elm_exp='AUGD'
      read_signal_mrm, ier,shot,'ELM','t_endELM',telm_begin,telm_end,phys_dim,exp=elm_exp
      if ier eq 0 then begin
        print, ''
        print, 'Exclude ELMs from data'
        print, ''
        elmfree = indgen(n_elements(spectrometer_data.time))*0+1
        preELM = -1.0*(0.001 + mean(spectrometer_data.exposure)/2.0)
        postELM = 0.001 + mean(spectrometer_data.exposure)/2.0
        time = spectrometer_data.time
        for t=0L, n_elements(telm_begin)-1 do $
            elmfree(where((time ge (telm_begin(t)+preELM)) and (time le (telm_end(t)+postELM)))) = 0
        
        powerbeam(where(elmfree eq 0)) = 0.5
        beam_orig(where(elmfree eq 0)) = 0.0 ;I do not want to fit frames in ELMs. This is used when no beam modulation is applied
      endif else begin
        ier = 0
        elm_exp = 'djcruz'
        read_signal_mrm, ier,shot,'ELM','t_endELM',telm_begin,telm_end,phys_dim,exp=elm_exp
        if ier eq 0 then begin
            print, ''
            print, 'Exclude ELMs from data'
            print, ''
            elmfree = indgen(n_elements(spectrometer_data.time))*0+1
            preELM = -1.0*(0.001 + mean(spectrometer_data.exposure)/2.0)
            postELM = 0.001 + mean(spectrometer_data.exposure)/2.0
            time = spectrometer_data.time
            for t=0L, n_elements(telm_begin)-1 do $
            elmfree(where((time ge (telm_begin(t)+preELM)) and (time le (telm_end(t)+postELM)))) = 0
            powerbeam(where(elmfree eq 0)) = 0.5
            beam_orig(where(elmfree eq 0)) = 0.0 ;I do not want to fit frames in ELMs. This is used when no beam modulation is applied
        endif else begin
            print, ''
            print, 'ELM shotfile does not exist!'
            print, ''
        endelse
      endelse
    endif
    spectrometer_data.beam = beam_orig
    spectrometer_data.modulation.flag=0
    powerbeam = reform(powerbeam,1,n_elements(powerbeam))
    names = ['GasPuffSe01']
    *spectrometer_data.modulation.beampower=powerbeam
    *spectrometer_data.modulation.beamlabels =names
    *spectrometer_data.modulation.orig_beam =spectrometer_data.beam
    *spectrometer_data.modulation.orig_data =spectrometer_data.intensity.data
    *spectrometer_data.modulation.orig_error = spectrometer_data.intensity.error
endelse

plot, time, powerbeam, xr=[interval_gaspuff_on(0), interval_gaspuff_on(1)], $
yr = [0, 1.1] , /xstyle, ytitle='Gas puff', xtitle='Time [s]'


poloidal_los = []
toroidal_los = []
for i =0, nlos-1 do begin 
    i_los_name = spectrometer_data.los_name[i]
    if strmid(i_los_name, 0, 3) eq 'CVH' then poloidal_los = [poloidal_los, i]
    if strmid(i_los_name, 0, 3) eq 'CXH' then toroidal_los = [toroidal_los, i]
endfor     
print, 'poloidal LOS:',  spectrometer_data.los_name[poloidal_los]
print, 'toroidal LOS:',  spectrometer_data.los_name[toroidal_los]

ntor_los = n_elements(toroidal_los)
npol_los = n_elements(poloidal_los)
; Do individually for each line of sight
if ntor_los gt 0 then spectrometer_data.PHI_orig[toroidal_los] = (spectrometer_data.PHI_orig[toroidal_los] + 2*!Pi)*180/!Pi
if npol_los gt 0 then spectrometer_data.PHI_orig[poloidal_los] = (spectrometer_data.PHI_orig[poloidal_los])*180/!Pi
spectrometer_data.PHI_pos = spectrometer_data.PHI_pos*180/!Pi

externalcostheta = fltarr(spectrometer_data.ntrack,spectrometer_data.nframe)+1.
x_pos  = spectrometer_data.R_pos*cos(spectrometer_data.phi_pos/!RADEG)
y_pos  = spectrometer_data.R_pos*sin(spectrometer_data.phi_pos/!RADEG)
z_pos  = spectrometer_data.z_pos
x_orig = spectrometer_data.R_orig*cos(spectrometer_data.phi_orig/!RADEG)
y_orig = spectrometer_data.R_orig*sin(spectrometer_data.phi_orig/!RADEG)
z_orig = spectrometer_data.z_orig
l_LOS  = SQRT((x_pos-x_orig)^2+(y_pos-y_orig)^2+(z_pos-z_orig)^2)
c_LOS  = 	[[(x_pos-x_orig)/l_LOS], $
        [(y_pos-y_orig)/l_LOS], $
         [(z_pos-z_orig)/l_LOS]]
c_tor  = [[-y_pos/spectrometer_data.R_pos],[+x_pos/spectrometer_data.R_pos],[FLTARR(spectrometer_data.ntrack)]]

if ntor_los gt 0 then begin   
    costheta_tor = TOTAL(c_LOS*c_tor,2)
    for j=0,ntor_los-1 do begin
        jlos = toroidal_los[j]
        externalcostheta[jlos,*] = abs(costheta_tor[jlos])
    endfor
endif

if npol_los gt 0 then begin   
    rvec=fltarr(n_elements(time), n_elements(spectrometer_data.R_pos),3)
    polvec=fltarr(n_elements(time), n_elements(spectrometer_data.R_pos),3)
    eqiread, shot, min(time), max(time), diag='EQH', ed=0, exp='AUGD',surfaces=surfaces
    rmag=interpol(surfaces.rmag,surfaces.time,time)
    zmag=interpol(surfaces.zmag,surfaces.time,time)
    for i_time = 0., spectrometer_data.nframe-1 do begin
        for j = 0, npol_los-1 do begin
            i_los = poloidal_los[j]
            trace_field_line_fast,surfaces,time(i_time),spectrometer_data.R_pos(i_los),$
                            spectrometer_data.z_pos(i_los),spectrometer_data.phi_pos(i_los), $
                            5.,0.002,r,z,phi,b_abs=b_abs,/no_plot
                            philos=spectrometer_data.phi_pos[i_los]*!pi/180.
                            rlos=spectrometer_data.R_pos[i_los]
                            zlos=spectrometer_data.z_pos[i_los]
            rvec[i_time,i_los,*]=[rmag[i_time]*cos(philos)-rlos*cos(philos),$
                                  rmag[i_time]*sin(philos)-rlos*sin(philos),$
                                  zmag[i_time]-zlos]
            rvec[i_time,i_los,*]=rvec[i_time,i_los,*]/sqrt(total(rvec[i_time, i_los, *]^2))
            polvec[i_time,i_los,*]=crossp(rvec[i_time, i_los, *],reform(c_tor[i_los,*]))
            costheta_pol = total(c_LOS(i_los,*)*polvec(i_time,i_los,*))
            externalcostheta[i_los,i_time] = abs(costheta_pol)
        endfor
    endfor
endif

spectrometer_data = create_struct(spectrometer_data,'externalcostheta',externalcostheta)

; Instfu function
spectrometer_data.instfu.y0 = replicate(1.,spectrometer_data.ntrack)
;spectrometer_data.instfu.xw = fwhm_pix[los_index]*dwdp*10
spectrometer_data.instfu.xw = data.header.spectrometer.instfu_gauss_nm[los_index]
spectrometer_data.instfu.xs = replicate(0.,spectrometer_data.ntrack)
spectrometer_data.instfu.reference = ' '

instfunc_eV = $
mean(spectrometer_data.instfu.xw/wlen)^2.0*mass*938.3e6/(8.0*alog(2.0))

print,'Instrument function in eV is ',instfunc_eV


;if extraoptions.(head_name).molecular_choice eq 1 then begin
;    rem_D2_lines,spectrometer_data,data_calib,wlen,reform(powerbeam)
;endif

; Clear memory
kill_var,dwdp
kill_var,exptime
kill_var,sad2
kill_var,ctsph
kill_var,time
kill_var,lam
kill_var,offset
kill_var,sens
kill_var,spec
kill_var,los_name
kill_var,data_calib
kill_var,err_bars
kill_var,err_bars_counts
kill_var,dwav_vect
kill_var,wav_vect

end
