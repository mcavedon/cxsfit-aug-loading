pro cxf_filter_blip,outdata, specdata
    
    ; pcano 25/2/2020
    ; Make sure just one profile is written per beam blip when average has been done

    if n_elements(specdata.time) gt n_elements(uniq(specdata.time)) then begin 
        k =0 
        dumm_time =[0]
        while k lt specdata.nframe do begin 
            repeat_time = where(specdata.time gt specdata.time[k]-1e-5 and $
            specdata.time lt specdata.time[k]+1e-5 , nrepeat)
            dumm_time = [dumm_time , specdata.time[k]]
            if nrepeat gt 1 then k = k +nrepeat else k = k+1
        endwhile
        help, dumm_time   
        dumm_time = dumm_time[1:n_elements(dumm_time)-1]


        nuntime = n_elements(dumm_time) 
        ; Declare the structures
        ; Outdata
        shp = size(outdata.temp.data, /dimensions)
        dumm_inte = fltarr(nuntime,shp[1], specdata.ntrack)
        dumm_inte_err = fltarr(nuntime,shp[1], specdata.ntrack)
        dumm_v = fltarr(nuntime, shp[1],specdata.ntrack)
        dumm_v_err = fltarr(nuntime,shp[1], specdata.ntrack)
        dumm_t = fltarr(nuntime,shp[1],specdata.ntrack)
        dumm_t_err = fltarr(nuntime,shp[1], specdata.ntrack)
        dumm_wvl_data= fltarr(nuntime,shp[1],specdata.ntrack)
        dumm_wvl_error= fltarr(nuntime, shp[1],specdata.ntrack)        
       
        dumm_baseline_data= fltarr(nuntime, specdata.ntrack)
        dumm_baseline_dataleft= fltarr(nuntime, specdata.ntrack)
        dumm_baseline_dataright= fltarr(nuntime, specdata.ntrack)
        dumm_baseline_error= fltarr(nuntime, specdata.ntrack)
        dumm_fit_status = fltarr(nuntime, specdata.ntrack)
        dumm_wlfp = fltarr(nuntime, specdata.ntrack)
        dumm_wshift = fltarr(nuntime, specdata.ntrack)
        dumm_pixref = fltarr(nuntime, specdata.ntrack)
        dumm_eff_beam = fltarr(nuntime)

        ; specdata
        dumm_specdata_inte_data= fltarr(specdata.npixel,specdata.ntrack,nuntime)
        dumm_specdata_inte_error= fltarr(specdata.npixel,specdata.ntrack,nuntime)
        dumm_specdata_inte_validity= fltarr(specdata.npixel,specdata.ntrack,nuntime)
        dumm_specdata_costheta= fltarr(specdata.ntrack,nuntime)
        dumm_specdata_exposure= fltarr(nuntime)
        dumm_specdata_beam = fltarr(nuntime)

        
        k = 0
        for i_uniq_time =0, nuntime-1 do begin 
        
            repeat_time = where(specdata.time gt specdata.time[k]-1e-5 and $
                specdata.time lt specdata.time[k]+1e-5 , nrepeat)
 ;           repeat_time = where(specdata.time eq specdata.time[k], nrepeat)
        for j = 0, shp[1]-1 do begin 

            dumm_inte[i_uniq_time,j,*] = outdata.intensity.data[k,j,*]
            dumm_inte_err[i_uniq_time,j,*] = outdata.intensity.error[k,j,*]
            dumm_v[i_uniq_time,j,*] = outdata.vrot.data[k,j,*]
            dumm_v_err[i_uniq_time,j,*] = outdata.vrot.error[k,j,*]
            dumm_t[i_uniq_time,j,*] = outdata.temp.data[k,j,*]
            dumm_t_err[i_uniq_time,j,*] = outdata.temp.error[k,j,*]        
            dumm_baseline_data[i_uniq_time,*]= outdata.baseline.data[k, *]
            dumm_baseline_dataleft[i_uniq_time,*]= outdata.baseline.dataleft[k, *]
            dumm_baseline_dataright[i_uniq_time,*]= outdata.baseline.dataright[k, *]
            dumm_baseline_error[i_uniq_time,*]= outdata.baseline.error[k, *]
            dumm_wvl_data[i_uniq_time,j,*]= outdata.wavelength.data[k,j,*]
            dumm_wvl_error[i_uniq_time,j,*]= outdata.wavelength.error[k,j,*]    
            dumm_fit_status[i_uniq_time,*] = outdata.fit_status[k,*]
            dumm_wlfp[i_uniq_time,*] =outdata.wlfp[k,*]      
            dumm_wshift[i_uniq_time,*] = outdata.wshift[k,*]      
            dumm_pixref[i_uniq_time,*] = outdata.pixref[k,*]      
            dumm_eff_beam[i_uniq_time] = outdata.eff_beam[k]      

            dumm_specdata_inte_data[*,*,i_uniq_time] = specdata.intensity.data[*,*,k]
            dumm_specdata_inte_error[*,*,i_uniq_time]=specdata.intensity.error[*,*,k]
            dumm_specdata_inte_validity[*,*,i_uniq_time]= specdata.intensity.validity[*,*,k]
            
            dumm_specdata_exposure[i_uniq_time] = specdata.exposure[k]
            dumm_specdata_beam[i_uniq_time]  =specdata.beam[k]
            dumm_specdata_costheta[*,i_uniq_time] = specdata.externalcostheta[*,k]
        endfor
            if nrepeat gt 1 then begin  
                 k = k+nrepeat
            endif else k = k+1
            
        
        endfor 
        
    
    
   ; Update the structures

   intensity ={data:dumm_specdata_inte_data,$
   error:dumm_specdata_inte_error,$
   validity:dumm_specdata_inte_validity,$
   reference : specdata.intensity.reference}

                                
   
   if strupcase(outdata.spectrometer) eq 'CMR' or $
      strupcase(outdata.spectrometer) eq 'CNR' or $
      strupcase(outdata.spectrometer) eq 'CPR' or $
      strupcase(outdata.spectrometer) eq 'CEF' $
then begin
 
   
   specdata =   { $ 
   machine:specdata.machine,$
   spectrometer: specdata.spectrometer,$
   input_file : specdata.INPUT_FILE,$
   shot_nr:specdata.SHOT_NR,$
   npixel:specdata.NPIXEL ,$
   ntrack:specdata.NTRACK ,$
   nframe_baseline:specdata.NFRAME_BASELINE ,$
   ninst:specdata.NINST      ,$
   modulation:specdata.MODULATION    ,$
   wlength:specdata.WLENGTH       ,$
   slit:specdata.SLIT         ,$
   los_name:specdata.LOS_NAME      ,$
   r_pos:specdata.R_POS           ,$
   z_pos:specdata.Z_POS         ,$
   phi_pos:specdata.PHI_POS      ,$
   r_orig:specdata.R_ORIG         ,$
   z_orig:specdata.Z_ORIG         ,$
   phi_orig:specdata.PHI_ORIG       ,$
   baseline:specdata.BASELINE       ,$
   wavelength:specdata.WAVELENGTH    ,$
   dispersion:specdata.DISPERSION       ,$
   instfu:specdata.INSTFU           ,$
   beam:dumm_specdata_beam, $
   exposure: dumm_specdata_exposure, $
   externalcostheta: dumm_specdata_costheta, $
   nframe: nuntime, $
   time: dumm_time, $
   intensity: intensity $
   }

endif else begin

 
   specdata =   { $ 
                machine:specdata.machine,$
                spectrometer: specdata.spectrometer,$
                input_file : specdata.INPUT_FILE,$
                shot_nr:specdata.SHOT_NR,$
                npixel:specdata.NPIXEL ,$
                ntrack:specdata.NTRACK ,$
                nframe: nuntime, $
                nframe_baseline:specdata.NFRAME_BASELINE ,$
                ninst:specdata.NINST      ,$
                modulation:specdata.MODULATION    ,$
                wlength:specdata.WLENGTH       ,$
                slit:specdata.SLIT         ,$
                los_name:specdata.LOS_NAME      ,$
                r_pos:specdata.R_POS           ,$
                z_pos:specdata.Z_POS         ,$
                phi_pos:specdata.PHI_POS      ,$
                r_orig:specdata.R_ORIG         ,$
                z_orig:specdata.Z_ORIG         ,$
                phi_orig:specdata.PHI_ORIG       ,$
                time: dumm_time, $
                exposure: dumm_specdata_exposure, $
                beam:dumm_specdata_beam, $
                baseline:specdata.BASELINE       ,$
                intensity: intensity, $
                wavelength:specdata.WAVELENGTH    ,$
                dispersion:specdata.DISPERSION       ,$
                instfu:specdata.INSTFU           ,$
                r_pos_time:specdata.R_POS_TIME,$
                z_pos_time:specdata.Z_POS_TIME,$
                phi_pos_time:specdata.PHI_POS_TIME,$ 
                dr_pos_time:specdata.DR_POS_TIME,$
                dz_pos_time:specdata.DZ_POS_TIME,$
                dphi_pos_time:specdata.DPHI_POS_TIME,$ 
                externalcostheta: dumm_specdata_costheta $
                }
   

endelse

   
   intens = {data:dumm_inte,error :dumm_inte_err}
   vrot = {data:dumm_v, error :dumm_v_err}
   temp = {data:dumm_t, error :dumm_t_err}
   wavelength = {data:dumm_wvl_data, error :dumm_wvl_error}

   baseline = {data:dumm_baseline_data, dataleft :dumm_baseline_dataleft, $
   dataright :dumm_baseline_dataright, error :dumm_baseline_error}

   outdata = { $ 
   code :     outdata.code,$
   machine :     outdata.machine,$
   spectrometer :     outdata.spectrometer,$
   shot_nr :     outdata.shot_nr,$
   ntrack :     outdata.ntrack,$
   nframe :     nuntime,$
   nlines :     outdata.nlines,$
   max_ngaussperline :     outdata.max_ngaussperline,$
   max_nfreeparamperline :     outdata.max_nfreeparamperline,$
   MAX_NPARAMCOUPLING :     outdata.MAX_NPARAMCOUPLING,$
   gaussperline :     outdata.gaussperline,$
   linetype :     outdata.linetype,$
   freeparamperline :     outdata.freeparamperline,$
   mass :     outdata.mass,$
   intensity: intens, $
   temp : temp,$
   vrot :vrot , $
   baseline : baseline ,$
   wavelength:wavelength , $
   couplings :     outdata.couplings,$
   fit_status :dumm_fit_status,$
   wlfp :dumm_wlfp,$
   wshift :dumm_wshift,$
   pixref :dumm_pixref,$
   eff_beam :dumm_eff_beam $
   }
   
   endif
   return


end
