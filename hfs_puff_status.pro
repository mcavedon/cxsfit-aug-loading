; Version 1 by U.Plank 23.02.2018
; 27.09.2018 add to check if HEB shotfile exists (based on routine
; written by A. Kappatou)
; Version 2 by D.Cruz 22.01.2019

PRO hfs_puff_status, diagnostic, shot, time, exp, $
                      beam_orig,powerbeam, puff_shotfile, err, old = old

; Return a vector of flags showing at which CXRS measurement
; time 'sufficient' puff voltage is available

  id  =  'CXRS_PUFF_STATUS: '

;
; check if needed system variable exist. Define it if not.
  defsysv, '!LIBDDWW', exists=exists
  if exists eq 0 then defsysv, '!LIBDDWW', '/usr/ads/lib/libddww.so', 1

; ------CVH/CXH-------



                                ;[V]  threshold of piezo valve status open
  threshold= 80

; intialise
  ier        = 0L
  dia_ref    = 0L
  datum      = string(' ',format='(a18)')
  dim        = 0L
  phys_unit  = 0L
  err_string = string(' ',format='(a80)')


; Open HEB shot file for gas valve injection time
  puff_exp  =  'AUGD'
  puff_diag =  'HEB'
  puff_shot =  shot
  puff_edn  =  0L
  puff_name =  'S01VALVE'
  puff_time = 1L                ; to force returning time base as well
                                ;read_signal

  read_signal_mrm,ier, puff_shot,puff_diag,puff_name,puff_time,puff_voltage,phys_dim,edition=puff_edn, exp=puff_exp
  IF ier NE 0 THEN BEGIN
     read_signal_mrm,ier, puff_shot,puff_diag,puff_name,puff_time,puff_voltage,phys_dim,edition=puff_edn, exp='MGRIEN'
        if ier ne 0 then begin
           err_string =  'Read_signal S01Valve error: '+STRTRIM(STRING(ier),2)
           GOTO, FINISH
        endif
 ENDIF


 ntime =  N_ELEMENTS(time)
 puff_ave_voltage =  FLTARR(ntime)

if not KEYWORD_SET(old) then begin
; Integrate valve signal over each CXRS exposure (according to NBI calculation)


; Calculate a and b coefficients so we reach treshold min at 0.15 and threshold max at 0.85. 0.15 and 0.85 are predefined values in beam modulation menu
; Beam = (piezo_voltage + a)/b
    threshold_min = -15.0 ;V
    threshold_max = 80.0 ;V
    a=(threshold_max/0.85 - threshold_min/0.15)/(1.0/0.15-1.0/0.85)
    b=(threshold_min + a)/0.15

        FOR i=0L, ntime-1 DO BEGIN
           tstart = time[i] - exp[i]/2.
           tstop  = time[i] + exp[i]/2.
           ;tstart = time[i]
           ;tstop  = time[i] + exp[i]
           wh_puff_time =  WHERE(puff_time GT tstart AND puff_time LT tstop, $
                                 n_puff_time)
           if n_puff_time eq 0 then begin
              puff_ave_voltage[i] = 0
           endif else begin
              puff_ave_voltage[i] = INT_TABULATED(puff_time[wh_puff_time], puff_voltage[wh_puff_time])/(tstop-tstart)
           endelse
        ENDFOR
        puff = (puff_ave_voltage + a)/b
        index = where(puff gt 1.0)
        puff(index) = 1.0
        
        if shot eq 39000 then begin
            on = where(puff gt 0.85)
            for j=0,n_elements(on)-1 do begin
                if puff(on(j)+1) lt 0.85 then begin
                    puff(on(j)+1) = puff(on(j))
                    puff(on(j)+2) = puff(on(j))
                    puff(on(j)+3) = 0.80
                    puff(on(j)+4) = 0.80
                    puff(on(j)+5) = 0.80
                    puff(on(j)+6) = 0.80
                endif
            endfor
        endif                      
        powerbeam=puff ; used for beam modulation
        beam_orig=puff ; used for t_ini and t_end beam

endif else begin ;taken from Ulrike's cxrs_puff_status.pro




  ;make array for the puff on times to speed up reading in routine in the for loop
       puff_index_on=where(puff_voltage gt threshold)
       ; this is when the puff was on all time
      ; if n_elements(puff_index_on) eq 1 then begin
       ;   puff_new=fltarr(ntime)
        ;  puff=fltarr(ntime)
         ; GOTO, FINISH
       ;endif
       puff_time_on=puff_time[puff_index_on[0]:n_elements(puff_time)-1]
       puff_voltage_on=puff_voltage[puff_index_on[0]:n_elements(puff_time)-1]


       FOR i=0L, ntime-1 DO BEGIN
          tstart = time[i] - exp[i]/2.
          tstop  = time[i] + exp[i]/2.
          wh_puff_time =  WHERE(puff_time GT tstart AND puff_time LT tstop, $
                                n_puff_time)
          if n_puff_time eq 0 then begin
             puff_ave_voltage[i] = 0
          endif else begin
             puff_ave_voltage[i] = INT_TABULATED(puff_time[wh_puff_time], puff_voltage[wh_puff_time])/(tstop-tstart)
          endelse
       ENDFOR
       puff = (puff_ave_voltage GT threshold)
       n_puff=n_elements(puff)
       puff_new1=fltarr(n_puff)
       puff_new=fltarr(n_puff)    ; change signal according to actual data (see gas_puff_sync)
       for i=0L, n_puff-4 do begin
          if puff[i] eq 1. and  puff[i+1] eq 0. then begin
             puff_new1[i+1]=1.
             puff_new[i+2]=0.5
             puff_new[i+1]=1
          endif

          if puff[i] eq 0. and puff[i+1] eq 1. then begin
             puff_new[i+1]=-0.5
          endif

       endfor

       powerbeam=puff+puff_new
       beam_orig=puff+puff_new1

    endelse



  puff_shotfile =  { experiment : puff_exp,  $
                     diagnostic : puff_diag, $
                     shot       : puff_shot, $
                     edition    : puff_edn   }

FINISH:
; Close shotfile
     s = call_external(!libddww,'ddgetaug','ddclose', $
                       ier,dia_ref)
     if (ier gt 0 and err_string EQ '') then begin
        s = call_external(!libddww,'ddgetaug','xxerrprt', $
                          -1L,err_string,ier,3L,'ddclose:')
     endif

     if STRLEN(STRTRIM(err_string, 2)) GT 0 THEN $
        err = id+err_string $
     else err =  ''


 END
