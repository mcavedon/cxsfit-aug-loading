PRO eqiread, shot, t1, t2,$ ;; basic call
             diag=diag, ed=ed, exp=exp,$               ;; sf location
             surfaces=surfaces ;structures of data
             ;flux_values=flux_values
  
  
  mu0=!pi*4e-7
  IF keyword_set(diag) THEN $
    diag=diag ELSE diag='EQH'
  IF keyword_set(ed) THEN $
    ed=long(ed) ELSE ed=0L
  IF keyword_set(exp) THEN $
    exp=exp ELSE exp='AUGD'
  diaref=0L
  err=0L
  err_string = string(' ',format='(a80)')
  tim='                  '

  it=0
  ixtim=0
  mdim=0L
  LiCTRL=0
  iCTRL=0
  LTline=0
  eqTEXT=''
  cfid01=''
  lssq=0.
  ssq=0.
  ssqnam=''
  tmid=(t1+t2)/2

                                ; Try to Open IDE
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddopen', $
                     err, exp, diag, LONG(shot), ed, diaref, tim)
  if (err gt 0) then begin
     diag='EQH'
                                ; Try to Open EQH
     s = CALL_EXTERNAL (!libddww,'ddgetaug','ddopen', $
                        err, exp, diag, LONG(shot), ed, diaref, tim)
  endif
                                ; if EQH does not work, open EQI
  if (err gt 0) then begin
     diag = 'EQI'
     s = CALL_EXTERNAL (!libddww,'ddgetaug','ddopen', $
                        err, exp, diag, LONG(shot), ed, diaref, tim)
  endif
                                ; if EQI does not work, open FPP
  if (err gt 0) then begin
     diag = 'FPP'
     s = CALL_EXTERNAL (!libddww,'ddgetaug','ddopen', $
                        err, exp, diag, LONG(shot), ed, diaref, tim)
  endif 
  ;; need to have /raw keyword here when reading EQH shotfiles. No
  ;; idea why, maybe as a result of the particular flavour of 
  ;; read_signal - look into this.
  read_signal,err,diaref,'PFM     ',FLOAT(t1),FLOAT(t2),$
              pfm,/raw
  read_signal,err,diaref,'Ri      ',FLOAT(t1),FLOAT(t2),$
              r
  read_signal,err,diaref,'Zj      ',FLOAT(t1),FLOAT(t2),$
              z
  read_signal,err,diaref,'time    ',FLOAT(t1),FLOAT(t2),$
              time
  read_signal,err,diaref,'Jpol    ',FLOAT(t1),FLOAT(t2),$
              jpoltemp
  read_signal,err,diaref,'PFxx    ',FLOAT(t1),FLOAT(t2),$
              spec
  read_signal,err,diaref,'RPFx    ',FLOAT(t1),FLOAT(t2),$
              rspec
  read_signal,err,diaref,'zPFx    ',FLOAT(t1),FLOAT(t2),$
              zspec
  
  ctrl=0L
  s = CALL_EXTERNAL (!libddww,'ddgetaug','ddclose',err,diaref)
  IF err NE 0 THEN BEGIN
     s= CALL_EXTERNAL (!libddww,'ddgetaug','xxerror',err,ctrl, ' ')
  ENDIF

  ;;time is not a timebase, but a signal in these shotfiles,
  ;;need to select desired time points
  times=where(time GE float(t1) AND time LE float(t2))
  
  if times[0] eq -1 then begin
    ;sometimes if time base I want towrite is between EQI 
    ;time points - I get an error RMM 03.12.2019
    
    tmdifmin=min(abs(time-(t1+t2)/2.),rmind)
    if tmdifmin le 0.1 then times=time[rmind]
  endif
  
  time=time[times]
  ntimes=n_elements(time)

  r     =r[*,ntimes-1]
  z     =z[*,ntimes-1]
  jpoltemp  =jpoltemp[*,times]
  spec  =spec[*,times]
  npfl = n_elements(jpoltemp) / ntimes / 2.

  nr    =n_elements(r)
  nz    =n_elements(z)
  pfm   =pfm[0:nr-1,0:nz-1,times]

  rmag =reform(rspec[0,times])
  zmag =reform(zspec[0,times])

  jpol    =fltarr(npfl,ntimes)
  jpolp   =fltarr(npfl,ntimes)
  FOR i=0,npfl-1 DO BEGIN
      jpol[i,*]    =jpoltemp[i*2,*]
      jpolp[i,*]   =jpoltemp[(i*2)+1,*]
   ENDFOR
  Bmag = jpol[npfl-1,*] * mu0 / (2. * !PI) / rmag
  f0 = jpol[0,*] * mu0 / (2. * !pi)
  Rb = f0 / Bmag


  dpsidz =fltarr(nr,nz,ntimes)
  d2psidz=fltarr(nr,nz,ntimes)
  dpsidr =fltarr(nr,nz,ntimes)
  d2psidr=fltarr(nr,nz,ntimes)

  shear=fltarr(npfl,ntimes)
  rhopol=fltarr(npfl,ntimes)
  rhotor=fltarr(npfl,ntimes)
  torshear=fltarr(npfl,ntimes)

  dqdr=fltarr(nr,nz)
  dqdz=fltarr(nr,nz)
  dr=r[1:*]-r[0:nr-2]
  dz=z[1:*]-z[0:nz-2]

  ;;calculate central difference first and second derivatives
  dr = mean(deriv(r))
  dz = mean(deriv(z))
  IF ntimes GT 1 THEN BEGIN
     dpsidr = (shift(pfm,[1,0,0])-shift(pfm,[-1,0,0]))/ (2. * dr)
     dpsidz = (shift(pfm,[0,1,0])-shift(pfm,[0,-1,0]))/ (2. * dz)
     
     d2psidr = (shift(pfm,[1,0,0]) - 2. * pfm + shift(pfm,[-1,0,0])) / (dr^2)
     d2psidz = (shift(pfm,[0,1,0]) - 2. * pfm + shift(pfm,[0,-1,0])) / (dz^2)

     dpsidrdz = (shift(dpsidr,[0,1,0])-shift(dpsidr,[0,-1,0]))/ (2. * dz)
     dpsidzdr = (shift(dpsidz,[1,0,0])-shift(dpsidr,[-1,0,0]))/ (2. * dr)

     mixed_partial = (shift(pfm,[1,1,0]) - shift(pfm,[1,-1,0]) - shift(pfm,[-1,1,0]) + shift(pfm,[-1,-1,0])) $ 
            * 0.25 / (dr*dz)
  ENDIF ELSE BEGIN
     dpsidr = (shift(pfm,[1,0])-shift(pfm,[-1,0]))/ (2. * dr)
     dpsidz = (shift(pfm,[0,1])-shift(pfm,[0,-1]))/ (2. * dz)
     
     d2psidr = (shift(pfm,[1,0]) - 2. * pfm + shift(pfm,[-1,0])) / (dr^2)
     d2psidz = (shift(pfm,[0,1]) - 2. * pfm + shift(pfm,[0,-1])) / (dz^2)
    
     dpsidrdz = (shift(dpsidr,[0,1])-shift(dpsidr,[0,-1]))/ (2. * dz)
     dpsidzdr = (shift(dpsidz,[1,0])-shift(dpsidr,[-1,0]))/ (2. * dr)   

     mixed_partial = (shift(pfm,[1,1]) - shift(pfm,[1,-1]) - shift(pfm,[-1,1]) + shift(pfm,[-1,-1])) $ 
            * 0.25 / (dr*dz)
  ENDELSE

  rbmag_av = mean(rb * bmag)
  bt = 1. / rebin(r,nr,nz,ntimes) * rbmag_av
  br=-1/rebin(r,nr,nz,ntimes)*dpsidz/(2*!pi)
  bz= 1/rebin(r,nr,nz,ntimes)*dpsidr/(2*!pi)
  bpol=sqrt(br^2+bz^2)


  surfaces={pfm:pfm,$
            b_pol:bpol,$
            br:br,$
            bz:bz,$
            bt:bt,$
            r:r,$
            z:z,$
            time:time,$
            rmag:rmag,$
            zmag:zmag}
            
END
