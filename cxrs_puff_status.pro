; Version 1 by U.Plank 23.02.2018


PRO cxrs_puff_status, diagnostic, shot, time, exp, puff_exp=puff_exp, puff_diag=puff_diag,puff_shot=puff_shot, puff_edn=puff_edn, puff_name=puff_name, puff_time=puff_time, $
                      puff,puff_new, puff_shotfile, err
  
; Return a vector of flags showing at which CXRS measurement
; time 'sufficient' puff voltage is available
  
  id  =  'CXRS_PUFF_STATUS: '
  
;  
; check if needed system variable exist. Define it if not.
  defsysv, '!LIBDDWW', exists=exists
  if exists eq 0 then defsysv, '!LIBDDWW', '/usr/ads/lib/libddww.so', 1
  
; ------CNR-------         
  
  
  
                                ;[V]  threshold of piezo valve status open
  threshold= 80
  
; intialise
  ier        = 0L
  dia_ref    = 0L
  datum      = string(' ',format='(a18)')
  dim        = 0L
  phys_unit  = 0L
  err_string = string(' ',format='(a80)')
   
  ntime =  N_ELEMENTS(time)  
  
; Open HEB shot file for gas valve injection time
  if not keyword_set(puff_exp) then puff_exp  =  'AUGD'
  if not keyword_set(puff_diag) then puff_diag =  'HEB'
  if not keyword_set(puff_shot) then puff_shot =  shot
  if not keyword_set(puff_edn) then puff_edn  =  0L
  if not keyword_set(puff_name) then puff_name =  'S16VALVE'
  if not keyword_set(puff_time) then puff_time = 1L                ; to force returning time base as well
 if shot eq 36909 then puff_shot=36937                            ;there's a time shift of 1 frame which would need to be changed
 if shot eq 38188 then puff_shot=38182 ; 38188 data transfer mistake in HEB of signal time base

 
  read_signal_mrm,ier, puff_shot,puff_diag,puff_name,puff_time,puff_voltage,phys_dim,edition=puff_edn, exp=puff_exp
 
  IF ier NE 0 THEN BEGIN
     read_signal_mrm,ier, puff_shot,puff_diag,puff_name,puff_time,puff_voltage,phys_dim,edition=puff_edn, exp='ULP'
     puff_exp='ULP'
     IF ier NE 0 THEN BEGIN
        read_signal_mrm,ier, puff_shot,puff_diag,puff_name,puff_time,puff_voltage,phys_dim,edition=puff_edn, exp='MGRIEN'
        puff_exp='MGRIEN'
        if ier eq 553713686 then begin
           
           print, 'No gas puff, only passive signal! '
           puff_new=fltarr(ntime)
           puff=fltarr(ntime)
           
           GOTO, FINISH
        endif else begin
           err_string =  'read_signal error: '+STRTRIM(STRING(ier),2)
           GOTO, FINISH2
        endelse
        
     ENDIF
  ENDIF
  if diagnostic eq 'CNR' and puff_name eq 'S16VALVE' and  max(puff_voltage) lt threshold then begin
     print,''
     print, 'No gas puff, only passive signal!' 
     print,''
     puff_new=fltarr(ntime)
     puff=fltarr(ntime) 
     GOTO, FINISH
  endif                         
  
  if shot le 35045 then begin
                                ; shift signal of piezo according to CX-signal (delta is about 1ms)
     
     

     delta=where(puff_time gt 0. and puff_time lt 0.001)  
     n_delta=N_ELEMENTS(delta)
     delta_voltage=FLTARR(n_delta)
     n_puff_voltage=N_ELEMENTS(puff_voltage) 
     puff_voltage_new=puff_voltage[0:n_puff_voltage-1-n_delta]
     puff_voltage=[delta_voltage,puff_voltage_new]
     
     
; Integrate valve signal over each CXRS exposure (according to NBI calculation)
     
 
     puff_ave_voltage =  FLTARR(ntime)
     
     
;make array for the puff on times to speed up reading in routine in the for loop
     puff_index_on=where(puff_voltage gt threshold)
     ;if n_elements(puff_index_on) eq 1 then begin
     ;   puff_new=FLTARR(ntime)
     ;   puff=FLTARR(ntime)
     ;   GOTO, FINISH      
     ;endif
     puff_time_on=puff_time[puff_index_on[0]:n_elements(puff_time)-1]
     puff_voltage_on=puff_voltage[puff_index_on[0]:n_elements(puff_time)-1]
     
     
     FOR i=0L, ntime-1 DO BEGIN
        tstart = time[i] - exp[i]/2.
        tstop  = time[i] + exp[i]/2.
        wh_puff_time =  WHERE(puff_time GT tstart AND puff_time LT tstop, $
                              n_puff_time)
        if n_puff_time eq 0 then begin
           puff_ave_voltage[i] = 0
        endif else begin                                                   
           puff_ave_voltage[i] = INT_TABULATED(puff_time[wh_puff_time], puff_voltage[wh_puff_time])/(tstop-tstart)
           
        endelse
     ENDFOR
     
     
     
     puff= (puff_ave_voltage GT threshold)
     n_puff=n_elements(puff)
     puff_new=fltarr(n_puff)
; change puff signal form according to CX signal
     
     for i=0L, n_puff-4 do begin
        if puff[i] eq 1 and  puff[i+1] eq 0 then begin
           puff_new[i+1]=1
           puff_new[i+2]=0.5
        endif
     endfor   
     puff_new=puff+puff_new
  endif else begin
     
     
     
                                ; Integrate valve signal over each CXRS exposure (according to NBI calculation)
     

     puff_ave_voltage =  FLTARR(ntime)
     
     
;make array for the puff on times to speed up reading in routine in the for loop
     puff_index_on=where(puff_voltage gt threshold)
     ; this is when the puff was on all time
    ; if n_elements(puff_index_on) eq 1 then begin
     ;   puff_new=fltarr(ntime)
      ;  puff=fltarr(ntime)
       ; GOTO, FINISH      
     ;endif
     puff_time_on=puff_time[puff_index_on[0]:n_elements(puff_time)-1]
     puff_voltage_on=puff_voltage[puff_index_on[0]:n_elements(puff_time)-1]
     
     
     FOR i=0L, ntime-1 DO BEGIN
        tstart = time[i] - exp[i]/2.
        tstop  = time[i] + exp[i]/2.
        wh_puff_time =  WHERE(puff_time GT tstart AND puff_time LT tstop, $
                              n_puff_time)
        if n_puff_time eq 0 then begin
           puff_ave_voltage[i] = 0
        endif else begin                                                   
           puff_ave_voltage[i] = INT_TABULATED(puff_time[wh_puff_time], puff_voltage[wh_puff_time])/(tstop-tstart)
        endelse
     ENDFOR   
     puff = (puff_ave_voltage GT threshold)
     n_puff=n_elements(puff)
     puff_new1=fltarr(n_puff) 
     puff_new=fltarr(n_puff)    ; change signal according to actual data (see gas_puff_sync)
     for i=0L, n_puff-4 do begin
        if puff[i] eq 1. and  puff[i+1] eq 0. then begin
           puff_new1[i+1]=1.         
           puff_new[i+2]=0.5
           puff_new[i+1]=1
        endif
        
        if puff[i] eq 0. and puff[i+1] eq 1. then begin
           puff_new[i+1]=-0.5
        endif 
        
     endfor
     
     puff_new=puff+puff_new
     puff=puff+puff_new1
     
  endelse
FINISH:
  puff_shotfile =  { experiment : puff_exp,  $
                     diagnostic : puff_diag, $
                     shot       : puff_shot, $
                     edition    : puff_edn   }
  
  
FINISH2:
  
; Close shotfile
  s = call_external(!libddww,'ddgetaug','ddclose', $
                    ier,dia_ref)
 
  if STRLEN(STRTRIM(err_string, 2)) gt 0  then begin
     s = call_external(!libddww,'ddgetaug','xxerrprt', $
                       -1L,err_string,ier,3L,'ddclose:')
     err = id+err_string
  endif else begin
      err =  ''
  endelse
   
END

