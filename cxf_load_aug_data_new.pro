; files
@/afs/ipp/home/s/sprd/XXX_DIAG/BASELINE/fit_baseline.pro
@/afs/ipp/home/c/cxrs/idl/COR_BOS_ausw/cxf_load_cor_bos.pro
@/afs/ipp/home/c/cxrs/idl/CER_ausw/cxf_load_cer.pro
@/afs/ipp/home/c/cxrs/CAR_diag/idl/load_car_shf_v4.pro
@/afs/ipp/home/c/cxrs/idl/cxsfit64/machines/aug/get_cxrs_loc_in_time.pro
@/afs/ipp/home/c/cxrs/cpp/read-edge-cx-cpp/idl/cxf_interface.pro
@/afs/ipp/home/s/sprd/idlpro/fit_spec/read_xvs_diag.pro
@/afs/ipp/home/c/cxrs/idl/cxsfit64/machines/aug/hfs_puff_status.pro

PRO cxf_load_aug_data,spectrometer_data,err,extraoptions=extraoptions,$
                      all_tracks=all_tracks,Beammod=beammod,$
                      passivecal=passivecal,roiadd=roiadd,$
                      neon=neon,cfrdata=cfrdata
;
; ldh 24.10.05 - first version based on CHEAP's
;                load_aug_spectra.pro

;  spectrometer_data : I/O : structure to contain spectrometer data
;                            (see cxf_define_spectrometer_data for details), it
;                            must be pre-loaded with machine, shot and
;                            spectrometer definitions
;  err               : O   : character string for error messages, null
;                            string on successful completion


err  = ''
id   = 'CXF_LOAD_AUG_DATA: '
ierr = 0L

; speed of light
c = 2.9979D8      ; [m/s]

; Planck constant   ; [J s]
h = 6.6261D-34

; Paths to diagnostic software
if (strmatch(!PATH, '*/cxrs/idl/CXRS_ausw*') eq 0L) then $
    !PATH = !PATH + ':' + expand_path('+/afs/ipp/home/c/cxrs/idl/CXRS_ausw')
if (strmatch(!PATH, '*/cxrs/idl/CER_ausw*') eq 0L) then $
    !PATH = !PATH + ':' + expand_path('+/afs/ipp/home/c/cxrs/idl/CER_ausw')
if (strmatch(!PATH, '*/cxrs/idl/CHR_ausw*') eq 0L) then $
    !PATH = !PATH + ':' + expand_path('+/afs/ipp/home/c/cxrs/idl/CHR_ausw')
if (strmatch(!PATH, '*/cxrs/idl/CMR_ausw*') eq 0L) then $
    !PATH = !PATH + ':' + expand_path('/afs/ipp/home/c/cxrs/idl/CMR_ausw')
if (strmatch(!PATH, '*/cxrs/idl/CPR_ausw*') eq 0L) then $
   !PATH = !PATH + ':' + expand_path('/afs/ipp/home/c/cxrs/idl/CPR_ausw')    
if (strmatch(!PATH, '*/cxrs/idl/CNR_ausw*') eq 0L) then $
    !PATH = !PATH + ':' + expand_path('/afs/ipp/home/c/cxrs/idl/CNR_ausw')    
if (strmatch(!PATH, '*/cxrs/idl/CXH_ausw*') eq 0L) then $
    !PATH = !PATH + ':' + expand_path('/afs/ipp/home/c/cxrs/idl/CXH_ausw')
if (strmatch(!PATH, '*/cxrs/idl/CVH_ausw*') eq 0L) then $
    !PATH = !PATH + ':' + expand_path('/afs/ipp/home/c/cxrs/idl/CVH_ausw')    
if (strmatch(!PATH, '*/epw/idl/5inal*') eq 0L) then $
    !PATH = !PATH + ':' + expand_path('+/afs/ipp/home/e/epw/idl/5inal')
if (strmatch(!PATH, '*/cxrs/idl/ILS_ausw*') eq 0L) then $   
    !PATH = !PATH + ':' + expand_path('+/afs/ipp/home/c/cxrs/idl/ILS_ausw')
 
setenv,'GET_SENS_CMR_PATH=/afs/ipp/home/c/cxrs/idl/CMR_ausw/'
setenv,'GET_SENS_CPR_PATH=/afs/ipp/home/c/cxrs/idl/CPR_ausw/'
setenv,'GET_SENS_CNR_PATH=/afs/ipp/home/c/cxrs/idl/CNR_ausw/'
setenv,'GET_SENS_CXH_PATH=/afs/ipp/home/c/cxrs/idl/CXH_ausw/'
setenv,'GET_SENS_CVH_PATH=/afs/ipp/home/c/cxrs/idl/CVH_ausw/'

; Input switches
shot     = spectrometer_data.shot_nr
detector = spectrometer_data.spectrometer.name
diagnosticL1 = STRSPLIT(detector, 'S', /EXTRACT)+'L'

IF NOT KEYWORD_SET(all_tracks) THEN all_tracks = 0

CASE strupcase(detector) OF
   'COR': BEGIN
      ;Additional information for the diagnostic are stored in extraoptions.CORBOS
      if keyword_set(extraoptions) then begin
        selhead           = extraoptions.COR.selhead
        addupframes       = extraoptions.COR.addupframes
        nframes           = extraoptions.COR.nframes
        fitting_method    = extraoptions.COR.fitting_method
        BeamAv            = extraoptions.COR.BeamAv
        beamdip           = extraoptions.COR.beamdip
        timeloc           = extraoptions.COR.timeloc
      endif else begin
        selhead           = 0
        addupframes       = 0
        nframes           = 1
        fitting_method    = 1
        BeamAv            = 0 
        beamdip           = 0
        timeloc           = 0
      endelse
      cxf_load_cor_bos,spectrometer_data,err=err,beammod=beammod,beamdip=beamdip,selhead=selhead,$
                       addupframes=addupframes,naddupframes=nframes,fitting_method=fitting_method,BeamAv=BeamAv,timeloc=timeloc
   END                     
   'BOS': BEGIN 
      ;Additional information for the diagnostic are stored in extraoptions.CORBOS
      if keyword_set(extraoptions) then begin
        selhead           =  extraoptions.BOS.selhead
        addupframes       = extraoptions.BOS.addupframes
        nframes           = extraoptions.BOS.nframes
        fitting_method    = extraoptions.BOS.fitting_method
        BeamAv            = extraoptions.BOS.BeamAv 
        beamdip           = extraoptions.BOS.beamdip
      endif else begin
        selhead           = 0
        addupframes       = 0
        nframes           = 1
        fitting_method    = 1
        BeamAv            = 0 
        beamdip           = 0
      endelse

      cxf_load_cor_bos,spectrometer_data,err=err,beammod=beammod,beamdip=beamdip,selhead=selhead,$
                       addupframes=addupframes,naddupframes=nframes,fitting_method=fitting_method,BeamAv=BeamAv
   END   
   'CUR': BEGIN
      ;Additional information for the diagnostic are stored in extraoptions.CORBOS
      if keyword_set(extraoptions) then begin
        selhead           = extraoptions.CUR.selhead
        addupframes       = extraoptions.CUR.addupframes
        nframes           = extraoptions.CUR.nframes
        fitting_method    = extraoptions.CUR.fitting_method
        BeamAv            = extraoptions.CUR.BeamAv           
        beamdip           = extraoptions.CUR.beamdip
        timeloc           = extraoptions.CUR.timeloc
      endif else begin
        selhead           = 0
        addupframes       = 0
        nframes           = 1
        fitting_method    = 1
        BeamAv            = 0 
        beamdip           = 0
        timeloc           = 0
      endelse
        
      cxf_load_cor_bos,spectrometer_data,err=err,beammod=beammod,beamdip=beamdip,selhead=selhead,$
                       addupframes=addupframes,naddupframes=nframes,fitting_method=fitting_method,BeamAv=BeamAv,timeloc=timeloc

   END
   ;'COR+CUR': BEGIN
       ;selhead=extraoptions.CORCUR.selhead
       ;addupframes=extraoptions.CORCUR.addupframes
       ;nframes=extraoptions.CORCUR.nframes
       ;fitting_method=extraoptions.CORCUR.fitting_method
       ;cxf_load_cor_bos,spectrometer_data,err=err,beammod=beammod,selhead=selhead,$
       ;                 addupframes=addupframes,naddupframes=nframes,fitting_method=fitting_method 
       
    ;END
   ;================================
   


;;===========================================================================================
;; CER ----- CER ----- CER ----- CER ----- CER ----- CER ----- CER ----- CER ----- CER ----- 
;;===========================================================================================

   'CER': BEGIN
      if not keyword_set(extraoptions) then begin
          CER={nframes:1}
          extraoptions = {CER:CER,timeloc:1}
          all_tracks = 1
          beammod = 0
          passivecal = 0
          roiadd = 0
          neon = 0
          cfrdata = 0
      endif 
    cxf_load_cer,shot,spectrometer_data,err=err,extraoptions=extraoptions,$
        all_tracks=all_tracks,beammod=beammod,passivecal=passivecal,$
        roiadd=roiadd,neon=neon,cfrdata=cfrdata   

   END                          ; processing for CER
;;===========================================================================================
;; CER ----- CER ----- CER ----- CER ----- CER ----- CER ----- CER ----- CER ----- CER ----- 
;;===========================================================================================
   

   'CHR': BEGIN
    
      if shot ge 30410 then begin ;Use same as COR and BOS 

         selhead=extraoptions.CHR.selhead
         addupframes=extraoptions.CHR.addupframes
         nframes=extraoptions.CHR.nframes
         fitting_method=extraoptions.CHR.fitting_method
         timeloc=extraoptions.timeloc
        
         ;how/can I include the time dependent R,Z,... in here?-RMM
       
         cxf_load_cor_bos,spectrometer_data,err=err,beammod=beammod,beamdip=beamdip,$
            selhead=selhead, addupframes=addupframes,naddupframes=nframes,$
            fitting_method=fitting_method,BeamAv=BeamAv,timeloc=timeloc

      endif else begin



; Load structure CHR containing all the data
         load_chr,shot,chr,err,passivecal=passivecal,neon=neon
         IF STRTRIM(err,2) NE '' THEN BEGIN
            err = id + err
            RETURN
         ENDIF
         
;can be made an option later
         all_trackschr=1
         IF all_trackschr THEN BEGIN
            ntrack  = chr.header.yDim
            whtrack = INDGEN(ntrack)
         ENDIF ELSE BEGIN
            whtrack =  WHERE(chr.header.R_pos GT 0, ntrack)
            IF ntrack EQ 0 THEN BEGIN
               err =  id+'No tracks with positive R_pos found!'
               RETURN
            ENDIF
         ENDELSE
         
         
; Call cxf_define_spectrometer_data with appropriate dimensions to
; complete structure definition
         npixel = chr.header.xDim
         ntrack = chr.header.yDim
         nframe = chr.header.Nframes
         ninst  = 1             ;chr.header.det_inst.nGaus
         spectrometer_data.npixel = npixel
         spectrometer_data.ntrack = ntrack
         spectrometer_data.nframe = nframe
         spectrometer_data.ninst  = ninst

         cxf_define_spectrometer_data,spectrometer_data,err
         IF err NE '' THEN RETURN

; Load the structure

; Line-of-sight names
         spectrometer_data.LOS_name = chr.header.LOS_name[whtrack]

; Beam crossing point
         spectrometer_data.R_pos   = chr.header.R_pos[whtrack]
         spectrometer_data.z_pos   = chr.header.z_pos[whtrack]
         spectrometer_data.phi_pos = chr.header.phi_pos[whtrack]

; Position of origin of the LOS
         spectrometer_data.R_orig   = chr.header.R_opt[whtrack]
         spectrometer_data.z_orig   = chr.header.z_opt[whtrack]
         spectrometer_data.phi_orig = chr.header.phi_opt[whtrack]

; Reference wavelength [A] !!!
         spectrometer_data.wlength = chr.header.wav_mid*10.

; Slit width [microns]
         spectrometer_data.slit = chr.header.SlitWid

; Time vector [s]
         spectrometer_data.time     = chr.data.time
         spectrometer_data.exposure = $
            chr.header.exptime + FLTARR(nframe)

; Calculate the wavelength intervals assuming the pixels are
; contiguous
         wav_vect  = chr.header.cor_wavel[*,whtrack]
         dwav_vect = FLTARR(npixel,ntrack)
         dwav_vect[0,*] = wav_vect[1,*]-wav_vect[0,*]
         dwav_vect[1:npixel-2,*] = 0.5*(wav_vect[2:npixel-1,*] $
                                        -wav_vect[0:npixel-3,*])
         dwav_vect[npixel-1,*] = wav_vect[npixel-1,*] $
                                 -wav_vect[npixel-2,*]
         spectrometer_data.wavelength.data      = wav_vect
; Uncertainty still to be defined!!!
         spectrometer_data.wavelength.error     = FLTARR(npixel,ntrack)
         spectrometer_data.wavelength.reference = $
            chr.header.wav_calib_file
         
;setting dispersion to a constant value from the center of the camera        
         dwav_vect[*]=dwav_vect[256]
         
         spectrometer_data.dispersion.data      = dwav_vect
; Uncertainty still to be defined!!!
         spectrometer_data.dispersion.error     = FLTARR(npixel,ntrack)
         spectrometer_data.dispersion.reference = $
            chr.header.wav_calib_file

         readout_noise = chr.header.noise.r_noise
         photoncount   = chr.header.noise.ph_count

                                ;number of background frames  
         n_bg_frames =  chr.header.bg_range[1]-chr.header.bg_range[0]+1
         
;***************************************************************************
;background substraction and brightness calibrations already done in load_chr2.pro - RMM Feb 2011

         if shot ge 25000 then begin
            spectrometer_data.intensity.data=chr.data.intens[*,whtrack,*]
            spectrometer_data.intensity.error=chr.data.intenserr[*,whtrack,*]

; Instrument function
            spectrometer_data.instfu.y0 = replicate(1.,ntrack) ;chr.header.det_inst.intens
            spectrometer_data.instfu.xw = chr.header.inst_wid_nm[whtrack]*10. ;chr.header.det_inst.width * 10.0/(2d0*alog(2d0))
            spectrometer_data.instfu.xs = replicate(0.,ntrack)                ;chr.header.det_inst.shift
            spectrometer_data.instfu.reference = ' '

         endif else begin 

;Begin CFM - old method
; Spectral intensity. Requires conversion from counts/(texp-pixel)
; to ph/(m^2 sr s nm) using the returned background level and cal_mW
; which is in (mW/(m^2 sr A)) / (counts/s/pixel)

; Uncertainties are based on a Gaussian readout noise (from tests on
; dark signal) and a SQRT(photons) uncertainty on the net signal.
; Required inputs are the readout noise and the number of
; photons/count (also determined empirically)   
            
            FOR j=0,ntrack-1 DO BEGIN
               FOR k=0,nframe-1 DO BEGIN
                  spectrometer_data.intensity.data[*,j,k] $
                     = (chr.data[k].intens[*,j]-chr.header.background[*,j]) $
                     *chr.header.cal_mW[*,j]*1.0D-2*wav_vect[*,j]*1.0D-9/(h*c) $
                     /chr.header.exptime
                  spectrometer_data.intensity.error[*,j,k] $
                     = SQRT(readout_noise^2*(n_bg_frames+1)/n_bg_frames $
                            + ((chr.data[k].intens[*,j]-chr.header.background[*,j]) > 0) $
                            / photoncount) $
                     *chr.header.cal_mW[*,j]*1.0D-2*wav_vect[*,j]*1.0D-9/(h*c) $
                     /chr.header.exptime
; Validity - check for saturation
                  wh_sat = WHERE(chr.data[k].intens[*,j] GE $
                                 2.^chr.header.dynamic_range-1,nsat)
                  IF nsat GT 0 THEN $
                     spectrometer_data.intensity.validity[wh_sat,j,k] = 2
               ENDFOR
            ENDFOR
            spectrometer_data.intensity.reference $
               = chr.header.int_calib_file

; Padova PI-detector had bad pixels.  Since they show up in the dark
; frames, we'll look for them there.
            IF shot LT 19546 THEN BEGIN ; Padova PI-detector
               bg_first  =  chr.header.bg_range[0]
               bg_last   =  chr.header.bg_range[1]
               bg_median =  MEDIAN(chr.data[bg_first:bg_last].intens)
               wh_spike  =  WHERE((chr.data[bg_first:bg_last].intens $
                                   - bg_median)/readout_noise GT 5.0,nspike)
               IF nspike GT 0 THEN BEGIN
                  bad_pix = wh_spike MOD npixel
                  bad_pix =  bad_pix[UNIQ(bad_pix,SORT(bad_pix))]
                  print, 'Bad pixels: ', bad_pix
                  print, 'Setting intensity=0, error=0, validity=1'
                  spectrometer_data.intensity.data[bad_pix,*,*] = 0.0
                  spectrometer_data.intensity.error[bad_pix,*,*] = 0.0
                  spectrometer_data.intensity.validity[bad_pix,*,*] = 1
               ENDIF
            ENDIF

; Instrument function
            spectrometer_data.instfu.y0 = chr.header.det_inst.intens
            spectrometer_data.instfu.xw = chr.header.det_inst.width * 10.0/(2d0*alog(2d0))
            spectrometer_data.instfu.xs = chr.header.det_inst.shift
            spectrometer_data.instfu.reference = chr.header.det_inst.reference
         endelse
         

; Load flag showing if 'sufficient' beam power is available for a
; given frame

         print, 'beam status taken from NBI3!  this can be changed in line 410 in'
         print, '/u/cxrs/idl/cxsfit/machines/aug/cxf_load_aug.data.pro'

         cxrs_beam_status, 'CER', shot, spectrometer_data.time, $
                           spectrometer_data.exposure, $
                           beam, NI_shotfile, err
         IF STRTRIM(err,2) NE '' THEN BEGIN
; If the shot file is missing then assume no beams (this is default in
; cxf_define_spectrometer_data)!!!
            IF STRPOS(err,'shotfile does not exist') NE -1 THEN BEGIN
               err = ''
            ENDIF ELSE BEGIN
               RETURN
            ENDELSE
         ENDIF ELSE $
            spectrometer_data.beam = beam


;RMM - General beam modulation code
;Input: Spectrometer_data
;Output: Modulated_data = New Data Structure to be fed to CXSFIT
;The intensity.data in this structure has the same time base but,
;The beam off frames are unchanged - the beam on frames have had a background
;frame subtracted from them
         IF KEYWORD_SET(BEAMMOD) THEN BEGIN
            beam_modulation,spectrometer_data,chr,whtrack,wav_vect,BeamModOut
                                ; stop        
            spectrometer_data=BeamModOut
         ENDIF
      endelse

    END                       ; processing for CHR


   'CMR': BEGIN
; Load structure CMR containing all the data
      test = 0
      naverage_baseline=4
      if test then begin
         cxf_interface,spectrometer_data,naverage_baseline=naverage_baseline
      endif else begin
         cmr_err=1
         if keyword_set(extraoptions) then begin
            CASE extraoptions.CMR.time_choice OF
               0: begin
                  load_cmr,shot,cmr,err,/ip_on
               end
               1: begin
                  load_cmr,shot,cmr,err
               end
               2: begin
                  load_cmr,shot,cmr,err,time_slice=extraoptions.CMR.time_intervall
               end
            endcase
         endif else begin
            load_cmr,shot,cmr,err,/ip_on
         endelse
         if shot lt 25900 and shot gt 20000 then correct_wvl_cmr,cmr
         if shot  lt 25900 and shot gt 20000 then begin
            get_err_bars_cmr,cmr,cmr_err
            stack_suited_channels,cmr,cmr_err
         endif else begin
            if shot lt 29750 and shot gt 20000  then begin
               get_err_bars_cmr,cmr,cmr_err
               stack_suited_channels_cmr,cmr,cmr_err        
            endif else begin
               if shot lt 30149 and shot gt 20000  then begin
                  print,'excluding CMR-6-1'
                  get_err_bars_cmr,cmr,cmr_err
                  stack_suited_channels_cmr_exclude,cmr,cmr_err,'CMR-6-1'                
               endif else begin
                  if shot gt 33740 or shot lt 10000 then begin
                     get_err_bars_cmr_14,cmr,cmr_err
                  endif else begin
                                ; CMR-3 used for SOL measuraments, hence exclude
                     if shot gt 33740 then begin 
                        exclude_channel_cmr_17,cmr,cmr_err,los_to_keep='CMR*',los_to_exclude='CMR-3-*'
                     endif else begin
                        if shot gt 36703 and shot lt 36715 or shot eq 36719 or shot eq 36728 or shot eq 36732 or shot eq 36733 then begin ; actually wrong sfh: ch 24 on CNR-6 and ch 25 on CNR-8 can be included if needed
                           exclude_channel_cmr_17,cmr,cmr_err
                           get_err_bars_cmr_14,cmr,cmr_err
                           
                           
                        endif else begin 
                           exclude_channel_cmr_17,cmr,cmr_err,los_to_keep='CMR*'
                        endelse
                        
                        get_err_bars_cmr_14,cmr,cmr_err
                        exclude_channel_cmr_14,cmr,cmr_err        
                     endelse
                  endelse
               endelse
            endelse
         endelse                
         
         
; Call cxf_define_spectrometer_data with appropriate dimensions to
; complete structure definition
         npixel = cmr.header.xDim
         ntrack = cmr.header.yDim
         nframe = cmr.header.Nframes
         if cmr.header.shot lt 25900 then ninst  = 1  $ ;cmr.header.det_inst.nGaus 
         else ninst=cmr.header.nGaus                    ; for shots>25899: 4 Gaussians used for instrument function
                                ; go back to using 1 Gaussian
         spectrometer_data.npixel = npixel
         spectrometer_data.ntrack = ntrack
         spectrometer_data.ninst  = ninst
         if cmr.header.shot gt 20000 then begin
            ind_frames=where((cmr.data.time gt -11. and cmr.data.time lt 10) or cmr.data.time lt 0.0)
            nframe=n_elements(ind_frames)
         endif else begin
            nframe=n_elements(cmr.data.time)
            ind_frames=indgen(nframe)
         endelse
         spectrometer_data.nframe_baseline = long(nframe/naverage_baseline)
         spectrometer_data.nframe = nframe
         cxf_define_spectrometer_data,spectrometer_data,err
         IF err NE '' THEN RETURN
; Load the structure
         
; Line-of-sight names
         spectrometer_data.LOS_name = cmr.header.LOS_name
         
; Beam crossing point
         spectrometer_data.R_pos   = cmr.header.maximum_r
         spectrometer_data.z_pos   = cmr.header.maximum_z
         spectrometer_data.phi_pos = cmr.header.maximum_phi
         
; Position of origin of the LOS
         spectrometer_data.R_orig   = cmr.header.R_opt
         spectrometer_data.z_orig   = cmr.header.z_opt
         spectrometer_data.phi_orig = cmr.header.phi_opt
                                ;    spectrometer_data.alpha_orig   = cmr.header.alpha_los
                                ;    spectrometer_data.beta_orig = cmr.header.beta_los
         
; Reference wavelength [A] !!!
         spectrometer_data.wlength = cmr.header.wavel(255)*10.
         
; Slit width [microns]
         spectrometer_data.slit = cmr.header.SlitWid
         
; Time vector [s]
         spectrometer_data.time     = cmr.data(ind_frames).time
         spectrometer_data.exposure = $
            cmr.header.exptime + FLTARR(nframe)
         
; Calculate the wavelength intervals assuming the pixels are
; contiguous
         wav_vect  = cmr.header.cor_wavel
         dwav_vect = FLTARR(npixel,ntrack)
      dwav_vect[0,*] = wav_vect[1,*]-wav_vect[0,*]
      dwav_vect[1:npixel-2,*] = 0.5*(wav_vect[2:npixel-1,*] $
                                     -wav_vect[0:npixel-3,*])
      dwav_vect[npixel-1,*] = wav_vect[npixel-1,*] $
                              -wav_vect[npixel-2,*]
      spectrometer_data.wavelength.data      = wav_vect
; Uncertainty still to be defined!!!
      spectrometer_data.wavelength.error     = FLTARR(npixel,ntrack)
      spectrometer_data.wavelength.reference = ' '
      spectrometer_data.dispersion.data      = dwav_vect
; Uncertainty still to be defined!!!
      spectrometer_data.dispersion.error     = FLTARR(npixel,ntrack)
      spectrometer_data.dispersion.reference = ' '
      
; Spectral intensity. Requires conversion from counts/(texp-pixel)
; to ph/(m^2 sr s nm) using the returned background level and cal_mW
; which is in (mW/(m^2 sr A)) / (counts/s/pixel)
      
; Uncertainties are based on a Gaussian readout noise (from tests on
; dark signal) and a SQRT(photons) uncertainty on the net signal.
; Required inputs are the readout noise and the number of
; photons/count (also determined empirically)
      
; >>> Hard-code the two input parameters here.  This could later be
; >>> made into a detector- and shot-dependent table and, possibly,
; >>> handled internally by load_chr rather than here.
                                ; get_err_bars,cmr,cmr_err
      
      n_bg_frames =  cmr.header.bg_range[1]-cmr.header.bg_range[0]+1
      FOR j=0,ntrack-1 DO BEGIN
         FOR k=0,nframe-1 DO BEGIN
            spectrometer_data.intensity.data[*,j,k] $
               = cmr.data[ind_frames(k)].intens_calib[*,j]
; Validity - check for saturation
            wh_sat = WHERE(cmr.data[ind_frames(k)].intens[*,j] GE $
                           2.^cmr.header.dynamic_range-1,nsat)
            IF nsat GT 0 THEN $
               spectrometer_data.intensity.validity[wh_sat,j,ind_frames(k)] = 2
         ENDFOR
      ENDFOR
      spectrometer_data.intensity.error $
         = cmr_err.err_bars(*,*,ind_frames)
      
      
      spectrometer_data.intensity.reference $
         = ' '
                                ; Instrument function
                                ;if shot lt 25900 then begin
      if ninst eq 1l then begin
         spectrometer_data.instfu.y0 = replicate(1.,ntrack)        ;cmr.header.det_inst.intens
         spectrometer_data.instfu.xw = cmr.header.inst_wid_nm*10.  ;cmr.header.det_inst.width * 10.0/(2d0*alog(2d0))
          spectrometer_data.instfu.xs = replicate(0.,ntrack);cmr.header.det_inst.shift
          spectrometer_data.instfu.reference = ' '
       endif else begin
                                ; for shots gt 25899 instrument functions includes 4 Gaussians (amplitude+width stored in structure)
                                ; however - treat inst func with 1 Gauss fit (at least for now ... -> there are still some problems in cxsfit-software ...)
          gauss_intens = fltarr(n_elements(cmr.header.inst_wid_nm(0,*,0)),n_elements(cmr.header.inst_wid_nm(0,0,*)))
          gauss_widths = fltarr(n_elements(cmr.header.inst_wid_nm(1,*,0)),n_elements(cmr.header.inst_wid_nm(1,0,*)))
          
          for i=0,ntrack-1 do begin
             gauss_intens(*,i) = cmr.header.inst_wid_nm(0,*,i)/max(cmr.header.inst_wid_nm(0,*,i))
             gauss_widths(*,i) = cmr.header.inst_wid_nm(1,*,i)
          endfor
          spectrometer_data.instfu.y0 = gauss_intens(*,*)
          spectrometer_data.instfu.xw = gauss_widths(*,*)*10.
          spectrometer_data.instfu.xs = replicate(0.,ninst,ntrack)
          spectrometer_data.instfu.reference = ' '
       endelse
       
       
; load Gas puff Se 16
       
       if cmr.header.shot gt 36703 and shot lt 36715 or shot eq 36719 or shot eq 36728 or shot eq 36732 or shot eq 36733 then begin
          cxrs_puff_status, 'CNR', cmr.header.shot, spectrometer_data.time, $
                            spectrometer_data.exposure, $
                            puff, puff_new, puff_shotfile, err
          IF STRTRIM(err,2) NE '' THEN BEGIN
                                ; If the shot file is missing then
                                ; assume no gas puff (this is default in
                                ; cxf_define_spectrometer_data)!!!
             IF STRPOS(err,'shotfile does not exist') NE -1 THEN BEGIN
                spectrometer_data.beam = spectrometer_data.time*0.
                err = ''
             ENDIF ELSE BEGIN
                RETURN
             ENDELSE
          ENDIF ELSE BEGIN 
             puff_new = reform(puff_new,1,n_elements(puff_new))
          ENDELSE
          
          spectrometer_data.beam = puff
          spectrometer_data.modulation.flag=0
          beams=[puff_new]
          names = ['GasPuffSe16']
          *spectrometer_data.modulation.beampower=beams
          *spectrometer_data.modulation.beamlabels =names
          *spectrometer_data.modulation.orig_beam =spectrometer_data.beam 
          *spectrometer_data.modulation.orig_data =spectrometer_data.intensity.data
          *spectrometer_data.modulation.orig_error = spectrometer_data.intensity.error  
       endif else begin
        if cmr.header.shot gt 1000 then begin

          cxrs_beam_status, 'CER', cmr.header.shot, spectrometer_data.time, $
                            spectrometer_data.exposure, $
                            beam, NI_shotfile, err
          IF STRTRIM(err,2) NE '' THEN BEGIN
                                ; If the shot file is missing then assume no beams (this is default in
                                ; cxf_define_spectrometer_data)!!!
             IF STRPOS(err,'shotfile does not exist') NE -1 THEN BEGIN
                spectrometer_data.beam = spectrometer_data.time*0.
                err = ''
                ENDIF ELSE BEGIN
                   RETURN
                ENDELSE
             ENDIF ELSE BEGIN
                beam = float(beam)
                for jt=0,spectrometer_data.nframe-2 do begin
                                ; Switch on beam
                   if beam[jt] - beam[jt+1] eq -1 then begin
                      beam[jt+1] = 0.3
                   endif
                                ; Switch off beam
                   if beam[jt] - beam[jt+1] eq 1 then begin
                      beam[jt] = 0.8
                   endif
                endfor
                spectrometer_data.beam = beam
             ENDELSE
             
             
             
             if cmr.header.shot gt 33740 then begin
                
                if extraoptions.CMR.time_choice ne 1 then begin
                                ; Adding Gas Puff as second beam
                   
                   puff_new = 0
                   cxrs_puff_status, 'CNR', cmr.header.shot, spectrometer_data.time, $
                                     spectrometer_data.exposure, $
                                     puff, puff_new, puff_shotfile, err
                   IF STRTRIM(err,2) NE '' or total(puff_new) EQ 0 THEN BEGIN
                                ; If the shot file is missing or puff was o = nframff then assume only heating beams
                      
                      beam = reform(beam,1,n_elements(beam))
                      names = ['NBIBox1']
                      spectrometer_data.modulation.flag=0
                      *spectrometer_data.modulation.beampower = beam
                      *spectrometer_data.modulation.beamlabels = names 
                      *spectrometer_data.modulation.orig_beam = beam 
                      *spectrometer_data.modulation.orig_data = spectrometer_data.intensity.data
                      *spectrometer_data.modulation.orig_error = spectrometer_data.intensity.error           
                      err = ''
                   ENDIF ELSE BEGIN
                      print, 'Remove time points where gas puff Se 16 was on'    
                                ; Remove time points when the gas puff was on! Frames corrupted
                      index2delete = WHERE(puff_new ne 0)
                      old_nframe = n_elements(spectrometer_data.time)
                      spectrometer_data.nframe = n_elements(spectrometer_data.time)-n_elements(index2delete)
                      new_time = spectrometer_data.time
                      new_exposure = spectrometer_data.exposure
                      new_beam = spectrometer_data.beam
                      new_beam = beam
                      remove,index2delete,new_time,new_exposure,new_beam
                      structure_modify,spectrometer_data,'time',new_time
                      structure_modify,spectrometer_data,'exposure',new_exposure
                      structure_modify,spectrometer_data,'beam',new_beam
                      new_intensity = spectrometer_data.intensity
                      new_data = fltarr(spectrometer_data.npixel,spectrometer_data.ntrack,spectrometer_data.nframe)
                      new_error = fltarr(spectrometer_data.npixel,spectrometer_data.ntrack,spectrometer_data.nframe)
                      new_validity = fltarr(spectrometer_data.npixel,spectrometer_data.ntrack,spectrometer_data.nframe)
                      j2delete = 0
                      jnew = 0
                      for jt=0,old_nframe-1 do begin
                         if jt ne index2delete[j2delete] then begin
                            new_data[*,*,jnew] = spectrometer_data.intensity.data[*,*,jt]
                            new_error[*,*,jnew] = spectrometer_data.intensity.error[*,*,jt]
                            new_validity[*,*,jnew] = spectrometer_data.intensity.validity[*,*,jt]
                            jnew = jnew + 1
                         endif else begin
                            if j2delete ne n_elements(index2delete)-1 then j2delete = j2delete + 1
                         endelse
                      endfor
                      structure_modify,new_intensity,'data',new_data
                      structure_modify,new_intensity,'error',new_error
                      structure_modify,new_intensity,'validity',new_validity
                      structure_modify,spectrometer_data,'intensity',new_intensity
                      nframe = spectrometer_data.nframe
                   ENDELSE
                endif else begin
                   print, 'LOAD ALL DATA: GAS PUFF SE 16 IS NOT SUBTRACTED !!!'
                endelse
          
             endif
          endelse
     endif
       
   
       
 
    
    
    ;; -----------------------------------------------------------
    ;; ---------- FIT THE BASELINE FOR ZEFF CALCULATION CMR-------
    ;; -----------------------------------------------------------
    fit_baseline,spectrometer_data.shot_nr,spectrometer_data.time,spectrometer_data.intensity.data $
                 ,spectrometer_data.intensity.error,baseline $
                 ,average=naverage_baseline;,/doplot,/ps
    spectrometer_data.baseline=baseline
    ; For the automatic fitting:
    if not keyword_set(extraoptions) then begin
       CMRtmp = {time_choice:0}
       extraoptions = {CMR:CMRtmp}
    endif

    



; Setting cos angle between LOS and B equal to 1 for CPR lines of sight and calculate it for CMR
    externalcostheta = fltarr(ntrack,nframe)+1.
    x_pos  = spectrometer_data.R_pos*cos(spectrometer_data.phi_pos/!RADEG)
    y_pos  = spectrometer_data.R_pos*sin(spectrometer_data.phi_pos/!RADEG)
    z_pos  = spectrometer_data.z_pos
    x_orig = spectrometer_data.R_orig*cos(spectrometer_data.phi_orig/!RADEG)
    y_orig = spectrometer_data.R_orig*sin(spectrometer_data.phi_orig/!RADEG)
    z_orig = spectrometer_data.z_orig
    l_LOS  = SQRT((x_pos-x_orig)^2+(y_pos-y_orig)^2+(z_pos-z_orig)^2)
    c_LOS  = 	[[(x_pos-x_orig)/l_LOS], $
            [(y_pos-y_orig)/l_LOS], $
             [(z_pos-z_orig)/l_LOS]]
    c_tor  = [[-y_pos/spectrometer_data.R_pos],[+x_pos/spectrometer_data.R_pos],[FLTARR(spectrometer_data.ntrack)]]
    costheta_tor = TOTAL(c_LOS*c_tor,2)
    for jlos=0,spectrometer_data.ntrack-1 do begin
        if STRMATCH(spectrometer_data.los_name[jlos],'CMR*') then begin
            externalcostheta[jlos,*] = costheta_tor[jlos]
        endif
        if STRMATCH(spectrometer_data.los_name[jlos],'CPR*') then begin
            externalcostheta[jlos,*] = 1.
        endif
    endfor
    spectrometer_data = create_struct(spectrometer_data,'externalcostheta',externalcostheta)
    endelse ; TEST IF


   
     END                        ; processing for CMR
    
    
    'CPR': BEGIN
       cpr_err=1
       if keyword_set(extraoptions) then begin
          CASE extraoptions.CPR.time_choice OF
             0: begin
                load_cpr,shot,cpr,err,/ip_on
             end
             1: begin
                load_cpr,shot,cpr,err
             end
             2: begin
                load_cpr,shot,cpr,err,time_slice=extraoptions.CPR.time_intervall
             end
          endcase
       endif else begin
          load_cpr,shot,cpr,err,/ip_on
       endelse
       
;        correct_wvl_cpr,cpr
       if shot lt 30149 then begin 
          get_err_bars_cpr,cpr,cpr_err
          stack_suited_channels_cpr,cpr,cpr_err
       endif else begin
          if shot lt 33757 then begin
             get_err_bars_cpr_14,cpr,cpr_err
             exclude_channel_cpr_14,cpr,cpr_err
          endif else begin
             get_err_bars_cpr_14,cpr,cpr_err
             exclude_channel_cpr_17,cpr,cpr_err,los_to_keep='CPR*'
          endelse
       endelse
       
; Call cxf_define_spectrometer_data with appropriate dimensions to
; complete structure definition
       npixel = cpr.header.xDim
       ntrack = cpr.header.yDim
       ninst  = 1               ;cmr.header.det_inst.nGaus
       spectrometer_data.npixel = npixel
       spectrometer_data.ntrack = ntrack
       spectrometer_data.ninst  = ninst
       naverage_baseline=4
       ind_frames=where((cpr.data.time gt -11. and cpr.data.time lt 10) or cpr.data.time lt 0.0)
       nframe=n_elements(ind_frames)
       spectrometer_data.nframe_baseline = long(nframe/naverage_baseline)
       spectrometer_data.nframe = nframe
       cxf_define_spectrometer_data,spectrometer_data,err
       IF err NE '' THEN RETURN
       
; Load the structure
       
; Line-of-sight names
       spectrometer_data.LOS_name = cpr.header.LOS_name
       
; Beam crossing point
       spectrometer_data.R_pos   = cpr.header.maximum_r
       spectrometer_data.z_pos   = cpr.header.maximum_z
       spectrometer_data.phi_pos = cpr.header.maximum_phi
       
; Position of origin of the LOS
       spectrometer_data.R_orig   = cpr.header.R_opt
       spectrometer_data.z_orig   = cpr.header.z_opt
       spectrometer_data.phi_orig = cpr.header.phi_opt
       
; Reference wavelength [A] !!!
       spectrometer_data.wlength = cpr.header.wavel(255)*10.
       
; Slit width [microns]
       spectrometer_data.slit = cpr.header.SlitWid
       
; Time vector [s]
       spectrometer_data.time     = cpr.data(ind_frames).time
       spectrometer_data.exposure = $
          cpr.header.exptime + FLTARR(nframe)
       
; Calculate the wavelength intervals assuming the pixels are
; contiguous
       wav_vect  = cpr.header.cor_wavel
       dwav_vect = FLTARR(npixel,ntrack)
      dwav_vect[0,*] = wav_vect[1,*]-wav_vect[0,*]
      dwav_vect[1:npixel-2,*] = 0.5*(wav_vect[2:npixel-1,*] $
                                     -wav_vect[0:npixel-3,*])
      dwav_vect[npixel-1,*] = wav_vect[npixel-1,*] $
                              -wav_vect[npixel-2,*]
      spectrometer_data.wavelength.data      = wav_vect
; Uncertainty still to be defined!!!
      spectrometer_data.wavelength.error     = FLTARR(npixel,ntrack)
      spectrometer_data.wavelength.reference = ' '
      spectrometer_data.dispersion.data      = dwav_vect
; Uncertainty still to be defined!!!
      spectrometer_data.dispersion.error     = FLTARR(npixel,ntrack)
      spectrometer_data.dispersion.reference = ' '
      
; Spectral intensity. Requires conversion from counts/(texp-pixel)
; to ph/(m^2 sr s nm) using the returned background level and cal_mW
; which is in (mW/(m^2 sr A)) / (counts/s/pixel)
      
; Uncertainties are based on a Gaussian readout noise (from tests on
; dark signal) and a SQRT(photons) uncertainty on the net signal.
; Required inputs are the readout noise and the number of
; photons/count (also determined empirically)
      
; >>> Hard-code the two input parameters here.  This could later be
; >>> made into a detector- and shot-dependent table and, possibly,
; >>> handled internally by load_chr rather than here.
                                ; get_err_bars,cmr,cmr_err
      
      n_bg_frames =  cpr.header.bg_range[1]-cpr.header.bg_range[0]+1
      FOR j=0,ntrack-1 DO BEGIN
         FOR k=0,nframe-1 DO BEGIN
            spectrometer_data.intensity.data[*,j,k] $
               = cpr.data[ind_frames(k)].intens_calib[*,j]
; Validity - check for saturation
            wh_sat = WHERE(cpr.data[ind_frames(k)].intens[*,j] GE $
                           2.^cpr.header.dynamic_range-1,nsat)
            IF nsat GT 0 THEN $
               spectrometer_data.intensity.validity[wh_sat,j,ind_frames(k)] = 2
         ENDFOR
      ENDFOR
      spectrometer_data.intensity.error $
         = cpr_err.err_bars(*,*,ind_frames)
      
      spectrometer_data.intensity.reference $
         = ' '
      
      
      ;; -----------------------------------------------------------
      ;; ---------- FIT THE BASELINE FOR ZEFF CALCULATION CPR ------
      ;; -----------------------------------------------------------
      fit_baseline,shot,spectrometer_data.time,spectrometer_data.intensity.data $
                   ,spectrometer_data.intensity.error,baseline $
                   ,average=naverage_baseline
      spectrometer_data.baseline=baseline
      
      
      
      cxrs_beam_status, 'CPR', shot, spectrometer_data.time, $
                        spectrometer_data.exposure, $
                        beam, NI_shotfile, err
      IF STRTRIM(err,2) NE '' THEN BEGIN
; If the shot file is missing then assume no beams (this is default in
; cxf_define_spectrometer_data)!!!
         IF STRPOS(err,'shotfile does not exist') NE -1 THEN BEGIN
            err = ''
         ENDIF ELSE BEGIN
            RETURN
         ENDELSE
      ENDIF ELSE BEGIN
        beam = float(beam)
        for jt=0,spectrometer_data.nframe-2 do begin
            ; Switch on beam
            if beam[jt] - beam[jt+1] eq -1 then begin
                beam[jt+1] = 0.3
            endif
            ; Switch off beam
            if beam[jt] - beam[jt+1] eq 1 then begin
                beam[jt] = 0.8
            endif
        endfor
        spectrometer_data.beam = beam
      ENDELSE
      
                                ; Deleting frames with gas puff
     ; For the automatic fitting:
      if not keyword_set(extraoptions) then begin
          CPRtmp = {time_choice:0}
          extraoptions = {CPR:CPRtmp}
      endif
      if cpr.header.shot gt 33740 then begin
         if extraoptions.CPR.time_choice ne 1 then begin

            puff_new = 0
            cxrs_puff_status, 'CNR', cpr.header.shot, spectrometer_data.time, $
                              spectrometer_data.exposure, $
                              puff, puff_new, puff_shotfile, err
            IF STRTRIM(err,2) NE '' or total(puff_new) EQ 0 THEN BEGIN
                                ; If the shot file is missing or puff was o = nframff then assume only heating beams
               beam = reform(beam,1,n_elements(beam))
               names = ['NBIBox1']
               spectrometer_data.modulation.flag=0
               *spectrometer_data.modulation.beampower = beam
               *spectrometer_data.modulation.beamlabels = names 
               *spectrometer_data.modulation.orig_beam = beam 
               *spectrometer_data.modulation.orig_data = spectrometer_data.intensity.data
               *spectrometer_data.modulation.orig_error = spectrometer_data.intensity.error           
               err = ''
            ENDIF ELSE BEGIN
               print, 'Remove time points where gas puff Se 16 was on'
                                ; Remove time points when the gas puff was on! Frames corrupted
               index2delete = WHERE(puff_new ne 0)
               old_nframe = n_elements(spectrometer_data.time)
               spectrometer_data.nframe = n_elements(spectrometer_data.time)-n_elements(index2delete)
               new_time = spectrometer_data.time
               new_exposure = spectrometer_data.exposure
               new_beam = beam
               remove,index2delete,new_time,new_exposure,new_beam
               structure_modify,spectrometer_data,'time',new_time
               structure_modify,spectrometer_data,'exposure',new_exposure
               structure_modify,spectrometer_data,'beam',new_beam
               new_intensity = spectrometer_data.intensity
               new_data = fltarr(spectrometer_data.npixel,spectrometer_data.ntrack,spectrometer_data.nframe)
               new_error = fltarr(spectrometer_data.npixel,spectrometer_data.ntrack,spectrometer_data.nframe)
               new_validity = fltarr(spectrometer_data.npixel,spectrometer_data.ntrack,spectrometer_data.nframe)
               j2delete = 0
               jnew = 0
               for jt=0,old_nframe-1 do begin
                  if jt ne index2delete[j2delete] then begin
                     new_data[*,*,jnew] = spectrometer_data.intensity.data[*,*,jt]
                     new_error[*,*,jnew] = spectrometer_data.intensity.error[*,*,jt]
                     new_validity[*,*,jnew] = spectrometer_data.intensity.validity[*,*,jt]
                     jnew = jnew + 1
                  endif else begin
                     if j2delete ne n_elements(index2delete)-1 then j2delete = j2delete + 1
                  endelse
               endfor
               structure_modify,new_intensity,'data',new_data
               structure_modify,new_intensity,'error',new_error
               structure_modify,new_intensity,'validity',new_validity
               structure_modify,spectrometer_data,'intensity',new_intensity
               nframe = spectrometer_data.nframe
            ENDELSE
            endif else begin
            print, 'LOAD ALL DATA: GAS PUFF SE 16 IS NOT SUBTRACTED !!!'
         endelse
      endif 
      
; Setting cos angle between LOS and B equal to 1 for CPR lines of sight and calculate it for CMR
      externalcostheta = fltarr(ntrack,nframe)+1.
      x_pos  = spectrometer_data.R_pos*cos(spectrometer_data.phi_pos/!RADEG)
      y_pos  = spectrometer_data.R_pos*sin(spectrometer_data.phi_pos/!RADEG)
      z_pos  = spectrometer_data.z_pos
      x_orig = spectrometer_data.R_orig*cos(spectrometer_data.phi_orig/!RADEG)
      y_orig = spectrometer_data.R_orig*sin(spectrometer_data.phi_orig/!RADEG)
      z_orig = spectrometer_data.z_orig
      l_LOS  = SQRT((x_pos-x_orig)^2+(y_pos-y_orig)^2+(z_pos-z_orig)^2)
      c_LOS  = 	[[(x_pos-x_orig)/l_LOS], $
                [(y_pos-y_orig)/l_LOS], $
                 [(z_pos-z_orig)/l_LOS]]
      c_tor  = [[-y_pos/spectrometer_data.R_pos],[+x_pos/spectrometer_data.R_pos],[FLTARR(spectrometer_data.ntrack)]]
      costheta_tor = TOTAL(c_LOS*c_tor,2)
      for jlos=0,spectrometer_data.ntrack-1 do begin
            if STRMATCH(spectrometer_data.los_name[jlos],'CMR*') then begin
                externalcostheta[jlos,*] = costheta_tor[jlos]
            endif
            if STRMATCH(spectrometer_data.los_name[jlos],'CPR*') then begin
                externalcostheta[jlos,*] = 1.
            endif
      endfor
      spectrometer_data = create_struct(spectrometer_data,'externalcostheta',externalcostheta)

; Instrument function
      spectrometer_data.instfu.y0 = replicate(1.,ntrack);cmr.header.det_inst.intens
      spectrometer_data.instfu.xw = cpr.header.inst_wid_nm*10. ;cmr.header.det_inst.width * 10.0/(2d0*alog(2d0))
      spectrometer_data.instfu.xs = replicate(0.,ntrack);cmr.header.det_inst.shift
      spectrometer_data.instfu.reference = ' '


    
      
   END                          ; processing for CPR      
    
    
    'CNR': BEGIN
       cnr_err=1
       if keyword_set(extraoptions) then begin
          CASE extraoptions.CNR.time_choice OF
             0: begin
                load_cnr,shot,cnr,err,/ip_on
             end
             1: begin
                load_cnr,shot,cnr,err
             end
             2: begin
                load_cnr,shot,cnr,err,time_slice=extraoptions.CNR.time_intervall
             end
          endcase
       endif else begin
          load_cnr,shot,cnr,err,/ip_on
       endelse
       get_err_bars_cnr_17,cnr,cnr_err
       exclude_channel_cnr_17,cnr,cnr_err
       
; Call cxf_define_spectrometer_data with appropriate dimensions to
; complete structure definition
       npixel = cnr.header.xDim
       ntrack = cnr.header.yDim
       ninst  = 1 ;cnr.header.det_inst.nGaus
       spectrometer_data.npixel = npixel
       spectrometer_data.ntrack = ntrack
       spectrometer_data.ninst  = ninst
       naverage_baseline=4
       ind_frames=where((cnr.data.time gt -11. and cnr.data.time lt 10) or cnr.data.time lt 0.0)
       nframe=n_elements(ind_frames)
       spectrometer_data.nframe_baseline = long(nframe/naverage_baseline)
       spectrometer_data.nframe = nframe
       cxf_define_spectrometer_data,spectrometer_data,err
       IF err NE '' THEN RETURN
       
; Load the structure
       
; Line-of-sight names
       spectrometer_data.LOS_name = cnr.header.LOS_name
       
; Puff crossing point
       spectrometer_data.R_pos   = cnr.header.maximum_r
       spectrometer_data.z_pos   = cnr.header.maximum_z
       spectrometer_data.phi_pos = cnr.header.maximum_phi
       
; Position of origin of the LOS
       spectrometer_data.R_orig   = cnr.header.R_opt
       spectrometer_data.z_orig   = cnr.header.z_opt
       spectrometer_data.phi_orig = cnr.header.phi_opt
       
; Reference wavelength [A] !!!
       spectrometer_data.wlength = cnr.header.wavel(255)*10.
       
; Slit width [microns]
       spectrometer_data.slit = cnr.header.SlitWid
       
; Time vector [s]
       spectrometer_data.time     = cnr.data(ind_frames).time
       spectrometer_data.exposure = $
          cnr.header.exptime + FLTARR(nframe)
       
; Calculate the wavelength intervals assuming the pixels are
; contiguous
       wav_vect  = cnr.header.cor_wavel
       dwav_vect = FLTARR(npixel,ntrack)
       dwav_vect[0,*] = wav_vect[1,*]-wav_vect[0,*]
       dwav_vect[1:npixel-2,*] = 0.5*(wav_vect[2:npixel-1,*] $
                                      -wav_vect[0:npixel-3,*])
       dwav_vect[npixel-1,*] = wav_vect[npixel-1,*] $
                               -wav_vect[npixel-2,*]
       spectrometer_data.wavelength.data      = wav_vect
; Uncertainty still to be defined!!!
       spectrometer_data.wavelength.error     = FLTARR(npixel,ntrack)
       spectrometer_data.wavelength.reference = ' '
       spectrometer_data.dispersion.data      = dwav_vect
; Uncertainty still to be defined!!!
       spectrometer_data.dispersion.error     = FLTARR(npixel,ntrack)
       spectrometer_data.dispersion.reference = ' '
       
; Spectral intensity. Requires conversion from counts/(texp-pixel)
; to ph/(m^2 sr s nm) using the returned background level and cal_mW
; which is in (mW/(m^2 sr A)) / (counts/s/pixel)
       
; Uncertainties are based on a Gaussian readout noise (from tests on
; dark signal) and a SQRT(photons) uncertainty on the net signal.
; Required inputs are the readout noise and the number of
; photons/count (also determined empirically)
       
; >>> Hard-code the two input parameters here.  This could later be
; >>> made into a detector- and shot-dependent table and, possibly,
; >>> handled internally by load_chr rather than here.
    ; get_err_bars,cmr,cmr_err
       
       n_bg_frames =  cnr.header.bg_range[1]-cnr.header.bg_range[0]+1
       FOR j=0,ntrack-1 DO BEGIN
          FOR k=0,nframe-1 DO BEGIN
             spectrometer_data.intensity.data[*,j,k] $
                = cnr.data[ind_frames(k)].intens_calib[*,j]
; Validity - check for saturation
             wh_sat = WHERE(cnr.data[ind_frames(k)].intens[*,j] GE $
                            2.^cnr.header.dynamic_range-1,nsat)
             IF nsat GT 0 THEN $
                spectrometer_data.intensity.validity[wh_sat,j,ind_frames(k)] = 2
          ENDFOR
       ENDFOR
       spectrometer_data.intensity.error $
          = cnr_err.err_bars(*,*,ind_frames)
       
       spectrometer_data.intensity.reference $
          = ' '
                                ;load active NBI Box 1 beams in order to get correct background subtraction     
       if cnr.header.shot gt 33740 then begin

          cxrs_puff_status, 'CNR', cnr.header.shot, spectrometer_data.time, $
                            spectrometer_data.exposure, $
                            puff, puff_new, puff_shotfile, err
          IF STRTRIM(err,2) NE '' THEN BEGIN
                                ; If the shot file is missing then
                                ; assume no gas puff (this is default in
                                ; cxf_define_spectrometer_data)!!!
             IF STRPOS(err,'shotfile does not exist') NE -1 THEN BEGIN
                spectrometer_data.beam = spectrometer_data.time*0.
                err = ''
             ENDIF ELSE BEGIN
                RETURN
             ENDELSE
          ENDIF ELSE BEGIN 
             puff_new = reform(puff_new,1,n_elements(puff_new))
          ENDELSE

          
          
          beam_CPR = 0
          cxrs_beam_status, 'CPR', cnr.header.shot, spectrometer_data.time, $
                            spectrometer_data.exposure, $
                            beam_CPR, NI_shotfile_CPR, err         
          
          beam_CPR = reform(beam_CPR,1,n_elements(beam_CPR))         
          IF STRTRIM(err,2) NE '' THEN BEGIN
             
             beam_CPR = spectrometer_data.time*0.
             err = ''
             
          ENDIF
          
          beam_CPR = reform(beam_CPR,1,n_elements(beam_CPR)) 
          
          cxrs_beam_status, 'CER', cnr.header.shot, spectrometer_data.time, $
                            spectrometer_data.exposure, $
                            beam_CMR, NI_shotfile_CMR, err              
          
          IF STRTRIM(err,2) NE '' THEN BEGIN
                                ; If the shot file is missing then assume no beams (this is default in
                ; cxf_define_spectrometer_data)!!!
                beam_CMR = spectrometer_data.time*0.
                err = ''

          ENDIF
          
          beam_CMR = reform(beam_CMR,1,n_elements(beam_CMR))
          
; load signal of Piezo valve Se 13
              
          cxrs_puff_status, 'CNR', cnr.header.shot, spectrometer_data.time, spectrometer_data.exposure, puff_name = 'S13VALVE', $
                            puff_Se13, puff_new_Se13, puff_shotfile_Se13, err_Se13
          IF STRTRIM(err,2) NE '' THEN BEGIN
                                ; If the shot file is missing then
                                ; assume no gas puff (this is default in
                                ; cxf_define_spectrometer_data)!!!
             IF STRPOS(err_Se13,'shotfile does not exist') NE -1 THEN BEGIN
                puff_new_Se13 = spectrometer_data.time*0.
                err_Se13 = ''
             ENDIF ELSE BEGIN
                RETURN
             ENDELSE
          ENDIF ELSE BEGIN
                                ; ignoring frames in beam modulation
                                ; where there is a change in NBI Box 1
                                ; beam power  
              
    ;index=where(beam_CMR_new eq 0.5 or beam_CPR_new eq 0.5)
    ;puff_new[index]=0.3            
              puff_new_Se13 = reform(puff_new_Se13,1,n_elements(puff_new_Se13))
           ENDELSE
           

           
; read ElM signal in order to remove elms from background subtraction           
           elm_exp='AUGD'

           if (cnr.header.shot eq 35977) or (cnr.header.shot eq 35976) or (cnr.header.shot eq 35966) or (cnr.header.shot eq 35965) then begin
              elm_exp='MCAVEDON'
           endif else begin
              elm_exp='AUGD'
           endelse

      
           read_signal_mrm, ier,cnr.header.shot,'ELM','t_endELM',telm_begin,telm_end,phys_dim,exp=elm_exp
           if ier eq 0 then begin
              
              
              print, ''
              print, 'Exclude ELMs from data'
              print, ''
              for t=0L, n_elements(telm_begin)-1 do begin
                 min_begin=min(abs(spectrometer_data.time-telm_begin[t]),location_begin)
                 min_end=min(abs(spectrometer_data.time-telm_end[t]),location_end)
                 if extraoptions.CNR.time_choice ne 1 then begin  
                    puff_new[location_begin:location_end]=0.7
  
                 endif ;else begin
                    ;if puff_new[location_begin] eq 0. then begin 
                     ;  puff_new[location_begin]=0.7
                   ; endif 
                    ;if puff_new[location_end] eq 0. then begin
                     ;  puff_new[location_end]=0.7
                    ;endif
                    
                ; endelse
              endfor
           endif else begin
              print, ''   
              print, 'ELM shotfile does not exist!'
              print, ''
           endelse    
           
           
           spectrometer_data.beam = puff
           spectrometer_data.modulation.flag=0
           beams=[puff_new,puff_new_Se13, beam_CPR,beam_CMR]
           names = ['GasPuffSe16', 'GasPuffSe13', 'CPRbeams', 'CMRbeams']
           *spectrometer_data.modulation.beampower=beams
           *spectrometer_data.modulation.beamlabels =names
           *spectrometer_data.modulation.orig_beam =spectrometer_data.beam 
           *spectrometer_data.modulation.orig_data =spectrometer_data.intensity.data
           *spectrometer_data.modulation.orig_error = spectrometer_data.intensity.error  
           
        endif
       
       
; Setting cos angle between LOS and B equal to 1 for CNR lines of sight and calculate it for CMR-3
    externalcostheta = fltarr(ntrack,nframe)+1.
    x_pos  = spectrometer_data.R_pos*cos(spectrometer_data.phi_pos/!RADEG)
    y_pos  = spectrometer_data.R_pos*sin(spectrometer_data.phi_pos/!RADEG)
    z_pos  = spectrometer_data.z_pos
    x_orig = spectrometer_data.R_orig*cos(spectrometer_data.phi_orig/!RADEG)
    y_orig = spectrometer_data.R_orig*sin(spectrometer_data.phi_orig/!RADEG)
    z_orig = spectrometer_data.z_orig
    l_LOS  = SQRT((x_pos-x_orig)^2+(y_pos-y_orig)^2+(z_pos-z_orig)^2)
    c_LOS  = 	[[(x_pos-x_orig)/l_LOS], $
                 [(y_pos-y_orig)/l_LOS], $
                 [(z_pos-z_orig)/l_LOS]]
      c_tor  = [[-y_pos/spectrometer_data.R_pos],[+x_pos/spectrometer_data.R_pos],[FLTARR(spectrometer_data.ntrack)]]
      costheta_tor = TOTAL(c_LOS*c_tor,2)
      for jlos=0,spectrometer_data.ntrack-1 do begin
         if STRMATCH(spectrometer_data.los_name[jlos],'CMR*') then begin
            externalcostheta[jlos,*] = costheta_tor[jlos]
         endif
         if STRMATCH(spectrometer_data.los_name[jlos],'CNR*') then begin
            externalcostheta[jlos,*] = 1.
         endif
      endfor
      spectrometer_data = create_struct(spectrometer_data,'externalcostheta',externalcostheta)
      
; Instrument function
      spectrometer_data.instfu.y0 = replicate(1.,ntrack)          ;cmr.header.det_inst.intens
      spectrometer_data.instfu.xw = cnr.header.inst_wid_nm*10.    ;cmr.header.det_inst.width * 10.0/(2d0*alog(2d0))
      spectrometer_data.instfu.xs = replicate(0.,ntrack)          ;cmr.header.det_inst.shift
      spectrometer_data.instfu.reference = ' '
      
   END                          ; processing for CNR      
    
      
      'CXH': BEGIN              ; Load structure CXH containing all the data

  
 cxh_err=1
if extraoptions.CXH.time_choice eq 2 then begin 
if shot lt 35000 then begin           
            
load_cxh_fvs,shot,cxh,err,/ip_on
special_flag=5
puff_diag='CXH'    

        
        if special_flag ne 1 and special_flag ne 2 and special_flag ne 3 and special_flag ne 4 and special_flag ne 5 and special_flag ne 8 then begin
        ; if special_flag eq 0 then begin
         
            correct_wvl_cxh,cxh
            get_err_bars_cxh,cxh,cxh_err

    ;      err = id + 'Coming soon! :-)'

        ; Call cxf_define_spectrometer_data with appropriate dimensions to
        ; complete structure definition
              npixel = cxh.header.xDim
              ntrack = cxh.header.yDim
              nframe = cxh.header.Nframes
              ninst  = cxh.header.nGaus
              spectrometer_data.npixel = npixel
              spectrometer_data.ntrack = ntrack
              spectrometer_data.ninst  = ninst
              ind_frames=where((cxh.data.time gt -11. and cxh.data.time lt 15) or cxh.data.time lt 0.0)
              nframe=n_elements(ind_frames)
              spectrometer_data.nframe = nframe
              cxf_define_spectrometer_data,spectrometer_data,err
              IF err NE '' THEN RETURN

            ; Load the structure

            ; Line-of-sight names
                  spectrometer_data.LOS_name = cxh.header.LOS_name

            ; Beam crossing point
                  spectrometer_data.R_pos   = cxh.header.maximum_r
                  spectrometer_data.z_pos   = cxh.header.maximum_z
                  spectrometer_data.phi_pos = cxh.header.maximum_phi

            ; Position of origin of the LOS
                  spectrometer_data.R_orig   = cxh.header.R_opt
                  spectrometer_data.z_orig   = cxh.header.z_opt
                  spectrometer_data.phi_orig = cxh.header.phi_opt
              ;    spectrometer_data.alpha_orig   = cxh.header.alpha_los
              ;    spectrometer_data.beta_orig = cxh.header.beta_los

            ; Reference wavelength [A] !!!
                  spectrometer_data.wlength = cxh.header.wavel(255)*10.

            ; Slit width [microns]
                  spectrometer_data.slit = cxh.header.SlitWid

            ; Time vector [s]
                  spectrometer_data.time     = cxh.data(ind_frames).time
                  STOP
                  spectrometer_data.exposure = $
                    cxh.header.exptime + FLTARR(nframe)

            ; Calculate the wavelength intervals assuming the pixels are
            ; contiguous
                  wav_vect  = cxh.header.cor_wavel
                  wav_vect  = reverse(wav_vect)
                  dwav_vect = FLTARR(npixel,ntrack)
                  dwav_vect[0,*] = wav_vect[1,*]-wav_vect[0,*]
                  dwav_vect[1:npixel-2,*] = 0.5*(wav_vect[2:npixel-1,*] $
                                                   -wav_vect[0:npixel-3,*])
                  dwav_vect[npixel-1,*] = wav_vect[npixel-1,*] $
                                            -wav_vect[npixel-2,*]
                  spectrometer_data.wavelength.data      = wav_vect
            ; Uncertainty still to be defined!!!
                  spectrometer_data.wavelength.error     = FLTARR(npixel,ntrack)
                  spectrometer_data.wavelength.reference = ' '
                  spectrometer_data.dispersion.data      = dwav_vect
            ; Uncertainty still to be defined!!!
                  spectrometer_data.dispersion.error     = FLTARR(npixel,ntrack)
                  spectrometer_data.dispersion.reference = ' '
            ; Spectral intensity. Requires conversion from counts/(texp-pixel)
            ; to ph/(m^2 sr s nm) using the returned background level and cal_mW
            ; which is in (mW/(m^2 sr A)) / (counts/s/pixel)

            ; Uncertainties are based on a Gaussian readout noise (from tests on
            ; dark signal) and a SQRT(photons) uncertainty on the net signal.
            ; Required inputs are the readout noise and the number of
            ; photons/count (also determined empirically)

            ; >>> Hard-code the two input parameters here.  This could later be
            ; >>> made into a detector- and shot-dependent table and, possibly,
            ; >>> handled internally by load_chr rather than here.
               ; get_err_bars,cmr,cmr_err

               ;     if puff_diag eq 'CXH_PUFF' then const_puff = 1d17 else const_puff=0.     
               ; doesn't work.... not sure why
               ;use constant = 0:
               const_puff = 1d17
                  n_bg_frames =  cxh.header.bg_range[1]-cxh.header.bg_range[0]+1
                  FOR j=0,ntrack-1 DO BEGIN
                    FOR k=0,nframe-1 DO BEGIN
                      spectrometer_data.intensity.data[*,j,k] $
                        = cxh.data[ind_frames(k)].intens_calib[*,j]+const_puff
            ; Validity - check for saturation
                      wh_sat = WHERE(cxh.data[ind_frames(k)].intens[*,j] GE 2.^cxh.header.dynamic_range-1,nsat)
                      IF nsat GT 0 THEN $
                        spectrometer_data.intensity.validity[wh_sat,j,ind_frames(k)] = 2
                    ENDFOR
                  ENDFOR
                     spectrometer_data.intensity.data =reverse(spectrometer_data.intensity.data)              
                     spectrometer_data.intensity.error $
                        = cxh_err.err_bars(*,*,ind_frames)
                     spectrometer_data.intensity.error=reverse(spectrometer_data.intensity.error)

                  spectrometer_data.intensity.reference $
                    = ' '

            ; Instrument function
                   gauss_intens = fltarr(n_elements(cxh.header.inst_wid_nm(0,*,0)),n_elements(cxh.header.inst_wid_nm(0,0,*)))
                   gauss_widths = fltarr(n_elements(cxh.header.inst_wid_nm(1,*,0)),n_elements(cxh.header.inst_wid_nm(1,0,*)))


                  FOR j=0,ntrack-1 DO BEGIN

                        gauss_intens(*,j) = cxh.header.inst_wid_nm(0,*,j)/max(cxh.header.inst_wid_nm(0,*,j))
                        gauss_widths(*,j) = cxh.header.inst_wid_nm(1,*,j)

                  ENDFOR

                  spectrometer_data.instfu.y0 = gauss_intens(*,*)  ;cxh.header.det_inst.intens
                  spectrometer_data.instfu.xw = gauss_widths(*,*)*10. ;cxh.header.det_inst.width * 10.0/(2d0*alog(2d0))
                  spectrometer_data.instfu.xs = replicate(0.,ninst,ntrack) ;cxh.header.det_inst.shift
                  spectrometer_data.instfu.reference = ' '



        endif else begin
        ;somehow this fails when reading in 27149, maybe it was connected to missing Dalpha 'if' statement in line 994? leave it now as it is, shouldn't change anything in loading routine (03/06/2013 elv)
         ; if special_flag eq 1 or special_flag eq 2 or special_flag eq 3 or special_flag eq 4 then begin 
          if special_flag ne 5 and special_flag ne 8 then begin       
        
            !P.multi=[0.,4.,2.]
            cor_wavel=cxh.header.cor_wavel
            data_corr_tmp=cxh.data.intens
            data_corr=cxh.data.intens
            data_corr_err=cxh.data.intenserr
            core_los = -1l
            ;add Dalpha wvl to be able to read in CER shotfile
            if (cxh.header.cor_wavel(256,8) gt 525.0 and cxh.header.cor_wavel(256,8) lt 530.0) or $
               (cxh.header.cor_wavel(256,8) gt 655. and cxh.header.cor_wavel(256,8) lt 658.0) or $            
               (cxh.header.cor_wavel(256,8) gt 494.0 and cxh.header.cor_wavel(256,8) lt 496.0) then begin

                reference_offset = -0.0501 ; offset to v3 start time for background weight frames
                valid_start = -0.0 ; offset to v3 start time for valid frames
                valid_end = 0.1 ; offset to v3 end time for valid frames
                ; end of definition
                cxh_tmp=cxh
                timep = cxh_tmp.data[*].time
                get_ventil3,shot,v3,err
                if v3.active eq 1 then begin
	            dummy = min(abs(v3.ventil_time[0]+reference_offset - timep),weight_ind)
	            time_slice_indecies_coresub=where(timep lt 0.21 and timep gt 0.1)
	            time_slice_indecies=where(timep lt 0.21 or (timep ge (v3.ventil_time[0]+valid_start) and timep le (v3.mano_time[1]+valid_end)))
	            time_slice_indecies_puff_on=where(timep(time_slice_indecies) ge (v3.ventil_time[0]+valid_start) and timep(time_slice_indecies) le (v3.ventil_time[1]+valid_end))
	            ;time_slice_indecies=[time_slice_indecies]
	            ;remove_background=1
	            ; find corresponding los

	            for i = 0,cxh_tmp.header.nroi-1 do begin
	                comp =  strsplit(cxh_tmp.header.los_name[i],'-',/EXTRACT)
                        if strcompress(cxh_tmp.header.los_name[i],/remove_all) ne 'SPECIAL' then begin
	                    if comp[1] eq 'S' then begin
                                if cxh_tmp.header.los_name[i] eq 'CXH-S-01' then core_los = i
		                for j = 0,cxh_tmp.header.nroi-1 do begin
		                    tocomp =  strsplit(cxh_tmp.header.los_name[j],'-',/EXTRACT)

                                    if n_elements(tocomp) eq 3 then begin 
		                        ;if (tocomp[1] eq 'B' and comp[2] eq tocomp[2]) or (tocomp[1] eq 'B' and comp[2] eq 8 and tocomp[2] eq 7) or (tocomp[1] eq 'B' and comp[2] eq 1 and tocomp[2] eq 2) then begin
		                        if (tocomp[1] eq 'B' and comp[2] eq tocomp[2]) then begin
			                    print, 'Matching pair ', cxh_tmp.header.los_name[i],' and ', cxh_tmp.header.los_name[j]
			                    if n_elements(pairs) eq 0 then pairs = [[i],[j]] $
			                    else pairs = [pairs,[[i],[j]]]
		                        endif
                                    endif

		                endfor
	                    endif
                        endif
	            endfor

	            sz = size(pairs,/dimension)
	            weights = fltarr(sz[0])
	            weights_alternative = fltarr(sz[0])
	            shft = fltarr(sz[0])

                    if special_flag eq 2 or special_flag eq 3 then begin

	                weights_coresub = fltarr(sz[0]*2.)
	                weights_alternative_coresub = fltarr(sz[0]*2.)
	                shft_coresub = fltarr(sz[0]*2.)
                    
                    endif

	            ;middles = [529.5,495.0,656.9,468.9]
	            middles = [526.5,495.0,656.9,468.9,418.,567.0]
	            fit_ranges = [[528.0, 530.0],[494.25,496.0],[655.0, 657.2],[466.00, 469.0],[417.00, 419.0],[566.8, 567.3]]
                ;				height		centre		width		offset
	            fit_inits = [	[	1.0e17,		529.1,		0.1,		0.5e17], $
			                [	3.0e17,		494.5,		0.1,		1.0e17], $
			                [	1.0e19,		656.1,		0.1,		2.0e17], $
			                [	6.0e17,		468.6,		0.1,		0.5e17], $
			                [	1.0e17,		417.6,		0.1,		0.5e17], $
		                        [	6.0e17,		566.95,		0.1,		0.5e17]]

	            middles_special = [526.5,495.0,656.9,468.9,418.,567.0]
	            fit_ranges_special = [[528.0, 530.0],[493.8,494.25],[655.0, 657.2],[466.00, 469.0],[417.00, 419.0],[566.8, 567.3]]
                ;				height		centre		width		offset
	            fit_inits_special = [[	1.0e17,		529.1,		0.1,		0.5e17], $
			                [	3.0e17,		494.03,		0.1,		1.0e17], $
			                [	1.0e19,		656.1,		0.1,		2.0e17], $
			                [	6.0e17,		468.6,		0.1,		0.5e17], $
			                [	1.0e17,		417.6,		0.1,		0.5e17], $
		                        [	6.0e17,		566.95,		0.1,		0.5e17]]
	            center = cxh_tmp.header.wav_mid
	            index = where(abs(middles - center) lt 1.0)

	            fit_range  = fit_ranges[*,index]
	            fit_range_special  = fit_ranges_special[*,index]
	            fit_init = fit_inits[*,index]
	            fit_init_special = fit_inits_special[*,index]
	            naverage = 25

                    if (special_flag eq 2l and core_los ne -1) or (special_flag eq 3l and core_los ne -1) then begin
                    
	                for i = 0, sz[0]*2.0-1 do begin
                            los2 = core_los
                            los1 = pairs(i)
                    
                        print,los1,los2
                        wvls=cor_wavel[*,los1]
	                nterms = 4
	                fit_inds = where(wvls ge fit_range_special[0] AND wvls le fit_range_special[1])

	                ; naverage frames starting from gas puff
	                ;av_inds = time_slice_indecies_coresub
	                av_inds = where(timep gt 0.1 and timep lt 0.15)
                        ; naverage frames centered around gas puff start
	                ;av_inds = indgen(naverage)-floor(naverage/2)+weight_ind

	                xs = cor_wavel[fit_inds,los1]
	                xb = cor_wavel[fit_inds,los2]

	                ys = total(data_corr_tmp[fit_inds,los1,av_inds],3)/naverage
	                yb = total(data_corr_tmp[fit_inds,los2,av_inds],3)/naverage

                        plot, xs,ys,thick=2,/psym
                        oplot,xb,yb,thick=2,/psym,color=255

	                normalize = 10.0^floor(alog10(max(ys)))
	                init = fit_init_special
	                init[0] = init[0]/normalize
	                init[3] = init[3]/normalize

	                ysfit = GAUSSFIT(xs, ys/normalize, s_coeff, NTERMS=nterms, ESTIMATES=init)
	                ybfit = GAUSSFIT(xb, yb/normalize, b_coeff, NTERMS=nterms, ESTIMATES=init)
	                weights_coresub[i] = s_coeff[0]/b_coeff[0]
	                shft_coresub[i]    = s_coeff[1]-b_coeff[1]
                        oplot,xs,ysfit*normalize
                        oplot,xb,ybfit*normalize,color=255

	                weights_alternative_coresub[i] = mean(median(ys,11))/mean(median(yb,11))

                        ;dummy=' '
                        ;read,dummy
	                    for i_time = max(where(timep(time_slice_indecies) lt 0.21)), n_elements(time_slice_indecies)-1 do begin

		                ;data_corr[*,los1,i_time] = data_corr[*,los1,i_time] - weights[i]*data_corr[*,los2,i_time]
		                ; do an interpolation from xb to (xs - shft)
		                ;ybi = interpol(data_corr[*,los2,i_time],cor_wavel[*,los2],(cor_wavel[*,los1]-shft[i]))

		                ybi = interpol(reform(data_corr[*,los2,time_slice_indecies(i_time)]),reform(cor_wavel[*,los2]+shft_coresub(i)),reform((cor_wavel[*,los1])))
;                                data_corr[*,los1,time_slice_indecies(i_time)] = reform(data_corr[*,los1,time_slice_indecies(i_time)]) - reform(weights_coresub[i]*ybi)
                                data_corr[*,los1,time_slice_indecies(i_time)] = reform(data_corr[*,los1,time_slice_indecies(i_time)]) - reform(weights_coresub[i]*ybi)
                                ;data_corr[*,los1,time_slice_indecies(i_time)] = reform(data_corr[*,los1,time_slice_indecies(i_time)]) - reform(ybi)
                                data_corr_err[*,los1,time_slice_indecies(i_time)] = sqrt(reform(data_corr_err[*,los1,time_slice_indecies(i_time)])^2+ reform(data_corr_err[*,los2,time_slice_indecies(i_time)]*weights_coresub[i])^2)

	                    endfor
	                endfor
                  print, 'Core_Weights: ', weights_coresub
                  print, 'Core_Shifts ', shft_coresub
                  print, ' '
                  print, 'Core_Alternative Weights: ', weights_alternative_coresub
                    
                    endif 
                    if special_flag eq 1 or special_flag eq 3 or special_flag eq 4 then begin
                    
                        data_corr_tmp=data_corr

	                for i = 0, sz[0]-1 do begin

	                    los1 = pairs[i,0]
	                    los2 = pairs[i,1]

                            print,los1,los2
                            wvls=cor_wavel[*,los1]
	                    nterms = 4
	                    fit_inds = where(wvls ge fit_range[0] AND wvls le fit_range[1])

	                    ; naverage frames starting from gas puff
	                    av_inds = indgen(naverage)+weight_ind;-naverage/2
	                    ; naverage frames centered around gas puff start
	                    ;av_inds = indgen(naverage)-floor(naverage/2)+weight_ind

	                    xs = cor_wavel[fit_inds,los1]
	                    xb = cor_wavel[fit_inds,los2]

	                    ys = total(data_corr_tmp[fit_inds,los1,av_inds],3)/naverage
	                    yb = total(data_corr_tmp[fit_inds,los2,av_inds],3)/naverage

                            plot, xs,ys,thick=2,/psym
                            oplot,xb,yb,thick=2,/psym,color=255

	                    normalize = 10.0^floor(alog10(max(ys)))
	                    init = fit_init
	                    init[0] = init[0]/normalize
	                    init[3] = init[3]/normalize

	                    ysfit = GAUSSFIT(xs, ys/normalize, s_coeff, NTERMS=nterms, ESTIMATES=init)
	                    ybfit = GAUSSFIT(xb, yb/normalize, b_coeff, NTERMS=nterms, ESTIMATES=init)
	                    weights[i] = s_coeff[0]/b_coeff[0]
	                    shft[i]    = s_coeff[1]-b_coeff[1]
                            oplot,xs,ysfit*normalize
                            oplot,xb,ybfit*normalize,color=255

	                    weights_alternative[i] = mean(median(ys,11))/mean(median(yb,11))


	                    for i_time = max(where(timep(time_slice_indecies) lt 0.21)), n_elements(time_slice_indecies)-1 do begin
		                ;data_corr[*,los1,i_time] = data_corr[*,los1,i_time] - weights[i]*data_corr[*,los2,i_time]
		                ; do an interpolation from xb to (xs - shft)
		                ;ybi = interpol(data_corr[*,los2,i_time],cor_wavel[*,los2],(cor_wavel[*,los1]-shft[i]))

		                ybi = interpol(reform(data_corr[*,los2,time_slice_indecies(i_time)]),reform(cor_wavel[*,los2]+shft(i)),reform((cor_wavel[*,los1])))
		                ;ybi = interpol(reform(data_corr[*,los2,time_slice_indecies(i_time)]),reform(cor_wavel[*,los2]),reform((cor_wavel[*,los1])))
                               ; plot,reform(data_corr[*,los1,i_time]),xr=[300,450],title='time='+string(float(timep(time_slice_indecies(i_time))))
                               ;data_corr[*,los1,time_slice_indecies(i_time)] = reform(data_corr[*,los1,time_slice_indecies(i_time)]) - reform(weights_alternative[i]*ybi)
                                if special_flag ne 4l then begin
                                    data_corr[*,los1,time_slice_indecies(i_time)] = reform(data_corr[*,los1,time_slice_indecies(i_time)]) - reform(weights[i]*ybi*0.9)
                                   ;data_corr_err[*,los1,time_slice_indecies(i_time)] = sqrt(reform(data_corr_err[*,los1,time_slice_indecies(i_time)])^2+ reform(data_corr_err[*,los2,time_slice_indecies(i_time)]*weights_alternative[i])^2)
                                    data_corr_err[*,los1,time_slice_indecies(i_time)] = sqrt(reform(data_corr_err[*,los1,time_slice_indecies(i_time)])^2+ reform(data_corr_err[*,los2,time_slice_indecies(i_time)]*weights[i])^2)
                                endif
                               ; oplot,reform(weights_alternative[i]*ybi),color=255
                               ; oplot,data_corr[*,los1,i_time],color=128
                            dummy=' '
                            ;read,dummy
	                    endfor
                            ;dummy=' '
                            read,dummy


	                endfor
                      print, 'Weights: ', weights
                      print, 'Shifts ', shft
                      print, ' '
                      print, 'Alternative Weights: ', weights_alternative

                  endif


                  if not (n_elements(pairs[*,0]) lt 2) then begin
                    los_map = pairs[*,0]
                    NumROI = n_elements(los_map)
                    yDim = n_elements(los_map)
                    data=cxh.data.intens*0.
                    data_err=cxh.data.intenserr*0.
                    data[*,[los_map],time_slice_indecies] = data_corr[*,[los_map],time_slice_indecies] ;+1d17
                    data_err[*,[los_map],time_slice_indecies] = data_corr_err[*,[los_map],time_slice_indecies] ;+1d17

                    cxh.data.intens = data  +1d17
                    cxh.data.intenserr = data_err ; +1d17

                    ; Call cxf_define_spectrometer_data with appropriate dimensions to
                    ; complete structure definition
                          npixel = cxh.header.xDim
                          ntrack = n_elements(los_map)
                          nframe = cxh.header.Nframes
                          ninst  = 1
                          spectrometer_data.npixel = npixel
                          spectrometer_data.ntrack = n_elements(los_map)
                          spectrometer_data.ninst  = ninst
                          ind_frames=where((cxh.data.time gt -11. and cxh.data.time lt 15) or cxh.data.time lt 0.0)
                          nframe=n_elements(ind_frames)
                          spectrometer_data.nframe = nframe
                          cxf_define_spectrometer_data,spectrometer_data,err
                          IF err NE '' THEN RETURN
                        ; Load the structure
                            spectrometer_data.intensity.data=cxh.data.intens[*,los_map,*]
                            spectrometer_data.intensity.error=cxh.data.intenserr[*,los_map,*]

                          spectrometer_data.intensity.reference $
                            = ' '

                        ; Line-of-sight names
                              spectrometer_data.LOS_name = cxh.header.LOS_name[los_map]

                        ; Beam crossing point
                              spectrometer_data.R_pos   = cxh.header.r_pos[los_map]
                              spectrometer_data.z_pos   = cxh.header.z_pos[los_map]
                              spectrometer_data.phi_pos = cxh.header.phi_pos[los_map]

                        ; Position of origin of the LOS
                              spectrometer_data.R_orig   = cxh.header.R_opt[los_map]
                              spectrometer_data.z_orig   = cxh.header.z_opt[los_map]
                              spectrometer_data.phi_orig = cxh.header.phi_opt[los_map]
                          ;    spectrometer_data.alpha_orig   = cxh.header.alpha_los
                          ;    spectrometer_data.beta_orig = cxh.header.beta_los

                        ; Reference wavelength [A] !!!
                              spectrometer_data.wlength = cxh.header.wav_mid*10.

                        ; Slit width [microns]
                              spectrometer_data.slit = cxh.header.SlitWid

                        ; Time vector [s]
                              spectrometer_data.time     = cxh.data(ind_frames).time
                              stop
                              spectrometer_data.exposure = $
                                cxh.header.exptime + FLTARR(nframe)

                        ; Calculate the wavelength intervals assuming the pixels are
                        ; contiguous
                              wav_vect  = cxh.header.cor_wavel[*,los_map]
                              dwav_vect = FLTARR(npixel,ntrack)
                              dwav_vect[0,*] = wav_vect[1,los_map]-wav_vect[0,los_map]
                              dwav_vect[1:npixel-2,*] = 0.5*(wav_vect[2:npixel-1,los_map] $
                                                               -wav_vect[0:npixel-3,los_map])
                              dwav_vect[npixel-1,*] = wav_vect[npixel-1,los_map] $
                                                        -wav_vect[npixel-2,los_map]
                              spectrometer_data.wavelength.data      = wav_vect
                        ; Uncertainty still to be defined!!!
                              spectrometer_data.wavelength.error     = FLTARR(npixel,ntrack)
                              spectrometer_data.wavelength.reference = ' '
                              spectrometer_data.dispersion.data      = dwav_vect
                        ; Uncertainty still to be defined!!!
                              spectrometer_data.dispersion.error     = FLTARR(npixel,ntrack)
                              spectrometer_data.dispersion.reference = ' '

                        ; Instrument function
                              spectrometer_data.instfu.y0 = replicate(1.,ntrack);cer.header.det_inst.intens
                              spectrometer_data.instfu.xw = cxh.header.inst_wid_nm[los_map]*10. ;cer.header.det_inst.width * 10.0/(2d0*alog(2d0))
                              spectrometer_data.instfu.xs = replicate(0.,ntrack);cer.header.det_inst.shift
                              spectrometer_data.instfu.reference = ' '

                            !P.multi=[0.,1.,1.]

                  endif else begin
                    los_map = indgen(NumROI)
                  endelse
                endif else begin
	            print, ''
	            print, 'Ventil3 inactive, falling back to full read!'
	            cxpuff = 0
                endelse


            endif

          endif else begin
            ; now process for special_flag eq 5 or special_flag eq 6 or special_flag eq 7 or special_flag eq 8 
            ;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
            ; test for toroidal HFS system on FVS spectrometer --- elv 16/03/2012
            get_err_bars_cxh_fvs,cxh,cxh_err
            
            !P.multi=[0.,4.,2.]
            cor_wavel=cxh.header.cor_wavel
            data_corr_tmp=cxh.data[*].intens_calib(*,*)
            data_corr=cxh.data[*].intens_calib(*,*)
            data_corr_err=cxh_err.err_bars ;(*,*,ind_frames)   cmr_err.err_bars(*,*,ind_frames)   ;cvh.data.intenserr
            core_los = -1l   ;this will be applied to poloidal HFS-LOS instead of CER-LOS --> both HFS systems are stored in FVS shotfile!
            if (cxh.header.cor_wavel(256,8) gt 525.0 and cxh.header.cor_wavel(256,8) lt 530.0) or $
               (cxh.header.cor_wavel(256,8) gt 493.5 and cxh.header.cor_wavel(256,8) lt 496.0) or $
               (cxh.header.cor_wavel(256,8) gt 655. and cxh.header.cor_wavel(256,8) lt 658.0) or $
               (cxh.header.cor_wavel(256,8) gt 565.5 and cxh.header.cor_wavel(256,8) lt 567.5) or $
               (cxh.header.cor_wavel(256,8) gt 467.5 and cxh.header.cor_wavel(256,8) lt 469.5) then begin


                reference_offset = -0.0501 ; offset to v3 start time for background weight frames
                valid_start = -0.0 ; offset to v3 start time for valid frames
                valid_end = 0.1 ; offset to v3 end time for valid frames
                ; end of definition
                cxh_tmp=cxh
                timep = cxh_tmp.data[*].time
                get_ventil3,shot,v3,err
                if v3.active eq 1 then begin
	            dummy = min(abs(v3.ventil_time[0]+reference_offset - timep),weight_ind)
	            time_slice_indecies_coresub=where(timep lt 0.21 and timep gt 0.1)
	            time_slice_indecies=where(timep lt 0.21 or (timep ge (v3.ventil_time[0]+valid_start) and timep le (v3.mano_time[1]+valid_end)))
	            time_slice_indecies_puff_on=where(timep(time_slice_indecies) ge (v3.ventil_time[0]+valid_start) and timep(time_slice_indecies) le (v3.ventil_time[1]+valid_end))
	            ;time_slice_indecies=[time_slice_indecies]
	            ;remove_background=1
	            ; find corresponding los

	            for i = 0,cxh_tmp.header.nroi-1 do begin
	                comp =  strsplit(cxh_tmp.header.los_name[i],'-',/EXTRACT)
                        if strcompress(cxh_tmp.header.los_name[i],/remove_all) ne 'SPECIAL' and strcompress(cxh_tmp.header.los_name[i],/remove_all) ne 'BACKGRND' then begin
	                    if comp[1] eq 'S' and comp[0] ne 'CVH' then begin
                                if cxh_tmp.header.los_name[i] eq 'CXH-S-01' then core_los = i
		                for j = 0,cxh_tmp.header.nroi-1 do begin
		                    tocomp =  strsplit(cxh_tmp.header.los_name[j],'-',/EXTRACT)

                                    if n_elements(tocomp) eq 3 then begin 
		                        ;if (tocomp[1] eq 'B' and comp[2] eq tocomp[2]) or (tocomp[1] eq 'B' and comp[2] eq 8 and tocomp[2] eq 7) or (tocomp[1] eq 'B' and comp[2] eq 1 and tocomp[2] eq 2) then begin
		                        if (tocomp[1] eq 'B' and tocomp[0] ne 'CVH' and comp[2] eq tocomp[2]) then begin
			                    print, 'Matching pair ', cxh_tmp.header.los_name[i],' and ', cxh_tmp.header.los_name[j]
			                    if n_elements(pairs) eq 0 then pairs = [[i],[j]] $
			                    else pairs = [pairs,[[i],[j]]]
		                        endif
                                    endif

		                endfor
	                    endif
                        endif
	            endfor

	            sz = size(pairs,/dimension)
	            weights = fltarr(sz[0])
	            weights_alternative = fltarr(sz[0])
	            shft = fltarr(sz[0])

                    if special_flag eq 6 or special_flag eq 7 then begin

	                weights_coresub = fltarr(sz[0]*2.)
	                weights_alternative_coresub = fltarr(sz[0]*2.)
	                shft_coresub = fltarr(sz[0]*2.)
                    
                    endif

	            ;middles = [529.5,495.0,656.9,468.9]
	            middles = [526.5,495.0,656.9,468.9,418.,567.0]
	            fit_ranges = [[528.0, 530.0],[494.25,496.0],[655.0, 657.2],[466.00, 469.0],[417.00, 419.0],[566.8, 567.3]]
                ;				height		centre		width		offset
	            fit_inits = [	[	1.0e17,		529.1,		0.1,		0.5e17], $
			                [	3.0e17,		494.5,		0.1,		1.0e17], $
			                [	1.0e19,		656.1,		0.1,		2.0e17], $
			                [	6.0e17,		468.6,		0.1,		0.5e17], $
			                [	1.0e17,		417.6,		0.1,		0.5e17], $
		                        [	6.0e17,		566.95,		0.1,		0.5e17]]

	            middles_special = [526.5,495.0,656.9,468.9,418.,567.0]
	            fit_ranges_special = [[528.0, 530.0],[493.8,494.25],[655.0, 657.2],[466.00, 469.0],[417.00, 419.0],[566.8, 567.3]]
                ;				height		centre		width		offset
	            fit_inits_special = [[	1.0e17,		529.1,		0.1,		0.5e17], $
			                [	3.0e17,		494.03,		0.1,		1.0e17], $
			                [	1.0e19,		656.1,		0.1,		2.0e17], $
			                [	6.0e17,		468.6,		0.1,		0.5e17], $
			                [	1.0e17,		417.6,		0.1,		0.5e17], $
		                        [	6.0e17,		566.95,		0.1,		0.5e17]]
	            center = cxh_tmp.header.wav_mid
	            index = where(abs(middles - center) lt 1.0)

	            fit_range  = fit_ranges[*,index]
	            fit_range_special  = fit_ranges_special[*,index]
	            fit_init = fit_inits[*,index]
	            fit_init_special = fit_inits_special[*,index]
	            naverage = 25

                    if (special_flag eq 6l and core_los ne -1) or (special_flag eq 7l and core_los ne -1) then begin
                    
	                for i = 0, sz[0]*2.0-1 do begin
                            los2 = core_los
                            los1 = pairs(i)
                    
                        print,los1,los2
                        wvls=cor_wavel[*,los1]
	                nterms = 4
	                fit_inds = where(wvls ge fit_range_special[0] AND wvls le fit_range_special[1])

	                ; naverage frames starting from gas puff
	                ;av_inds = time_slice_indecies_coresub
	                av_inds = where(timep gt 0.1 and timep lt 0.15)
                        ; naverage frames centered around gas puff start
	                ;av_inds = indgen(naverage)-floor(naverage/2)+weight_ind

	                xs = cor_wavel[fit_inds,los1]
	                xb = cor_wavel[fit_inds,los2]

	                ys = total(data_corr_tmp[fit_inds,los1,av_inds],3)/naverage
	                yb = total(data_corr_tmp[fit_inds,los2,av_inds],3)/naverage

                        plot, xs,ys,thick=2,/psym
                        oplot,xb,yb,thick=2,/psym,color=255

	                normalize = 10.0^floor(alog10(max(ys)))
	                init = fit_init_special
	                init[0] = init[0]/normalize
	                init[3] = init[3]/normalize

	                ysfit = GAUSSFIT(xs, ys/normalize, s_coeff, NTERMS=nterms, ESTIMATES=init)
	                ybfit = GAUSSFIT(xb, yb/normalize, b_coeff, NTERMS=nterms, ESTIMATES=init)
	                weights_coresub[i] = s_coeff[0]/b_coeff[0]
	                shft_coresub[i]    = s_coeff[1]-b_coeff[1]
                        oplot,xs,ysfit*normalize
                        oplot,xb,ybfit*normalize,color=255

	                weights_alternative_coresub[i] = mean(median(ys,11))/mean(median(yb,11))

                        ;dummy=' '
                        ;read,dummy
	                    for i_time = max(where(timep(time_slice_indecies) lt 0.21)), n_elements(time_slice_indecies)-1 do begin

		                ;data_corr[*,los1,i_time] = data_corr[*,los1,i_time] - weights[i]*data_corr[*,los2,i_time]
		                ; do an interpolation from xb to (xs - shft)
		                ;ybi = interpol(data_corr[*,los2,i_time],cor_wavel[*,los2],(cor_wavel[*,los1]-shft[i]))

		                ybi = interpol(reform(data_corr[*,los2,time_slice_indecies(i_time)]),reform(cor_wavel[*,los2]+shft_coresub(i)),reform((cor_wavel[*,los1])))
;                                data_corr[*,los1,time_slice_indecies(i_time)] = reform(data_corr[*,los1,time_slice_indecies(i_time)]) - reform(weights_coresub[i]*ybi)
                                data_corr[*,los1,time_slice_indecies(i_time)] = reform(data_corr[*,los1,time_slice_indecies(i_time)]) - reform(weights_coresub[i]*ybi)
                                ;data_corr[*,los1,time_slice_indecies(i_time)] = reform(data_corr[*,los1,time_slice_indecies(i_time)]) - reform(ybi)
                                data_corr_err[*,los1,time_slice_indecies(i_time)] = sqrt(reform(data_corr_err[*,los1,time_slice_indecies(i_time)])^2+ reform(data_corr_err[*,los2,time_slice_indecies(i_time)]*weights_coresub[i])^2)

	                    endfor
	                endfor
                  print, 'Core_Weights: ', weights_coresub
                  print, 'Core_Shifts ', shft_coresub
                  print, ' '
                  print, 'Core_Alternative Weights: ', weights_alternative_coresub
                    
                    endif 
                    if special_flag eq 5 or special_flag eq 7 or special_flag eq 8 then begin
                    
                        data_corr_tmp=data_corr

	                for i = 0, sz[0]-1 do begin

	                    los1 = pairs[i,0]
	                    los2 = pairs[i,1]

                            print,los1,los2
                            wvls=cor_wavel[*,los1]
	                    nterms = 4
	                    fit_inds = where(wvls ge fit_range[0] AND wvls le fit_range[1])

	                    ; naverage frames starting from gas puff
	                    av_inds = indgen(naverage)+weight_ind;-naverage/2
	                    ; naverage frames centered around gas puff start
	                    ;av_inds = indgen(naverage)-floor(naverage/2)+weight_ind

	                    xs = cor_wavel[fit_inds,los1]
	                    xb = cor_wavel[fit_inds,los2]

	                    ys = total(data_corr_tmp[fit_inds,los1,av_inds],3)/naverage
	                    yb = total(data_corr_tmp[fit_inds,los2,av_inds],3)/naverage

                            plot, xs,ys,thick=2,/psym
                            oplot,xb,yb,thick=2,/psym,color=255

	                    normalize = 10.0^floor(alog10(max(ys)))
	                    init = fit_init
	                    init[0] = init[0]/normalize
	                    init[3] = init[3]/normalize

	                    ysfit = GAUSSFIT(xs, ys/normalize, s_coeff, NTERMS=nterms, ESTIMATES=init)
	                    ybfit = GAUSSFIT(xb, yb/normalize, b_coeff, NTERMS=nterms, ESTIMATES=init)
	                    weights[i] = s_coeff[0]/b_coeff[0]
	                    shft[i]    = s_coeff[1]-b_coeff[1]
                            oplot,xs,ysfit*normalize
                            oplot,xb,ybfit*normalize,color=255

	                    weights_alternative[i] = mean(median(ys,11))/mean(median(yb,11))


	                    for i_time = max(where(timep(time_slice_indecies) lt 0.21)), n_elements(time_slice_indecies)-1 do begin
		                ;data_corr[*,los1,i_time] = data_corr[*,los1,i_time] - weights[i]*data_corr[*,los2,i_time]
		                ; do an interpolation from xb to (xs - shft)
		                ;ybi = interpol(data_corr[*,los2,i_time],cor_wavel[*,los2],(cor_wavel[*,los1]-shft[i]))

		                ybi = interpol(reform(data_corr[*,los2,time_slice_indecies(i_time)]),reform(cor_wavel[*,los2]+shft(i)),reform((cor_wavel[*,los1])))
		                ;ybi = interpol(reform(data_corr[*,los2,time_slice_indecies(i_time)]),reform(cor_wavel[*,los2]),reform((cor_wavel[*,los1])))
                               ; plot,reform(data_corr[*,los1,i_time]),xr=[300,450],title='time='+string(float(timep(time_slice_indecies(i_time))))
                               ;data_corr[*,los1,time_slice_indecies(i_time)] = reform(data_corr[*,los1,time_slice_indecies(i_time)]) - reform(weights_alternative[i]*ybi)
                                if special_flag ne 8l then begin
                                    data_corr[*,los1,time_slice_indecies(i_time)] = reform(data_corr[*,los1,time_slice_indecies(i_time)]) - reform(weights[i]*ybi*0.9)
                                   ;data_corr_err[*,los1,time_slice_indecies(i_time)] = sqrt(reform(data_corr_err[*,los1,time_slice_indecies(i_time)])^2+ reform(data_corr_err[*,los2,time_slice_indecies(i_time)]*weights_alternative[i])^2)
                                    data_corr_err[*,los1,time_slice_indecies(i_time)] = sqrt(reform(data_corr_err[*,los1,time_slice_indecies(i_time)])^2+ reform(data_corr_err[*,los2,time_slice_indecies(i_time)]*weights[i])^2)
                                endif
                               ; oplot,reform(weights_alternative[i]*ybi),color=255
                               ; oplot,data_corr[*,los1,i_time],color=128
                            dummy=' '
                            ;read,dummy
	                    endfor
                            ;dummy=' '
                            read,dummy
                            ;stop


	                endfor
                      print, 'Weights: ', weights
                      print, 'Shifts ', shft
                      print, ' '
                      print, 'Alternative Weights: ', weights_alternative

                  endif
                  
                  if not (n_elements(pairs[*,0]) lt 2) then begin
                    los_map_tmp = pairs[*,0]
                    ; sort channels 
                    ind_los=los_map_tmp*0.
                    for i_los=0,n_elements(los_map_tmp)-1 do begin
                        sort_los_tmp=strsplit(cxh_tmp.header.los_name[los_map_tmp(i_los)],'-',/EXTRACT)
                        ind_los(i_los)=sort_los_tmp(2)
                    endfor
                    ;los_map = los_map_tmp(sort(ind_los))
                    los_map = los_map_tmp(reverse(sort(ind_los)))
                    
                    NumROI = n_elements(los_map)
                    yDim = n_elements(los_map)
                    ind_frames=where((cxh.data.time gt -11. and cxh.data.time lt 15) or cxh.data.time lt 0.0)                    
                    data=cxh.data(ind_frames).intens_calib(*,*)*0.
                    data_err= cxh_err.err_bars(*,*,ind_frames)*0. ;cvh.data.intenserr*0.    ;cmr_err.err_bars(*,*,ind_frames)
                    data[*,[los_map],time_slice_indecies] = data_corr[*,[los_map],time_slice_indecies] ;+1d17
                    data_err[*,[los_map],time_slice_indecies] = data_corr_err[*,[los_map],time_slice_indecies] ;+1d17

                    if shot ge 32743l then $
                     cxh.data[ind_frames].intens_calib[*,*] = data +1d17 $ ;(*,los_map,*)  ;add 1d17 because the signal is getting negative for some frames 
                    else $
                    cxh.data[*].intens_calib[*,*] = data +1d17 ;(*,los_map,*)  ;add 1d17 because the signal is getting negative for some frames

                    ;cvh.data.intenserr = data_err ; +1d17
                    cxh_err.err_bars = data_err;(*,los_map,*)
                    
                    ; Call cxf_define_spectrometer_data with appropriate dimensions to
                    ; complete structure definition
                          npixel = cxh.header.xDim
                          ntrack = n_elements(los_map)
                          nframe = cxh.header.Nframes
                          ninst  = cxh.header.nGaus
                          spectrometer_data.npixel = npixel
                          spectrometer_data.ntrack = n_elements(los_map)
                          spectrometer_data.ninst  = ninst
                          ;ind_frames=where((cvh.data.time gt -11. and cvh.data.time lt 15) or cvh.data.time lt 0.0)
                          nframe=n_elements(ind_frames)
                          spectrometer_data.nframe = nframe
                          
                          cxf_define_spectrometer_data,spectrometer_data,err
                          IF err NE '' THEN RETURN

                        
                        ; Load the structure
                        if shot ge 32743l then spectrometer_data.intensity.data=cxh.data[ind_frames].intens_calib[*,los_map] $
                        else $
                            spectrometer_data.intensity.data=cxh.data[*].intens_calib[*,los_map]
                            ;spectrometer_data.intensity.error=cvh.data.intenserr[*,los_map,*]
                            
                            ; do something similar as for CMR - but take background frames from end of discharge
                            n_bg_frames =  cxh.header.bg_range[19]-cxh.header.bg_range[0]+1
                            FOR j=0,ntrack-1 DO BEGIN
                                FOR k=0,nframe-1 DO BEGIN
                                    spectrometer_data.intensity.data[*,j,k] $
                                        = cxh.data[ind_frames(k)].intens_calib[*,los_map(j)]
                                    ; Validity - check for saturation
                                    wh_sat = WHERE(cxh.data[ind_frames(k)].intens[*,j] GE $
                                                2.^cxh.header.dynamic_range-1,nsat)
                                    IF nsat GT 0 THEN $
                                        spectrometer_data.intensity.validity[wh_sat,j,ind_frames(k)] = 2
                                ENDFOR
                            ENDFOR
                            spectrometer_data.intensity.error $
                                = cxh_err.err_bars(*,los_map,ind_frames)
                    
                          spectrometer_data.intensity.reference $
                            = ' '

                        ; Line-of-sight names
                              spectrometer_data.LOS_name = cxh.header.LOS_name[los_map]

                        ; Beam crossing point
                              spectrometer_data.R_pos   =  cxh.header.maximum_r[los_map] ;cvh.header.r_pos[los_map]
                              spectrometer_data.z_pos   =  cxh.header.maximum_z[los_map] ;cvh.header.z_pos[los_map]
                              spectrometer_data.phi_pos =  cxh.header.maximum_phi[los_map] ;cvh.header.phi_pos[los_map]

                        ; Position of origin of the LOS
                              spectrometer_data.R_orig   = cxh.header.R_opt[los_map]
                              spectrometer_data.z_orig   = cxh.header.z_opt[los_map]
                              spectrometer_data.phi_orig = cxh.header.phi_opt[los_map]
                          ;    spectrometer_data.alpha_orig   = cxh.header.alpha_los
                          ;    spectrometer_data.beta_orig = cxh.header.beta_los

                        ; Reference wavelength [A] !!!
                              spectrometer_data.wlength = cxh.header.wav_mid*10.

                        ; Slit width [microns]
                              spectrometer_data.slit = cxh.header.SlitWid

                        ; Time vector [s]
                              spectrometer_data.time     = cxh.data(ind_frames).time
                              
                              spectrometer_data.exposure = $
                                cxh.header.exptime + FLTARR(nframe)

                        ; Calculate the wavelength intervals assuming the pixels are
                        ; contiguous
                              wav_vect  = cxh.header.cor_wavel[*,los_map]
                              dwav_vect = FLTARR(npixel,ntrack)
                              dwav_vect[0,*] = wav_vect[1,los_map]-wav_vect[0,los_map]
                              dwav_vect[1:npixel-2,*] = 0.5*(wav_vect[2:npixel-1,los_map] $
                                                               -wav_vect[0:npixel-3,los_map])
                              dwav_vect[npixel-1,*] = wav_vect[npixel-1,los_map] $
                                                        -wav_vect[npixel-2,los_map]
                              spectrometer_data.wavelength.data      = wav_vect
                        ; Uncertainty still to be defined!!!
                              spectrometer_data.wavelength.error     = FLTARR(npixel,ntrack)
                              spectrometer_data.wavelength.reference = ' '
                              spectrometer_data.dispersion.data      = dwav_vect
                        ; Uncertainty still to be defined!!!
                              spectrometer_data.dispersion.error     = FLTARR(npixel,ntrack)
                              spectrometer_data.dispersion.reference = ' '

                        ; Instrument function
                              spectrometer_data.instfu.y0 = replicate(1.,ntrack);cer.header.det_inst.intens
                              spectrometer_data.instfu.xw = cxh.header.inst_wid_nm[los_map]*10. ;cer.header.det_inst.width * 10.0/(2d0*alog(2d0))
                              spectrometer_data.instfu.xs = replicate(0.,ntrack);cer.header.det_inst.shift
                              spectrometer_data.instfu.reference = ' '

                            !P.multi=[0.,1.,1.]

                  endif else begin
                    los_map = indgen(NumROI)
                  endelse
                endif else begin
	            print, ''
	            print, 'Ventil3 inactive, falling back to full read!'
	            cxpuff = 0
                endelse


            endif            
            
            ;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
          endelse        
        endelse
                



;stop
; Load flag showing if 'sufficient' beam power is available for a
; given frame

      cxrs_beam_status, puff_diag, shot, spectrometer_data.time, $
                        spectrometer_data.exposure, $
                        beam, NI_shotfile, err
      IF STRTRIM(err,2) NE '' THEN BEGIN
; If the shot file is missing then assume no beams (this is default in
; cxf_define_spectrometer_data)!!!
        IF STRPOS(err,'shotfile does not exist') NE -1 THEN BEGIN
         err = ''
        ENDIF ELSE BEGIN
           RETURN
        ENDELSE
      ENDIF ELSE $
        spectrometer_data.beam = beam
        
;stop


endif else begin
print, 'Discharge should be < 35000'
endelse
stop
endif
                    
    if shot ge 35000 then begin
    
    if (extraoptions.CXH.time_choice eq 0) or (extraoptions.CXH.time_choice eq 1) then begin

    err  = ''
    id   = 'CXF_LOAD_AUG_DATA: '
    ierr = 0L
    
    ; speed of light
    c = 2.9979D8      ; [m/s]

    ; Planck constant   ; [J s]
    h = 6.6261D-34
    
    ; Read losNames of FVS and EVS

    los_names=''
    los_index=''
    spectr='xxx'
    
    ; Open EVS shotfile
    error=0L
    diaref=0L
    edition=0L
    date='123456789012345668'	; String with 18 characters
    result=call_external(!libddww,'ddgetaug','ddopen', $
           error,'AUGD','EVS',shot,edition,diaref,date)
    ; Read the LOS name
    type=6L
    losname='        '
    physunit=0L
    signalname='PARAM'
    CHANEL_ID='CHAN_xx'
    n_los=25L
    for i=0,(n_los-1) do begin
        if i lt 9 then begin
            nr=strcompress('0'+string((i+1)),/remove_all)
            endif else begin
            nr=strcompress(string((i+1)),/remove_all)
            endelse
        strput,CHANEL_ID,nr,5
        result=call_external(!libddww,'ddgetaug','ddparm', $
                error,diaref,'PARAM',CHANEL_ID,6L,8L,losname,physunit)
        det_name = strmid(losname,0,3)
        if strcmp(det_name,'CXH') eq 1 then begin
            los_index=[los_index,i]
            los_names=los_names+losname
            spectr = 'EVS'
        endif
    endfor
    ; Close the shotfile
    result=call_external(!libddww,'ddgetaug','ddclose', $
           error,diaref)
    
    
    ; Open FVS shotfile
    if strcmp(spectr,'EVS') eq 0 then begin
    error=0L
    diaref=0L
    edition=0L
    date='123456789012345668'	; String with 18 characters
    result=call_external(!libddww,'ddgetaug','ddopen', $
           error,'AUGD','FVS',shot,edition,diaref,date)
    ; Read the LOS name
    type=6L
    losname='        '
    physunit=0L
    signalname='PARAM'
    CHANEL_ID='CHAN_xx'
    n_los=24L
    
    for i=0,(n_los-1) do begin
        if i lt 9 then begin
            nr=strcompress('0'+string((i+1)),/remove_all)

        endif else begin
            nr=strcompress(string((i+1)),/remove_all)

        endelse
        strput,CHANEL_ID,nr,5
        result=call_external(!libddww,'ddgetaug','ddparm', $
                error,diaref,'PARAM',CHANEL_ID,6L,8L,losname,physunit)
        det_name = strmid(losname,0,3)

        if strcmp(det_name,'CXH') eq 1 then begin
            los_index=[los_index,i]
            los_names=los_names+losname
            spectr = 'FVS'
        endif
   
    endfor
    ; Close the shotfile
    result=call_external(!libddww,'ddgetaug','ddclose', $
           error,diaref)
    endif
     
     los_names= strsplit(los_names,/EXTRACT)
     los_index=los_index(1:(n_elements(los_index)-1))
     print,'Spectrometer used:   ',spectr
     print,'Channels used:', los_index+1L
    
    ; Input switches
    shot     = spectrometer_data.shot_nr
    spectrometer_data.spectrometer.name = spectr        
    diagnosticL1 = STRSPLIT(spectr, 'S', /EXTRACT)+'L'
    error = 0L
    
    read_xvs_diag, shot, spectr, dwdp, exptime, sad2, ctsph, time, lam, offset, sens, spec, wlen, slit, $
           gratcons, op_ang, pixw, magnification, fwhm_pix, neon_done, neon, lambda_neon, r1, phi1, z1, $
           r2, phi2, z2, los_name=los_name
    
    ;Select frames with Ip > 0
 
    timebase = 1L
    read_signal_mrm,0L,shot,'MAG','Ipa',timebase,variable,phys_dim,edition=0L,exp='AUGD'
    threshold = 2.0e5    
    timebase = timebase(where((variable gt threshold) or (variable lt -1.0*threshold)))
    interval_ip_on = [timebase(0),timebase(n_elements(timebase)-1)]
 
    spec = spec (*,where((time gt interval_ip_on(0)) and (time lt interval_ip_on(1))),*)
    time = time(where((time gt interval_ip_on(0)) and (time lt interval_ip_on(1))))
 
    if extraoptions.CXH.time_choice eq 1 then begin ; Select frames [t_ini_gaspuff, t_end_gaspuff]
    timebase = 1L
    read_signal_mrm,0L,shot,'HEB','S01VALVE',timebase,variable,phys_dim,edition=0L,exp='AUGD'
    threshold  = 80 ; [V] the valve is considered opened at 80 V
    timebase = timebase(where( variable gt threshold))
    interval_gaspuff_on = [timebase(0)-0.005,(timebase(n_elements(timebase)-1)+0.005)]
    spec = spec (*,where((time gt interval_gaspuff_on(0)) and (time lt interval_gaspuff_on(1))),*)
    time = time(where((time gt interval_gaspuff_on(0)) and (time lt interval_gaspuff_on(1))))
    endif
    if n_elements(los_index) eq 1 then begin
        npixel = n_elements(spec[*,0,0])
        nframe  = n_elements(spec[0,*,0])

        spec = spec[*,*,los_index]
        sens = sens[*,los_index]
        lam = lam[*,los_index]
        offset = offset[*,los_index]

        spec = reform(spec,npixel,nframe,1L)
        sens = reform(sens,npixel,1L)
        lam = reform(lam,npixel,1L)
        offset = reform(offset,npixel,1L)

    endif else begin
        spec = spec[*,*,los_index]
        sens = sens[*,los_index]
        lam = lam[*,los_index]
        offset = offset[*,los_index]
    endelse
    los_name = los_name[los_index]
 
    spectrometer_data.npixel = n_elements(spec[*,0,0])
    spectrometer_data.nframe  = n_elements(spec[0,*,0])
    spectrometer_data.ntrack = n_elements(spec[0,0,*])
    spectrometer_data.ninst  = 1
    cxf_define_spectrometer_data,spectrometer_data,err
    spectrometer_data.LOS_name = los_name
    spectrometer_data.R_orig = r1[los_index]
    spectrometer_data.Z_orig = z1[los_index]
    spectrometer_data.PHI_orig = phi1[los_index]
    spectrometer_data.R_pos = r2[los_index]
    spectrometer_data.Z_pos = z2[los_index]
    spectrometer_data.PHI_pos = phi2[los_index]
    ; Reference wavelength [A]
    spectrometer_data.wlength = wlen*10

    ; Slit width [microns]
    spectrometer_data.slit = slit

    ; Time vector [s]
    spectrometer_data.time     = time
    spectrometer_data.exposure = exptime + FLTARR(spectrometer_data.nframe)

    ; Calculate the wavelength intervals assuming the pixels are contiguous
    wav_vect  = lam
    dwav_vect = FLTARR(spectrometer_data.npixel,spectrometer_data.ntrack)
    dwav_vect[0,*] = wav_vect[1,*]-wav_vect[0,*]
    dwav_vect[1:spectrometer_data.npixel-2,*] = 0.5*(wav_vect[2:spectrometer_data.npixel-1,*] $
                                   -wav_vect[0:spectrometer_data.npixel-3,*])
    dwav_vect[spectrometer_data.npixel-1,*] = wav_vect[spectrometer_data.npixel-1,*] $
                            -wav_vect[spectrometer_data.npixel-2,*]
    spectrometer_data.wavelength.data      = wav_vect
    spectrometer_data.wavelength.error     = FLTARR(spectrometer_data.npixel,$
                                spectrometer_data.ntrack)
    spectrometer_data.wavelength.reference = ' '
    spectrometer_data.dispersion.data      = dwav_vect
    spectrometer_data.dispersion.error     = FLTARR(spectrometer_data.npixel,$
                                spectrometer_data.ntrack)
    spectrometer_data.dispersion.reference = ' '

    ; Calculate error bars and calibrated data
    ppc = 1./ctsph
    ron = sad2
    err_bars = dblarr(spectrometer_data.npixel,spectrometer_data.ntrack,$
        spectrometer_data.nframe)
    err_bars_counts = sqrt(spec*ctsph+sad2)
    data_calib = dblarr(spectrometer_data.npixel,spectrometer_data.ntrack,$
        spectrometer_data.nframe)
    for jtime = 0,spectrometer_data.nframe-1 do begin
        err_bars[*,*,jtime] = err_bars_counts[*,jtime,*]/sens/exptime/dwdp
        data_calib[*,*,jtime] = spec[*,jtime,*]/sens/exptime/dwdp
    endfor

    spectrometer_data.intensity.data = data_calib
    spectrometer_data.intensity.error = err_bars
    spectrometer_data.intensity.reference = ' '

    spectrometer_data.instfu.y0 = replicate(1.,spectrometer_data.ntrack)
    spectrometer_data.instfu.xw = fwhm_pix[los_index]*dwdp*10
    spectrometer_data.instfu.xs = replicate(0.,spectrometer_data.ntrack)
    spectrometer_data.instfu.reference = ' '

    externalcostheta = fltarr(spectrometer_data.ntrack,spectrometer_data.nframe)+1.
    spectrometer_data = create_struct(spectrometer_data,'externalcostheta',externalcostheta)
    
    ; Load flag showing if 'sufficient' beam =puff is available for a
    ; given frame
      if shot ge 35313 then begin
        if shot ne 35396 then begin
         hfs_puff_status, 'CXH', shot, spectrometer_data.time, $
                           spectrometer_data.exposure, $
                           beam, beam_new, puff_shotfile, err
         IF STRTRIM(err,2) NE '' THEN BEGIN
                                ; If the shot file is missing then assume no beams (this is default in
                                ; cxf_define_spectrometer_data)!!!
            IF STRPOS(err,'shotfile does not exist') NE -1 THEN BEGIN
               spectrometer_data.beam = spectrometer_data.time*0.
               err = ''
            ENDIF ELSE BEGIN
               RETURN
            ENDELSE
         ENDIF ELSE BEGIN
; read ElM signal in order to remove elms from background subtraction           
           elm_exp='AUGD'
           read_signal_mrm, ier,shot,'ELM','t_endELM',telm_begin,telm_end,phys_dim,exp=elm_exp
           if ier eq 0 then begin
              print, ''
              print, 'Exclude ELMs from data'
              print, ''
              for t=0L, n_elements(telm_begin)-1 do begin
                 min_begin=min(abs(spectrometer_data.time-(telm_begin[t]-0.001)),location_begin)
                 min_end=min(abs(spectrometer_data.time-(telm_end[t]+0.001)),location_end)
                 for k=location_begin, location_end+1 do begin
                    beam_new[k]=0.7
                 endfor
              endfor
           endif else begin
              print, ''   
              print, 'ELM shotfile does not exist!'
              print, ''
           endelse    
            spectrometer_data.beam = beam
            spectrometer_data.modulation.flag=0
            beam_new = reform(beam_new,1,n_elements(beam_new))
            names = ['GasPuffSe01']
            *spectrometer_data.modulation.beampower=beam_new
            *spectrometer_data.modulation.beamlabels =names 
            *spectrometer_data.modulation.orig_beam =spectrometer_data.beam 
            *spectrometer_data.modulation.orig_data =spectrometer_data.intensity.data
            *spectrometer_data.modulation.orig_error = spectrometer_data.intensity.error           
            
         ENDELSE
         endif else begin ;shot = 35396
         
            beam = dblarr(n_elements(spectrometer_data.time))
            ini_beam = 3.018 ;[s]
            delta_t = 0.020 ; [s]
            for i = 0,15 do begin
                index_on = where( (time gt ini_beam) and (time le (ini_beam+delta_t)))
                beam(index_on) = 1.0
                beam(index_on(0)-1) = 0.5
                beam(index_on(n_elements(index_on)-1)+1) = 0.5
                ini_beam = ini_beam + 0.1
            endfor 
            
; read ElM signal in order to remove elms from background subtraction           
           elm_exp='AUGD'
           read_signal_mrm, ier,shot,'ELM','t_endELM',telm_begin,telm_end,phys_dim,exp=elm_exp
           if ier eq 0 then begin
              print, ''
              print, 'Exclude ELMs from data'
              print, ''
              for t=0L, n_elements(telm_begin)-1 do begin
                 min_begin=min(abs(spectrometer_data.time-(telm_begin[t]-0.001)),location_begin)
                 min_end=min(abs(spectrometer_data.time-(telm_end[t]+0.001)),location_end)
                 for k=location_begin, location_end+1 do begin
                    if beam[k] ne 1.0 then beam[k]=0.7
                 endfor
              endfor
           endif else begin
              print, ''   
              print, 'ELM shotfile does not exist!'
              print, ''
           endelse                
               
            spectrometer_data.beam = beam
            spectrometer_data.modulation.flag=0
            names = ['GasPuffSe01']
            *spectrometer_data.modulation.beampower=reform(beam,1,n_elements(beam))
            *spectrometer_data.modulation.beamlabels =names 
            *spectrometer_data.modulation.orig_beam =spectrometer_data.beam 
            *spectrometer_data.modulation.orig_data =spectrometer_data.intensity.data
            *spectrometer_data.modulation.orig_error = spectrometer_data.intensity.error
            
        endelse       
      endif

    spectrometer_data.spectrometer.name = 'CXH'
    spectrometer_data.PHI_orig = (spectrometer_data.PHI_orig + 2*!Pi)*180/!Pi
    spectrometer_data.PHI_pos = spectrometer_data.PHI_pos*180/!Pi

    kill_var,dwdp
    kill_var,exptime
    kill_var,sad2
    kill_var,ctsph
    kill_var,time
    kill_var,lam
    kill_var,offset
    kill_var,sens
    kill_var,spec
    kill_var,los_name
    kill_var,data_calib
    kill_var,err_bars
    kill_var,err_bars_counts
    kill_var,dwav_vect
    kill_var,wav_vect
    
    endif             
    endif
                    
    END ; processing for CXH
    
    
'CVH': BEGIN
    
; Load structure CVH containing all the data
 
cvh_err=1
if extraoptions.CVH.time_choice eq 2 then begin  
if shot lt 35000 then begin          
                    
load_cvh_fvs,shot,cvh,err,/ip_on ;,/single_gauss
puff_diag='CVH'
special_flag=1
              
get_err_bars_cvh,cvh,cvh_err

       ; stack_suited_channels_cvh,cvh,cvh_err        

      

;This is a copy of the analysis Thomas did for the toroidal HFS system on the CER spectrometer
; test for poloidal HFS system on FVS spectrometer --- elv 16/03/2012
            !P.multi=[0.,3.,2.]
            cor_wavel=cvh.header.cor_wavel
            data_corr_tmp=cvh.data[*].intens_calib(*,*)
            data_corr=cvh.data[*].intens_calib(*,*)
            data_corr_err=cvh_err.err_bars ;(*,*,ind_frames)   cmr_err.err_bars(*,*,ind_frames)   ;cvh.data.intenserr
            core_los = -1l   ;this will be applied to toroidal HFS-LOS instead of CER-LOS --> both HFS systems are stored in FVS shotfile!

            if (cvh.header.cor_wavel(256,8) gt 525.0 and cvh.header.cor_wavel(256,8) lt 530.0) or $
               (cvh.header.cor_wavel(256,8) gt 493.5 and cvh.header.cor_wavel(256,8) lt 496.0) or $
               (cvh.header.cor_wavel(256,8) gt 655.0 and cvh.header.cor_wavel(256,8) lt 658.0) or $               
               (cvh.header.cor_wavel(256,8) gt 565.5 and cvh.header.cor_wavel(256,8) lt 567.5) then begin
               
                reference_offset = -0.0501 ; offset to v3 start time for background weight frames
                valid_start = -0.0 ; offset to v3 start time for valid frames
                valid_end = 0.1 ; offset to v3 end time for valid frames
                ; end of definition
                cvh_tmp=cvh
                timep = cvh_tmp.data[*].time
                get_ventil3,shot,v3,err
                if v3.active eq 1 then begin
	            dummy = min(abs(v3.ventil_time[0]+reference_offset - timep),weight_ind)
	            time_slice_indecies_coresub=where(timep lt 0.21 and timep gt 0.1)
	            time_slice_indecies=where(timep lt 0.21 or (timep ge (v3.ventil_time[0]+valid_start) and timep le (v3.mano_time[1]+valid_end)))
	            time_slice_indecies_puff_on=where(timep(time_slice_indecies) ge (v3.ventil_time[0]+valid_start) and timep(time_slice_indecies) le (v3.ventil_time[1]+valid_end))
	            ;time_slice_indecies=[time_slice_indecies]
	            ;remove_background=1
	            ; find corresponding los

	            for i = 0,cvh_tmp.header.nroi-1 do begin
	                comp =  strsplit(cvh_tmp.header.los_name[i],'-',/EXTRACT)
                        if strcompress(cvh_tmp.header.los_name[i],/remove_all) ne 'SPECIAL' and strcompress(cvh_tmp.header.los_name[i],/remove_all) ne 'BACKGRND' then begin
	                    if comp[1] eq 'S' and comp[0] ne 'CXH' then begin
                                if cvh_tmp.header.los_name[i] eq 'CVH-S-01' then core_los = i
		                for j = 0,cvh_tmp.header.nroi-1 do begin
		                    tocomp =  strsplit(cvh_tmp.header.los_name[j],'-',/EXTRACT)

                                    if n_elements(tocomp) eq 3 then begin 
		                      
                                        if (tocomp[1] eq 'B' and tocomp[0] ne 'CXH' and comp[2] eq '6' and tocomp[2] eq '5') then begin
                                            print,'processing CVH-S-6 (CVH-B-6 broken for campaign 2012)'
                                            print,'Taking pair ', cvh_tmp.header.los_name[i],' and ', cvh_tmp.header.los_name[j]
                                            pairs = [pairs,[[i],[j]]]
                                        endif
                                        if (tocomp[1] eq 'B' and tocomp[0] ne 'CXH' and comp[2] eq tocomp[2]) then begin
		                        ;if (tocomp[1] eq 'B' and tocomp[0] ne 'CXH' and comp[2] eq tocomp[2] and comp[2] ne '6') then begin
			                    print, 'Matching pair ', cvh_tmp.header.los_name[i],' and ', cvh_tmp.header.los_name[j]
			                    if n_elements(pairs) eq 0 then pairs = [[i],[j]] $
			                    else pairs = [pairs,[[i],[j]]]
		                        endif
                                    endif

		                endfor
	                    endif
                        endif
	            endfor

	            sz = size(pairs,/dimension)
	            weights = fltarr(sz[0])
	            weights_alternative = fltarr(sz[0])
	            shft = fltarr(sz[0])

                    if special_flag eq 2 or special_flag eq 3 then begin

	                weights_coresub = fltarr(sz[0]*2.)
	                weights_alternative_coresub = fltarr(sz[0]*2.)
	                shft_coresub = fltarr(sz[0]*2.)
                    
                    endif

	            ;middles = [529.5,495.0,656.9,468.9]
	            middles = [526.5,495.0,656.9,468.9,418.,567.0]
	            ;fit_ranges = [[528.0, 530.0],[494.25,496.0],[655.0, 657.2],[466.00, 469.0],[417.00, 419.0],[566.8, 567.3]]
	            fit_ranges = [[528.0, 530.0],[494.25,496.0],[655.0, 657.2],[466.00, 469.0],[417.00, 419.0],[566.82, 567.3]]
                ;				height		centre		width		offset
	            fit_inits = [	[	1.0e17,		529.1,		0.1,		0.5e17], $
			                [	3.0e17,		494.5,		0.1,		1.0e17], $
			                [	1.0e19,		656.1,		0.1,		2.0e17], $
			                [	6.0e17,		468.6,		0.1,		0.5e17], $
			                [	1.0e17,		417.6,		0.1,		0.5e17], $
		                        ;[	6.0e17,		566.95,		0.1,		0.5e17]]
                                        [	9.0e17,		566.95,		0.1,		1.0e17]]

	            middles_special = [526.5,495.0,656.9,468.9,418.,567.0]
	          ; fit_ranges_special = [[528.0, 530.0],[493.8,494.25],[655.0, 657.2],[466.00, 469.0],[417.00, 419.0],[566.8, 567.3]]
                    fit_ranges_special = [[528.0, 530.0],[493.8,494.25],[655.0, 657.2],[466.00, 469.0],[417.00, 419.0],[566.82,567.3]]
                ;				height		centre		width		offset
	            fit_inits_special = [[	1.0e17,		529.1,		0.1,		0.5e17], $
			                [	3.0e17,		494.03,		0.1,		1.0e17], $
			                [	1.0e19,		656.1,		0.1,		2.0e17], $
			                [	6.0e17,		468.6,		0.1,		0.5e17], $
			                [	1.0e17,		417.6,		0.1,		0.5e17], $
		                        ;[	6.0e17,		566.95,		0.1,		0.5e17]]
		                        [	9.0e17,		566.95,		0.1,		1.0e17]]
	            center = cvh_tmp.header.wav_mid
	            index = where(abs(middles - center) lt 1.0)

	            fit_range  = fit_ranges[*,index]
	            fit_range_special  = fit_ranges_special[*,index]
	            fit_init = fit_inits[*,index]
	            fit_init_special = fit_inits_special[*,index]
	            naverage = 25

                    if (special_flag eq 2l and core_los ne -1) or (special_flag eq 3l and core_los ne -1) then begin
                    
	                for i = 0, sz[0]*2.0-1 do begin
                            los2 = core_los
                            los1 = pairs(i)
                    
                        print,los1,los2
                        wvls=cor_wavel[*,los1]
	                nterms = 4
	                fit_inds = where(wvls ge fit_range_special[0] AND wvls le fit_range_special[1])

	                ; naverage frames starting from gas puff
	                ;av_inds = time_slice_indecies_coresub
	                av_inds = where(timep gt 0.1 and timep lt 0.15)
                        ; naverage frames centered around gas puff start
	                ;av_inds = indgen(naverage)-floor(naverage/2)+weight_ind

	                xs = cor_wavel[fit_inds,los1]
	                xb = cor_wavel[fit_inds,los2]

	                ys = total(data_corr_tmp[fit_inds,los1,av_inds],3)/naverage
	                yb = total(data_corr_tmp[fit_inds,los2,av_inds],3)/naverage

                        plot, xs,ys,thick=2,/psym
                        oplot,xb,yb,thick=2,/psym,color=255

	                normalize = 10.0^floor(alog10(max(ys)))
	                init = fit_init_special
	                init[0] = init[0]/normalize
	                init[3] = init[3]/normalize

	                ysfit = GAUSSFIT(xs, ys/normalize, s_coeff, NTERMS=nterms, ESTIMATES=init)
	                ybfit = GAUSSFIT(xb, yb/normalize, b_coeff, NTERMS=nterms, ESTIMATES=init)
	                weights_coresub[i] = s_coeff[0]/b_coeff[0]
	                shft_coresub[i]    = s_coeff[1]-b_coeff[1]
                        oplot,xs,ysfit*normalize
                        oplot,xb,ybfit*normalize,color=255

	                weights_alternative_coresub[i] = mean(median(ys,11))/mean(median(yb,11))

                        ;dummy=' '
                        ;read,dummy
	                    for i_time = max(where(timep(time_slice_indecies) lt 0.21)), n_elements(time_slice_indecies)-1 do begin

		                ;data_corr[*,los1,i_time] = data_corr[*,los1,i_time] - weights[i]*data_corr[*,los2,i_time]
		                ; do an interpolation from xb to (xs - shft)
		                ;ybi = interpol(data_corr[*,los2,i_time],cor_wavel[*,los2],(cor_wavel[*,los1]-shft[i]))

		                ybi = interpol(reform(data_corr[*,los2,time_slice_indecies(i_time)]),reform(cor_wavel[*,los2]+shft_coresub(i)),reform((cor_wavel[*,los1])))
;                                data_corr[*,los1,time_slice_indecies(i_time)] = reform(data_corr[*,los1,time_slice_indecies(i_time)]) - reform(weights_coresub[i]*ybi)
                                data_corr[*,los1,time_slice_indecies(i_time)] = reform(data_corr[*,los1,time_slice_indecies(i_time)]) - reform(weights_coresub[i]*ybi)
                                ;data_corr[*,los1,time_slice_indecies(i_time)] = reform(data_corr[*,los1,time_slice_indecies(i_time)]) - reform(ybi)
                                data_corr_err[*,los1,time_slice_indecies(i_time)] = sqrt(reform(data_corr_err[*,los1,time_slice_indecies(i_time)])^2+ reform(data_corr_err[*,los2,time_slice_indecies(i_time)]*weights_coresub[i])^2)

	                    endfor
	                endfor
                  print, 'Core_Weights: ', weights_coresub
                  print, 'Core_Shifts ', shft_coresub
                  print, ' '
                  print, 'Core_Alternative Weights: ', weights_alternative_coresub
                    
                    endif 
                    if special_flag eq 1 or special_flag eq 3 or special_flag eq 4 then begin
                    
                        data_corr_tmp=data_corr

	                for i = 0, sz[0]-1 do begin

	                    los1 = pairs[i,0]
	                    los2 = pairs[i,1]

                            print,los1,los2
                            wvls=cor_wavel[*,los1]
	                    nterms = 4
	                    fit_inds = where(wvls ge fit_range[0] AND wvls le fit_range[1])

	                    ; naverage frames starting from gas puff
	                    av_inds = indgen(naverage)+weight_ind;-naverage/2
	                    ; naverage frames centered around gas puff start
	                    ;av_inds = indgen(naverage)-floor(naverage/2)+weight_ind

	                    xs = cor_wavel[fit_inds,los1]
	                    xb = cor_wavel[fit_inds,los2]

	                    ys = total(data_corr_tmp[fit_inds,los1,av_inds],3)/naverage
                            ;yb = total(data_corr_tmp[fit_inds,los2,av_inds],3)/naverage                            
                           ; yb = ys*0.
	                    ;for k_pix=0,n_elements(fit_inds)-1 do begin
                            ;    for k_chan=0,n_elements(los2)-1 do begin
                            ;        for k_frame=0,n_elements(av_inds)-1 do begin
                                
                                        ;if cvh.header.los_name(los2) eq 'CVH-B-6' then begin
                                        if cvh.header.los_name(los2) eq 'CVH-B-6' or cvh.header.los_name(los2) eq 'CVH-S-6' then begin
                                            yb_tmp=ys*0.
                                          ;for k_pix=0,n_elements(fit_inds)-1 do begin
                                          ; for k_frame=0,n_elements(av_inds)-1 do begin
                                            print,'Interpolating BKG channel 5 and 7 to get BKG signal of channel 6 (CVH-B-6 broken)'
                                            ind_los_nb1=where(cvh.header.los_name eq 'CVH-B-5')                ;neighbouring channel 1
                                            ind_los_nb2=where(cvh.header.los_name eq 'CVH-B-7')                  ;neighbouring channel 2
                                            ;los_intpol=interpol([data_corr_tmp(fit_inds(k_pix),los2(ind_los_nb1),av_inds(k_frame)),data_corr_tmp(fit_inds(k_pix),los2(ind_los_nb2),av_inds(k_frame))],[xb(fit_inds(k_pix),ind_los_nb1),xb(fit_inds(k_pix),ind_los_nb2)],xb(fit_inds(k_pix),los2(k_chan)))
                                           ; yb = total(data_corr_tmp[fit_inds(k_pix),los2(k_chan),av_inds(k_frame)],3)/naverage  
 ;los_intpol=interpol(reform([data_corr_tmp(fit_inds(k_pix),ind_los_nb1(0),av_inds(k_frame)),data_corr_tmp(fit_inds(k_pix),ind_los_nb2(0),av_inds(k_frame))]),[cor_wavel(fit_inds(k_pix),ind_los_nb1(0)),cor_wavel(fit_inds(k_pix),ind_los_nb2(0))],xb(k_pix))
                                          los_intpol=interpol(total(reform([data_corr_tmp(fit_inds,ind_los_nb1(0),av_inds),data_corr_tmp(fit_inds,ind_los_nb2(0),av_inds)]),2)/naverage,[cor_wavel(fit_inds,ind_los_nb1(0)),cor_wavel(fit_inds,ind_los_nb2(0))],xb) 
                                            ;yb_tmp(k_pix)=los_intpol
                                            yb=los_intpol
                                         ;  endfor
                                         ; endfor
                                         ; yb=total(yb_tmp,1)/naverage
                                         ; stop                      
                                        endif else begin
                                            yb = total(data_corr_tmp[fit_inds,los2,av_inds],3)/naverage
                                        endelse
                            ;        endfor
                            ;    endfor
                            ;endfor	                    
                            

                            plot, xs,ys,thick=2,/psym
                            oplot,xb,yb,thick=2,/psym,color=255

	                    normalize = 10.0^floor(alog10(max(ys)))
	                    init = fit_init
	                    init[0] = init[0]/normalize
	                    init[3] = init[3]/normalize
;stop
	                    ysfit = GAUSSFIT(xs, ys/normalize, s_coeff, NTERMS=nterms, ESTIMATES=init)
	                    ybfit = GAUSSFIT(xb, yb/normalize, b_coeff, NTERMS=nterms, ESTIMATES=init)
	                    weights[i] = s_coeff[0]/b_coeff[0]
	                    shft[i]    = s_coeff[1]-b_coeff[1]
                            oplot,xs,ysfit*normalize
                            oplot,xb,ybfit*normalize,color=255

	                    weights_alternative[i] = mean(median(ys,11))/mean(median(yb,11))


	                    for i_time = max(where(timep(time_slice_indecies) lt 0.21)), n_elements(time_slice_indecies)-1 do begin
		                if cvh.header.los_name(los2) eq 'CVH-B-6' then begin
                                    ybi = interpol( reform([data_corr(*,ind_los_nb1(0),time_slice_indecies(i_time)),data_corr(*,ind_los_nb2(0),time_slice_indecies(i_time))]),reform([cor_wavel(*,ind_los_nb1(0))+shft(i),cor_wavel(*,ind_los_nb2(0))+shft(i)]),reform( cor_wavel(*,los1) ) )                                   
                                 if special_flag ne 4l then begin
                                    data_corr[*,los1,time_slice_indecies(i_time)] = reform(data_corr[*,los1,time_slice_indecies(i_time)]) - reform(weights[i]*ybi*0.9)
                                   ;data_corr_err[*,los1,time_slice_indecies(i_time)] = sqrt(reform(data_corr_err[*,los1,time_slice_indecies(i_time)])^2+ reform(data_corr_err[*,los2,time_slice_indecies(i_time)]*weights_alternative[i])^2)
                                    data_err_tmp_ch6=interpol( [data_corr_err(*,ind_los_nb1(0),time_slice_indecies(i_time)),data_corr_err(*,ind_los_nb2(0),time_slice_indecies(i_time))],[cor_wavel(*,ind_los_nb1(0)),cor_wavel(*,ind_los_nb2(0))],cor_wavel(*,los1) )
                                    data_corr_err[*,los1,time_slice_indecies(i_time)] = sqrt(reform(data_corr_err[*,los1,time_slice_indecies(i_time)])^2+ reform(data_err_tmp_ch6(*)*weights[i])^2) 
                                  endif 
                                endif else begin
                                
                                ;data_corr[*,los1,i_time] = data_corr[*,los1,i_time] - weights[i]*data_corr[*,los2,i_time]
		                ; do an interpolation from xb to (xs - shft)
		                ;ybi = interpol(data_corr[*,los2,i_time],cor_wavel[*,los2],(cor_wavel[*,los1]-shft[i]))
                    
		                ybi = interpol(reform(data_corr[*,los2,time_slice_indecies(i_time)]),reform(cor_wavel[*,los2]+shft(i)),reform((cor_wavel[*,los1])))
		                ;ybi = interpol(reform(data_corr[*,los2,time_slice_indecies(i_time)]),reform(cor_wavel[*,los2]),reform((cor_wavel[*,los1])))
                               ; plot,reform(data_corr[*,los1,i_time]),xr=[300,450],title='time='+string(float(timep(time_slice_indecies(i_time))))
                               ;data_corr[*,los1,time_slice_indecies(i_time)] = reform(data_corr[*,los1,time_slice_indecies(i_time)]) - reform(weights_alternative[i]*ybi)
                                
                                
                                if special_flag ne 4l then begin
                                    data_corr[*,los1,time_slice_indecies(i_time)] = reform(data_corr[*,los1,time_slice_indecies(i_time)]) - reform(weights[i]*ybi*0.9)
                                   ;data_corr_err[*,los1,time_slice_indecies(i_time)] = sqrt(reform(data_corr_err[*,los1,time_slice_indecies(i_time)])^2+ reform(data_corr_err[*,los2,time_slice_indecies(i_time)]*weights_alternative[i])^2)
                                    data_corr_err[*,los1,time_slice_indecies(i_time)] = sqrt(reform(data_corr_err[*,los1,time_slice_indecies(i_time)])^2+ reform(data_corr_err[*,los2,time_slice_indecies(i_time)]*weights[i])^2)
                                endif
                                
                              endelse
                               ; oplot,reform(weights_alternative[i]*ybi),color=255
                               ; oplot,data_corr[*,los1,i_time],color=128
                            dummy=' '
                            ;read,dummy
	                    endfor
                            ;dummy=' '
                            read,dummy
                            ;stop


	                endfor
                      print, 'Weights: ', weights
                      print, 'Shifts ', shft
                      print, ' '
                      print, 'Alternative Weights: ', weights_alternative

                  endif
;stop

                  if not (n_elements(pairs[*,0]) lt 2) then begin
                    los_map_tmp = pairs[*,0]
                    ; sort channels - this is hard-coded for now, need to implement it differently
                    ;los_map = [los_map_tmp(4),los_map_tmp(2),los_map_tmp(1),los_map_tmp(0),los_map_tmp(3)]
                    ind_los=los_map_tmp*0.
                    for i_los=0,n_elements(los_map_tmp)-1 do begin
                        sort_los_tmp=strsplit(cvh_tmp.header.los_name[los_map_tmp(i_los)],'-',/EXTRACT)
                        ind_los(i_los)=sort_los_tmp(2)
                    endfor                   
                    ;los_map = los_map_tmp(sort(ind_los))            
                    los_map = los_map_tmp(reverse(sort(ind_los)))  
                           
                    NumROI = n_elements(los_map)
                    yDim = n_elements(los_map)
                    ind_frames=where((cvh.data.time gt -11. and cvh.data.time lt 15) or cvh.data.time lt 0.0)                    
                    data=cvh.data(ind_frames).intens_calib(*,*)*0.
                    data_err= cvh_err.err_bars(*,*,ind_frames)*0. ;cvh.data.intenserr*0.    ;cmr_err.err_bars(*,*,ind_frames)
                    data[*,[los_map],time_slice_indecies] = data_corr[*,[los_map],time_slice_indecies] ;+1d17
                    data_err[*,[los_map],time_slice_indecies] = data_corr_err[*,[los_map],time_slice_indecies] ;+1d17

                    cvh.data[*].intens_calib[*,*] = data +1d17 ;(*,los_map,*)  ;add 1d17 because the signal is getting negative for some frames
                    ;cvh.data.intenserr = data_err ; +1d17
                    cvh_err.err_bars = data_err;(*,los_map,*)
                    
                    ; Call cxf_define_spectrometer_data with appropriate dimensions to
                    ; complete structure definition
                          npixel = cvh.header.xDim
                          ntrack = n_elements(los_map)
                          nframe = cvh.header.Nframes
                          ninst  = cvh.header.nGaus
                          spectrometer_data.npixel = npixel
                          spectrometer_data.ntrack = n_elements(los_map)
                          spectrometer_data.ninst  = ninst
                          ;ind_frames=where((cvh.data.time gt -11. and cvh.data.time lt 15) or cvh.data.time lt 0.0)
                          nframe=n_elements(ind_frames)
                          spectrometer_data.nframe = nframe
                          
                          cxf_define_spectrometer_data,spectrometer_data,err
                          IF err NE '' THEN RETURN

                        
                        ; Load the structure
                            spectrometer_data.intensity.data=cvh.data[*].intens_calib[*,los_map]
                            ;spectrometer_data.intensity.error=cvh.data.intenserr[*,los_map,*]
                            
                            ; do something similar as for CMR - but take background frames from end of discharge
                            n_bg_frames =  cvh.header.bg_range[19]-cvh.header.bg_range[0]+1
                            FOR j=0,ntrack-1 DO BEGIN
                                FOR k=0,nframe-1 DO BEGIN
                                    spectrometer_data.intensity.data[*,j,k] $
                                        = cvh.data[ind_frames(k)].intens_calib[*,los_map(j)]
                                    ; Validity - check for saturation
                                    wh_sat = WHERE(cvh.data[ind_frames(k)].intens[*,j] GE $
                                                2.^cvh.header.dynamic_range-1,nsat)
                                    IF nsat GT 0 THEN $
                                        spectrometer_data.intensity.validity[wh_sat,j,ind_frames(k)] = 2
                                ENDFOR
                            ENDFOR
                            spectrometer_data.intensity.error $
                                = cvh_err.err_bars(*,los_map,ind_frames)
                    
                          spectrometer_data.intensity.reference $
                            = ' '

                        ; Line-of-sight names
                              spectrometer_data.LOS_name = cvh.header.LOS_name[los_map]

                        ; Beam crossing point
                              spectrometer_data.R_pos   =  cvh.header.maximum_r[los_map] ;cvh.header.r_pos[los_map]
                              spectrometer_data.z_pos   =  cvh.header.maximum_z[los_map] ;cvh.header.z_pos[los_map]
                              spectrometer_data.phi_pos =  cvh.header.maximum_phi[los_map] ;cvh.header.phi_pos[los_map]

                        ; Position of origin of the LOS
                              spectrometer_data.R_orig   = cvh.header.R_opt[los_map]
                              spectrometer_data.z_orig   = cvh.header.z_opt[los_map]
                              spectrometer_data.phi_orig = cvh.header.phi_opt[los_map]
                          ;    spectrometer_data.alpha_orig   = cxh.header.alpha_los
                          ;    spectrometer_data.beta_orig = cxh.header.beta_los

                        ; Reference wavelength [A] !!!
                              spectrometer_data.wlength = cvh.header.wav_mid*10.

                        ; Slit width [microns]
                              spectrometer_data.slit = cvh.header.SlitWid

                        ; Time vector [s]
                              spectrometer_data.time     = cvh.data(ind_frames).time
                              spectrometer_data.exposure = $
                                cvh.header.exptime + FLTARR(nframe)

                        ; Calculate the wavelength intervals assuming the pixels are
                        ; contiguous
                              wav_vect  = cvh.header.cor_wavel[*,los_map]
                              dwav_vect = FLTARR(npixel,ntrack)
                              dwav_vect[0,*] = wav_vect[1,los_map]-wav_vect[0,los_map]
                              dwav_vect[1:npixel-2,*] = 0.5*(wav_vect[2:npixel-1,los_map] $
                                                               -wav_vect[0:npixel-3,los_map])
                              dwav_vect[npixel-1,*] = wav_vect[npixel-1,los_map] $
                                                        -wav_vect[npixel-2,los_map]
                              spectrometer_data.wavelength.data      = wav_vect
                        ; Uncertainty still to be defined!!!
                              spectrometer_data.wavelength.error     = FLTARR(npixel,ntrack)
                              spectrometer_data.wavelength.reference = ' '
                              spectrometer_data.dispersion.data      = dwav_vect
                        ; Uncertainty still to be defined!!!
                              spectrometer_data.dispersion.error     = FLTARR(npixel,ntrack)
                              spectrometer_data.dispersion.reference = ' '

                        ; Setting cos angle between LOS and B equal to 1
                              spectrometer_data = create_struct(spectrometer_data,'externalcostheta',fltarr(ntrack,nframe)+1.)

                        ; Instrument function
                              spectrometer_data.instfu.y0 = replicate(1.,ntrack);cer.header.det_inst.intens
                              spectrometer_data.instfu.xw = cvh.header.inst_wid_nm[los_map]*10. ;cer.header.det_inst.width * 10.0/(2d0*alog(2d0))
                              spectrometer_data.instfu.xs = replicate(0.,ntrack);cer.header.det_inst.shift
                              spectrometer_data.instfu.reference = ' '

                            !P.multi=[0.,1.,1.]

                  endif else begin
                    los_map = indgen(NumROI)
                  endelse
                endif else begin
	            print, ''
	            print, 'Ventil3 inactive, falling back to full read!'
	            cxpuff = 0
                endelse


            endif

; Load flag showing if 'sufficient' beam power is available for a
; given frame
; same as for CXH - added CVH to cxrs_beam_status

      cxrs_beam_status, puff_diag, shot, spectrometer_data.time, $
                        spectrometer_data.exposure, $
                        beam, NI_shotfile, err
      IF STRTRIM(err,2) NE '' THEN BEGIN
; If the shot file is missing then assume no beams (this is default in
; cxf_define_spectrometer_data)!!!
        IF STRPOS(err,'shotfile does not exist') NE -1 THEN BEGIN
         err = ''
        ENDIF ELSE BEGIN
           RETURN
        ENDELSE
      ENDIF ELSE $
        spectrometer_data.beam = beam
        
  endif else begin
  print, 'Discharge should be < 35000'
  endelse
  stop
  endif
   
   
    if shot ge 35000  then begin
    
    if (extraoptions.CVH.time_choice eq 0) or (extraoptions.CVH.time_choice eq 1) then begin
    
    err  = ''
    id   = 'CXF_LOAD_AUG_DATA: '
    ierr = 0L
    
    ; speed of light
    c = 2.9979D8      ; [m/s]

    ; Planck constant   ; [J s]
    h = 6.6261D-34
    
    ; Read losNames of FVS and EVS

    los_names=''
    los_index=''
    spectr='xxx'
    
    ; Open EVS shotfile
    error=0L
    diaref=0L
    edition=0L
    date='123456789012345668'	; String with 18 characters
    result=call_external(!libddww,'ddgetaug','ddopen', $
           error,'AUGD','EVS',shot,edition,diaref,date)
    ; Read the LOS name
    type=6L
    losname='        '
    physunit=0L
    signalname='PARAM'
    CHANEL_ID='CHAN_xx'
    n_los=25L
    for i=0,(n_los-1) do begin
        if i lt 9 then begin
            nr=strcompress('0'+string((i+1)),/remove_all)
            endif else begin
            nr=strcompress(string((i+1)),/remove_all)
            endelse
        strput,CHANEL_ID,nr,5
        result=call_external(!libddww,'ddgetaug','ddparm', $
                error,diaref,'PARAM',CHANEL_ID,6L,8L,losname,physunit)
        det_name = strmid(losname,0,3)
        if strcmp(det_name,'CVH') eq 1 then begin
            los_index=[los_index,i]
            los_names=los_names+losname
            spectr = 'EVS'
        endif
    endfor
    ; Close the shotfile
    result=call_external(!libddww,'ddgetaug','ddclose', $
           error,diaref)
    
    
    ; Open FVS shotfile
    if strcmp(spectr,'EVS') eq 0 then begin
    error=0L
    diaref=0L
    edition=0L
    date='123456789012345668'	; String with 18 characters
    result=call_external(!libddww,'ddgetaug','ddopen', $
           error,'AUGD','FVS',shot,edition,diaref,date)
    ; Read the LOS name
    type=6L
    losname='        '
    physunit=0L
    signalname='PARAM'
    CHANEL_ID='CHAN_xx'
    n_los=24L
    
    for i=0,(n_los-1) do begin
        if i lt 9 then begin
            nr=strcompress('0'+string((i+1)),/remove_all)

        endif else begin
            nr=strcompress(string((i+1)),/remove_all)

        endelse
        strput,CHANEL_ID,nr,5
        result=call_external(!libddww,'ddgetaug','ddparm', $
                error,diaref,'PARAM',CHANEL_ID,6L,8L,losname,physunit)
        det_name = strmid(losname,0,3)

        if strcmp(det_name,'CVH') eq 1 then begin
            los_index=[los_index,i]
            los_names=los_names+losname
            spectr = 'FVS'
        endif
   
    endfor
    ; Close the shotfile
    result=call_external(!libddww,'ddgetaug','ddclose', $
           error,diaref)
    endif
     
     los_names= strsplit(los_names,/EXTRACT)
     los_index=los_index(1:(n_elements(los_index)-1))
     print,'Spectrometer used:   ',spectr
     print,'Channels used:', los_index+1L
    
    ; Input switches
    shot     = spectrometer_data.shot_nr
    spectrometer_data.spectrometer.name = spectr        
    diagnosticL1 = STRSPLIT(spectr, 'S', /EXTRACT)+'L'
    error = 0L
    
    read_xvs_diag, shot, spectr, dwdp, exptime, sad2, ctsph, time, lam, offset, sens, spec, wlen, slit, $
           gratcons, op_ang, pixw, magnification, fwhm_pix, neon_done, neon, lambda_neon, r1, phi1, z1, $
           r2, phi2, z2, los_name=los_name
    
    ;Select frames with Ip > 0
 
    timebase = 1L
    read_signal_mrm,0L,shot,'MAG','Ipa',timebase,variable,phys_dim,edition=0L,exp='AUGD'
    threshold = 2.0e5    
    timebase = timebase(where((variable gt threshold) or (variable lt -1.0*threshold)))
    interval_ip_on = [timebase(0),timebase(n_elements(timebase)-1)]
 
    spec = spec (*,where((time gt interval_ip_on(0)) and (time lt interval_ip_on(1))),*)
    time = time(where((time gt interval_ip_on(0)) and (time lt interval_ip_on(1))))
 
    if extraoptions.CVH.time_choice eq 1 then begin ; Select frames [t_ini_gaspuff, t_end_gaspuff]
    timebase = 1L
    read_signal_mrm,0L,shot,'HEB','S01VALVE',timebase,variable,phys_dim,edition=0L,exp='AUGD'
    threshold  = 80 ; [V] the valve is considered opened at 80 V
    timebase = timebase(where( variable gt threshold))
    interval_gaspuff_on = [timebase(0)-0.005,(timebase(n_elements(timebase)-1)+0.005)]
    spec = spec (*,where((time gt interval_gaspuff_on(0)) and (time lt interval_gaspuff_on(1))),*)
    time = time(where((time gt interval_gaspuff_on(0)) and (time lt interval_gaspuff_on(1))))
    endif
    if n_elements(los_index) eq 1 then begin
        npixel = n_elements(spec[*,0,0])
        nframe  = n_elements(spec[0,*,0])

        spec = spec[*,*,los_index]
        sens = sens[*,los_index]
        lam = lam[*,los_index]
        offset = offset[*,los_index]

        spec = reform(spec,npixel,nframe,1L)
        sens = reform(sens,npixel,1L)
        lam = reform(lam,npixel,1L)
        offset = reform(offset,npixel,1L)

    endif else begin
        spec = spec[*,*,los_index]
        sens = sens[*,los_index]
        lam = lam[*,los_index]
        offset = offset[*,los_index]
    endelse
    los_name = los_name[los_index]
 
    spectrometer_data.npixel = n_elements(spec[*,0,0])
    spectrometer_data.nframe  = n_elements(spec[0,*,0])
    spectrometer_data.ntrack = n_elements(spec[0,0,*])
    spectrometer_data.ninst  = 1
    cxf_define_spectrometer_data,spectrometer_data,err
    spectrometer_data.LOS_name = los_name
    spectrometer_data.R_orig = r1[los_index]
    spectrometer_data.Z_orig = z1[los_index]
    spectrometer_data.PHI_orig = phi1[los_index]
    spectrometer_data.R_pos = r2[los_index]
    spectrometer_data.Z_pos = z2[los_index]
    spectrometer_data.PHI_pos = phi2[los_index]
    ; Reference wavelength [A]
    spectrometer_data.wlength = wlen*10

    ; Slit width [microns]
    spectrometer_data.slit = slit

    ; Time vector [s]
    spectrometer_data.time     = time
    spectrometer_data.exposure = exptime + FLTARR(spectrometer_data.nframe)

    ; Calculate the wavelength intervals assuming the pixels are contiguous
    wav_vect  = lam
    dwav_vect = FLTARR(spectrometer_data.npixel,spectrometer_data.ntrack)
    dwav_vect[0,*] = wav_vect[1,*]-wav_vect[0,*]
    dwav_vect[1:spectrometer_data.npixel-2,*] = 0.5*(wav_vect[2:spectrometer_data.npixel-1,*] $
                                   -wav_vect[0:spectrometer_data.npixel-3,*])
    dwav_vect[spectrometer_data.npixel-1,*] = wav_vect[spectrometer_data.npixel-1,*] $
                            -wav_vect[spectrometer_data.npixel-2,*]
    spectrometer_data.wavelength.data      = wav_vect
    spectrometer_data.wavelength.error     = FLTARR(spectrometer_data.npixel,$
                                spectrometer_data.ntrack)
    spectrometer_data.wavelength.reference = ' '
    spectrometer_data.dispersion.data      = dwav_vect
    spectrometer_data.dispersion.error     = FLTARR(spectrometer_data.npixel,$
                                spectrometer_data.ntrack)
    spectrometer_data.dispersion.reference = ' '

    ; Calculate error bars and calibrated data
    ppc = 1./ctsph
    ron = sad2
    err_bars = dblarr(spectrometer_data.npixel,spectrometer_data.ntrack,$
        spectrometer_data.nframe)
    err_bars_counts = sqrt(spec*ctsph+sad2)
    data_calib = dblarr(spectrometer_data.npixel,spectrometer_data.ntrack,$
        spectrometer_data.nframe)
    for jtime = 0,spectrometer_data.nframe-1 do begin
        err_bars[*,*,jtime] = err_bars_counts[*,jtime,*]/sens/exptime/dwdp
        data_calib[*,*,jtime] = spec[*,jtime,*]/sens/exptime/dwdp
    endfor

    spectrometer_data.intensity.data = data_calib
    spectrometer_data.intensity.error = err_bars
    spectrometer_data.intensity.reference = ' '

    spectrometer_data.instfu.y0 = replicate(1.,spectrometer_data.ntrack)
    spectrometer_data.instfu.xw = fwhm_pix[los_index]*dwdp*10
    spectrometer_data.instfu.xs = replicate(0.,spectrometer_data.ntrack)
    spectrometer_data.instfu.reference = ' '

    externalcostheta = fltarr(spectrometer_data.ntrack,spectrometer_data.nframe)+1.
    spectrometer_data = create_struct(spectrometer_data,'externalcostheta',externalcostheta)

    ; Load flag showing if 'sufficient' beam =puff is available for a
    ; given frame
    if shot ge 35313 then begin
        if shot ne 35396 then begin
         hfs_puff_status, 'CVH', shot, spectrometer_data.time, $
                           spectrometer_data.exposure, $
                           beam, beam_new, puff_shotfile, err
         IF STRTRIM(err,2) NE '' THEN BEGIN
                                ; If the shot file is missing then assume no beams (this is default in
                                ; cxf_define_spectrometer_data)!!!
            IF STRPOS(err,'shotfile does not exist') NE -1 THEN BEGIN
               spectrometer_data.beam = spectrometer_data.time*0.
               err = ''
            ENDIF ELSE BEGIN
               RETURN
            ENDELSE
         ENDIF ELSE BEGIN

; read ElM signal in order to remove elms from background subtraction           
           elm_exp='AUGD'
           read_signal_mrm, ier,shot,'ELM','t_endELM',telm_begin,telm_end,phys_dim,exp=elm_exp
           if ier eq 0 then begin
              print, ''
              print, 'Exclude ELMs from data'
              print, ''
              for t=0L, n_elements(telm_begin)-1 do begin
                 min_begin=min(abs(spectrometer_data.time-(telm_begin[t]-0.001)),location_begin)
                 min_end=min(abs(spectrometer_data.time-(telm_end[t]+0.001)),location_end)
                 for k=location_begin, location_end+1 do begin
                     beam_new[k]=0.7
                 endfor
              endfor
           endif else begin
              print, ''   
              print, 'ELM shotfile does not exist!'
              print, ''
           endelse    

            spectrometer_data.beam = beam
            spectrometer_data.modulation.flag=0
            beam_new = reform(beam_new,1,n_elements(beam_new))
            names = ['GasPuffSe01']
            *spectrometer_data.modulation.beampower=beam_new
            *spectrometer_data.modulation.beamlabels =names 
            *spectrometer_data.modulation.orig_beam =spectrometer_data.beam 
            *spectrometer_data.modulation.orig_data =spectrometer_data.intensity.data
            *spectrometer_data.modulation.orig_error = spectrometer_data.intensity.error           
            
         ENDELSE
         endif else begin ;shot =35396
         
            beam = dblarr(n_elements(spectrometer_data.time))
            ini_beam = 3.018 ;[s]
            delta_t = 0.020 ; [s]
            for i = 0,15 do begin
                index_on = where( (time gt ini_beam) and (time le (ini_beam+delta_t)))
                beam(index_on) = 1.0
                beam(index_on(0)-1) = 0.5
                beam(index_on(n_elements(index_on)-1)+1) = 0.5
                ini_beam = ini_beam + 0.1
            endfor 
            
  ; read ElM signal in order to remove elms from background subtraction           
           elm_exp='AUGD'
           read_signal_mrm, ier,shot,'ELM','t_endELM',telm_begin,telm_end,phys_dim,exp=elm_exp
           if ier eq 0 then begin
              print, ''
              print, 'Exclude ELMs from data'
              print, ''
              for t=0L, n_elements(telm_begin)-1 do begin
                 min_begin=min(abs(spectrometer_data.time-(telm_begin[t]-0.001)),location_begin)
                 min_end=min(abs(spectrometer_data.time-(telm_end[t]+0.001)),location_end)
                 for k=location_begin, location_end+1 do begin
                    if beam[k] ne 1.0 then beam[k]=0.7
                 endfor
              endfor
           endif else begin
              print, ''   
              print, 'ELM shotfile does not exist!'
              print, ''
           endelse              
               
            spectrometer_data.beam = beam
            spectrometer_data.modulation.flag=0
            names = ['GasPuffSe01']
            *spectrometer_data.modulation.beampower=reform(beam,1,n_elements(beam))
            *spectrometer_data.modulation.beamlabels =names 
            *spectrometer_data.modulation.orig_beam =spectrometer_data.beam 
            *spectrometer_data.modulation.orig_data =spectrometer_data.intensity.data
            *spectrometer_data.modulation.orig_error = spectrometer_data.intensity.error
            
        endelse       
     endif

    spectrometer_data.spectrometer.name = 'CVH'
    spectrometer_data.PHI_orig = spectrometer_data.PHI_orig*180/!Pi
    spectrometer_data.PHI_pos = spectrometer_data.PHI_pos*180/!Pi

    kill_var,dwdp
    kill_var,exptime
    kill_var,sad2
    kill_var,ctsph
    kill_var,time
    kill_var,lam
    kill_var,offset
    kill_var,sens
    kill_var,spec
    kill_var,los_name
    kill_var,data_calib
    kill_var,err_bars
    kill_var,err_bars_counts
    kill_var,dwav_vect
    kill_var,wav_vect
    
    endif             
    endif
 
    END ; processing for CVH


    'LIA': BEGIN

; Load structure containing all the LIA data
      cxf_l5gui,shot,spectrometer_data,err
      IF STRTRIM(err,2) NE '' THEN BEGIN
        err = id + err
        RETURN
      ENDIF

    END                       ; processing for LIA

    
    'FVS': BEGIN
    
    err  = ''
    id   = 'CXF_LOAD_AUG_DATA: '
    ierr = 0L
    
    ; speed of light
    c = 2.9979D8      ; [m/s]

    ; Planck constant   ; [J s]
    h = 6.6261D-34
    
    spectr='FVS'
    
    ; Input switches
    shot     = spectrometer_data.shot_nr
    spectrometer_data.spectrometer.name = spectr        
    diagnosticL1 = STRSPLIT(spectr, 'S', /EXTRACT)+'L'
    error = 0L
    
    read_xvs_diag, shot, spectr, dwdp, exptime, sad2, ctsph, time, lam, offset, sens, spec, wlen, slit, $
           gratcons, op_ang, pixw, magnification, fwhm_pix, neon_done, neon, lambda_neon, r1, phi1, z1, $
           r2, phi2, z2, los_name=los_name
           
    LOSindex = where(los_name ne 'NEON    ')
    spec = spec[*,*,LOSindex]
    sens = sens[*,LOSindex]
    lam = lam[*,LOSindex]
    los_name = los_name[LOSindex]
 
    spectrometer_data.npixel = n_elements(spec[*,0,0])
    spectrometer_data.nframe  = n_elements(spec[0,*,0])
    spectrometer_data.ntrack = n_elements(spec[0,0,*])
    spectrometer_data.ninst  = 1
    cxf_define_spectrometer_data,spectrometer_data,err
    spectrometer_data.LOS_name = los_name
    spectrometer_data.R_orig = r1[LOSindex]
    spectrometer_data.Z_orig = z1[LOSindex]
    spectrometer_data.PHI_orig = phi1[LOSindex]
    spectrometer_data.R_pos = r2[LOSindex]
    spectrometer_data.Z_pos = z2[LOSindex]
    spectrometer_data.PHI_pos = phi2[LOSindex]
    ; Reference wavelength [A]
    spectrometer_data.wlength = wlen*10

    ; Slit width [microns]
    spectrometer_data.slit = slit

    ; Time vector [s]
    spectrometer_data.time     = time
    spectrometer_data.exposure = exptime + FLTARR(spectrometer_data.nframe)

    ; Calculate the wavelength intervals assuming the pixels are contiguous
    wav_vect  = lam
    dwav_vect = FLTARR(spectrometer_data.npixel,spectrometer_data.ntrack)
    dwav_vect[0,*] = wav_vect[1,*]-wav_vect[0,*]
    dwav_vect[1:spectrometer_data.npixel-2,*] = 0.5*(wav_vect[2:spectrometer_data.npixel-1,*] $
                                   -wav_vect[0:spectrometer_data.npixel-3,*])
    dwav_vect[spectrometer_data.npixel-1,*] = wav_vect[spectrometer_data.npixel-1,*] $
                            -wav_vect[spectrometer_data.npixel-2,*]
    spectrometer_data.wavelength.data      = wav_vect
    spectrometer_data.wavelength.error     = FLTARR(spectrometer_data.npixel,$
                                spectrometer_data.ntrack)
    spectrometer_data.wavelength.reference = ' '
    spectrometer_data.dispersion.data      = dwav_vect
    spectrometer_data.dispersion.error     = FLTARR(spectrometer_data.npixel,$
                                spectrometer_data.ntrack)
    spectrometer_data.dispersion.reference = ' '

    ; Calculate error bars and calibrated data
    ppc = 1./ctsph
    ron = sad2
    err_bars = dblarr(spectrometer_data.npixel,spectrometer_data.ntrack,$
        spectrometer_data.nframe)
    err_bars_counts = sqrt(spec*ctsph+sad2)
    data_calib = dblarr(spectrometer_data.npixel,spectrometer_data.ntrack,$
        spectrometer_data.nframe)
    for jtime = 0,spectrometer_data.nframe-1 do begin
        err_bars[*,*,jtime] = err_bars_counts[*,jtime,*]/sens/exptime/dwdp
        data_calib[*,*,jtime] = spec[*,jtime,*]/sens/exptime/dwdp
    endfor

    spectrometer_data.intensity.data = data_calib
    spectrometer_data.intensity.error = err_bars
    spectrometer_data.intensity.reference = ' '

    spectrometer_data.instfu.y0 = replicate(1.,spectrometer_data.ntrack)
    spectrometer_data.instfu.xw = fwhm_pix[LOSindex]*dwdp*10
    spectrometer_data.instfu.xs = replicate(0.,spectrometer_data.ntrack)
    spectrometer_data.instfu.reference = ' '

    externalcostheta = fltarr(spectrometer_data.ntrack,spectrometer_data.nframe)+1.
    spectrometer_data = create_struct(spectrometer_data,'externalcostheta',externalcostheta)

    kill_var,dwdp
    kill_var,exptime
    kill_var,sad2
    kill_var,ctsph
    kill_var,time
    kill_var,lam
    kill_var,offset
    kill_var,sens
    kill_var,spec
    kill_var,los_name
    kill_var,data_calib
    kill_var,err_bars
    kill_var,err_bars_counts
    kill_var,dwav_vect
    kill_var,wav_vect
    END ; End processing for FVS 
    
    
    'EVS': BEGIN
    
    err  = ''
    id   = 'CXF_LOAD_AUG_DATA: '
    ierr = 0L
    
    ; speed of light
    c = 2.9979D8      ; [m/s]

    ; Planck constant   ; [J s]
    h = 6.6261D-34
    
    spectr='EVS'
    
    ; Input switches
    shot     = spectrometer_data.shot_nr
    spectrometer_data.spectrometer.name = spectr        
    diagnosticL1 = STRSPLIT(spectr, 'S', /EXTRACT)+'L'
    error = 0L
    
    read_xvs_diag, shot, spectr, dwdp, exptime, sad2, ctsph, time, lam, offset, sens, spec, wlen, slit, $
           gratcons, op_ang, pixw, magnification, fwhm_pix, neon_done, neon, lambda_neon, r1, phi1, z1, $
           r2, phi2, z2, los_name=los_name
           
    LOSindex = where(los_name ne 'NEON    ')
    spec = spec[*,*,LOSindex]
    sens = sens[*,LOSindex]
    lam = lam[*,LOSindex]
    los_name = los_name[LOSindex]
 
    spectrometer_data.npixel = n_elements(spec[*,0,0])
    spectrometer_data.nframe  = n_elements(spec[0,*,0])
    spectrometer_data.ntrack = n_elements(spec[0,0,*])
    spectrometer_data.ninst  = 1
    cxf_define_spectrometer_data,spectrometer_data,err
    spectrometer_data.LOS_name = los_name
    spectrometer_data.R_orig = r1[LOSindex]
    spectrometer_data.Z_orig = z1[LOSindex]
    spectrometer_data.PHI_orig = phi1[LOSindex]
    spectrometer_data.R_pos = r2[LOSindex]
    spectrometer_data.Z_pos = z2[LOSindex]
    spectrometer_data.PHI_pos = phi2[LOSindex]
    ; Reference wavelength [A]
    spectrometer_data.wlength = wlen*10

    ; Slit width [microns]
    spectrometer_data.slit = slit

    ; Time vector [s]
    spectrometer_data.time     = time
    spectrometer_data.exposure = exptime + FLTARR(spectrometer_data.nframe)

    ; Calculate the wavelength intervals assuming the pixels are contiguous
    wav_vect  = lam
    dwav_vect = FLTARR(spectrometer_data.npixel,spectrometer_data.ntrack)
    dwav_vect[0,*] = wav_vect[1,*]-wav_vect[0,*]
    dwav_vect[1:spectrometer_data.npixel-2,*] = 0.5*(wav_vect[2:spectrometer_data.npixel-1,*] $
                                   -wav_vect[0:spectrometer_data.npixel-3,*])
    dwav_vect[spectrometer_data.npixel-1,*] = wav_vect[spectrometer_data.npixel-1,*] $
                            -wav_vect[spectrometer_data.npixel-2,*]
    spectrometer_data.wavelength.data      = wav_vect
    spectrometer_data.wavelength.error     = FLTARR(spectrometer_data.npixel,$
                                spectrometer_data.ntrack)
    spectrometer_data.wavelength.reference = ' '
    spectrometer_data.dispersion.data      = dwav_vect
    spectrometer_data.dispersion.error     = FLTARR(spectrometer_data.npixel,$
                                spectrometer_data.ntrack)
    spectrometer_data.dispersion.reference = ' '

    ; Calculate error bars and calibrated data
    ppc = 1./ctsph
    ron = sad2
    err_bars = dblarr(spectrometer_data.npixel,spectrometer_data.ntrack,$
        spectrometer_data.nframe)
    err_bars_counts = sqrt(spec*ctsph+sad2)
    data_calib = dblarr(spectrometer_data.npixel,spectrometer_data.ntrack,$
        spectrometer_data.nframe)
    for jtime = 0,spectrometer_data.nframe-1 do begin
        err_bars[*,*,jtime] = err_bars_counts[*,jtime,*]/sens/exptime/dwdp
        data_calib[*,*,jtime] = spec[*,jtime,*]/sens/exptime/dwdp
    endfor

    spectrometer_data.intensity.data = data_calib
    spectrometer_data.intensity.error = err_bars
    spectrometer_data.intensity.reference = ' '

    spectrometer_data.instfu.y0 = replicate(1.,spectrometer_data.ntrack)
    spectrometer_data.instfu.xw = fwhm_pix[LOSindex]*dwdp*10
    spectrometer_data.instfu.xs = replicate(0.,spectrometer_data.ntrack)
    spectrometer_data.instfu.reference = ' '

    externalcostheta = fltarr(spectrometer_data.ntrack,spectrometer_data.nframe)+1.
    spectrometer_data = create_struct(spectrometer_data,'externalcostheta',externalcostheta)

    kill_var,dwdp
    kill_var,exptime
    kill_var,sad2
    kill_var,ctsph
    kill_var,time
    kill_var,lam
    kill_var,offset
    kill_var,sens
    kill_var,spec
    kill_var,los_name
    kill_var,data_calib
    kill_var,err_bars
    kill_var,err_bars_counts
    kill_var,dwav_vect
    kill_var,wav_vect
    END ; End processing for EVS 
    
    
    ELSE:
  ENDCASE

    ; Start processing for CAR, CBR, CCR
    ; outside the CASE because it is 3 times the same thing
    if (detector eq 'CAR' and shot le 34995) or detector eq 'CBR' or detector eq 'CCR' then begin

        ; Check that shot is in range where we have all the data (including
        ; calibrations, instrument functions, etc.)
        ;IF shot LT 28080 THEN BEGIN ;-AK-TD: correct starting shot number
        IF shot LT 27880 THEN BEGIN ;-AK-TD: correct starting shot number
           ; err = id+'CXSFIT not allowed for ILS shots before 28080!'
           ; RETURN
           print,'CXSFIT not allowed for ILS shots before 28080! Still loading...'
        ENDIF

        ; Call load_ils_shf to get data (one routine to select another routine to load the right diagnostic)
        load_ils_shf, shot, ils, detector, err,roiadd=roiadd,passivecal=passivecal,neon=neon,$
                selhead=selhead
        IF STRTRIM(err,2) NE '' THEN BEGIN
            err = id + err
            RETURN
        ENDIF

        ; Call cxf_define_spectrometer_data with appropriate dimensions to
        ; complete structure definition                                     ;-AK: Huh?!

        IF all_tracks THEN BEGIN
            ntrack  = ils.header.NumROI
            whtrack = INDGEN(ntrack)
        ENDIF ELSE BEGIN
            whtrack =  WHERE(ils.header.R_pos GT 0, ntrack)
            IF ntrack EQ 0 THEN BEGIN
                err =  id+'No tracks with positive R_pos found!'
                RETURN
            ENDIF
        ENDELSE
    
        ;# gaussians in inst_funct this has never been anything but 1.
        ;cxsfit can't handle anything but 1 - RMM 
        ninst  = 1;ils.header.det_inst.nGaus ??
        
        if detector eq 'CAR' then addupframes=extraoptions.car.nframes
        if detector eq 'CCR' then addupframes=extraoptions.ccr.nframes
          
        npixel = ils.header.NumPixels
        nframe_act=ils.header.Numframes
        nframe = ils.header.Numframes/addupframes
        nframe=nframe[0]
        
        
        spectrometer_data.npixel = npixel
        spectrometer_data.ntrack = ntrack
        spectrometer_data.nframe = nframe
        spectrometer_data.ninst  = ninst
        cxf_define_spectrometer_data,spectrometer_data,err
        IF err NE '' THEN RETURN

        ; Load the structure
        spectrometer_data.LOS_name = ils.header.LOS_Name[whtrack]

        ; Beam crossing point
        spectrometer_data.R_pos   = ils.header.R_pos[whtrack]
        spectrometer_data.z_pos   = ils.header.z_pos[whtrack]
        spectrometer_data.phi_pos = ils.header.phi_pos[whtrack]
        ; Position of origin of the LOS
        spectrometer_data.R_orig   = ils.header.R_opt[whtrack]
        spectrometer_data.z_orig   = ils.header.z_opt[whtrack]
        spectrometer_data.phi_orig = ils.header.phi_opt[whtrack]
    
        ; Reference wavelength [A] !!!
        spectrometer_data.wlength = ils.header.Wvl*10.   ;-AK: what's the use?

        ; Slit width [microns]
        spectrometer_data.slit = ils.header.SlitWid

        ; Time vector [s]
        ;    spectrometer_data.time     = ils.data.time
        time = ils.data.time
        exptime = ils.header.ExpTime
        spectrometer_data.exposure = $
        ils.header.ExpTime + FLTARR(nframe)


        if addupframes ge 2 then begin
            for iindex=0,nframe-1 do begin
                spectrometer_data.time[iindex]=(time[iindex*addupframes]+time[(iindex+1)*addupframes-1])/2.    
            endfor 
            exptime=exptime*float(addupframes)

          endif else begin
            spectrometer_data.time=ils.data.time 
        endelse
        
        spectrometer_data.exposure[*]=exptime

        ; kappatou 28012014:
        ; I consider the time-dependent R,z,phi as an extra option, because not all systems 
        ; have this implemented yet. It can be an option in the widget, but it could also 
        ; be just fixed per diagnostic. 

        ; I need to define: time-dep R, z, phi (3 matrices ntrack,nt)
        ; also time-dep costheta for the calculation of rotation 
        ; It's placed here cause I need the time array
        if extraoptions.timeloc eq 1 then begin
            ; read in the time-dependent R,z,phi,costheta
            get_cxrs_loc_in_time,shot,detector,spectrometer_data,time_loc
            R_pos_time    = time_loc.R_pos_time
            dR_pos_time    = time_loc.dR_pos_time
            z_pos_time    = time_loc.z_pos_time
            dz_pos_time    = time_loc.dz_pos_time
            phi_pos_time  = time_loc.phi_pos_time
            dphi_pos_time  = time_loc.dphi_pos_time
            costheta_time = time_loc.costheta_time 
            spectrometer_data = create_struct(spectrometer_data,'R_pos_time',R_pos_time,'z_pos_time',z_pos_time,'phi_pos_time',phi_pos_time,'dR_pos_time',dR_pos_time,'dz_pos_time',dz_pos_time,'dphi_pos_time',dphi_pos_time,'externalcostheta',costheta_time)
        endif

        ; Calculate the wavelength intervals assuming the pixels are contiguous
        wav_vect  = ils.header.cor_wavel[*,whtrack]
     
        ;This is calculating a dispersion based on the input wavelengths 
        ;It's not necessarily linear - talk to Thomas again if this is a problem.
        ;It is - should give cxsfit a linear dispersion at the wavlength of interest

        dwav_vect = FLTARR(npixel,ntrack)
        dwav_vect[0,*] = wav_vect[1,*]-wav_vect[0,*]
        dwav_vect[1:npixel-2,*] = 0.5*(wav_vect[2:npixel-1,*] $
                                       -wav_vect[0:npixel-3,*])
        dwav_vect[npixel-1,*] = wav_vect[npixel-1,*] $
                                -wav_vect[npixel-2,*]
        spectrometer_data.wavelength.data      = wav_vect
        ; Uncertainty still to be defined!!!
        spectrometer_data.wavelength.error     = FLTARR(npixel,ntrack)
        spectrometer_data.wavelength.reference = $
        ils.header.wav_calib_file
    
        ;setting it equal to a contant value - from the center - RMM   ; - AK - Next line commented out!!!
        ;    dwav_vect[*]=dwav_vect[511] - AK got rid of this, for more info, ask Rachael! :)
    
        spectrometer_data.dispersion.data      = dwav_vect
        ; Uncertainty still to be defined!!!
        spectrometer_data.dispersion.error     = FLTARR(npixel,ntrack)
        spectrometer_data.dispersion.reference = $
        ils.header.wav_calib_file

        ;CFM comment ... 
        ; Spectral intensity. Requires conversion from counts/(texp-pixel)
        ; to ph/(m^2 sr s nm) using the returned background level and cal_mW
        ; which is in (mW/(m^2 sr A)) / (counts/s/pixel)
        
        ; Uncertainties are based on a Gaussian readout noise (from tests on
        ; dark signal) and a SQRT(photons) uncertainty on the net signal.
        ; Required inputs are the readout noise and the number of
        ; photons/count (also determined empirically)
   
        readout_noise =  ils.header.noise.r_noise
        photoncount   =  ils.header.noise.ph_count
        
        ;number of background frames  
        n_bg_frames =  ils.header.bg_range
        
        ;perform background subtraction and apply brightness calibrations
        ;short names to make understanding the calculation easy - waste of memory I know. -RMM
        ;    if shot gt 25000 then begin

        ; Instrument function
        spectrometer_data.instfu.y0 = replicate(1.,ntrack);cer.header.det_inst.intens
        spectrometer_data.instfu.xw = ils.header.inst_wid_nm[whtrack]*10. ;cer.header.det_inst.width * 10.0/(2d0*alog(2d0))
        spectrometer_data.instfu.xs = replicate(0.,ntrack);cer.header.det_inst.shift
        spectrometer_data.instfu.reference = ' '

        spectrometer_data.intensity.reference $
            = ils.header.int_calib_file
      
        ;-AK:
        ; This is to deal with the 54 fibers on the spectrometer but 27 ROIs.
        ;     whtrack = (whtrack + 1) /2   ; AK: this is ONLY for the intensity, not wvl, inst.fnc, etc... !!!
        if ils.header.numroi eq 15 then whtrack=findgen(ils.header.numroi)
        ; ^ this is for the cases that I use 15 los instead of 27.
        intensity_data=ils.data.intens[*,whtrack,*]
        intensity_error=ils.data.intenserr[*,whtrack,*] 
        ;spectrometer_data.intensity.data=ils.data.intens[*,whtrack,*]
        ;spectrometer_data.intensity.error=ils.data.intenserr[*,whtrack,*]       
        ; Load flag showing if 'sufficient' beam power is available for a
        ; given frame
   
        if addupframes ge 2 then begin
            for iindex=0,nframe-1 do begin
                spectrometer_data.intensity.data[*,*,iindex]=TOTAL(intensity_data[*,*,(iindex*addupframes):((iindex+1)*addupframes-1)],3)
                ;Gaussian Error propagation
                spectrometer_data.intensity.error[*,*,iindex]=sqrt(TOTAL(intensity_error[*,*,(iindex*addupframes):((iindex+1)*addupframes-1)]^2,3))
            endfor
            spectrometer_data.intensity.data[*,*,*]=spectrometer_data.intensity.data[*,*,*]/float(addupframes)
            spectrometer_data.intensity.error[*,*,*]=spectrometer_data.intensity.error[*,*,*]/float(addupframes)
        endif else begin
            spectrometer_data.intensity.data=intensity_data
            spectrometer_data.intensity.error=intensity_error
        endelse

        print, 'attention, BEN was here and divided exposure time by 6!' ;-AK: Huh?!
        print, '~/idl/cxsfit/machines/aug/cxf_load_aug_data.pro line 215'
        cxrs_beam_status, detector, shot, spectrometer_data.time, $
                        spectrometer_data.exposure/6.0, $  
                        beam, NI_shotfile, err
        IF STRTRIM(err,2) NE '' THEN BEGIN
        ; If the shot file is missing then assume no beams (this is default in
        ; cxf_define_spectrometer_data)!!!
            IF STRPOS(err,'shotfile does not exist') NE -1 THEN BEGIN
                err = ''
            ENDIF ELSE BEGIN
                RETURN
            ENDELSE
        ENDIF ELSE $
            spectrometer_data.beam = beam

        ;RMM - General beam modulation code
        ;Input: Spectrometer_data
        ;Output: Modulated_data = New Data Structure to be fed to CXSFIT
        ;The intensity.data in this structure has the same time base but,
        ;The beam off frames are unchanged - the beam on frames have had a background
        ;frame subtracted from them
        IF KEYWORD_SET(BEAMMOD) THEN BEGIN
            if beammod eq 1 then begin
                beam_modulation,spectrometer_data,ils,whtrack,wav_vect,BeamModOut
                spectrometer_data=BeamModOut
            endif else begin
                if beammod eq 2 then begin
                    beam_modulation_lastframes,spectrometer_data,ils,whtrack,wav_vect,BeamModOut
                    spectrometer_data=BeamModOut
                endif
            endelse  
        ENDIF
    print,'end processing ILS'
    endif ; End processing for ILS  

; HERE starts "new" CAR - Nov 2018  
if (detector eq 'CAR' and shot gt 34995) then begin
 

; Call load_ils_shf to get data (one routine to select another routine to load the right diagnostic)
    load_car_shf_v4, shot, car, err

    IF err ne 0 THEN BEGIN
        print,'Error in load_car_shf_v4.pro'
        RETURN
    ENDIF

; Call cxf_define_spectrometer_data with appropriate dimensions to
; complete structure definition                                   

    IF all_tracks THEN BEGIN
        ntrack  = car.header.NumROI
        whtrack = INDGEN(ntrack)
    ENDIF ELSE BEGIN
        whtrack =  WHERE(car.header.R_pos GT 0, ntrack)
        IF ntrack EQ 0 THEN BEGIN
            err =  id+'No tracks with positive R_pos found!'
            RETURN
        ENDIF
    ENDELSE
    
    ;now cxsfit can handle more than 1 inst.functions - first try...
    spectrometer_data.npixel = car.header.NumPixels
    spectrometer_data.ntrack = car.header.NumROI
    spectrometer_data.nframe = car.header.NumFrames
    spectrometer_data.nframe_baseline = car.header.numframes
    spectrometer_data.ninst  = car.header.ninst
    cxf_define_spectrometer_data,spectrometer_data,err
    IF err NE '' THEN RETURN

; Load the structure
    spectrometer_data.LOS_name = car.header.LOS_Name[whtrack]

; the phi's are converted to CER-like phi's in the loading routine (AK-Dec2018)
; Beam crossing point
    spectrometer_data.R_pos   = car.header.R_pos[whtrack]
    spectrometer_data.z_pos   = car.header.z_pos[whtrack]
    spectrometer_data.phi_pos = car.header.phi_pos[whtrack]
; Position of origin of the LOS
    spectrometer_data.R_orig   = car.header.R_opt[whtrack]
    spectrometer_data.z_orig   = car.header.z_opt[whtrack]
    spectrometer_data.phi_orig = car.header.phi_opt[whtrack]
    stop
    
; Reference wavelength [A] !!!
    spectrometer_data.wlength = car.header.Wvl*10.   ;-AK: what's the use? - upd2018 - I still don't know

; Slit width [microns]
    spectrometer_data.slit = car.header.SlitWid

; Time vector [s]
    spectrometer_data.time     = car.data.time
    spectrometer_data.exposure = car.header.ExpTime + FLTARR(car.header.NumFrames)

; kappatou 28012014: - Nov2018 I keep this the same. should work.
; I consider the time-dependent R,z,phi as an extra option, because not all systems 
; have this implemented yet. It can be an option in the widget, but it could also 
; be just fixed per diagnostic. Now selected for CAR.

; I need to define: time-dep R, z, phi (3 matrices ntrack,nt)
; also time-dep costheta for the calculation of rotation 
; It's placed here cause I need the time array
if extraoptions.timeloc eq 1 then begin
    ; read in the time-dependent R,z,phi,costheta
    get_cxrs_loc_in_time,shot,detector,spectrometer_data,time_loc
    R_pos_time    = time_loc.R_pos_time
    dR_pos_time    = time_loc.dR_pos_time
    z_pos_time    = time_loc.z_pos_time
    dz_pos_time    = time_loc.dz_pos_time
    phi_pos_time  = time_loc.phi_pos_time
    dphi_pos_time  = time_loc.dphi_pos_time
    costheta_time = time_loc.costheta_time 
    spectrometer_data = create_struct(spectrometer_data,'R_pos_time',R_pos_time,'z_pos_time',z_pos_time,'phi_pos_time',phi_pos_time,'dR_pos_time',dR_pos_time,'dz_pos_time',dz_pos_time,'dphi_pos_time',dphi_pos_time,'externalcostheta',costheta_time)
endif

; Calculate the wavelength intervals assuming the pixels are
; contiguous
    wav_vect  = car.header.cor_wavel[*,whtrack]

;This is calculating a dispersion based on the input wavelengths 
;It's not necessarily linear - talk to Thomas again if this is a problem.
;It is - should give cxsfit a linear dispersion at the wavlength of interest

    dwav_vect = FLTARR(car.header.NumPixels,car.header.NumROI)
    dwav_vect[0,*] = wav_vect[1,*]-wav_vect[0,*]
    dwav_vect[1:car.header.NumPixels-2,*] = 0.5*(wav_vect[2:car.header.NumPixels-1,*] $
                                       -wav_vect[0:car.header.NumPixels-3,*])
    dwav_vect[car.header.NumPixels-1,*] = wav_vect[car.header.NumPixels-1,*] $
                                -wav_vect[car.header.NumPixels-2,*]
    spectrometer_data.wavelength.data      = wav_vect
; Uncertainty still to be defined!!!
    spectrometer_data.wavelength.error     = FLTARR(car.header.NumPixels,car.header.NumROI)
    spectrometer_data.wavelength.reference = car.header.wav_calib_file
    
;setting it equal to a contant value - from the center - RMM   ; - AK - Next line commented out!!!
;    dwav_vect[*]=dwav_vect[511] - AK got rid of this, for more info, ask Rachael! :)
    
    spectrometer_data.dispersion.data      = dwav_vect
; Uncertainty still to be defined!!!
    spectrometer_data.dispersion.error     = FLTARR(car.header.NumPixels,car.header.NumROI)
    spectrometer_data.dispersion.reference = car.header.wav_calib_file

; Instrument function ; HOW TO DO THIS? - let's see if this works. look into load_car_shf_v4
      spectrometer_data.instfu.y0 = car.header.instfu_y0
      spectrometer_data.instfu.xw = car.header.instfu_xw*10. ;cer.header.det_inst.width * 10.0/(2d0*alog(2d0))
      spectrometer_data.instfu.xs = car.header.instfu_xs ;cer.header.det_inst.shift
      spectrometer_data.instfu.reference = ' '
      spectrometer_data.intensity.reference = car.header.int_calib_file
      
      spectrometer_data.intensity.data=car.data.intens[*,whtrack,*]
      spectrometer_data.intensity.error=car.data.intenserr[*,whtrack,*]       

; Load flag showing if 'sufficient' beam power is available for a
; given frame
      print, 'attention, BEN was here and divided exposure time by 6!' ;-AK: Huh?!
      print, '~/idl/cxsfit/machines/aug/cxf_load_aug_data.pro line 215'
      cxrs_beam_status, detector, shot, spectrometer_data.time, $
                        spectrometer_data.exposure/6.0, $  
                        beam, NI_shotfile, err
      IF STRTRIM(err,2) NE '' THEN BEGIN
; If the shot file is missing then assume no beams (this is default in
; cxf_define_spectrometer_data)!!!
        IF STRPOS(err,'shotfile does not exist') NE -1 THEN BEGIN
         err = ''
        ENDIF ELSE BEGIN
           RETURN
        ENDELSE
      ENDIF ELSE $
        spectrometer_data.beam = beam
    
;   ;; ---------- FIT THE BASELINE FOR ZEFF CALCULATION ----------
;   ;; -----------------------------------------------------------
;    ; prepare data to load into fit_baseline.pro routine
;   data_in={intens:car.data.intens, intenserr:car.data.intenserr, $
;            time:car.data.time,ntr:car.header.NumROI,R_pos:car.header.R_pos}
;   fit_baseline_for_ils,shot,data_in,baseline
;   spectrometer_data.baseline=baseline
;    

;RMM - General beam modulation code
;Input: Spectrometer_data
;Output: Modulated_data = New Data Structure to be fed to CXSFIT
;The intensity.data in this structure has the same time base but,
;The beam off frames are unchanged - the beam on frames have had a background
;frame subtracted from them
  IF KEYWORD_SET(BEAMMOD) THEN BEGIN
    if beammod eq 1 then begin
      beam_modulation,spectrometer_data,car,whtrack,wav_vect,BeamModOut
    ;  stop        
      spectrometer_data=BeamModOut
    endif else begin
        if beammod eq 2 then begin
           beam_modulation_lastframes,spectrometer_data,car,whtrack,wav_vect,BeamModOut
           spectrometer_data=BeamModOut
        endif
    endelse  
  ENDIF
endif ; End processing for "new" CAR


; Here processing for other formats of data can be added (data from
; CER/CHR for various 'identical' shots should all be handled above)
END
