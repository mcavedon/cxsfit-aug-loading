pro cxf_aug_read_w,topinfo_base

  diagnostic_list = ['CER','CHR','CMR','CPR','CXH','CVH','LIA','COR','BOS','CNR','CUR','CAR','CBR','CCR','CFR','COR+CUR','FVS','EVS','CDL', 'CGF', 'CEF']

; default values
  cpulse = '0'
; set to our favourite pulse for testing:
  cpulse = '17148'
  cxf_last_shotnr,shot
  cpulse=string(shot,f='(i5)')
  diagnostic_index = 0L ; CER
  login_info = get_login_info()
  if login_info.user_name eq 'aleb' then $
      diagnostic_index = 7L ; COR
  if login_info.user_name eq 'mcavedon' then $
      diagnostic_index = 2L ; CMR
  if login_info.user_name eq 'ulp' then $
      diagnostic_index = 9L ; CNR
  if login_info.user_name eq 'elv' then $
      diagnostic_index = 2L ; CMR
 if login_info.user_name eq 'pcano' then $
      diagnostic_index = 20L ; CEF
 if login_info.user_name eq 'amperez' then $
      diagnostic_index = 20L ; CEF
 if login_info.user_name eq 'ajvv' then $
      diagnostic_index = 19L ; CGF
  

; check to see if we can re-use previous data
  widget_control,topinfo_base,get_uvalue=topinfo
  if ptr_valid(topinfo.expdata) then $
    if (*topinfo.expdata).machine.name eq 'AUG' then begin
      cpulse = STRTRIM(STRING((*topinfo.expdata).shot_nr),2)
      diagnostic_name = (*topinfo.expdata).spectrometer.name
      diagnostic_index = WHERE(diagnostic_list eq diagnostic_name) > 0
    endif

; build widget
  tlb=widget_base(title='Read Data',/column)

  base=widget_base(tlb,/row)
  label=widget_label(base,value='Diagnostic:')
  diagnostic=widget_droplist(base,value=diagnostic_list)
  widget_control,diagnostic,set_droplist_select=diagnostic_index

  base=widget_base(tlb,/row)
  label=widget_label(base,value='Pulse:')
  pulse=widget_text(base,value=cpulse,uvalue=cpulse,/editable,/TRACKING_EVENTS)

  base=widget_base(tlb,/row)
  ok_button=widget_button(base,value='Ok')
  cancel_button=widget_button(base,value='Cancel')

  if (diagnostic_index eq 2) or (diagnostic_index eq 3) or (diagnostic_index eq 9) then $
    extrabase_edge=widget_base(tlb,/column,/frame,sensitive=1) else $       
    extrabase_edge=widget_base(tlb,/column,/frame,sensitive=0)                               
  if (diagnostic_index eq 4) or (diagnostic_index eq 5) then $
    extrabase_cxh=widget_base(tlb,/column,/frame,sensitive=1) else $
                                extrabase_cxh=widget_base(tlb,/column,/frame,sensitive=0)

  base=widget_base(extrabase_edge,/row)
  base2=widget_base(base,/column)
  base3=widget_base(base,/column)


  label=widget_label(base2,value='Edge Options (CMR/CPR/CNR):')

  button_names=['read only timeframes with I_p > 0','read all data ', 'read only following time interval ']
 
     

  
  time_choice_buttons_edge= cw_bgroup(base2,button_names,/column,/exclusive,set_value=0)


  base4=widget_base(base2,/row)
  starttime_edge=widget_text(base4,/editable)
  endtime_edge=widget_text(base4,/editable)

  base_additional_options=widget_base(base3,/column)
   label_additional_options=widget_label(base_additional_options,value='Additional option for CNR:')                  
   molecular_choice_buttons_edge=cw_bgroup(base_additional_options,['keep molecular lines','remove molecular lines'] $
                                ,/column,/exclusive,set_value=0)

  ; Averaging frames is implemented into the beam modulation analysis
  ;base_additional_options_edge=widget_base(base,/row)
  ;label_additional_options_edge=widget_label(base_additional_options_edge,value='Additional options:') 
  ;beam_buttons_edge =cw_bgroup(base_additional_options_edge,['add up frames'] $
                                ;,/row,/nonexclusive,set_value=[0])
  ;nframes_init_edge = STRTRIM(1, 2)
  ;nframes_text_edge=widget_text(base_additional_options_edge,value=nframes_init_edge,uvalue=nframes_init_edge,editable=1,sensitive=1)
  
  
;start test of adding a widget to the cxsfit read AUG shot option - similar to T.P.'s CMR options
;include CVH to widget - elv

  base=widget_base(extrabase_cxh,/column)
  base2=widget_base(base,/column)
  base3=widget_base(base2,/row)
  ;label=widget_label(base2,value='CXH Options')
    label=widget_label(base2,value='HFS Options (CXH/CVH)')

  button_names=['Read only timeframes with I_p > 0','Read only timeframes with I_p > 0 and gas puff on','Read FVS data with puff background substraction (<35000)']
 ; time_choice_buttons_cxh= cw_bgroup(base,button_names,/column,/exclusive,set_value=0)
    button_array=lonarr(n_elements(button_names))
    time_choice_buttons_cxh = widget_base(base3,/column,/exclusive)
    for i=0,n_elements(button_array)-1 do begin
        button_array(i) = widget_button(time_choice_buttons_cxh,value=button_names(i))
    endfor
    widget_control,button_array(0),/set_button 
  button_names=['Keep D2 lines','Remove D2 lines']
  molecular_choice_buttons_cxh=cw_bgroup(base3,button_names,/column,/exclusive,set_value=0)

;;========================================================================================
;;========================================================================================
;;for the core diagnostics create another box of options
;;this works don't touch it!!  -RMM
    if (diagnostic_index eq 0)  or (diagnostic_index eq 1 and pulse le 30150 ) or $
       (diagnostic_index eq 12) or (diagnostic_index eq 13) or $
       (diagnostic_index eq 14) or (diagnostic_index eq 11) then $
            extrabase_core=widget_base(tlb,/column,/frame,sensitive=1) else $
            extrabase_core=widget_base(tlb,/column,/frame,sensitive=0)
            
    base=widget_base(extrabase_core,/row)
    label=widget_label(base,value='Core Options')
    base3=widget_base(base,/column)
    base2=widget_base(base3,/row,/sensitive)

    ;name of the options in this box
    button_names_core=['All Time Frames','Beam Modulation','Beam Mod (only last frames)',$
        'Beam blips','Beam blips - addup frames','Gas blips']
    beam_buttons_core=cw_bgroup(base2,button_names_core,/column,/exclusive,set_value=0)

    button_names_core1=['Use Individual ROI','Add ROI W/ Like Major R']
    beam_buttons_core1=cw_bgroup(base2,button_names_core1,/column,/exclusive,set_value=0)
    
    button_names_core2=['Use Default Wavelength Calibrations (+Ne if >30242)','Choose Neon File to Calibrate','Calibrate Wavelength W/ Passive B,C,N']
    beam_buttons_core2=cw_bgroup(base2,button_names_core2,/column,/exclusive,set_value=0)
    
    button_names_core3=['All tracks','Only CER head', 'Only CFP head']
    beam_buttons_core3=cw_bgroup(base2,button_names_core3,/column,/exclusive,set_value=0)

    base4=widget_base(base3,/row)
    core_options=cw_bgroup(base4,['add up frames'] $
                                ,/row,/nonexclusive,set_value=[0])
    nframes_init = STRTRIM(2, 2)
    core_options_nframes=widget_text(base4,value=nframes_init,uvalue=nframes_init,editable=1,sensitive=0)
    
;;========================================================================================
;;========================================================================================
    
    ;;***********************************************************************
    ;; Widget window for BOS and COR diagnostics
    ;if (diagnostic_index eq 7) or (diagnostic_index eq 8) or (diagnostic_index eq 10) or (diagnostic_index eq 11) then $
    if (diagnostic_index eq 7) or (diagnostic_index eq 8) or $
       (diagnostic_index eq 10)or (diagnostic_index eq 15) then $
       extrabase_ben=widget_base(tlb,/column,/frame,sensitive=1) else $
          extrabase_ben=widget_base(tlb,/column,/frame,sensitive=0)
    base=widget_base(extrabase_ben,/row)
    label=widget_label(base,value='COR/CUR/BOS options')
    base2=widget_base(base,/column)
    base3=widget_base(base2,/row)

    base_loading_options=widget_base(base3,/column)
    label_loading_options=widget_label(base_loading_options,value='Loading options')
    beam_buttons_ben1=cw_bgroup(base_loading_options,['All Time Frames','Beam Modulation','Beam Modulation with Averaging','Beam dip'] $
                                ,/column,/exclusive,set_value=0)
    
    base_additional_options=widget_base(base3,/column)
    label_additional_options=widget_label(base_additional_options,value='Additional options')                  
    beam_buttons_ben2=cw_bgroup(base_additional_options,['only LFS','add up frames'] $
                                ,/column,/nonexclusive,set_value=[1,0])
    nframes_init = STRTRIM(1, 2)
    nframes_text=widget_text(base_additional_options,value=nframes_init,uvalue=nframes_init,editable=1,sensitive=0)

    base_fitting_options=widget_base(base3,/column)
    label_fitting_options=widget_label(base_fitting_options,value='Fitting options')                  
    fitting_buttons=cw_bgroup(base_fitting_options,['Ne spectrum + line fit (default)','Ne spectrum fit','Ne line fit','No Ne fit','Multi Gauss','Multi Gauss (from NetCDF)'] $
                                ,/column,/exclusive,set_value=4)
    ;Changed default value to 4: starting in 2018 we are using multiG instfuncs
    
 
    ;;***********************************************************************

    
  info={ topinfo_base:topinfo_base , $
	 diagnostic:diagnostic, $
	 pulse:pulse, $
	 ok_button:ok_button, $
	 cancel_button:cancel_button, $
	 extrabase_edge:extrabase_edge, $
	 extrabase_cxh:extrabase_cxh, $
	 time_choice_buttons_edge:time_choice_buttons_edge, $
         time_choice_buttons_cxh:time_choice_buttons_cxh, $
         molecular_choice_buttons_edge:molecular_choice_buttons_edge, $        
         molecular_choice_buttons_cxh:molecular_choice_buttons_cxh, $        
     button_array:button_array, $         
	 starttime_edge:starttime_edge, $
	 endtime_edge:endtime_edge, $
     extrabase_core:extrabase_core, $
     extrabase_ben:extrabase_ben,   $  
     ;nframes_text_edge:nframes_text_edge, $
     beam_buttons_core:beam_buttons_core, $
     beam_buttons_core1:beam_buttons_core1, $
     beam_buttons_core2:beam_buttons_core2, $
     core_options:core_options,$
     core_options_nframes:core_options_nframes,$
     beam_buttons_ben1:beam_buttons_ben1,   $
     beam_buttons_ben2:beam_buttons_ben2,   $
     nframes_text:nframes_text,             $
     fitting_buttons:fitting_buttons}

  widget_control,tlb,set_uvalue=info     			
  widget_control, tlb, /realize
  xmanager, 'cxf_aug_read_w',tlb,event_handler='cxf_aug_read_e'


end
